<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Order extends Model {

    protected $table = 'orders';
    protected $fillable = array(
        'customer_id',
        'order_no',
        'address_id',
        'payment_mode',
        'total_discount',
        'total_tax',
        'total_delivery_charge',
        'grand_total',
        'coupon_applied',
        'counpon_id',
        'coupon_amount',
        'status',
        'created_at',
        'updated_at',
    );

    public function orderItems() {
        return $this->hasMany('App\Model\OrderItem', 'order_id');
    }

}
