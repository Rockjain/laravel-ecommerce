<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model {

    protected $table = 'customer_cart';
    protected $fillable = array(
        'customer_id',
        'product_id',
        'quantity',
        'status',
        'created_at',
        'updated_at'
    );

}
