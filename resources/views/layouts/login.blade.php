<!DOCTYPE html>
<html lang="en" class="body-full-height">
    <head>        
        <!-- META SECTION -->
        <title>Aaima : Login</title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="favicon.ico" type="image/x-icon" />
        <!-- END META SECTION -->
        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="<?= url('/'); ?>/adminassets/css/theme-default.css"/>
        <!-- EOF CSS INCLUDE --> 
        <script type="text/javascript" src="<?= url('/'); ?>/adminassets/js/plugins/jquery/jquery.min.js"></script>
        <script src="<?= url('/'); ?>/adminassets/js/jquery-validate.min.js"></script>
        <script type="text/javascript" src="<?= url('/'); ?>/adminassets/js/plugins/jquery/jquery-ui.min.js"></script>
    </head>
    <body>
        <div class="login-container">
            @yield('content')
        </div>
    </body>
    <script type="text/javascript">
        $("#LoginForm").validate();
    </script>
</html>






