@extends('layouts.login')
@section('content')
<div class="login-box animated fadeInDown">
    <div class="login-logo" style="background-size:contain;" ></div>
    <div class="login-body">
        <div class="login-title"><strong>Welcome</strong>, Please login</div>
        <form method="post" id="LoginForm" action="{{ route('login') }}" class="form-horizontal" >
            @csrf
            <div class="form-group">
                <div class="col-md-12">
                    <input type="email" name="email" class="form-control" placeholder="Email" required/>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <input type="password" name="password" class="form-control" placeholder="Password" required/>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-6">
                    <a href="" class="btn btn-link btn-block">Forgot your password?</a>
                </div>
                <div class="col-md-6">
                    <button type="submit" class="btn btn-info btn-block">Log In</button>
                </div>
            </div>
        </form>
    </div>
    <div class="login-footer">
        <div class="pull-left">
            &copy; 2019 AAIMA
        </div>
    </div>
</div>
@endsection
