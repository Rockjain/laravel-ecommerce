<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Model\Customers;
use Illuminate\Http\Request;

class CustomersController extends Controller {

	public function __construct() {
		$this->middleware('auth');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {
		$customers = Customers::orderBy('id', 'desc')->get();
		return view('customers.index', ['customers' => $customers]);
	}

	public function filtered_customers() {
		extract($_POST);
		if ($filter_name != '') {
			$where['firstname'] = $filter_name;
		}
		if ($filter_email != '') {
			$where['email'] = $filter_name;
		}
		if ($filter_customer_group_id != '') {
			$where['customer_group_id'] = $filter_customer_group_id;
		}
		if ($filter_status != '') {
			$where['status'] = $filter_status;
		}
		if ($filter_sponser_id != '') {
			$where['sponser_id'] = $filter_sponser_id;
		}
		if ($filter_date_added != '') {
			$where['date_added'] = $filter_date_added . '%';
		}

		$customers = Customers::where($where)->get();
		// return view('Customers.index', ['customers' => $customers]);
		return back()->withInput()->with(['customers' => $customers]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function addCustomer() {

		return view('Customers.add_customer');
	}

	public function checkSponser() {

		extract($_POST);
		$where['referred_by'] = $sponser_id;
		$customers = Customers::where($where)->count();
		return $customers;

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {

		$addArr = [
			'customer_group_id' => $request->input('customer_group_id'),
			'firstname' => $request->input('firstname'),
			'lastname' => $request->input('lastname'),
			'email' => $request->input('email'),
			'telephone' => $request->input('telephone'),
			'fax' => $request->input('fax'),
			//'aadhar_card' => $request->input('aadhar_card'),
			'bank_account_number' => $request->input('bank_account'),
			'bank_swift_code' => $request->input('ifsc'),
			'pan_card' => $request->input('pan_card'),
			//'nominee_name' => $request->input('nominee_name'),
			//'nominee_relation' => $request->input('nominee_relation'),
			//'nominee_aadhar_card' => $request->input('nominee_aadhar_card'),
			'password' => md5($request->input('password')),
			'pass' => $request->input('password'),
			'newsletter' => $request->input('newsletter'),
			'status' => $request->input('status'),
			'approved' => $request->input('approved'),
			'safe' => $request->input('safe'),
		];

//echo "<pre>";print_r($addArr);die;
		$return = Customers::create($addArr);

		return back()->withInput()->with('error', 'Error creating new Customers');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\Model\Customers  $Customers
	 * @return \Illuminate\Http\Response
	 */
	public function show(Customers $Customers) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\Model\Customers  $Customers
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Customers $customers) {
		//$categories = Category::where(['status' => '1'])->get();
		$customersInfo = Customers::find($customers->id);
		//$catbrands = \App\Model\CategoryBrand::where(['category_id' => $CustomersInfo->category_id])->get();

		//$customersdetails = CustomersAttribute::where(['Customers_id' => $Customers->id])->get();
		return view('customers.edit', ['customersInfo' => $customersInfo]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\Model\Customers  $Customers
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, Customers $Customers) {
		$updateArr = [
			'name' => $request->input('name'),
			'price' => $request->input('price'),
			'quantity' => $request->input('quantity'),
			'description' => $request->input('description'),
			'category_id' => $request->input('category_id'),
			'brand_id' => $request->input('brand_id'),
			'discount_percent' => $request->input('discount_percent'),
			'discount_quantity' => $request->input('discount_quantity'),
			'discount_start_date' => date('Y:m:d', strtotime($request->input('discount_start_date'))),
			'discount_end_date' => date('Y:m:d', strtotime($request->input('discount_end_date'))),
		];

		$path = "/images/Customersimages/";
		$check = $this->uploadFile($request, 'Customers_image', $path);
		if ($check):
			$nameArray = explode('.', $check);
			$ext = end($nameArray);

			$insertParam['file_path'] = url('/') . $path;
			$insertParam['file_name'] = $check;
			$insertParam['file_type'] = $ext;

			$getImgData = $this->insertFile($insertParam);

			$updateArr['image'] = $getImgData['id'];
		endif;

		$return = Customers::where('id', $Customers->id)->update($updateArr);
		if ($return) {
			if ($request->input('values')):
				foreach ($request->input('values') as $value):
					$attrArr = [
						'Customers_id' => $Customers->id,
						'attribute_value' => $value['attr_val'],
						'type' => $value['attr_type'],
					];
					CustomersAttribute::where('id', $value['attr_id'])->update($attrArr);
				endforeach;
			endif;
			return redirect()->route('Customers.index')->with('success', 'Customers updated successfully');
		}
		return back()->withInput();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\Model\Customers  $Customers
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Customers $Customers) {
		//
	}

}
