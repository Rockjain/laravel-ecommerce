<meta name="csrf-token" content="{{ csrf_token() }}">
@extends('layouts.admin')
@section('content')
  <!-- PAGE CONTENT WRAPPER -->
  <div class="page-content-wrap">
    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">Add New Customer
            </h3>
          </div>
          <div class="tabs">
            <form action="/admin/add_customer" method="post" enctype="multipart/form-data" id="form-customer" class="form-horizontal">
              {{csrf_field()}}
              <ul class="nav nav-tabs">
                <li class="active">
                  <a href="#tab-general" data-toggle="tab" aria-expanded="true">General
                  </a>
                </li>
                <li class="">
                  <a href="#tab-history" data-toggle="tab" aria-expanded="false">History
                  </a>
                </li>
                <li class="">
                  <a href="#tab-transaction" data-toggle="tab" aria-expanded="false">Transactions
                  </a>
                </li>
                <li class="">
                  <a href="#tab-reward" data-toggle="tab" aria-expanded="false">Reward Points
                  </a>
                </li>
                <li class="">
                  <a href="#tab-downline" data-toggle="tab" aria-expanded="false">Downline
                  </a>
                </li>
                <li class="">
                  <a href="#tab-ip" data-toggle="tab" aria-expanded="false">IP Addresses
                  </a>
                </li>
              </ul>
              <div class="tab-content panel-body">
                <div class="tab-pane active" id="tab-general">
                  <div class="row">
                    <div class="col-sm-2">
                      <ul class="nav nav-pills nav-stacked" id="address">
                        <li class="">
                          <a href="#tab-customer" data-toggle="tab" aria-expanded="false">General
                          </a>
                        </li>
                        <li class="active">
                          <a href="#tab-address1" data-toggle="tab" aria-expanded="true">
                            <i class="fa fa-minus-circle" onclick="$('#address a:first').tab('show'); $('#address a[href=\'#tab-address1\']').parent().remove(); $('#tab-address1').remove();">
                            </i> Address 1
                          </a>
                        </li>
                        <li id="address-add">
                          <a onclick="addAddress();">
                            <i class="fa fa-plus-circle">
                            </i> Add Address
                          </a>
                        </li>
                      </ul>
                    </div>
                    <div class="col-sm-10">
                      <div class="tab-content">
                        <div class="tab-pane" id="tab-customer">
                          <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-customer-group">Customer Group
                            </label>
                            <div class="col-sm-10">
                              <select name="customer_group_id" id="input-customer-group" class="form-control">
                                <option value="1" selected="selected">Default
                                </option>
                                <option value="4">Distributor
                                </option>
                                <option value="3">Guest
                                </option>
                              </select>
                            </div>
                          </div>
                          <div class="form-group required">
                            <label class="col-sm-2 control-label" for="input-firstname">First Name
                            </label>
                            <div class="col-sm-10">
                              <input type="text" name="firstname" value=" JI GPP" placeholder="First Name" id="input-firstname" class="form-control">
                            </div>
                          </div>
                          <div class="form-group required">
                            <label class="col-sm-2 control-label" for="input-lastname">Last Name
                            </label>
                            <div class="col-sm-10">
                              <input type="text" name="lastname" value="GARG" placeholder="Last Name" id="input-lastname" class="form-control">
                            </div>
                          </div>
                          <div class="form-group required">
                            <label class="col-sm-2 control-label" for="input-email">E-Mail
                            </label>
                            <div class="col-sm-10">
                              <input type="text" name="email" value="XGPGARG@GMAIL.COM" placeholder="E-Mail" id="input-email" class="form-control">
                            </div>
                          </div>
                          <div class="form-group required">
                            <label class="col-sm-2 control-label" for="input-telephone">Telephone
                            </label>
                            <div class="col-sm-10">
                              <input type="text" name="telephone" value="9312242687" placeholder="Telephone" id="input-telephone" class="form-control">
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-fax">Fax
                            </label>
                            <div class="col-sm-10">
                              <input type="text" name="fax" value="1234" placeholder="Fax" id="input-fax" class="form-control">
                            </div>
                          </div>
                          <div class="form-group custom-field custom-field13" data-sort="6" style="display: block;">
                            <label class="col-sm-2 control-label" for="input-custom-field13">Aadhar Card
                            </label>
                            <div class="col-sm-10">
                              <input type="text" name="custom_field[13]" value="45266" placeholder="Aadhar Card" id="input-custom-field13" class="form-control">
                            </div>
                          </div>
                          <div class="form-group custom-field custom-field9" data-sort="7" style="display: block;">
                            <label class="col-sm-2 control-label" for="input-custom-field9">Bank Account Number
                            </label>
                            <div class="col-sm-10">
                              <input type="text" name="custom_field[9]" value="giy89yytry" placeholder="Bank Account Number" id="input-custom-field9" class="form-control">
                            </div>
                          </div>
                          <div class="form-group custom-field custom-field10" data-sort="8" style="display: block;">
                            <label class="col-sm-2 control-label" for="input-custom-field10">IFSC Code
                            </label>
                            <div class="col-sm-10">
                              <input type="text" name="custom_field[10]" placeholder="IFSC Code" id="input-custom-field10" class="form-control">
                            </div>
                          </div>
                          <div class="form-group custom-field custom-field8" data-sort="9" style="display: block;">
                            <label class="col-sm-2 control-label" for="input-custom-field8">Pan Card
                            </label>
                            <div class="col-sm-10">
                              <input type="text" name="custom_field[8]" placeholder="Pan Card" id="input-custom-field8" class="form-control">
                            </div>
                          </div>
                          <div class="form-group custom-field custom-field14" data-sort="10" style="display: block;">
                            <label class="col-sm-2 control-label" for="input-custom-field14">Nominee Name
                            </label>
                            <div class="col-sm-10">
                              <input type="text" name="custom_field[14]" placeholder="Nominee Name" id="input-custom-field14" class="form-control">
                            </div>
                          </div>
                          <div class="form-group custom-field custom-field15" data-sort="11" style="display: block;">
                            <label class="col-sm-2 control-label" for="input-custom-field15">Relation of Nominee
                            </label>
                            <div class="col-sm-10">
                              <input type="text" name="custom_field[15]" placeholder="Relation of Nominee" id="input-custom-field15" class="form-control">
                            </div>
                          </div>
                          <div class="form-group custom-field custom-field16" data-sort="12" style="display: block;">
                            <label class="col-sm-2 control-label" for="input-custom-field16">Aadhar Card of Nominee
                            </label>
                            <div class="col-sm-10">
                              <input type="text" name="custom_field[16]" placeholder="Aadhar Card of Nominee" id="input-custom-field16" class="form-control">
                            </div>
                          </div>
                          <div class="form-group required">
                            <label class="col-sm-2 control-label" for="input-password">Password
                            </label>
                            <div class="col-sm-10">
                              <input type="password" name="password" value="" placeholder="Password" id="input-password" class="form-control" autocomplete="off">
                            </div>
                          </div>
                          <div class="form-group required">
                            <label class="col-sm-2 control-label" for="input-confirm">Confirm
                            </label>
                            <div class="col-sm-10">
                              <input type="password" name="confirm" value="" placeholder="Confirm" autocomplete="off" id="input-confirm" class="form-control">
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-newsletter">Newsletter
                            </label>
                            <div class="col-sm-10">
                              <select name="newsletter" id="input-newsletter" class="form-control">
                                <option value="1" selected="selected">Enabled
                                </option>
                                <option value="0">Disabled
                                </option>
                              </select>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-status">Status
                            </label>
                            <div class="col-sm-10">
                              <select name="status" id="input-status" class="form-control">
                                <option value="1" selected="selected">Enabled
                                </option>
                                <option value="0">Disabled
                                </option>
                              </select>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-approved">Approved
                            </label>
                            <div class="col-sm-10">
                              <select name="approved" id="input-approved" class="form-control">
                                <option value="1" selected="selected">Yes
                                </option>
                                <option value="0">No
                                </option>
                              </select>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-safe">Safe
                            </label>
                            <div class="col-sm-10">
                              <select name="safe" id="input-safe" class="form-control">
                                <option value="1">Yes
                                </option>
                                <option value="0" selected="selected">No
                                </option>
                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="tab-pane active" id="tab-address1">
                          <input type="hidden" name="address[1][address_id]" value="3398">
                          <div class="form-group required">
                            <label class="col-sm-2 control-label" for="input-firstname1">First Name
                            </label>
                            <div class="col-sm-10">
                              <input type="text" name="address[1][firstname]" value=" JI GP" placeholder="First Name" id="input-firstname1" class="form-control">
                            </div>
                          </div>
                          <div class="form-group required">
                            <label class="col-sm-2 control-label" for="input-lastname1">Last Name
                            </label>
                            <div class="col-sm-10">
                              <input type="text" name="address[1][lastname]" value="GARG" placeholder="Last Name" id="input-lastname1" class="form-control">
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-company1">Company
                            </label>
                            <div class="col-sm-10">
                              <input type="text" name="address[1][company]" placeholder="Company" id="input-company1" class="form-control">
                            </div>
                          </div>
                          <div class="form-group required">
                            <label class="col-sm-2 control-label" for="input-address-11">Address 1
                            </label>
                            <div class="col-sm-10">
                              <input type="text" name="address[1][address_1]" value="gurgaon" placeholder="Address 1" id="input-address-11" class="form-control">
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-address-21">Address 2
                            </label>
                            <div class="col-sm-10">
                              <input type="text" name="address[1][address_2]" placeholder="Address 2" id="input-address-21" class="form-control">
                            </div>
                          </div>
                          <div class="form-group required">
                            <label class="col-sm-2 control-label" for="input-city1">City
                            </label>
                            <div class="col-sm-10">
                              <input type="text" name="address[1][city]" value="gurgaon" placeholder="City" id="input-city1" class="form-control">
                            </div>
                          </div>
                          <div class="form-group required">
                            <label class="col-sm-2 control-label" for="input-postcode1">Postcode
                            </label>
                            <div class="col-sm-10">
                              <input type="text" name="address[1][postcode]" value="122001" placeholder="Postcode" id="input-postcode1" class="form-control">
                            </div>
                          </div>
                          <div class="form-group required">
                            <label class="col-sm-2 control-label" for="input-country1">Country
                            </label>
                            <div class="col-sm-10">
                              <select name="address[1][country_id]" id="input-country1" onchange="country(this, '1', '1486');" class="form-control">
                                <option value=""> --- Please Select ---
                                </option>
                                <option value="244">Aaland Islands
                                </option>
                                <option value="1">Afghanistan
                                </option>
                                <option value="2">Albania
                                </option>
                                <option value="3">Algeria
                                </option>
                                <option value="4">American Samoa
                                </option>
                                <option value="5">Andorra
                                </option>
                                <option value="6">Angola
                                </option>
                                <option value="7">Anguilla
                                </option>
                                <option value="8">Antarctica
                                </option>
                                <option value="9">Antigua and Barbuda
                                </option>
                                <option value="10">Argentina
                                </option>
                                <option value="11">Armenia
                                </option>
                                <option value="12">Aruba
                                </option>
                                <option value="252">Ascension Island (British)
                                </option>
                                <option value="13">Australia
                                </option>
                                <option value="14">Austria
                                </option>
                                <option value="15">Azerbaijan
                                </option>
                                <option value="16">Bahamas
                                </option>
                                <option value="17">Bahrain
                                </option>
                                <option value="18">Bangladesh
                                </option>
                                <option value="19">Barbados
                                </option>
                                <option value="20">Belarus
                                </option>
                                <option value="21">Belgium
                                </option>
                                <option value="22">Belize
                                </option>
                                <option value="23">Benin
                                </option>
                                <option value="24">Bermuda
                                </option>
                                <option value="25">Bhutan
                                </option>
                                <option value="26">Bolivia
                                </option>
                                <option value="245">Bonaire, Sint Eustatius and Saba
                                </option>
                                <option value="27">Bosnia and Herzegovina
                                </option>
                                <option value="28">Botswana
                                </option>
                                <option value="29">Bouvet Island
                                </option>
                                <option value="30">Brazil
                                </option>
                                <option value="31">British Indian Ocean Territory
                                </option>
                                <option value="32">Brunei Darussalam
                                </option>
                                <option value="33">Bulgaria
                                </option>
                                <option value="34">Burkina Faso
                                </option>
                                <option value="35">Burundi
                                </option>
                                <option value="36">Cambodia
                                </option>
                                <option value="37">Cameroon
                                </option>
                                <option value="38">Canada
                                </option>
                                <option value="251">Canary Islands
                                </option>
                                <option value="39">Cape Verde
                                </option>
                                <option value="40">Cayman Islands
                                </option>
                                <option value="41">Central African Republic
                                </option>
                                <option value="42">Chad
                                </option>
                                <option value="43">Chile
                                </option>
                                <option value="44">China
                                </option>
                                <option value="45">Christmas Island
                                </option>
                                <option value="46">Cocos (Keeling) Islands
                                </option>
                                <option value="47">Colombia
                                </option>
                                <option value="48">Comoros
                                </option>
                                <option value="49">Congo
                                </option>
                                <option value="50">Cook Islands
                                </option>
                                <option value="51">Costa Rica
                                </option>
                                <option value="52">Cote D'Ivoire
                                </option>
                                <option value="53">Croatia
                                </option>
                                <option value="54">Cuba
                                </option>
                                <option value="246">Curacao
                                </option>
                                <option value="55">Cyprus
                                </option>
                                <option value="56">Czech Republic
                                </option>
                                <option value="237">Democratic Republic of Congo
                                </option>
                                <option value="57">Denmark
                                </option>
                                <option value="58">Djibouti
                                </option>
                                <option value="59">Dominica
                                </option>
                                <option value="60">Dominican Republic
                                </option>
                                <option value="61">East Timor
                                </option>
                                <option value="62">Ecuador
                                </option>
                                <option value="63">Egypt
                                </option>
                                <option value="64">El Salvador
                                </option>
                                <option value="65">Equatorial Guinea
                                </option>
                                <option value="66">Eritrea
                                </option>
                                <option value="67">Estonia
                                </option>
                                <option value="68">Ethiopia
                                </option>
                                <option value="69">Falkland Islands (Malvinas)
                                </option>
                                <option value="70">Faroe Islands
                                </option>
                                <option value="71">Fiji
                                </option>
                                <option value="72">Finland
                                </option>
                                <option value="74">France, Metropolitan
                                </option>
                                <option value="75">French Guiana
                                </option>
                                <option value="76">French Polynesia
                                </option>
                                <option value="77">French Southern Territories
                                </option>
                                <option value="126">FYROM
                                </option>
                                <option value="78">Gabon
                                </option>
                                <option value="79">Gambia
                                </option>
                                <option value="80">Georgia
                                </option>
                                <option value="81">Germany
                                </option>
                                <option value="82">Ghana
                                </option>
                                <option value="83">Gibraltar
                                </option>
                                <option value="84">Greece
                                </option>
                                <option value="85">Greenland
                                </option>
                                <option value="86">Grenada
                                </option>
                                <option value="87">Guadeloupe
                                </option>
                                <option value="88">Guam
                                </option>
                                <option value="89">Guatemala
                                </option>
                                <option value="256">Guernsey
                                </option>
                                <option value="90">Guinea
                                </option>
                                <option value="91">Guinea-Bissau
                                </option>
                                <option value="92">Guyana
                                </option>
                                <option value="93">Haiti
                                </option>
                                <option value="94">Heard and Mc Donald Islands
                                </option>
                                <option value="95">Honduras
                                </option>
                                <option value="96">Hong Kong
                                </option>
                                <option value="97">Hungary
                                </option>
                                <option value="98">Iceland
                                </option>
                                <option value="99" selected="selected">India
                                </option>
                                <option value="100">Indonesia
                                </option>
                                <option value="101">Iran (Islamic Republic of)
                                </option>
                                <option value="102">Iraq
                                </option>
                                <option value="103">Ireland
                                </option>
                                <option value="254">Isle of Man
                                </option>
                                <option value="104">Israel
                                </option>
                                <option value="105">Italy
                                </option>
                                <option value="106">Jamaica
                                </option>
                                <option value="107">Japan
                                </option>
                                <option value="257">Jersey
                                </option>
                                <option value="108">Jordan
                                </option>
                                <option value="109">Kazakhstan
                                </option>
                                <option value="110">Kenya
                                </option>
                                <option value="111">Kiribati
                                </option>
                                <option value="253">Kosovo, Republic of
                                </option>
                                <option value="114">Kuwait
                                </option>
                                <option value="115">Kyrgyzstan
                                </option>
                                <option value="116">Lao People's Democratic Republic
                                </option>
                                <option value="117">Latvia
                                </option>
                                <option value="118">Lebanon
                                </option>
                                <option value="119">Lesotho
                                </option>
                                <option value="120">Liberia
                                </option>
                                <option value="121">Libyan Arab Jamahiriya
                                </option>
                                <option value="122">Liechtenstein
                                </option>
                                <option value="123">Lithuania
                                </option>
                                <option value="124">Luxembourg
                                </option>
                                <option value="125">Macau
                                </option>
                                <option value="127">Madagascar
                                </option>
                                <option value="128">Malawi
                                </option>
                                <option value="129">Malaysia
                                </option>
                                <option value="130">Maldives
                                </option>
                                <option value="131">Mali
                                </option>
                                <option value="132">Malta
                                </option>
                                <option value="133">Marshall Islands
                                </option>
                                <option value="134">Martinique
                                </option>
                                <option value="135">Mauritania
                                </option>
                                <option value="136">Mauritius
                                </option>
                                <option value="137">Mayotte
                                </option>
                                <option value="138">Mexico
                                </option>
                                <option value="139">Micronesia, Federated States of
                                </option>
                                <option value="140">Moldova, Republic of
                                </option>
                                <option value="141">Monaco
                                </option>
                                <option value="142">Mongolia
                                </option>
                                <option value="242">Montenegro
                                </option>
                                <option value="143">Montserrat
                                </option>
                                <option value="144">Morocco
                                </option>
                                <option value="145">Mozambique
                                </option>
                                <option value="146">Myanmar
                                </option>
                                <option value="147">Namibia
                                </option>
                                <option value="148">Nauru
                                </option>
                                <option value="149">Nepal
                                </option>
                                <option value="150">Netherlands
                                </option>
                                <option value="151">Netherlands Antilles
                                </option>
                                <option value="152">New Caledonia
                                </option>
                                <option value="153">New Zealand
                                </option>
                                <option value="154">Nicaragua
                                </option>
                                <option value="155">Niger
                                </option>
                                <option value="156">Nigeria
                                </option>
                                <option value="157">Niue
                                </option>
                                <option value="158">Norfolk Island
                                </option>
                                <option value="112">North Korea
                                </option>
                                <option value="159">Northern Mariana Islands
                                </option>
                                <option value="160">Norway
                                </option>
                                <option value="161">Oman
                                </option>
                                <option value="162">Pakistan
                                </option>
                                <option value="163">Palau
                                </option>
                                <option value="247">Palestinian Territory, Occupied
                                </option>
                                <option value="164">Panama
                                </option>
                                <option value="165">Papua New Guinea
                                </option>
                                <option value="166">Paraguay
                                </option>
                                <option value="167">Peru
                                </option>
                                <option value="168">Philippines
                                </option>
                                <option value="169">Pitcairn
                                </option>
                                <option value="170">Poland
                                </option>
                                <option value="171">Portugal
                                </option>
                                <option value="172">Puerto Rico
                                </option>
                                <option value="173">Qatar
                                </option>
                                <option value="174">Reunion
                                </option>
                                <option value="175">Romania
                                </option>
                                <option value="176">Russian Federation
                                </option>
                                <option value="177">Rwanda
                                </option>
                                <option value="178">Saint Kitts and Nevis
                                </option>
                                <option value="179">Saint Lucia
                                </option>
                                <option value="180">Saint Vincent and the Grenadines
                                </option>
                                <option value="181">Samoa
                                </option>
                                <option value="182">San Marino
                                </option>
                                <option value="183">Sao Tome and Principe
                                </option>
                                <option value="184">Saudi Arabia
                                </option>
                                <option value="185">Senegal
                                </option>
                                <option value="243">Serbia
                                </option>
                                <option value="186">Seychelles
                                </option>
                                <option value="187">Sierra Leone
                                </option>
                                <option value="188">Singapore
                                </option>
                                <option value="189">Slovak Republic
                                </option>
                                <option value="190">Slovenia
                                </option>
                                <option value="191">Solomon Islands
                                </option>
                                <option value="192">Somalia
                                </option>
                                <option value="193">South Africa
                                </option>
                                <option value="194">South Georgia &amp; South Sandwich Islands
                                </option>
                                <option value="113">South Korea
                                </option>
                                <option value="248">South Sudan
                                </option>
                                <option value="195">Spain
                                </option>
                                <option value="196">Sri Lanka
                                </option>
                                <option value="249">St. Barthelemy
                                </option>
                                <option value="197">St. Helena
                                </option>
                                <option value="250">St. Martin (French part)
                                </option>
                                <option value="198">St. Pierre and Miquelon
                                </option>
                                <option value="199">Sudan
                                </option>
                                <option value="200">Suriname
                                </option>
                                <option value="201">Svalbard and Jan Mayen Islands
                                </option>
                                <option value="202">Swaziland
                                </option>
                                <option value="203">Sweden
                                </option>
                                <option value="204">Switzerland
                                </option>
                                <option value="205">Syrian Arab Republic
                                </option>
                                <option value="206">Taiwan
                                </option>
                                <option value="207">Tajikistan
                                </option>
                                <option value="208">Tanzania, United Republic of
                                </option>
                                <option value="209">Thailand
                                </option>
                                <option value="210">Togo
                                </option>
                                <option value="211">Tokelau
                                </option>
                                <option value="212">Tonga
                                </option>
                                <option value="213">Trinidad and Tobago
                                </option>
                                <option value="255">Tristan da Cunha
                                </option>
                                <option value="214">Tunisia
                                </option>
                                <option value="215">Turkey
                                </option>
                                <option value="216">Turkmenistan
                                </option>
                                <option value="217">Turks and Caicos Islands
                                </option>
                                <option value="218">Tuvalu
                                </option>
                                <option value="219">Uganda
                                </option>
                                <option value="220">Ukraine
                                </option>
                                <option value="221">United Arab Emirates
                                </option>
                                <option value="222">United Kingdom
                                </option>
                                <option value="223">United States
                                </option>
                                <option value="224">United States Minor Outlying Islands
                                </option>
                                <option value="225">Uruguay
                                </option>
                                <option value="226">Uzbekistan
                                </option>
                                <option value="227">Vanuatu
                                </option>
                                <option value="228">Vatican City State (Holy See)
                                </option>
                                <option value="229">Venezuela
                                </option>
                                <option value="230">Viet Nam
                                </option>
                                <option value="231">Virgin Islands (British)
                                </option>
                                <option value="232">Virgin Islands (U.S.)
                                </option>
                                <option value="233">Wallis and Futuna Islands
                                </option>
                                <option value="234">Western Sahara
                                </option>
                                <option value="235">Yemen
                                </option>
                                <option value="238">Zambia
                                </option>
                                <option value="239">Zimbabwe
                                </option>
                              </select>
                            </div>
                          </div>
                          <div class="form-group custom-field custom-field5" data-sort="8" style="display: block;">
                            <label class="col-sm-2 control-label">Aadhar Card(front scanned)
                            </label>
                            <div class="col-sm-10">
                              <input type="file" class="file" data-preview-file-type="any"/>
                              <a target="_blank" href="">
                                <i class="fa fa-eye">
                                </i>
                              </a>
                              <input type="hidden" name="address[1][custom_field][5]" value="">
                            </div>
                          </div>
                          <div class="form-group custom-field custom-field12" data-sort="9" style="display: block;">
                            <label class="col-sm-2 control-label">Aadhar Card(back scanned)
                            </label>
                            <div class="col-sm-10">
                              <input type="file" class="file" data-preview-file-type="any"/>
                              <a target="_blank" href="">
                                <i class="fa fa-eye">
                                </i>
                              </a>
                              <input type="hidden" name="address[1][custom_field][12]" value="">
                            </div>
                          </div>
                          <div class="form-group required">
                            <label class="col-sm-2 control-label" for="input-zone1">Region / State
                            </label>
                            <div class="col-sm-10">
                              <select name="address[1][zone_id]" id="input-zone1" class="form-control">
                                <option value=""> --- Please Select ---
                                </option>
                                <option value="1475">Andaman and Nicobar Islands
                                </option>
                                <option value="1476">Andhra Pradesh
                                </option>
                                <option value="1477">Arunachal Pradesh
                                </option>
                                <option value="1478">Assam
                                </option>
                                <option value="1479">Bihar
                                </option>
                                <option value="1480">Chandigarh
                                </option>
                                <option value="1481">Dadra and Nagar Haveli
                                </option>
                                <option value="1482">Daman and Diu
                                </option>
                                <option value="1483">Delhi
                                </option>
                                <option value="1484">Goa
                                </option>
                                <option value="1485">Gujarat
                                </option>
                                <option value="1486" selected="selected">Haryana
                                </option>
                                <option value="1487">Himachal Pradesh
                                </option>
                                <option value="1488">Jammu and Kashmir
                                </option>
                                <option value="1489">Karnataka
                                </option>
                                <option value="1490">Kerala
                                </option>
                                <option value="1491">Lakshadweep Islands
                                </option>
                                <option value="1492">Madhya Pradesh
                                </option>
                                <option value="1493">Maharashtra
                                </option>
                                <option value="1494">Manipur
                                </option>
                                <option value="1495">Meghalaya
                                </option>
                                <option value="1496">Mizoram
                                </option>
                                <option value="1497">Nagaland
                                </option>
                                <option value="1498">Orissa
                                </option>
                                <option value="1499">Puducherry
                                </option>
                                <option value="1500">Punjab
                                </option>
                                <option value="1501">Rajasthan
                                </option>
                                <option value="1502">Sikkim
                                </option>
                                <option value="1503">Tamil Nadu
                                </option>
                                <option value="4231">Telangana
                                </option>
                                <option value="1504">Tripura
                                </option>
                                <option value="1505">Uttar Pradesh
                                </option>
                                <option value="4236">Uttarakhand
                                </option>
                                <option value="1506">West Bengal
                                </option>
                              </select>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-2 control-label">Default Address
                            </label>
                            <div class="col-sm-10">
                              <label class="radio">
                                <input type="radio" name="address[1][default]" value="1" checked="checked">
                              </label>
                            </div>
                          </div>
                          <div class="form-group custom-field custom-field11" data-sort="14" style="display: block;">
                            <label class="col-sm-2 control-label">Bank Account Copy(scanned)
                            </label>
                            <div class="col-sm-10">
                              <input type="file" class="file" data-preview-file-type="any"/>
                              <a target="_blank" href="">
                                <i class="fa fa-eye">
                                </i>
                              </a>
                              <input type="hidden" name="address[1][custom_field][11]" value="">
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="tab-pane" id="tab-history">
                  <div id="history">
                    <div class="table-responsive">
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <td class="text-left">Date Added
                            </td>
                            <td class="text-left">Comment
                            </td>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td class="text-center" colspan="2">No results!
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="row">
                      <div class="col-sm-6 text-left">
                      </div>
                      <div class="col-sm-6 text-right">Showing 0 to 0 of 0 (0 Pages)
                      </div>
                    </div>
                  </div>
                  <br>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-comment">Comment
                    </label>
                    <div class="col-sm-10">
                      <textarea name="comment" rows="8" placeholder="Comment" id="input-comment" class="form-control">
                      </textarea>
                    </div>
                  </div>
                  <div class="text-right">
                    <button type="button" id="button-history" data-loading-text="Loading..." class="btn btn-primary">
                      <i class="fa fa-plus-circle">
                      </i> Add History
                    </button>
                  </div>
                </div>
                <div class="tab-pane" id="tab-transaction">
                  <div id="transaction">
                    <div class="table-responsive">
                      <table class="table table-bordered table-hover">
                        <thead>
                          <tr>
                            <td class="text-left">Date Added
                            </td>
                            <td class="text-left">Description
                            </td>
                            <td class="text-right">Amount
                            </td>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td class="text-center" colspan="3">No results!
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="row">
                      <div class="col-sm-6 text-left">
                      </div>
                      <div class="col-sm-6 text-right">Showing 0 to 0 of 0 (0 Pages)
                      </div>
                    </div>
                  </div>
                  <br>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-transaction-description">Description
                    </label>
                    <div class="col-sm-10">
                      <input type="text" name="description" placeholder="Description" id="input-transaction-description" class="form-control">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-amount">Amount
                    </label>
                    <div class="col-sm-10">
                      <input type="text" name="amount" placeholder="Amount" id="input-amount" class="form-control">
                    </div>
                  </div>
                  <div class="text-right">
                    <button type="button" id="button-transaction" data-loading-text="Loading..." class="btn btn-primary">
                      <i class="fa fa-plus-circle">
                      </i> Add Transaction
                    </button>
                  </div>
                </div>
                <div class="tab-pane" id="tab-reward">
                  <div id="reward">
                    <div class="table-responsive">
                      <table class="table table-bordered table-hover">
                        <thead>
                          <tr>
                            <td class="text-left">Date Added
                            </td>
                            <td class="text-left">Description
                            </td>
                            <td class="text-right">Points
                            </td>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td class="text-center" colspan="3">No results!
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="row">
                      <div class="col-sm-6 text-left">
                      </div>
                      <div class="col-sm-6 text-right">Showing 0 to 0 of 0 (0 Pages)
                      </div>
                    </div>
                  </div>
                  <br>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-reward-description">Description
                    </label>
                    <div class="col-sm-10">
                      <input type="text" name="description" placeholder="Description" id="input-reward-description" class="form-control">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-points">
                      <span data-toggle="tooltip" title="" data-original-title="Use minus to remove points">Points
                      </span>
                    </label>
                    <div class="col-sm-10">
                      <input type="text" name="points" placeholder="Points" id="input-points" class="form-control">
                    </div>
                  </div>
                  <div class="text-right">
                    <button type="button" id="button-reward" data-loading-text="Loading..." class="btn btn-primary">
                      <i class="fa fa-plus-circle">
                      </i> Add Reward Points
                    </button>
                  </div>
                </div>
                <div class="tab-pane" id="tab-cashback">
                  <div id="cashback">
                    <div class="table-responsive">
                      <table class="table table-bordered table-hover">
                        <thead>
                          <tr>
                            <td class="text-center">Date Added
                            </td>
                            <td class="text-center">Coupon Code
                            </td>
                            <td class="text-center">Cashback Amount
                            </td>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td class="text-center" colspan="3">No results!
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="row">
                      <div class="col-sm-6 text-left">
                      </div>
                      <div class="col-sm-6 text-right">Showing 0 to 0 of 0 (0 Pages)
                      </div>
                    </div>
                  </div>
                  <br>
                  <div class="text-right">
                  </div>
                </div>
                <div class="tab-pane" id="documents">
                  <!--<fieldset>
<legend>Attached Documents </legend>
<table class="table table-bordered">
<tbody>
<tr>
<td>Aadhar Card Front</td>
<td><a href="" target="_blank"><img class="thumb" src="" width="50px" height="50px"/></td>
</tr>
<tr>
<td>Aadhar Card Back</td>
<td><a href="" target="_blank"><img class="thumb" src="" width="50px" height="50px"/></td>
</tr>
<tr>
<td>Pan Card</td>
<td><a href="" target="_blank"><img class="thumb" src="" width="50px" height="50px"/></td>
</tr>
<tr>
<td>Cheque Photo</td>
<td><a href="" target="_blank"><img class="thumb" src="" width="50px" height="50px"/></td>
</tr>
</tbody>
</table>
</fieldset>-->
                </div>
                <div class="tab-pane" id="tab-downline">
                  <div class="col-md-12 tree">
                    <ul style="overflow-x: auto;white-space: nowrap;padding-bottom:50px;">
                      <li>
                        <a href="javascript:void(0);"> ()
                          <br>&nbsp;
                          Star:0 &nbsp; &nbsp; Points:0
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="tab-pane" id="tab-ip">
                  <div id="ip">
                    <div class="table-responsive">
                      <table class="table table-bordered table-hover">
                        <thead>
                          <tr>
                            <td class="text-left">Sponser ID
                            </td>
                            <td class="text-right">Total Accounts
                            </td>
                            <td class="text-left">Date Added
                            </td>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td class="text-center" colspan="3">No results!
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="row">
                      <div class="col-sm-6 text-left">
                      </div>
                      <div class="col-sm-6 text-right">Showing 0 to 0 of 0 (0 Pages)
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="panel-footer">
                <button type="submit" class="btn btn-info pull-right">Save
                  <span class="fa fa-save">
                  </span>
                </button>
              </form>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- END PAGE CONTENT WRAPPER -->
@endsection
