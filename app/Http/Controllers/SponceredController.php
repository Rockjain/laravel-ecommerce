<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;




class SponceredController extends Controller 
{
    //
    public function index() {
        return view('sponcered.index');
    }

     public function view() {
        $sponcered = DB::table('sponcered')->select('*')->get();

        return view('sponcered.view', ['sponcered' => $sponcered]);
    }

    public function parent_cat($parent_id) {
        $parent = DB::table('adv_category')->select('*')->where('parent',$parent_id)->get();
        return $parent;
    }

    public function insert(Request $request) {
        extract($_POST);
        if ($form_type == 'add') {
            $path = "/images/testimonialsimages/";
            $check = $this->uploadFile($request, 'image', $path);
            if ($check):
                $nameArray = explode('.', $check);
                $ext = end($nameArray);

                $insertParam['file_path'] = $path;
                $insertParam['file_name'] = $check;
                $insertParam['file_type'] = $ext;

                $getImgData = $this->insertFile($insertParam);

                $adArr['image'] = $getImgData['id'];

            endif;
            $data = array('name' => $name, 'description' => $description, 'status' => $status, 'image' => $check);
            $data = DB::table('sponcered')->insert($data);
            echo"$data";
        }else if ($form_type == 'update') {
            $path = "/images/testimonialsimages/";
            $check = $this->uploadFile($request, 'image', $path);
            if ($check) {
                $nameArray = explode('.', $check);
                $ext = end($nameArray);

                $insertParam['file_path'] = $path;
                $insertParam['file_name'] = $check;
                $insertParam['file_type'] = $ext;

                $getImgData = $this->insertFile($insertParam);

                $adArr['image'] = $getImgData['id'];
            } else {
                $check = $image_update;
            }


            $data = DB::table('sponcered')->where('id', $id)->update([
                'name' => $name, 'description' => $description, 'status' => $status, 'image' => $check
            ]);
            if ($data) {
                echo 'updated';
            } else {
                echo 'updated';
            }
        }
    }

    public function edit(Request $request) {
        $id = $request->id;
        $advertisements = DB::table('sponcered')->select('*')->where('id', $id)->get();
        return view('sponcered.index', ['advertisements' => $advertisements]);
    }

    public function delete(Request $request) {
        $id = $request->id;
        $status = DB::table('sponcered')->delete($id);
        if ($status) {
            return redirect()->action('SponceredController@view');
        }
    }

    
}
