@extends('layouts.admin')
@section('content')
<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Edit Attribute</h3>
                </div>
                @include('partials.errors')
                @include('partials.success')
                <div class="panel-body">
                    <form method="post" id="attributeForm" class="form-horizontal" action="{{route('attributes.update',[$attribute->id])}}" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <input type="hidden" name="_method" value="put">
                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Attribute Name</label>
                            <div class="col-md-6 col-xs-12">  
                                <input type="text" name="name" class="form-control" placeholder="Attribute Name" value="{{$attribute->name}}" required/>                                       
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Type</label>
                            <div class="col-md-6 col-xs-12">                            
                                <input type="hidden" name="type" value="{{$attribute->type}}">
                                <select class="form-control" onchange="appendValueDiv(this)" name="type" disabled required />
                                <option value="" disabled selected>select type</option>
                                <option value="SELECT"<?= ($attribute->type == 'SELECT') ? 'selected' : ''; ?>>SELECT</option>
                                <option value="INPUT"<?= ($attribute->type == 'INPUT') ? 'selected' : ''; ?>>INPUT</option>
                                <option value="TOGGLE"<?= ($attribute->type == 'TOGGLE') ? 'selected' : ''; ?>>TOGGLE</option>
                                </select>
                            </div>
                        </div>
                        @if($attribute->type=='SELECT')
                        <div class="col-md-12 col-xs-12 toggleDiv appendDiv form-row">
                            @if($attribute->attributeValues)
                            @foreach($attribute->attributeValues as $key=>$value)
                            <div class="row">
                                <label class="col-md-3 col-xs-12 control-label">Attribute Value</label>
                                <div class="col-md-6 col-xs-12">  
                                    <input type="text" name="attribute_values[{{$value->id}}]" class="form-control" placeholder="Attribute Value" value="{{$value->attribute_value}}" required/>                                       
                                </div>
                            </div>
                            @endforeach
                            @endif
                        </div>
                        @endif
                        <div class="form-group">
                            <div class="col-md-12 col-xs-12" style="margin-top: 10px;">  
                                <p class="text-center">
                                    <input type="submit" class="btn btn-info" value="Update Attribute"/> 
                                </p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
</div>
<script>
    function appendValueDiv(objRef) {
        var obj = $(objRef);
        var type = obj.find('option:selected').val();
        if (type == 'SELECT') {
            $(".toggleDiv").slideDown();
        } else {
            $(".toggleDiv").slideUp();
        }
    }

    function appendMoreValue(objRef) {
        var obj = $(objRef);
        var string = '<div class="row">'
                + '<label class="col-md-3 col-xs-12 control-label">Attribute Value</label>'
                + '<div class="col-md-6 col-xs-12">'
                + '<input type="text" name="attribute_values[]" class="form-control" placeholder="Attribute Value" required/>'
                + '</div>'
                + '<a style="cursor: pointer" onclick="removeDiv(this)"><i class="fa fa-trash-o"></i></a>'
                + '</div>';
        obj.parent().parent('.appendDiv').append(string);
    }
    function removeDiv(objRef) {
        var obj = $(objRef);
        obj.parent().remove();
    }
</script>
@endsection