<!doctype html>
<html>
<head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <meta charset="utf-8">
    <title>Invoice</title>
    
    <style>
    tbody{
     line-height: 2em;
    }
    .invoice-box {
        max-width: 800px;
        margin: auto;
        padding: 15px;
        border: 1px solid #ccc;
       /* box-shadow: 0 0 10px rgba(0, 0, 0, .15);*/
        font-size: 12px;
        line-height: 20px;
        font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        color: #555;
    }
    
    .invoice-box table {
        width: 100%;
        line-height: inherit;
        text-align: left;
    }
    
    .invoice-box table td {
        padding: 5px;
        vertical-align: top;
    }
    
    .invoice-box table tr td:nth-child(2) {
        text-align: right;
    }
.border-bottom{border-bottom:1px solid #f2f2f2;}
    .invoice-box table tr.top table td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.top table td.title {
        font-size: 45px;
        line-height: 45px;
        color: #333;
    }
    
    .invoice-box table tr.information table td {
        padding-bottom: 40px;
    }
    
    .invoice-box table tr.heading td {
        background: #eee !important;
		-webkit-print-color-adjust:exact;
        border-bottom: 1px solid #ddd;
        font-weight: bold;
    }
    
    .invoice-box table tr.details td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.item td{
        border-bottom: 1px solid #eee;
    }
	.invoice-box table tr td table.item-description tr td:first-child{border-left:1px solid #eee}
	.invoice-box table tr td table.item-description tr td{border-bottom: 1px solid #eee;border-right:1px solid #eee}
    
    .invoice-box table tr.item.last td {
        border-bottom: none;
    }
    
    .invoice-box table tr.total td:nth-child(2) {
        border-top: 2px solid #eee;
        font-weight: bold;
    }
    
    @media only screen and (max-width: 600px) {
        .invoice-box table tr.top table td {
            width: 100%;
            display: block;
            text-align: center;
        }
        
        .invoice-box table tr.information table td {
            width: 100%;
            display: block;
            text-align: center;
        }
    }
    
    /** RTL **/
    .rtl {
        direction: rtl;
        font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
    }
    
    .rtl table {
        text-align: right;
    }
    
    .rtl table tr td:nth-child(2) {
        text-align: left;
    }
    </style>
</head>

<body>
    <div class="invoice-box">
        <table cellpadding="0" cellspacing="0">
            <tr class="top">
                <td colspan="2" class="border-bottom">
                    <table>
                        <tr>
                            <td class="title">
                                <img src="http://dev.aaima.org/frontassets/images/logo.png" style="width:100%; max-width:120px;">
                            </td>
                            <td style="text-align: center;">
                                <p style="font-size: 20px;">TAX INVOICE</p>
                            </td>
                            
                            <!-- <td>
                                Invoice #: 123<br>
								Order ID: AAIMA136<br/>
                                Order Date: February 16, 2019<br>
                            </td> -->
                        </tr>
                    </table>
                </td>
            </tr>
              <?php $customerInfo = getCustomer($orderInfo->customer_id);?>
            <tr class="information">
                <td colspan="2">
                    <table>
                        <tr>
                            <td width="50%" style="text-align:left;">
                                 <strong>aaima global (p) ltd.</strong>
                                    <br>
                                    1st Floor, Anand Colony,          
                                    Near Chintpurni Mata Mandir <br>        
                                    Railway Road Gurgaon - 122001           
                                    Haryana - India 
                                    <br>
            <b>GST Number: </b>06ABACS1646Q1ZI<br>
            <b>CIN Number: </b>U93090HR2018PTC073544<br>
                            </td>
                            
                            <td width="50%" style="text-align:left;"><strong>GSTIN  :   06ABACS1646Q1ZZ 
</strong><br>
						ORDER NO.: #{{$orderInfo->order_no}}<br>
                        INVOICE NO.: INV-{{$orderInfo->id}}<br>
                        INVOICE DATE : {{date('d-m-Y',strtotime($orderInfo->created_at))}}
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr class="information">
                <td colspan="2">
                    <table>
                        <tr>
                        <td width="50%" style="text-align:left;border: 1px solid #ccc;padding-bottom: 0;">
                            <strong>BILL TO  </strong>
                            <p ><?= $customerInfo->firstname . ' ' . $customerInfo->lastname; ?></p>
                            <p>{{$orderInfo->address->address_1.' '.$orderInfo->address->address_2}}<br>{{$orderInfo->address->city.' '.$orderInfo->address->state}}<br>{{$orderInfo->address->country.' '.$orderInfo->address->postcode}}</p>
                   <!--          <p style="border: 1px solid #ccc;font-size: 20px;"></p>
                            <p style="border: 1px solid #ccc;font-size: 20px;"></p>
                            <p style="border: 1px solid #ccc;font-size: 20px;"></p>
                            -->

<!--                             <p style="font-size:16px;">State __________________PIN _______________</p>  
 -->                        <!--     <br>
                            <br> -->
                            </td>

                             <td width="50%" style="text-align:left;border: 1px solid #ccc;padding-bottom: 0;">
                            <strong>Ship TO  </strong>
                          <p ><?= $customerInfo->firstname . ' ' . $customerInfo->lastname; ?></p>
                            <p>{{$orderInfo->address->address_1.' '.$orderInfo->address->address_2}}<br>{{$orderInfo->address->city.' '.$orderInfo->address->state}}<br>{{$orderInfo->address->country.' '.$orderInfo->address->postcode}}</p>
                            </td>
                            
                           
                        </tr>
                    </table>
                </td>
            </tr>
           <tr>
            
            
              <!--   <tr>
                    <td></td>
                <td style="text-align:left;"><b>DISPATCH BY  :  Courier /By Hand</b></td>
                </tr> -->
                <tr>
                    <td></td>
                <td style="text-align:left;"><b>PAYMENT:Paypal Express Checkout</b></td>
                </tr>

            </tr>
            
            <tr>
                <td colspan="2" style="padding-bottom:50px;">
                   <table class="item-description" style="border-collapse:collapse;">
      <thead>
        <tr class="heading">
             <td style="text-align:left;"><b>S No.</b></td>
          <td style="text-align:left;"><b>Product</b></td>
          <td style="text-align:center;"><b>HSN CODE
</b></td>
          <td style="text-align:center;"><b>QTY
</b></td>
          <td style="text-align:center;"><b>BASIC PRICE / UNIT
</b></td>
          <td style="text-align:center;"><b>TAXABLE VALUE
</b></td>
<td style="text-align:center;"><b>CGST
</b></td>
<td style="text-align:center;"><b>SGST
</b></td>
<td style="text-align:center;"><b>Total Tax
</b></td>
<td style="text-align:center;"><b>AMOUNT
</b></td>
        </tr>
      </thead>
      <tbody>
           @if($orderInfo->orderItems)
           <?php 

           $i=1;?>
                                                    @foreach($orderInfo->orderItems as $item)
                                                 <?php
           // echo "<pre>";
           // print_r($item);
           // echo "</pre>";
           ?>
                <tr>
                    <td style="text-align:left;"><?php echo $i++;?></td>
          <td style="text-align:left;"><?= $item->name; ?>      
</td>
          <td style="text-align:center;"><?= $item->hsn_code; ?>
</td>
  <?php  $tax= $item->tax;echo "<br>";
          $tax2=$tax/2;
          $total_amount=$item->unit_price;
          $amount_without_tax=$total_amount*($tax2/100);
          $quantity=$item->quantity;
          $total_tax=$amount_without_tax*$quantity*2;
          $total_tax=$total_amount*$quantity -$total_tax ;

          ?>
          <td style="text-align:center;">{{$item->quantity}}</td>
          <td style="text-align:center;"><i class="fa fa-inr"></i> {{$item->unit_price}}</td>
          <td style="text-align:center;"><i class="fa fa-inr"></i> <?php echo $total_tax;?></td>
        
          <td style="text-align:center;"><i class="fa fa-inr"></i> <?php echo $amount_without_tax*$quantity;?></td>
          <td style="text-align:center;"><i class="fa fa-inr"></i> <?php echo $amount_without_tax*$quantity;?></td>
          <td style="text-align:center;"><i class="fa fa-inr"></i> <?php echo $amount_without_tax*$quantity*2;?></td>
          <td style="text-align:center;"><i class="fa fa-inr"></i> {{$item->total_amount}}</td>
        </tr>
          @endforeach
                                                    @endif
    <!--       <tr>
                    <td style="text-align:left;"></td>
          <td style="text-align:left;">     
</td>
          <td style="text-align:center;">
</td>
          <td style="text-align:center;"></td>
          <td style="text-align:center;"></td>
          <td style="text-align:center;"></td>
          <td style="text-align:center;"><?php echo $tax2;?>%</td>
          <td style="text-align:center;"><?php echo $tax2;?>%</td>
          <td style="text-align:center;"></td>
          <td style="text-align:center;"></td>
        </tr> -->
        
	                    
                        <tr>

          <td colspan="9" style="text-align:right;"><b>DELIVERY CHARGES     
</b></td>
          <td style="text-align:center;"><i class="fa fa-inr"></i> 50.00</td>
        </tr>
                <tr>
          <td colspan="9" style="text-align:right;"><b>TOTAL</b></td>
          <?php 
$grand_total=$orderInfo->grand_total
          ?>
          <td style="text-align:center;"><i class="fa fa-inr"></i> <?php echo $grand_total+50;?></td>
          <input type="hidden" id="h_v" value="<?php echo $grand_total+50;?>">
        </tr>
              </tbody>
    </table>
                </td>
                
               
            </tr>
			<tr>
            <td colspan="2" style="padding:0;">
            <p style="text-align:center;"><b>INVOICE VALUE (in words):</b><span id="word"></span> Rupees Only</p>
            </td>
            </tr>
			<tr>
			<td colspan="2" style="padding:0;">
			<p style="text-align:left;"><em><b>Note : </b> All disputes are subject to Gurugram juridiction.</em></p>
			</td>
			</tr>
            <tr>
            <td colspan="2" style="padding:0;">
            <p style="text-align:right;"><em>This is a computer generated invoice. No signature required.</em></p>
            </td>
            </tr>
			
			<tr>
			<td>
			<p style="text-align:left;    margin-top: 30px;
}">
           
            <b>email:</b>  customercare@aaima.org  <br>    
            <b>customer care:</b> #0124- 424 1987  


             </p>
			</td>
			<td>
			<p style="text-align:right">
                                                             

                <br> 
            <b>Website:</b>www.aaima.org<br>    
            <b>whatsapp:</b> 8447951987
           </p>
			</td>
			</tr>
			
			<!-- <tr>
			<td colspan="2" style="border-top:2px solid #999;">
			<p style="text-align:center">Enjoy your shopping! Visit the My products page in case you any questions?<br/>
Get in touch with our 24x7 Customer Care Team</p>
			</td>
			</tr> -->
			
           
           
        </table>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <script>


    function convertNumberToWords(amount) {
    var words = new Array();
    words[0] = '';
    words[1] = 'One';
    words[2] = 'Two';
    words[3] = 'Three';
    words[4] = 'Four';
    words[5] = 'Five';
    words[6] = 'Six';
    words[7] = 'Seven';
    words[8] = 'Eight';
    words[9] = 'Nine';
    words[10] = 'Ten';
    words[11] = 'Eleven';
    words[12] = 'Twelve';
    words[13] = 'Thirteen';
    words[14] = 'Fourteen';
    words[15] = 'Fifteen';
    words[16] = 'Sixteen';
    words[17] = 'Seventeen';
    words[18] = 'Eighteen';
    words[19] = 'Nineteen';
    words[20] = 'Twenty';
    words[30] = 'Thirty';
    words[40] = 'Forty';
    words[50] = 'Fifty';
    words[60] = 'Sixty';
    words[70] = 'Seventy';
    words[80] = 'Eighty';
    words[90] = 'Ninety';
    amount = amount.toString();
    var atemp = amount.split(".");
    var number = atemp[0].split(",").join("");
    var n_length = number.length;
    var words_string = "";
    if (n_length <= 9) {
        var n_array = new Array(0, 0, 0, 0, 0, 0, 0, 0, 0);
        var received_n_array = new Array();
        for (var i = 0; i < n_length; i++) {
            received_n_array[i] = number.substr(i, 1);
        }
        for (var i = 9 - n_length, j = 0; i < 9; i++, j++) {
            n_array[i] = received_n_array[j];
        }
        for (var i = 0, j = 1; i < 9; i++, j++) {
            if (i == 0 || i == 2 || i == 4 || i == 7) {
                if (n_array[i] == 1) {
                    n_array[j] = 10 + parseInt(n_array[j]);
                    n_array[i] = 0;
                }
            }
        }
        value = "";
        for (var i = 0; i < 9; i++) {
            if (i == 0 || i == 2 || i == 4 || i == 7) {
                value = n_array[i] * 10;
            } else {
                value = n_array[i];
            }
            if (value != 0) {
                words_string += words[value] + " ";
            }
            if ((i == 1 && value != 0) || (i == 0 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Crores ";
            }
            if ((i == 3 && value != 0) || (i == 2 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Lakhs ";
            }
            if ((i == 5 && value != 0) || (i == 4 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Thousand ";
            }
            if (i == 6 && value != 0 && (n_array[i + 1] != 0 && n_array[i + 2] != 0)) {
                words_string += "Hundred and ";
            } else if (i == 6 && value != 0) {
                words_string += "Hundred ";
            }
        }
        words_string = words_string.split("  ").join(" ");
    }
    return words_string;
}
window.onload = function() {
    var hv = $('#h_v').val();
word.innerHTML=convertNumberToWords(hv);
};

    </script>
    </script>
</body>
</html>