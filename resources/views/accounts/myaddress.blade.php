@extends('layouts.account')
@section('content')
<div class="container mtb_30">
    <div class="row">
        <div class="col-sm-4 col-md-3">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="panel panel-default account-sidebar">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <?php
                                    $session = Session::get('user_data');
                                    if (!empty($session)):
                                        $customerInfo = getCustomer($session['customer_id']);
                                        if ($customerInfo):
                                            $fileInfo = getFile($customerInfo->photo);
                                            if ($fileInfo) {
                                                $photo = $fileInfo->file_path . $fileInfo->file_name;
                                            } else {
                                                $photo = url('/') . '/frontassets/images/no_img.png';
                                            }
                                        endif;
                                    endif;
                                    ?>
                                    <div class="pull-left"><p><img src="<?= $photo; ?>" alt="user" /></p></div>
                                    <div class="pull-left"><p>Welcome<br/> <strong><?= $customerInfo->firstname . ' ' . $customerInfo->lastname; ?></strong></p>
                                        <p>ID: <strong><?= 'AAIMA' . $customerInfo->id; ?></strong></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body account-sidebar-menu">
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                           <ul class="list-group">
                                        <li class="list-group-item"><a href="/myaccount"><i class="fa fa-user"></i> My Profile <span class="badge"><i class="fa fa-angle-right"></i></span></a></li>
                                        <li class="list-group-item"><a href="/myaddress" class="active"><i class="fa fa-map-marker"></i> Manage Addresses <span class="badge"><i class="fa fa-angle-right"></i></span></a></li>
                                     <!--    <li class="list-group-item"><a href="/myreward"><i class="fa fa-trophy"></i> My Rewards <span class="badge"><i class="fa fa-angle-right"></i></span></a></li> -->
                                        <li class="list-group-item"><a href="/mybusinessassociate"><i class="fa fa-sitemap"></i> My Business Associate <span class="badge"><i class="fa fa-angle-right"></i></span></a></li>
                                        <li class="list-group-item"><a href="/mywishlist"><i class="fa fa-heart"></i> My Wishlist <span class="badge"><i class="fa fa-angle-right"></i></span></a></li>
                                        <li class="list-group-item"><a href="/myorder" ><i class="fa fa-list"></i> My Orders <span class="badge"><i class="fa fa-angle-right"></i></span></a></li>
                                    <!--     <li class="list-group-item"><a href="/myreturn"><i class="fa fa-retweet"></i> My Returns <span class="badge"><i class="fa fa-angle-right"></i></span></a></li> -->
                                        <li class="list-group-item"><a href="/logoutuser" onclick="return confirm('Are you sure, you want to logout ?')" class="logout"><i class="fa fa-power-off"></i> Logout</a></li>
                                    </ul> 
                           
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="col-sm-8 col-md-9">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="panel panel-default account-content">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <h3>Manage Addresses</h3>
                                </div>
                            </div>
                        </div>
                        @include('partials.errors')
                        @include('partials.success')
                        <div class="panel-body manage-address-content">
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <div class="border-content" onclick="addAddress(this)" style="cursor: pointer">
                                        <h3><i class="fa fa-plus"></i> Add Address</h3>
                                    </div>
                                </div>
                            </div>
                            @if($addressInfo)
                            @foreach($addressInfo as $key=>$address)
                            <div class="row address<?= $key; ?>">
                                <div class="col-sm-12 col-md-12">
                                    <div class="border-content row">
                                        <p class="pull-left"><i class="fa fa-home"></i> {{$address->address_1.' '.$address->address_2.' '.$address->city}} </p>
                                        <div class="pull-right dropdown address-edit-tool">
                                            <p class="dropdown-toggle" data-toggle="dropdown">
                                                <i class="fa fa-ellipsis-h"></i></p>
                                            <ul class="dropdown-menu">
                                                <li><a href="javascript:void(0)" onclick="editAddress(this)" address_id="{{$address->id}}"><i class="fa fa-edit"></i> Edit </a></li>
                                                <li><a href="javascript:void(0)" onclick="deleteAddress(this,<?= $key; ?>)" address_id="{{$address->id}}"><i class="fa fa-trash"></i> Delete</a></li>
                                                <li><a href="javascript:void(0)" onclick="makeDefault(this)" address_id="{{$address->id}}"><i class="fa fa-home"></i> Make this default</a></li>
                                            </ul>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                            <form method="post" id="defaultForm" action="set_default_address/{{$address->id}}">
                                {{csrf_field()}}
                            </form>
                            @endforeach
                            @endif
                            <div class="row" id="new-address-form">
                                <div class="col-sm-12 col-md-6">
                                    <form action="/add_address" id="addAddressForm" method="post" enctype="multipart/form-data" class="form-horizontal">
                                        {{csrf_field()}}
                                        <div class="row">
                                            <div class="form-group required col-sm-12">
                                                <label class="col-sm-4 control-label" for="input-address-1">Address 1</label>
                                                <div class="col-sm-8">
                                                    <input type="text" name="address_1" value="" placeholder="Address" class="form-control" required maxlength="128">
                                                </div>
                                            </div>
                                            <div class="form-group required col-sm-12">
                                                <label class="col-sm-4 control-label" for="input-address-1">Address 2</label>
                                                <div class="col-sm-8">
                                                    <input type="text" name="address_2" value="" placeholder="Address" class="form-control" maxlength="128">
                                                </div>
                                            </div>
                                            <div class="form-group required col-sm-12">
                                                <label class="col-sm-4 control-label" for="input-city">City</label>
                                                <div class="col-sm-8">
                                                    <input type="text" name="city" value="" placeholder="City" class="form-control" required maxlength="128">
                                                </div>
                                            </div>
                                            <div class="form-group required col-sm-12">
                                                <label class="col-sm-4 control-label" for="input-zone">Region / State</label>
                                                <div class="col-sm-8">
                                                    <select name="zone_id" required class="form-control">
                                                        <option disabled selected>Select State</option>
                                                        @if($states)
                                                        @foreach($states as $state)
                                                        <option value="{{$state->state_id}}">{{$state->state_name}}</option>
                                                        @endforeach
                                                        @endif   
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group col-sm-12">
                                                <label class="col-sm-4 control-label" for="input-country">Country</label>
                                                <div class="col-sm-8">
                                                    <select name="country_id" class="form-control" required>
                                                        <option disabled selected>Select Country</option>
                                                        @if($countries)
                                                        @foreach($countries as $country)
                                                        <option value="{{$country->country_id}}">{{$country->country_name}}</option>
                                                        @endforeach
                                                        @endif
                                                    </select> 
                                                </div>
                                            </div>
                                            <div class="form-group required col-sm-12">
                                                <label class="col-sm-4 control-label" for="input-postcode" style="">Post Code</label>
                                                <div class="col-sm-8">
                                                    <input type="text" name="postcode" placeholder="Post Code" class="form-control" maxlength="10" required>
                                                </div>
                                            </div>
                                            <div class="buttons col-sm-6">
                                                <div class="pull-right"><input type="button" id="canceladdressbutton" value="Cancel" class="btn btn-default">
                                                </div>
                                            </div>
                                            <div class="buttons col-sm-6">
                                                <div class="pull-right"><input type="submit" value="Add" class="btn btn-blue">
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="row" id="update-address-form" style="display: none">
                                <div class="col-sm-12 col-md-6">
                                    <form id="editAddressForm" method="post" enctype="multipart/form-data" class="form-horizontal">
                                        {{csrf_field()}}
                                        <div class="row">
                                            <div class="form-group required col-sm-12">
                                                <label class="col-sm-4 control-label" for="input-address-1">Address 1</label>
                                                <div class="col-sm-8">
                                                    <input type="text" name="address_1" placeholder="Address 1" id="address1" class="form-control" required maxlength="128">
                                                </div>
                                            </div>
                                            <div class="form-group required col-sm-12">
                                                <label class="col-sm-4 control-label" for="input-address-1">Address 2</label>
                                                <div class="col-sm-8">
                                                    <input type="text" name="address_2" placeholder="Address 2" id="address2" class="form-control" maxlength="128">
                                                </div>
                                            </div>
                                            <div class="form-group required col-sm-12">
                                                <label class="col-sm-4 control-label" for="input-city">City</label>
                                                <div class="col-sm-8">
                                                    <input type="text" name="city" placeholder="City" id="city" class="form-control" required maxlength="128">
                                                </div>
                                            </div>
                                            <div class="form-group required col-sm-12">
                                                <label class="col-sm-4 control-label" for="input-zone">Region / State</label>
                                                <div class="col-sm-8">
                                                    <select name="zone_id" id="zone_id" class="form-control" required>
                                                        <option disabled selected>Select State</option>
                                                        @if($states)
                                                        @foreach($states as $state)
                                                        <option value="{{$state->state_id}}">{{$state->state_name}}</option>
                                                        @endforeach
                                                        @endif   
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group col-sm-12">
                                                <label class="col-sm-4 control-label" for="input-country">Country</label>
                                                <div class="col-sm-8">
                                                    <select name="country_id" id="country_id" class="form-control" required>
                                                        <option disabled selected>Select Country</option>
                                                        @if($countries)
                                                        @foreach($countries as $country)
                                                        <option value="{{$country->country_id}}">{{$country->country_name}}</option>
                                                        @endforeach
                                                        @endif
                                                    </select> 
                                                </div>
                                            </div>
                                            <div class="form-group required col-sm-12">
                                                <label class="col-sm-4 control-label" for="input-postcode" style="">Post Code</label>
                                                <div class="col-sm-8">
                                                    <input type="text" name="postcode" placeholder="Post Code" id="postcode" class="form-control" maxlength="10">
                                                </div>
                                            </div>
                                            <div class="buttons col-sm-6">
                                                <div class="pull-right"><input type="button" id="canceladdressbutton" value="Cancel" class="btn btn-default">
                                                </div>
                                            </div>
                                            <div class="buttons col-sm-6">
                                                <div class="pull-right"><input type="submit" value="Update" class="btn btn-blue">
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function makeDefault(obj) {
        $("#defaultForm").submit();
    }

    function addAddress(obj) {
        $("#new-address-form").toggle();
    }
    function editAddress(obj) {
        var address_id = $(obj).attr('address_id');
        $.ajax({
            url: '/getaddress',
            type: 'post',
            dataType: 'json',
            data: {address_id: address_id},
            cache: false
        }).done(function (response) {
            if (response.error_code == 200) {
                $("#update-address-form").show();

                $("#address1").val(response.result.address_1);
                $("#address2").val(response.result.address_2);
                $("#city").val(response.result.city);
                $("#zone_id").val(response.result.zone_id);
                $("#country_id").val(response.result.country_id);
                $("#postcode").val(response.result.postcode);

                $("#editAddressForm").attr("action", "update_address/" + response.result.id)
            } else {
                alert("Some error occured, please try again");
            }
        });
    }

    function deleteAddress(obj, sno) {
        var address_id = $(obj).attr('address_id');
        if (confirm("Are you sure, you want to delete ?")) {
            $.ajax({
                url: '/deleteaddress',
                type: 'post',
                dataType: 'json',
                data: {address_id: address_id},
                cache: false
            }).done(function (response) {
                if (response.error_code == 200) {
                    $('.address' + sno).remove();

                    Command: toastr["success"](response.message)
                    toastr.options = {
                        "closeButton": false,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": false,
                        "positionClass": "toast-top-right",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    }
                } else {
                    Command: toastr["error"](response.message)
                    toastr.options = {
                        "closeButton": false,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": false,
                        "positionClass": "toast-top-right",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    }
                }
            });
        }
    }
</script>
@endsection