<?php
if(!empty($category)){
  $id = $category[0]->id;
  $parent = $category[0]->parent;
  $name = $category[0]->name;
  $description = $category[0]->description;
  $status = $category[0]->status;
  $form_type = 'update';
}else{
  $name = '';
  $id = '';
$parent = '';
$description  = '';
$status = '1';
$form_type = 'add';
}
?>
<meta name="csrf-token" content="{{ csrf_token() }}">
@extends('layouts.admin')
@section('content')

<script src="https://cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>
<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Add New Category</h3>
                </div>
                @include('partials.errors')
                @include('partials.success')
                <div class="panel-body">
                    <form method="post" id="category_form"  class="form-horizontal" enctype="multipart/form-data">
                        {{csrf_field()}}
                       <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Parent Category</label>
                            <div class="col-md-6 col-xs-12">
                                <select class="form-control select" name="parent" data-live-search="true">
                                    <option value="0">Parent</option>
                                    @if($parents)
                                    @foreach($parents as $key=>$category)
                                    <option value="{{$category->id}}" <?php if($parent==$category->id){echo 'selected';} ?> >{{$category->name}}</option>
                                    @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <input type="hidden" name="form_type" value="{{$form_type}}">
                        <input type="hidden" name="id" value="{{$id}}">
                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Category Name</label>
                            <div class="col-md-6 col-xs-12">
                                <input type="text" name="name" class="form-control" value="{{$name}}" placeholder="Category Name" required/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Description</label>
                            <div class="col-md-6 col-xs-12">
                                <textarea class="form-control" name="description" placeholder="Description" rows="4">{{$description}}</textarea>
                            </div>
                        </div>
                         <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Status</label>
                            <div class="col-md-6 col-xs-12">                                                                                
                               <select class="form-control" name="status">
                                   <option value="1" >Active</option>
                                   <option value="0"<?php if($status==0){echo 'selected';} ?>>Inactive</option>
                               </select>
                            </div>
                        </div>
  
                       
                        <div class="form-group">
                            <div class="col-md-12 col-xs-12">  
                                <p class="text-center">
                                    <input type="submit" class="btn btn-info" id="submit" value="Submit Page"/> 
                                </p>
                            </div>
                        
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
<script>
    // CKEDITOR.replace('description');
     // var desc = CKEDITOR.instances.description.getData();
    $("#category_form").validate({
    rules:{
        name:"required"
    },
submitHandler: function(form) {
        $.ajax({
    type: "POST",
    url: "/admin/advert/cat_store",
    data: $("#category_form").serialize(),
    beforeSend:function() {
    },
     success: function(msg)
        {
        if(msg == 1){
Command: toastr["success"]("Category has been added Successfully. Redirecting you to Category lists!")

toastr.options = {
  "closeButton": false,
  "debug": false,
  "newestOnTop": false,
  "progressBar": false,
  "positionClass": "toast-top-right",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}
                 window.setTimeout(function() {
                    window.location.replace("/admin/advert/category");
                 }, 3000);
}else if(msg=='updated'){
                Command: toastr["success"]("Category has been updated Successfully. Redirecting you to Category lists!")
                toastr.options = {
                  "closeButton": false,
                  "debug": false,
                  "newestOnTop": false,
                  "progressBar": false,
                  "positionClass": "toast-top-right",
                  "preventDuplicates": false,
                  "onclick": null,
                  "showDuration": "300",
                  "hideDuration": "1000",
                  "timeOut": "5000",
                  "extendedTimeOut": "1000",
                  "showEasing": "swing",
                  "hideEasing": "linear",
                  "showMethod": "fadeIn",
                  "hideMethod": "fadeOut"
                }
                 window.setTimeout(function() {
                    window.location.replace("/admin/advert/category");
                 }, 3000);
            }else{
              alert('You made no changes!')
            }
        }
    });
        return false;
    },

});
   
</script>
@endsection