<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProductReview extends Model {

    protected $table = 'product_reviews';
    protected $fillable = array(
        'customer_id',
        'product_id',
        'name',
        'review',
        'rating',
        'status',
        'created_at',
        'updated_at',
    );

}
