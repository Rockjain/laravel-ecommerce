<meta name="csrf-token" content="{{ csrf_token() }}">
@extends('layouts.admin')
@section('content')
<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Category Attribute Mapping</h3>
                </div>
                @include('partials.errors')
                @include('partials.success')
                <div class="panel-body">
                    <form method="post" id="catAttrForm" class="form-horizontal" action="/admin/add_cat_attributes" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Category</label>
                            <div class="col-md-6 col-xs-12">   
                                <select class="form-control" name="category_id" onclick="fetchCatAttributes(this)">
                                    <option disabled selected>Select Category</option>
                                    @if($categories)
                                    @foreach($categories as $key=>$category)
                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                    @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Attributes</label>
                            <div class="col-md-6 col-xs-12">                                                                                
                                <select class="form-control multiselect attribute_id" onchange="appendValueDiv(this)" multiple name="attributes[]" required />

                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12 col-xs-12" style="margin-top: 10px;">  
                                <p class="text-center">
                                    <input type="submit" class="btn btn-info" value="Submit"/> 
                                </p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">                                
                    <h3 class="panel-title">All Category Attribute Mappings</h3>                         
                </div>
                <div class="panel-body">
                    <table class="table datatable">
                        <thead>
                            <tr>
                                <th>Category</th>
                                <th>Attributes</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($categoryattributes)
                            @foreach($categoryattributes as $catattr)
                            <tr>
                                <td>{{$catattr->categoryDetail->name}}</td>
                                <td>
                                    @if($catattr->attributes)                                    
                                    <ul>
                                        @foreach($catattr->attributes as $key=>$value)
                                        <li>{{$value->attributeDetail->name}}
                                            <label class="switch" style="top: 12px">
                                                <input type="checkbox" <?php
                                                if ($value->status == '1') {
                                                    echo "checked";
                                                }
                                                ?> id="{{$value->id}}" onchange="checkStatus(this);" id="switch-{{$key}}}">
                                                <span class="slider round"></span>
                                            </label>
                                            <a href="javascript:void(0)" id="{{$value->id}}" onclick="removeMappedAttr(this)"><i class="fa fa-trash-o"></i></a>
                                        </li>
                                        @endforeach
                                    </ul>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function fetchCatAttributes(objRef) {
        var $obj = $(objRef);
        var category_id = $obj.find('option:selected').val();
        $.ajax({
            url: '/admin/get_cat_attributes',
            data: {category_id: category_id},
            type: 'POST',
            dataType: 'json',
            cache: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
        }).done(function (response) {
            var string = "";
            if (response.status == '1') {
                $.each(response.attributes, function (i, v) {
                    string += "<option value='" + v.id + "'>" + v.attribute_name + "</option>";
                });
                $(".attribute_id").html(string);

            } else {
                string += "<option value=''>No Attribute Found</option>";
                $(".attribute_id").html(string);
            }
        });
    }

    function checkStatus(obj) {
        var id = $(obj).attr("id");
        var checked = $(obj).is(':checked');
        if (checked == true) {
            var status = 1;
        } else {
            var status = 0;
        }
        $.ajax({
            url: '/admin/change_status',
            type: 'post',
            dataType: 'json',
            data: {method: 'changeCatAttrStatus', status: status, id: id},
            cache: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
        }).done(function (response) {
            if (response.status == '1') {
                Command: toastr["success"]("Status updated successfully")

                toastr.options = {
                    "closeButton": false,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": false,
                    "positionClass": "toast-top-right",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                }
            } else {
                Command: toastr["error"]("Some error found, please try again")

                toastr.options = {
                    "closeButton": false,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": false,
                    "positionClass": "toast-top-right",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                }
            }
        });
    }

    function removeMappedAttr(obj) {
        var id = $(obj).attr("id");
        if (confirm("Are you sure you want to remove this ?")) {
            $.ajax({
                url: '/admin/delete_cat_attr',
                data: {id: id},
                type: 'POST',
                dataType: 'json',
                cache: false,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            }).done(function (response) {
                if (response.error_code == 200) {
                    $(obj).parent('li').remove();

                    Command: toastr["success"](response.message)

                    toastr.options = {
                        "closeButton": false,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": false,
                        "positionClass": "toast-top-right",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    }
                } else {
                    Command: toastr["error"](response.message)

                    toastr.options = {
                        "closeButton": false,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": false,
                        "positionClass": "toast-top-right",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    }
                }
            });
        }

    }
</script>
@endsection