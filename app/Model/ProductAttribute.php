<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProductAttribute extends Model {

    protected $table = 'product_attributes';
    protected $fillable = array(
        'product_id',
        'attribute_id',
        'attribute_value',
        'type',
        'created_at',
        'updated_at'
    );

    public function productDetail() {
        return $this->belongsTo('App\Model\Product', 'product_id');
    }

    public function attributeDetail() {
        return $this->belongsTo('App\Model\Attribute', 'attribute_id');
    }

    public function attributeValues() {
        return $this->hasMany('App\Model\Attributevalue', 'attribute_id');
    }

    public function attributeValue() {
        return $this->belongsTo('App\Model\Attributevalue', 'attribute_value');
    }

}
