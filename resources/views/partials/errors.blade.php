@if (session()->has('error'))
<div class="alert alert-danger alert-dismissible">
    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
    <strong>{!! session()->get('error') !!}</strong>.
</div>
@endif