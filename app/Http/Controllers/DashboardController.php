<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Customer;
use App\Model\Order;
use App\Model\Product;

class DashboardController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $total_customer = Customer::count();
        $total_order = Order::count();
        $total_product = Product::count();
//        echo $total_customer;die;
        return view('home', ['total_customer' => $total_customer, 'total_order' => $total_order, 'total_product' => $total_product]);
    }

}
