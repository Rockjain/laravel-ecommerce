

@extends('layouts.admin')
@section('content')

<!--       <div class="page-content">
 -->                
  <!--               <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                    <li class="xn-icon-button">
                        <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
                    </li>
          
                     <li class="xn-icon-button pull-right">
                        <a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span></a>                        
                    </li> 
                </ul> -->
                          
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                
                 <div class="row">
                        <div class="col-md-12">

                           <div class="panel panel-default">
                            
                             <div class="panel-heading">
                             <h3 class="panel-title">Orders</h3>
                              <ul class="panel-controls menu-buttons">
                                              <!-- <li><a href="invoice.php" class="text-info" target="_blank" data-toggle="tooltip" title="Print Shipping List" ><span class="fa fa-truck"></span></a></li> -->
                                                <li><a href="../invoice/{{$orderInfo->id}}" class="text-info" target="_blank" class="text-info" data-toggle="tooltip" title="Print Invoice"><span class="fa fa-print"></span></a></li>
                                               <!-- <li><a href="#" class="text-info" data-toggle="tooltip" title="Edit"><span class="fa fa-edit"></span></a></li>
                                                <li><a href="#" class="text-danger" data-toggle="tooltip" title="Cancel"><span class="fa fa-reply"></span></a></li> -->
                                            </ul>  
                             </div>
                            
                                    <div class="panel-body">
                                        <div class="row">
      <div class="col-md-4">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-shopping-cart"></i> Order Details</h3>
          </div>
          <table class="table">
            <tbody>
              <tr>
                <td style="width: 1%;"><button data-toggle="tooltip" title="" class="btn btn-info btn-xs" data-original-title="Store"><i class="fa fa-shopping-cart fa-fw"></i></button></td>
                <td><a href="#" target="_blank">AAIMA</a></td>
              </tr>
              <tr>
                <td><button data-toggle="tooltip" title="" class="btn btn-info btn-xs" data-original-title="Date Added"><i class="fa fa-calendar fa-fw"></i></button></td>
                <td>{{date('d-m-Y',strtotime($orderInfo->created_at))}}</td>
              </tr>
              <tr>
                <td><button data-toggle="tooltip" title="" class="btn btn-info btn-xs" data-original-title="Payment Method"><i class="fa fa-credit-card fa-fw"></i></button></td>
                <td>PayPal Express Checkout</td>
              </tr>
                            <tr>
                <td><button data-toggle="tooltip" title="" class="btn btn-info btn-xs" data-original-title="Shipping Method"><i class="fa fa-truck fa-fw"></i></button></td>
                <td>Flat Shipping Rate</td>
              </tr>
                          </tbody>
          </table>
        </div>
      </div>
      <?php $customerInfo = getCustomer($orderInfo->customer_id);?>
      <div class="col-md-4">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-user"></i> Customer Details</h3>
          </div>
          <table class="table">
            <tbody><tr>
              <td style="width: 1%;"><button data-toggle="tooltip" title="" class="btn btn-info btn-xs" data-original-title="Customer"><i class="fa fa-user fa-fw"></i></button></td>
              <td> <a href="/admin/" target="_blank"><?= $customerInfo->firstname . ' ' . $customerInfo->lastname; ?></a>
                </td>
            </tr>
            <tr>
              <td><button data-toggle="tooltip" title="" class="btn btn-info btn-xs" data-original-title="Sponser Id"><i class="fa fa-group fa-fw"></i></button></td>
              <td><?= $customerInfo->sponser_id; ?></td>
            </tr>
            <tr>
              <td><button data-toggle="tooltip" title="" class="btn btn-info btn-xs" data-original-title="E-Mail"><i class="fa fa-envelope-o fa-fw"></i></button></td>
              <td><a href="mailto:<?= $customerInfo->email; ?>"><?= $customerInfo->email; ?></a></td>
            </tr>
            <tr>
              <td><button data-toggle="tooltip" title="" class="btn btn-info btn-xs" data-original-title="Telephone"><i class="fa fa-phone fa-fw"></i></button></td>
              <td><?= $customerInfo->telephone; ?></td>
            </tr>
          </tbody></table>
        </div>
      </div>
      <div class="col-md-4">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-cog"></i> Options</h3>
          </div>
          <table class="table">
            <tbody>
              <tr>
                <td>Invoice</td>
                <td id="invoice" class="text-right">INV-{{$orderInfo->id}}</td>
                <td style="width: 1%;" class="text-center">                
                <button disabled="disabled" class="btn btn-success btn-xs"><i class="fa fa-refresh"></i></button>
                  </td>
              </tr>
              <tr>
                <td>Reward Points</td>
                <td class="text-right">15</td>
                <td class="text-center">                                 
                <button id="button-reward-add" data-loading-text="Loading..." data-toggle="tooltip" title="" class="btn btn-success btn-xs" data-original-title="Add Reward Points"><i class="fa fa-plus-circle"></i></button>
                </td>
              </tr>
              <tr>
                <td>Affiliate                  </td>
                <td class="text-right">Rs.0.00</td>
                <td class="text-center">              
                <button disabled="disabled" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i></button>
                  </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
        </div>
        
        
                                </div>
                                
                                <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-info-circle"></i> Order (#{{$orderInfo->order_no}})</h3>
      </div>
      <div class="panel-body">
        <table class="table table-bordered">
          <thead>
            <tr>
              <td style="width: 50%;" class="text-left">Payment Address</td>
                            <td style="width: 50%;" class="text-left">Shipping Address</td>
                          </tr>
          </thead>
          <tbody>
            <tr>
                 <td class="text-left">{{$orderInfo->address->firstname.' '.$orderInfo->address->lastname}}<br>{{$orderInfo->address->address_1.' '.$orderInfo->address->address_2}}<br>{{$orderInfo->address->city.' '.$orderInfo->address->state}}<br>{{$orderInfo->address->country.' '.$orderInfo->address->postcode}}</td>
               <td class="text-left">{{$orderInfo->address->firstname.' '.$orderInfo->address->lastname}}<br>{{$orderInfo->address->address_1.' '.$orderInfo->address->address_2}}<br>{{$orderInfo->address->city.' '.$orderInfo->address->state}}<br>{{$orderInfo->address->country.' '.$orderInfo->address->postcode}}</td>
                          </tr>
          </tbody>
        </table>
        <table class="table table-bordered">
          <thead>
         <tr class="heading">
             <td style="text-align:left;"><b>S No.</b></td>
          <td style="text-align:left;"><b>Product</b></td>
          <td style="text-align:center;"><b>HSN CODE
</b></td>
          <td style="text-align:center;"><b>QTY
</b></td>
          <td style="text-align:center;"><b>BASIC PRICE / UNIT
</b></td>
          <td style="text-align:center;"><b>TAXABLE VALUE
</b></td>
<td style="text-align:center;"><b>CGST
</b></td>
<td style="text-align:center;"><b>SGST
</b></td>
<td style="text-align:center;"><b>Total Tax
</b></td>
<td style="text-align:center;"><b>AMOUNT
</b></td>
        </tr>
          </thead>
          <tbody>
                       @if($orderInfo->orderItems)
           <?php 

           $i=1;?>
                                                    @foreach($orderInfo->orderItems as $item)
                                                 <?php
           // echo "<pre>";
           // print_r($item);
           // echo "</pre>";
           ?>
                <tr>
                    <td style="text-align:left;"><?php echo $i++;?></td>
          <td style="text-align:left;"><?= $item->name; ?>      
</td>
          <td style="text-align:center;"><?= $item->hsn_code; ?>
</td>
  <?php  $tax= $item->tax;echo "<br>";
          $tax2=$tax/2;
          $total_amount=$item->unit_price;
          $amount_without_tax=$total_amount*($tax2/100);
          $quantity=$item->quantity;
          $total_tax=$amount_without_tax*$quantity*2;
          $total_tax=$total_amount*$quantity -$total_tax ;

          ?>
          <td style="text-align:center;">{{$item->quantity}}</td>
          <td style="text-align:center;"><i class="fa fa-inr"></i> {{$item->unit_price}}</td>
          <td style="text-align:center;"><i class="fa fa-inr"></i> <?php echo $total_tax;?></td>
        
          <td style="text-align:center;"><i class="fa fa-inr"></i> <?php echo $amount_without_tax*$quantity;?></td>
          <td style="text-align:center;"><i class="fa fa-inr"></i> <?php echo $amount_without_tax*$quantity;?></td>
          <td style="text-align:center;"><i class="fa fa-inr"></i> <?php echo $amount_without_tax*$quantity*2;?></td>
          <td style="text-align:center;"><i class="fa fa-inr"></i> {{$item->total_amount}}</td>
        </tr>
          @endforeach
                                                    @endif
                                    <tr>
              <td colspan="9" class="text-right">Delievery Charges</td>
              <td class="text-right"><i class="fa fa-inr"></i> 50 </td>
            </tr>
                        <tr>
              <td colspan="9" class="text-right">Total</td>
                 <?php 
$grand_total=$orderInfo->grand_total
          ?>
              <td class="text-right"><i class="fa fa-inr"></i> <?php echo $grand_total+50;?></td>
            </tr>
                      </tbody>
        </table>
              </div>
    </div>
    
    
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-comment-o"></i> Order History</h3>
      </div>
      <div class="panel-body tabs nopadding">
      
        <ul class="nav nav-tabs">
          <li class="active"><a href="#tab-history" data-toggle="tab">History</a></li>
<!--           <li><a href="#tab-additional" data-toggle="tab">Additional</a></li>
 -->                  </ul>
        <div class="tab-content">
          <div class="tab-pane active" id="tab-history">
            <div id="history"><div class="table-responsive">
              <table class="table table-bordered">
    <thead>
      <tr>
        <td class="text-left">Date Added</td>
        <td class="text-left">Comment</td>
        <td class="text-left">Status</td>
<!--         <td class="text-left">Customer Notified</td>
 -->      </tr>
    </thead>
    <tbody>
                  <tr>
        <td class="text-left">{{date('d-m-Y',strtotime($orderInfo->created_at))}}</td>
        <td class="text-left"></td>
        <td class="text-left"><?php 
        $status=$orderInfo->status;
if($status==1){
    echo "Request";
}
elseif($status==2){
    echo "Process";
}elseif($status==3){
    echo "Dispatched";
}
elseif($status==4){
    echo "Delivered";
}
elseif($status==5){
    echo "Cancel";
}
        ?>
            
        </td>
<!--         <td class="text-left">No</td>
 -->      </tr>
         @if($orderInfo->history)
         @foreach($orderInfo->history as $history)

      <tr>
        <td class="text-left">{{date('d-m-Y',strtotime($history->created_at))}}</td>
        <td class="text-left">{{$history->comment}}</td>
        <td class="text-left"><?php 
        $status=$history->status;
if($status==1){
    echo "Request";
}
elseif($status==2){
    echo "Process";
}elseif($status==3){
    echo "Dispatched";
}
elseif($status==4){
    echo "Delivered";
}
elseif($status==5){
    echo "Cancel";
}
        ?>
            
        </td>
      </tr>
      @endforeach;
      @endif;
                </tbody>
  </table>
</div>
<div class="row">
  <div class="col-sm-6 text-left"></div>
  <div class="col-sm-6 text-right">Showing 1 to 1 of 1 (1 Pages)</div>
</div>
</div>
            <br>
            <fieldset>
              <legend>Add Order History</legend>
              <form class="form-horizontal" method="post" action="/admin/orders/order_history">
                {{csrf_field()}}
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="input-order-status">Order Status</label>
                  <div class="col-sm-10">
                    <input type="hidden" name="order_id" value="{{$orderInfo->id}}">
                    <select name="status" id="input-order-status" class="form-control">
                        <option value="1">Request</option>
                        <option value="2">Process</option>
                        <option value="3">Dispatch</option>
                        <option value="4">Delivered</option>
                        <option value="5">Cancel</option>
                  </select>
                  </div>
                </div>
              <!--   <div class="form-group">
                  <label class="col-sm-2 control-label" for="input-override"><span data-toggle="tooltip" title="" data-original-title="If the customers order is being blocked from changing the order status due to an anti-fraud extension enable override.">Override</span></label>
                  <div class="col-sm-10">
                    <input type="checkbox" name="override" value="1" id="input-override">
                  </div>
                </div> -->
             <!--    <div class="form-group">
                  <label class="col-sm-2 control-label" for="input-notify">Notify Customer</label>
                  <div class="col-sm-10">
                    <input type="checkbox" name="notify" value="1" id="input-notify">
                  </div>
                </div> -->
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="input-comment">Comment</label>
                  <div class="col-sm-10">
                    <textarea name="comment" rows="8" id="input-comment" class="form-control"></textarea>
                  </div>
                </div>
                   <div class="text-right">
              <button id="button-history" type="submit" data-loading-text="Loading..." class="btn btn-primary"><i class="fa fa-plus-circle"></i> Add History</button>
            </div>
              </form>
            </fieldset>
         
          </div>
          <div class="tab-pane" id="tab-additional">
                        <div class="table-responsive">
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <td colspan="2">Account Custom Fields</td>
                  </tr>
                </thead>
                <tbody>
                                    <tr>
                    <td>Bank Account Number</td>
                    <td>34167973568</td>
                  </tr>
                                    <tr>
                    <td>IFSC Code</td>
                    <td>SBIN0000645</td>
                  </tr>
                                    <tr>
                    <td>Pan Card</td>
                    <td></td>
                  </tr>
                                    <tr>
                    <td>Nominee Name</td>
                    <td></td>
                  </tr>
                                    <tr>
                    <td>Relation of Nominee</td>
                    <td></td>
                  </tr>
                                    <tr>
                    <td>Aadhar Card of Nominee</td>
                    <td></td>
                  </tr>
                                  </tbody>
              </table>
            </div>
                                    <div class="table-responsive">
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <td colspan="2">Payment Address Custom Fields</td>
                  </tr>
                </thead>
                <tbody>
                                    <tr>
                    <td>Aadhar Card(back scanned)</td>
                    <td></td>
                  </tr>
                                    <tr>
                    <td>Bank Account Copy(scanned)</td>
                    <td></td>
                  </tr>
                                  </tbody>
              </table>
            </div>
                                    <div class="table-responsive">
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <td colspan="2">Shipping Address Custom Fields</td>
                  </tr>
                </thead>
                <tbody>
                                    <tr>
                    <td>Aadhar Card(back scanned)</td>
                    <td></td>
                  </tr>
                                    <tr>
                    <td>Bank Account Copy(scanned)</td>
                    <td></td>
                  </tr>
                                  </tbody>
              </table>
            </div>
                        <div class="table-responsive">
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <td colspan="2">Browser</td>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>IP Address</td>
                    <td>42.111.9.210</td>
                  </tr>
                                    <tr>
                    <td>User Agent</td>
                    <td>Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36</td>
                  </tr>
                  <tr>
                    <td>Accept Language</td>
                    <td>en-US,en;q=0.9</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
                  </div>
      </div>
    </div>
    
                        </div>
                    </div>
                
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->



@endsection