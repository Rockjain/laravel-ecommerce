@extends('layouts.account')
@section('content')

<div class="container mtb_30">

    <div class="row">
        <div class="col-sm-4 col-md-3" id="column-left">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="panel panel-default account-sidebar">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <?php
                                    $session = Session::get('user_data');
                                    if (!empty($session)):
                                        $customerInfo = getCustomer($session['customer_id']);
                                        if ($customerInfo):
                                            $fileInfo = getFile($customerInfo->photo);
                                            if ($fileInfo) {
                                                $photo = $fileInfo->file_path . $fileInfo->file_name;
                                            } else {
                                                $photo = url('/') . '/frontassets/images/no_img.png';
                                            }
                                        endif;
                                    endif;
                                    ?>
                                    <div class="pull-left"><p><img src="<?= $photo; ?>" alt="user" /></p></div>
                                    <div class="pull-left"><p>Welcome<br/> <strong><?= $customerInfo->firstname . ' ' . $customerInfo->lastname; ?></strong></p>
                                        <p>ID: <strong><?= 'AAIMA' . $customerInfo->id; ?></strong></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body account-sidebar-menu">
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <ul class="list-group">
                                        <li class="list-group-item"><a href="/myaccount"><i class="fa fa-user"></i> My Profile <span class="badge"><i class="fa fa-angle-right"></i></span></a></li>
                                        <li class="list-group-item"><a href="/myaddress"><i class="fa fa-map-marker"></i> Manage Addresses <span class="badge"><i class="fa fa-angle-right"></i></span></a></li>
                                     <!--    <li class="list-group-item"><a href="/myreward"><i class="fa fa-trophy"></i> My Rewards <span class="badge"><i class="fa fa-angle-right"></i></span></a></li> -->
                                        <li class="list-group-item"><a href="/mybusinessassociate"><i class="fa fa-sitemap"></i> My Business Associate <span class="badge"><i class="fa fa-angle-right"></i></span></a></li>
                                        <li class="list-group-item"><a href="/mywishlist"><i class="fa fa-heart"></i> My Wishlist <span class="badge"><i class="fa fa-angle-right"></i></span></a></li>
                                        <li class="list-group-item"><a href="/myorder" class="active"><i class="fa fa-list"></i> My Orders <span class="badge"><i class="fa fa-angle-right"></i></span></a></li>
                                    <!--     <li class="list-group-item"><a href="/myreturn"><i class="fa fa-retweet"></i> My Returns <span class="badge"><i class="fa fa-angle-right"></i></span></a></li> -->
                                        <li class="list-group-item"><a href="/logoutuser" onclick="return confirm('Are you sure, you want to logout ?')" class="logout"><i class="fa fa-power-off"></i> Logout</a></li>
                                    </ul> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-8 col-md-9" id="column-right">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="panel panel-default account-content order-content">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <h3>My Orders</h3>
                                </div>
                            </div>

                        </div>

                        <div class="panel-body order-history">
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    @if($orders)
                                    @foreach($orders as $order)
                                    <div class="panel panel-default">
                                        <div class="panel-heading">

                                            <div class="row">
                                                <div class="col-xs-6 col-sm-6 col-md-9">
                                                    <a href="/order_detail/{{$order->id}}" class="order-number">{{$order->order_no}}</a>
                                                    <a>
                                                        <b><?php
                                                            switch ($order->status) {
                                                                case 1:
                                                                    echo "New Order Request";
                                                                    break;
                                                                case 2:
                                                                    echo "Order Processed";
                                                                    break;
                                                                case 3:
                                                                    echo "Order Dispatched";
                                                                    break;
                                                                case 4:
                                                                    echo "Order Delivered";
                                                                    break;
                                                                default:
                                                                    echo "N/A";
                                                            }
                                                            ?>
                                                        </b>
                                                    </a>
                                                </div>
                                                <div class="col-xs-6 col-sm-6 col-md-3">
                                                    <a href="#" class="order-help"><i class="fa fa-question"></i> Need Help</a>
                                                    <a href="/order_detail/{{$order->id}}" class="order-track"><i class="fa fa-eye"></i> View</a>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="panel-body order-short-info">
                                            @if($order->orderItems)
                                            @foreach($order->orderItems as $item)
                                            <?php
                                            $imageInfo = getFile($item->image);
                                            if ($imageInfo) {
                                                $image = $imageInfo->file_path . $imageInfo->file_name;
                                            } else {
                                                $image = url('/') . '/frontassets/images/no_img.png';
                                            }
                                            ?>
                                            <div class="row">
                                                <div class="col-xs-4 col-sm-2 col-md-2">
                                                    <p class="text-center">
                                                        <a href="/product_detail/{{$item->product_id}}" target="_blank">
                                                            <img src="<?= $image; ?>" style="height:80px"/>
                                                        </a>
                                                    </p>
                                                </div>
                                                <div class="col-xs-8 col-sm-6 col-md-6">
                                                    <h5 class="order-item-name"><a href="/product_detail/{{$item->product_id}}" target="_blank">{{$item->name}}</a></h5>
                                                    <h4 class="order-item-price">&#x20B9; {{$item->total_amount}}</h4>
                                                </div>
                                                <!--                                                <div class="col-xs-12 col-sm-4 col-md-4">
                                                                                                    <p class="order-item-attribute order-status">Shipped on 17 Nov 2018</p>
                                                                                                    <p class="order-item-attribute">Return policy ended on Nov 18th '18</p>
                                                                                                    <p class=""><strong>Rewards Points:</strong> 100</p>
                                                                                                </div>-->
                                            </div>
                                            @endforeach
                                            @endif
                                        </div>
                                        <div class="panel-footer">
                                            <div class="row">
                                                <div class="col-xs-6 col-sm-6 col-md-9">
                                                    <p class="timeConvert" created_time="<?= strtotime($order->created_at) * 1000; ?>"></p>
                                                </div>
                                                <div class="col-xs-6 col-sm-6 col-md-3">
                                                    <?php 
                                                    $grand_total=$order->grand_total;

                                                    ?>
                                                    <p class="text-right">Order Total <strong>&#x20B9; <?php echo $grand_total+50;?></strong></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        var element = $(".timeConvert");
        console.log(element)
        $.each(element, function (i, e) {
            var time = $(e).attr("created_time");
            var my_time = getTime(parseInt(time));
            $(e).html(my_time);
        });
    });

    function getTime(timestamp) {
        var unix_timestamp = timestamp;

        var date = new Date(unix_timestamp);
        var month = date.getMonth();
        var year = date.getFullYear();
        var daten = date.getDate();
        var hours = date.getHours();
        if (hours < 10) {
            hours = "0" + hours;
        }
        var minutes = date.getMinutes();
        if (minutes < 10) {
            minutes = "0" + minutes;
        }
        // var seconds = date.getSeconds();
        // if (seconds < 10) {
        //     seconds = "0" + date.getSeconds();
        // }

        var ampm = hours >= 12 ? 'PM' : 'AM';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        //var months = Array('01', '01', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12');

        var months = Array('Jan', 'Feb', 'Mar', 'April', 'May', 'Jun', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec');
        var standard_time = "Ordered on " + daten + ' ' + months[month] + ', ' + year + ' ' + hours + ':' + minutes + ' ' + ampm;
        return standard_time;

    }
</script>
@endsection