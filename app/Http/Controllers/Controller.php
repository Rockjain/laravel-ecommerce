<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Model\Appfiles;
use Illuminate\Support\Facades\DB;

class Controller extends BaseController {

    use AuthorizesRequests,
        DispatchesJobs,
        ValidatesRequests;

    public function uploadFile($request, $fileName, $path) {
        $return = false;
        //echo url('/');exit;
        if ($request->hasFile($fileName)) :
            $file = $request->file($fileName);
            $fullName = $file->getClientOriginalName();
            $stringName = $this->my_random_string(explode('.', $fullName)[0]);
            $fileName = $stringName . time() . '.' . $file->getClientOriginalExtension();
            $destinationPath = public_path($path);
            $check = $file->move($destinationPath, $fileName);
            $return = $check ? $fileName : false;
        endif;
        return $return;
    }

    public function insertFile($req) {
        $AppFiles = new Appfiles();

        $AppFiles->file_path = $req['file_path'];
        $AppFiles->file_name = $req['file_name'];
        $AppFiles->file_type = $req['file_type'];
        $AppFiles->status = 1;

        $insert = $AppFiles->save();
        $lastId = $AppFiles->id;
        $filesData = AppFiles::select(['id', DB::raw("CONCAT(file_path,'',file_name)  AS image")])->where('id', '=', $lastId)->first();
        return $filesData ? $filesData : new \stdClass();
    }

    public function updateFile($req, $id) {
        Appfiles::where('id', $id)->update($req);

        $filesData = AppFiles::select(['id', DB::raw("CONCAT(file_path,'',file_name)  AS image")])->where('id', '=', $id)->first();
        return $filesData ? $filesData : new \stdClass();
    }

    public function my_random_string($char) {
        $characters = $char;
        $length = 10;
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

}
