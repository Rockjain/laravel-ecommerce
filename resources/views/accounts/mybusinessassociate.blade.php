@extends('layouts.account')
@section('content')
   <!-- =====  CONTAINER START  ===== -->

				<div class="container mtb_30">
			
					<div class="row">
					<div class="col-sm-4 col-md-3" id="column-left">
					<div class="row">
					<div class="col-sm-12 col-md-12">
		         <div class="panel panel-default account-sidebar">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <?php
                                   $session = Session::get('user_data');
                                    if (!empty($session)):
                                        $customerInfo = getCustomer($session['customer_id']);
                                        if ($customerInfo):
                                            $fileInfo = getFile($customerInfo->photo);
                                            if ($fileInfo) {
                                                $photo = $fileInfo->file_path . $fileInfo->file_name;
                                            } else {
                                                $photo = url('/') . '/frontassets/images/no_img.png';
                                            }
                                        endif;
                                    endif;
                                    ?>
                                    <div class="pull-left"><p><img src="<?= $photo; ?>" alt="user" /></p></div>
                                    <div class="pull-left"><p>Welcome<br/> <strong><?= $customerInfo->firstname . ' ' . $customerInfo->lastname; ?></strong></p>
                                        <p>ID: <strong><?= 'AAIMA' . $customerInfo->id; ?></strong></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body account-sidebar-menu">
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <ul class="list-group">
                                        <li class="list-group-item"><a href="http://ba.aaima.org/index.php?route=account/downlinea&customer_id=<?php echo $session['customer_id'];?>"><i class="fa fa-user"></i> My Downline <span class="badge"><i class="fa fa-angle-right"></i></span></a></li>
                                        <li class="list-group-item"><a href="http://ba.aaima.org/index.php?route=account/monthlyrewardsa&customer_id=<?php echo $session['customer_id'];?>"><i class="fa fa-map-marker"></i> Monthly Rewards <span class="badge"><i class="fa fa-angle-right"></i></span></a></li>
                                        <li class="list-group-item"><a href="http://ba.aaima.org/index.php?route=account/activea&customer_id=<?php echo $session['customer_id'];?>" ><i class="fa fa-trophy"></i> My Team <span class="badge"><i class="fa fa-angle-right"></i></span></a></li>
                                       <li class="list-group-item"><a href="http://ba.aaima.org/index.php?route=account/leadershipincomea&customer_id=<?php echo $session['customer_id'];?>"><i class="fa fa-sitemap"></i> Leadership Income <span class="badge"><i class="fa fa-angle-right"></i></span></a></li>
                                        <li class="list-group-item"><a href="http://ba.aaima.org/index.php?route=account/paymenta&customer_id=<?php echo $session['customer_id'];?>"><i class="fa fa-heart"></i> Payment Options <span class="badge"><i class="fa fa-angle-right"></i></span></a></li>
                                      
                                        <li class="list-group-item"><a href="/logoutuser" onclick="return confirm('Are you sure, you want to logout ?')" class="logout"><i class="fa fa-power-off"></i> Logout</a></li>
                                    </ul> 
                                </div>
                            </div>
                        </div>
                    </div>
						</div>

					</div>
					</div>
						<div class="col-sm-8 col-md-9" id="column-right">
					<div class="row">
					<div class="col-sm-12 col-md-12">
					<div class="panel panel-default account-content">
					 <div class="panel-heading">
					  <div class="row">
	<div class="col-sm-12 col-md-12">
	<div class="pull-left"><h3>My Business Associate</h3></div>
   <div class="pull-right"><p></p></div>
   </div>
   </div>
   
					 </div>
					 
  <div class="panel-body">
  
    <div class="row">
	<div class="col-sm-12 col-md-6 col-md-offset-3">

	<div class="table-responsive">
  <table class="table table-bordered table-hover">
    <thead>
      <tr>
        <th class="text-center">My Referral Code</th>
        <th class="text-center">My Balance Point</th>
      </tr>
    </thead>
    <tbody>
	<tr>
        <td class="text-center"><?= 'AAIMA' . $customerInfo->id; ?></td>
        <td class="text-center">0</td>
      </tr>
         <!--    <tr>
        <td class="text-center" colspan="3">No results!</td>
      </tr> -->
          </tbody>
  </table>
</div>
	
   </div>
   </div>
   
   
  </div>
</div>
						</div>

					</div>
					</div>
					

					</div>
				
				</div>
					
	
    <!-- =====  CONTAINER END  ===== -->
   
  </div> <!---wrapper class ends here-- started in header--->
  @endsection
  <!-- a id="scrollup">Scroll</a>
  <script src="js/jQuery_v3.1.1.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery.magnific-popup.js"></script>
  <script src="js/jquery.firstVisitPopup.js"></script>
  <script src="js/custom.js"></script>
 -->