<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Appfiles extends Model {

    protected $table = 'files';
    protected $fillable = array(
        'file_path',
        'file_name',
        'file_type',
        'status ',
        'created_at',
        'updated_at'
    );

    public function getFullnameAttribute() {
        return "{$this->file_path}{$this->file_name}";
    }

}
