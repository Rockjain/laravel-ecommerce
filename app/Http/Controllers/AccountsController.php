<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Wishlist;
use App\Model\Customer;
use App\Model\Address;
use App\Model\Orderaddress;
use App\Model\State;
use App\Model\OrderHistory;
use App\Model\Country;
use App\Model\Cart;
use App\Model\Order;

class AccountsController extends Controller {

    public function myaccount(Request $request) {
        $user_data = $request->session()->get('user_data');

        if (!$user_data) {
            return redirect('/loginuser')->with('error', 'Please login first');
        }
        $customerInfo = Customer::where(['id' => $user_data['customer_id']])->first();
        return view('accounts/myaccount', ['customerInfo' => $customerInfo]);
    }

    public function update_profile(Request $request, $id = null) {
        $user_data = $request->session()->get('user_data');
        $updateArr = [
            'firstname' => $request->input('firstname'),
            'lastname' => $request->input('lastname') != '' ? $request->input('lastname') : '',
            'email' => $request->input('email') != '' ? $request->input('email') : '',
            'telephone' => $request->input('telephone') != '' ? $request->input('telephone') : '',

            'aadhar_card'      => $request->input('aadharcard') != '' ? $request->input('aadharcard') : '',
            'bank_account'     => $request->input('bankAccount') != '' ? $request->input('bankAccount') : '',
            'ifsc_code'        => $request->input('IfscCode') != '' ? $request->input('IfscCode') : '',
            'pan_card'         => $request->input('panCard') != '' ? $request->input('panCard') : '',
            'nominee_name'     => $request->input('nomineeName') != '' ? $request->input('nomineeName') : '',
            'nominee_relation' => $request->input('relationOfNominee') != '' ? $request->input('relationOfNominee') : '',
            'aadhar_nominee'   => $request->input('aadharNominee') != '' ? $request->input('aadharNominee') : '',
            'newsletter' => $request->input('newsletter'),
        ];

        $path = "/images/brandimages/";
        $check = $this->uploadFile($request, 'photo', $path);
        if ($check):
            $nameArray = explode('.', $check);
            $ext = end($nameArray);

            $insertParam['file_path'] = url('/') . $path;
            $insertParam['file_name'] = $check;
            $insertParam['file_type'] = $ext;

            $getImgData = $this->insertFile($insertParam);

            $updateArr['photo'] = $getImgData['id'];
        endif;

        $update = Customer::where('id', $id)->update($updateArr);
        if ($update) {
            return redirect('myaccount')->with('success', 'Profile updated successfully');
        }
        return back()->withInput()->with('error', 'Error in updating profile');
    }

    public function mywishlist(Request $request) {
        $user_data = $request->session()->get('user_data');
        if (!$user_data) {
            return redirect('/loginuser')->with('error', 'Please login first');
        }
        $wishlist = Wishlist::where(['customer_id' => $user_data['customer_id']])->get();

        return view('accounts/mywishlist', ['wishlist' => $wishlist]);
    }

    public function myorder(Request $request) {
        $user_data = $request->session()->get('user_data');
        if (!$user_data) {
            return redirect('/loginuser')->with('error', 'Please login first');
        }
        $orders = Order::where(['customer_id' => $user_data['customer_id']])->orderBy('id','desc')->get();
        return view('accounts/myorder', ['orders' => $orders]);
    }

    public function order_detail(Request $request, $id = null) {
        $user_data = $request->session()->get('user_data');
        if (!$user_data) {
            return redirect('/loginuser')->with('error', 'Please login first');
        }

        $order = Order::find($id);
        $history = OrderHistory::where(['order_id'=>$id])->get();
            $order['history']=$history;

        if ($order):
            $address = Orderaddress::where(['order_id' => $id])->first();
//          echo "<pre>";
//        print_r($address);
//        die;
            if ($address) {
                $state = State::where(['state_id' => $address->zone_id])->first();
                if ($state) {
                    $address['state'] = $state->state_name;
                } else {
                    $address['state'] = '';
                }
                $country = Country::where(['country_id' => $address->country_id])->first();
                if ($country) {
                    $address['country'] = $country->country_name;
                } else {
                    $address['country'] = '';
                }
                $order['address'] = $address;
            } else {
                $order['address'] = [];
            }
        endif;

        return view('accounts.order_detail', ['orderInfo' => $order]);
    }

    public function return_request(Request $request) {
        return view('orders.return_request');
    }

    public function myreward(Request $request) {
         $user_data = $request->session()->get('user_data');
        if (!$user_data) {
            return redirect('/loginuser')->with('error', 'Please login first');
        }
        return view('accounts/myreward');
    }

    public function mybusinessassociate() {
        return view('accounts/mybusinessassociate');
    }

    public function myaddress(Request $request) {
        $user_data = $request->session()->get('user_data');
        if (!$user_data) {
            return redirect('/loginuser')->with('error', 'Please login first');
        }
        $addressInfo = Address::where(['customer_id' => $user_data['customer_id']])->get();
//        echo "<pre>";
//        print_r($addressInfo);
//        die;
        $states = State::where(['country_id' => '1'])->get();
        $countries = Country::get();

        return view('accounts/myaddress', ['addressInfo' => $addressInfo, 'states' => $states, 'countries' => $countries]);
    }

    public function add_address(Request $request) {
        $user_data = $request->session()->get('user_data');

        $addArr = [
            'customer_id' => $user_data['customer_id'],
            'address_1' => $request->input('address_1'),
            'address_2' => $request->input('address_2'),
            'city' => $request->input('city'),
            'zone_id' => $request->input('zone_id'),
            'country_id' => $request->input('country_id'),
            'postcode' => $request->input('postcode'),
        ];

        $return = Address::create($addArr);
        if ($return) {
            return redirect('myaddress')->with('success', 'Address added successfully');
        }
        return back()->withInput()->with('error', 'Error in adding address');
    }

    public function update_address(Request $request, $id = null) {
        $updateArr = [
            'address_1' => $request->input('address_1'),
            'address_2' => $request->input('address_2'),
            'city' => $request->input('city'),
            'zone_id' => $request->input('zone_id'),
            'country_id' => $request->input('country_id'),
            'postcode' => $request->input('postcode'),
        ];
        $update = Address::where('id', $id)->update($updateArr);
        if ($update) {
            return redirect('myaddress')->with('success', 'Address updated successfully');
        }
        return back()->withInput()->with('error', 'Error in updating address');
    }

    public function set_default_address(Request $request, $id = null) {
//        $user_data = $request->session()->get('user_data');
//        Address::where('customer_id', $user_data['customer_id'])->update(['is_default' => '0']);
        $update = Address::where('id', $id)->update(['is_default' => '1']);
        if ($update) {
            return redirect('myaddress')->with('success', 'Address set as default');
        }
        return back()->withInput()->with('error', 'Error in set default address');
    }

    public function mydownline() {
        return view('accounts/mydownline');
    }

    public function myreturn(Request $request) {
        $user_data = $request->session()->get('user_data');
        if (!$user_data) {
            return redirect('/loginuser')->with('error', 'Please login first');
        }
        return view('accounts/myreturn');
    }

    public function mycart(Request $request) {
        $user_data = $request->session()->get('user_data');
        if ($user_data) {
            $customer_id = $user_data['customer_id'];
        } else {
            if (!empty($_COOKIE['customer_cookie'])) {
                $customer_id = $_COOKIE['customer_cookie'];
            } else {
                $customer_id = '';
            }
        }
        $usercart = Cart::where(['customer_id' => $customer_id])->get();

        if ($usercart):
            $cartArr = [];
            $s_total = '0';
            foreach ($usercart as $key => $row):
                $productInfo = \App\Model\Product::where(['id' => $row->product_id])->first();

                $cartArr[$key] = $row;
                $price=$productInfo->price - (($productInfo->discount_percent * $productInfo->price) / 100);
                $s_total = $s_total + ($row->quantity * $price);
            endforeach;
        endif;

        return view('accounts/mycart', ['usercart' => $cartArr, 's_total' => $s_total != '' ? $s_total : '0', 'user_data' => $user_data]);
    }
}
