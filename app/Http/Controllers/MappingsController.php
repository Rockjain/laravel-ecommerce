<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Model\CategoryAttribute;
use App\Model\CategoryBrand;
use App\Model\Category;
use App\Model\Attribute;
use App\Model\Brand;
use App\Model\Attributevalue;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;

class MappingsController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function category_attribute_mapping() {
        $categories = Category::where(['status' => 1])->get();
        $attributes = Attribute::where(['status' => 1])->get();

        $categoryattributes = CategoryAttribute::select('category_id')->distinct()->get();
        if ($categoryattributes):
            foreach ($categoryattributes as $key => $catattr):
                $attrInfo = CategoryAttribute::where(['category_id' => $catattr->category_id])->get();
                $categoryattributes[$key]['attributes'] = $attrInfo;
            endforeach;
        endif;
        return view('mappings.category_attribute_mapping', ['categories' => $categories, 'attributes' => $attributes, 'categoryattributes' => $categoryattributes]);
    }

    public function get_cat_attributes(Request $request) {
        $category_id = $request->input('category_id');

        $mappedAttribute = CategoryAttribute::where(['category_id' => $category_id])->selectRaw('GROUP_CONCAT(attribute_id) as attribute_ids')->first();
//        echo "<pre>";
//        print_r($mappedAttribute);
//        die;
        if ($mappedAttribute->attribute_ids) {
            $attribute_ids = "AND id NOT IN ($mappedAttribute->attribute_ids)";
        } else {
            $attribute_ids = '';
        }
//        echo $attribute_ids;die;
        $attributes = DB::select("select * from attributes where status='1' $attribute_ids");
//        echo "<pre>";
//        print_r($attributes);
//        die;
        if (!empty($attributes)):
            foreach ($attributes as $key => $attr):
                $attributes[$key]->attribute_name = $attr->name;
            endforeach;
        else:
            $attributes = Attribute::where(['status' => 1])->get();
        endif;

        if ($attributes) {
            $res = ['status' => '1', 'message' => 'Success', 'attributes' => $attributes];
        } else {
            $res = ['status' => '0', 'message' => 'Error', 'brands' => []];
        }
        echo json_encode($res);
        die;
    }

    public function category_brand_mapping() {
        $categories = Category::where(['status' => 1])->get();
        $brands = Brand::where(['status' => 1])->get();

        $categorybrands = CategoryBrand::select('category_id')->distinct()->get();
        if ($categorybrands):
            foreach ($categorybrands as $key => $catbrand):
                $brandInfo = CategoryBrand::where(['category_id' => $catbrand->category_id])->get();
                $categorybrands[$key]['brands'] = $brandInfo;
            endforeach;
        endif;
//        echo "<pre>";print_r($categorybrands);die;
        return view('mappings.category_brand_mapping', ['categories' => $categories, 'brands' => $brands, 'categorybrands' => $categorybrands]);
    }

    public function get_cat_brands(Request $request) {
        $category_id = $request->input('category_id');

        $mappedBrand = CategoryBrand::where(['category_id' => $category_id])->selectRaw('GROUP_CONCAT(brand_id) as brand_ids')->first();
        if ($mappedBrand->brand_ids) {
            $brand_ids = "AND id NOT IN ($mappedBrand->brand_ids)";
        } else {
            $brand_ids = '';
        }
        $brands = DB::select("select * from brands where status='1' $brand_ids");

        if (!empty($brands)):
            foreach ($brands as $key => $brand):
                $brands[$key]->brand_id = $brand->id;
                $brands[$key]->brand_name = $brand->title;
            endforeach;
        else:
            $brands = Brand::where(['status' => 1])->get();
        endif;

        if ($brands) {
            $res = ['status' => '1', 'message' => 'Success', 'brands' => $brands];
        } else {
            $res = ['status' => '0', 'message' => 'Error', 'brands' => []];
        }
        echo json_encode($res);
        die;
    }

    public function add_cat_attributes(Request $request) {
        if ($request->input('attributes')[0]) {
            foreach ($request->input('attributes') as $value) {
                $addArr = [
                    'category_id' => $request->input('category_id'),
                    'attribute_id' => $value,
                    'status' => '1',
                ];
                CategoryAttribute::create($addArr);
            }
            return redirect('admin/category_attribute_mapping')->with('success', 'Category Attribute Mapped Successfully');
        }

        return back()->withInput()->with('error', 'Some error occured, please try again');
    }

    public function add_cat_brands(Request $request) {
        if ($request->input('brands')[0]) {
            foreach ($request->input('brands') as $value) {
                $addArr = [
                    'category_id' => $request->input('category_id'),
                    'brand_id' => $value,
                    'status' => '1',
                ];
                CategoryBrand::create($addArr);
            }
            return redirect('admin/category_brand_mapping')->with('success', 'Category Brand Mapped Successfully');
        }

        return back()->withInput()->with('error', 'Some error occured, please try again');
    }

}
