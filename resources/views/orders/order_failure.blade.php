@extends('layouts.default')
@section('content')
<div class="container mtb_30">
    <div class="row">
        <div class="col-sm-12 col-sm-offset-1 col-md-10 col-md-offset-1 white-bg">
            <div class="confirmation-wrapper ptb_50 text-center">
                <div class="payment-failure">
                    <div class="content">
                        <div class="icon">
                            <i class="fa fa-thumbs-down text-danger"></i>
                        </div>
                        <h2 class="heading uppercase mt-0 text-danger">Sorry! your Order has not Successfully Placed!</h2>
                        <p>Payment Declined</p>
                    </div>
                </div>
                <button class="btn btn-primary"><i class="fa fa-retweet"></i> Retry</button>
            </div>
        </div>
    </div>
</div>
@endsection