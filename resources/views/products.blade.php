<style type="text/css">
    .product-name{
  
    height: 30px !important;
}

.product-detail{
    height: 100px;
}
.product-thumb .button-group {
    bottom: 110px!important;
}
</style>

@extends('layouts.default')
@section('content')
<!-- =====  CONTAINER START  ===== -->
<div class="container">
    <div class="row ">
        <form method="post" id="filterProductForm">  
            <div id="column-left" class="col-sm-4 col-md-4 col-lg-3 mt_30">
                <div class="">
                    <div class="category">
                        <div class="menu-bar" data-target="#category-menu,#category-menu-responsive" data-toggle="collapse" aria-expanded="true" role="button">
                            <h4 class="category_text"> Filter Category</h4>
                            <span class="i-bar"><i class="fa fa-bars"></i></span>
                        </div>
                    </div>
                    <div id="category-menu-responsive" class="navbar collapse " aria-expanded="true" style="" role="button">
                        <div class="nav-responsive">
                            <ul class="nav  main-navigation collapse in">
                                @if($categories)
                                @foreach($categories as $key=>$category)
                                <li>
                                    <label>
                                        <a href="/products/{{$category->id}}">{{$category->name}}</a> 
                                    </label>
                                </li>
                                @endforeach
                                @endif
                            </ul>
                        </div>
                    </div>
                    <div id="category-menu" class="navbar collapse in  mb_40" aria-expanded="true" style="" role="button">
                        <div class="nav-responsive">
                            <ul class="nav  main-navigation collapse in">
                                @if($categories)
                                @foreach($categories as $key=>$category)
                                <li>
                                    <label>
                                        <a href="/products/{{$category->id}}">{{$category->name}}</a> 
                                    </label>
                                </li>
                                @endforeach
                                @endif
                            </ul>
                        </div>
                    </div>
                    <div class="filter left-sidebar-widget mtb_20 white-bg">
                        <div class="heading-part mt_10 ">
                            <h2 class="main_title">Refine Search</h2>
                        </div>
                        <div class="filter-block pb_10">

                            <p>
                                <label for="amount">Price range:</label><br>
                                <input type="text" id="amount" readonly>
                            </p>
                            <input type="hidden" id="minprice" name="min_price" value="<?= $min_price; ?>">
                            <input type="hidden" id="maxprice" name="max_price" value="<?= $max_price; ?>">
                            <div id="slider-range" class="mtb_20"></div>
                            <div class="list-group">
                                @if($attribites)
                                @foreach($attribites as $key1=>$attribite)
                                @if($attribite->attributeDetail->type == 'SELECT')
                                <div class="list-group-item mb_10">
                                    <label>{{$attribite->attributeDetail->name}}</label>
                                    <div id="filter-group1">
                                        @if($attribite->attributeDetail->attributeValues)
                                        @foreach($attribite->attributeDetail->attributeValues as $key2=>$value)
                                        <div class="checkbox">
                                            <label>
                                                <input value="{{$value->id}}" type="checkbox" name="attributes[{{$attribite->attributeDetail->id}}][{{$key2}}]"> {{$value->attribute_value}}
                                            </label>
                                        </div>
                                        @endforeach
                                        @endif
                                    </div>
                                </div>
                                @endif
                                @endforeach
                                @endif
                                <button type="submit" class="btn btn-primary">Refine Search</button>
                            </div>
                            <input type="hidden" name="category_id" value="<?= $category_id; ?>">
                        </div>
                    </div>              
                </div>
            </div>
            <div id="column-list" class="col-sm-8 col-md-8 col-lg-9 mtb_30">
                <!-- =====  BANNER STRAT  ===== -->


                <div class="row">
                    <div class="breadcrumb ptb_20">
                        <h1>{{$name->name}}</h1>
                        <ul>
                            <li><a href="<?= url('/'); ?>">Home</a></li>
                            <li class="active">{{$name->name}}</li>
                        </ul>
                    </div>
                </div>
                <!-- =====  BREADCRUMB END===== -->
                <div class="row sorting-box">
                    <div class="category-page-wrapper">
                        <div class="col-xs-6 sort-wrapper">
                            <label class="control-label" for="input-sort" style="color:#dd0016;">Sort By :</label>
                            <div class="sort-inner">
                                <select id="input-sort" class="form-control" name="sort_by">
                                <option value="default" selected="selected">Default</option>
                                    <option value="name_asc">Name (A - Z)</option>
                                    <option value="name_desc">Name (Z - A)</option>
                                    <option value="price_asc">Price (Low &gt; High)</option>
                                    <option value="price_desc">Price (High &gt; Low)</option>
                                    <option value="rating_asc">Rating (Highest)</option>
                                    <option value="rating_desc">Rating (Lowest)</option>
                                </select>


                            </div>
                             <span>
                               <!--  <i class="fa fa-angle-down" aria-hidden="true"></i> -->
                    <button type="submit" class="btn btn-primary" style=" padding:2px 8px;"><i class="fa fa-check" aria-hidden="true"></i></button>
                            </span> 
                           
                        </div>
                        <div class="col-xs-4 page-wrapper">
                        </div>
                        <div class="col-xs-2 text-right list-grid-wrapper">
                            <div class="btn-group btn-list-grid">
                                <button type="button" id="list-view" class="btn btn-default list-view"></button>
                                <button type="button" id="grid-view" class="btn btn-default grid-view active"></button>
                            </div>
                        </div>
                    </div>
                </div>
                @if($productcount)
                <div class="row white-bg product-grid filteredproducts">       
                    @foreach($products as $key=>$data)    
                    <div class="product-layout  product-grid  col-lg-3 col-md-4 col-sm-6 col-xs-12 ">
                        <div class="item">
                            <div class="product-thumb clearfix mb_30">
                                <div class="image product-imageblock"> 
                                    <a href="/product_detail/{{$data->id}}"> 
                                        <img data-name="product_image" src="{{!empty($data->fileDetail)?$data->fileDetail->file_path.$data->fileDetail->file_name:url('/').'/frontassets/images/no_img.png'}}" title="{{$data->name}}" class="img-responsive" style="height: 210px"> 
                                        <img data-name="product_image" src="{{!empty($data->fileDetail)?$data->fileDetail->file_path.$data->fileDetail->file_name:url('/').'/frontassets/images/no_img.png'}}" title="{{$data->name}}" class="img-responsive" style="height: 210px"> 
                                    </a> 
                                </div>
                                <div class="caption product-detail text-left">
                                    <h6 data-name="product_name" class="product-name mt_20">
                                        <a href="#" title="{{$data->name}}"><?= substr($data->name, 0, 30) . '...' ?></a>
                                    </h6>
                                    <div class="rating">
                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                    </div>
                                    <!-- <span><i class="fa fa-inr"></i> <del class="text-danger">14990</del></span>
                                    <span class="price"><span class="amount">
                                            <span class="currencySymbol"><i class="fa fa-inr"></i> </span>{{$data->price}}</span>
                                    </span> -->
                                    @if($data->discount_percent!=0)
                                <span><i class="fa fa-inr"></i> <del class="text-danger">{{$data->price}}</del></span>
                                @endif
                                <span class="price"><span class="amount"><span class="currencySymbol"> <i class="fa fa-inr"></i> </span><?= number_format($data->price - (($data->discount_percent * $data->price) / 100), 2, '.', ''); ?></span>
                                </span>

                                    <span>
<!--  -->                                    </span>
                                    <p class="product-desc mt_20 mb_60"> </p>
                                    <div class="button-group text-center">
                                        <div class="wishlist" title="Add to wishlist"><a href="javascript:void(0)" product_id="{{$data->id}}" onclick="addtowishlist(this)"><span>wishlist</span></a></div>
                                        <div class="quickview" title="Quick View"><a href="/product_detail/{{$data->id}}"><span>Quick View</span></a></div>
                                        <div class="add-to-cart" title="Add to cart"><a href="javascript:void(0)" product_id="{{$data->id}}" onclick="addtocart(this)"><span>Add to cart</span></a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                @else
                <div class="row white-bg">  
                    <h3 align="center">No Product Found</h3>
                </div>
                @endif
                <!--            <div class="pagination-nav text-center mt_50">
                                <ul>
                                    <li><a href="#"><i class="fa fa-angle-left"></i></a></li>
                                    <li class="active"><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                                </ul>
                            </div>-->
            </div>
        </form>
    </div>

</div>
<!-- =====  CONTAINER END  ===== -->
<script>
    $("#filterProductForm").on('submit', function (e) {
        e.preventDefault();
        var string = '';
        $.ajax({
            url: '/filterproducts',
            type: 'post',
            dataType: 'json',
            data: $(this).serialize(),
            cache: false
        }).done(function (response) {
//            console.log(response);
            if (response.error_code == 200) {
                $.each(response.result, function (i, v) {
                    string += '<div class="product-layout  product-grid  col-lg-3 col-md-4 col-sm-6 col-xs-12 ">'
                            + '<div class="item">'
                            + '<div class="product-thumb clearfix mb_30">'
                            + '<div class="image product-imageblock">'
                            + '<a href="/product_detail/' + v.id + '">'
                            + '<img data-name="product_image" src="' + v.image + '" title="' + v.name + '" class="img-responsive" style="height: 210px"> '
                            + '<img data-name="product_image" src="' + v.image + '" title="' + v.name + '" class="img-responsive" style="height: 210px"> '
                            + '</a> '
                            + '</div>'
                            + '<div class="caption product-detail text-left">'
                            + '<h6 data-name="product_name" class="product-name mt_20">'
                            + '<a href="#" title="' + v.name + '">' + v.name + '</a>'
                            + '</h6>'
                            + '<div class="rating">'
                            + '<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>'
                            + '<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>'
                            + '<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>'
                            + '<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>'
                            + '<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>'
                            + '</div>'
                            + '<span class="price"><span class="amount">'
                            + '<span class="currencySymbol"><i class="fa fa-inr"></i> </span>' + v.price + '</span>'
                            + '</span>'
                            + '<p class="product-desc mt_20 mb_60"> More room to move. With 80GB or 160GB of storage and up to 40 hours of battery life, the new iPod classic lets you enjoy up to 40,000 songs or up to 200 hours of video or any combination wherever you go.Cover Flow. Browse through your music collection by flipping..</p>'
                            + '<div class="button-group text-center">'
                            + '<div class="wishlist" title="Add to wishlist"><a href="javascript:void(0)" product_id="' + v.id + '" onclick="addtowishlist(this)"><span>wishlist</span></a></div>'
                            + '<div class="quickview" title="Quick View"><a href="/product_detail/' + v.id + '"><span>Quick View</span></a></div>'
                            + '<div class="add-to-cart" title="Add to cart"><a href="javascript:void(0)" product_id="' + v.id + '" onclick="addtocart(this)"><span>Add to cart</span></a></div>'
                            + '</div>'
                            + '</div>'
                            + '</div>'
                            + '</div>'
                            + '</div>';
                });
                $(".filteredproducts").html(string);
            } else {
                $(".filteredproducts").html('<p><h3 align="center">No Product Found</h3></p>');
            }
        });
    })
    $(function () {
        $("#slider-range").slider({
            range: true,
            min: <?= $min_price; ?>,
            max: <?= $max_price; ?>,
            values: [<?= $min_price; ?>, <?= $max_price; ?>],
            slide: function (event, ui) {
                $("#amount").val("Rs " + ui.values[0] + " - Rs " + ui.values[1]);
                $("#minprice").val(ui.values[0]);
                $("#maxprice").val(ui.values[1]);
            }
        });
        $("#amount").val("Rs " + $("#slider-range").slider("values", 0) +
                " - Rs " + $("#slider-range").slider("values", 1));
    });
</script>
@endsection