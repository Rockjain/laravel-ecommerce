@extends('layouts.default')
@section('content')
<div class="container">
    <div class="row ">
        <div id="column-list" class="col-sm-12 col-md-12 col-lg-12 mtb_30">
            <div class="row">
                <div class="breadcrumb ptb_20">
                    <h1>Searched Products</h1>
                    <ul>
                        <li><a href="<?= url('/'); ?>">Home</a></li>
                        <li class="active">Searched Products</li>
                    </ul>
                </div>
            </div>
            <div class="row sorting-box">
                <div class="category-page-wrapper">
                    <div class="col-xs-6 sort-wrapper">

                    </div>
                    <div class="col-xs-4 page-wrapper">
                    </div>
                    <div class="col-xs-2 text-right list-grid-wrapper">
                        <div class="btn-group btn-list-grid">
                            <button type="button" id="list-view" class="btn btn-default list-view"></button>
                            <button type="button" id="grid-view" class="btn btn-default grid-view active"></button>
                        </div>
                    </div>
                </div>
            </div>
            @if($productcount)
            <div class="row white-bg product-grid filteredproducts">       
                @foreach($products as $key=>$data)       
                <div class="product-layout  product-grid  col-lg-3 col-md-4 col-sm-6 col-xs-12 ">
                    <div class="item">
                        <div class="product-thumb clearfix mb_30">
                            <div class="image product-imageblock"> 
                                <a href="/product_detail/{{$data->id}}"> 
                                    <img data-name="product_image" src="{{!empty($data->fileDetail)?$data->fileDetail->file_path.$data->fileDetail->file_name:url('/').'/frontassets/images/no_img.png'}}" title="{{$data->name}}" class="img-responsive" style="height: 210px"> 
                                    <img data-name="product_image" src="{{!empty($data->fileDetail)?$data->fileDetail->file_path.$data->fileDetail->file_name:url('/').'/frontassets/images/no_img.png'}}" title="{{$data->name}}" class="img-responsive" style="height: 210px"> 
                                </a> 
                            </div>
                            <div class="caption product-detail text-left">
                                <h6 data-name="product_name" class="product-name mt_20">
                                    <a href="#" title="{{$data->name}}">{{$data->name}}</a>
                                </h6>
                                <div class="rating">
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                </div>
                                <span class="price"><span class="amount">
                                        <span class="currencySymbol"><i class="fa fa-inr"></i> </span>{{$data->price}}</span>
                                </span>
                                <p class="product-desc mt_20 mb_60"> More room to move. With 80GB or 160GB of storage and up to 40 hours of battery life, the new iPod classic lets you enjoy up to 40,000 songs or up to 200 hours of video or any combination wherever you go.Cover Flow. Browse through your music collection by flipping..</p>
                                <div class="button-group text-center">
                                    <div class="wishlist" title="Add to wishlist"><a href="javascript:void(0)" product_id="{{$data->id}}" onclick="addtowishlist(this)"><span>wishlist</span></a></div>
                                    <div class="quickview" title="Quick View"><a href="/product_detail/{{$data->id}}"><span>Quick View</span></a></div>
                                    <div class="add-to-cart" title="Add to cart"><a href="javascript:void(0)" product_id="{{$data->id}}" onclick="addtocart(this)"><span>Add to cart</span></a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            @else
            <div class="row white-bg">  
                <h3 align="center">No Product Found</h3>
            </div>
            @endif
        </div>
    </div>
</div>
@endsection