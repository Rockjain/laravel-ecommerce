@extends('layouts.admin')
@section('content')
<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">                                
                    <h3 class="panel-title">All Orders</h3> 
                </div>
                <div class="panel-body">
                    <div class="well">
                         <form action="/admin/filter_orders" method="post">
                                {{csrf_field()}}
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label" for="input-name">Order ID</label>
                                    <input type="text" name="order_no" placeholder="Order ID" id="input-name" class="form-control" autocomplete="off" value="<?= isset($_POST['order_no']) ? $_POST['order_no'] : ''; ?>">
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="input-model">Customer</label>
                                    <input type="text" name="name" placeholder="Name" id="input-model" class="form-control" autocomplete="off" value="<?= isset($_POST['name']) ? $_POST['name'] : ''; ?>">
                                    <ul class="dropdown-menu"></ul>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <!-- <div class="form-group">
                                    <label class="control-label" for="input-price">Order Status</label>
                                    <input type="text" name="filter_price" value="" placeholder="Price" id="input-price" class="form-control">
                                </div> -->
                                <div class="form-group">
                                        <label class="control-label">Order Status</label>
                                        <select name="status" class="form-control">
                                            <option disabled selected>Select</option>
                                            <option value="1" <?php if (isset($_POST['status']) && $_POST['status'] == '1'): ?>selected<?php endif; ?>>Request</option>
                                            <option value="2" <?php if (isset($_POST['status']) && $_POST['status'] == '2'): ?>selected<?php endif; ?>>Process</option>
                                             <option value="3" <?php if (isset($_POST['status']) && $_POST['status'] == '3'): ?>selected<?php endif; ?>>Dispatched</option>
                                              <option value="4" <?php if (isset($_POST['status']) && $_POST['status'] == '4'): ?>selected<?php endif; ?>>Delivered</option>
                                        </select>                                           
                                    </div>
                                <div class="form-group">
                                    <label class="control-label" for="input-quantity">Total</label>
                                    <input type="text" name="grand_total" placeholder="Grand Total" id="input-quantity" class="form-control" value="<?= isset($_POST['grand_total']) ? $_POST['grand_total'] : ''; ?>" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label" for="input-status">Date Added</label>
                                    <div class="input-group date">
                                        <input type="text" name="created_at" placeholder="" data-date-format="yyyy-mm-dd" id="" class="form-control" value="<?= isset($_POST['created_at']) ? $_POST['created_at'] : ''; ?>" autocomplete="off">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="input-image">Date Modified</label>
                                    <div class="input-group date">
                                        <input type="text" name="updated_at" placeholder="" data-date-format="yyyy-mm-dd" id="" class="form-control" value="<?= isset($_POST['updated_at']) ? $_POST['updated_at'] : ''; ?>" autocomplete="off">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                                        </span>
                                    </div>
                                </div>
                                <button type="submit" id="button-filter" class="btn btn-info pull-right"><i class="fa fa-filter"></i> Filter</button>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
                <div class="panel-body">
                    <table class="table table-bordered table-stripped">
                        <thead>
                            <tr>
                                <th>S.No</th>
                                <th>Order ID</th> 
                                 <th>Sku Code</th> 
                                <th>Customer</th> 
                                <th>Order Date</th>
                                <th>Order Amount</th>	
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($orders)
                            @foreach($orders as $key=>$order)
                            <?php
                            $customer = getCustomer($order->customer_id);
                            ?>
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$order->order_no}}</td>
                                <td>{{$order->sku_code}}</td>
                                <td><?= $customer->firstname . ' ' . $customer->lastname; ?></td>
                                <td class="timeConvert" created_time="<?= strtotime($order->created_at) * 1000; ?>"></td>
                                <td><i class="fa fa-inr"></i> {{$order->grand_total}}</td>
                                <td><span class="text-info">Enabled</span></td>
                                <td>
                                    <a href="/admin/orders/order_detail/{{ $order->id }}" data-toggle="tooltip" title="View" class="btn btn-success btn-rounded text-center"><i class="fa fa-eye"></i> </a> 
                                </td>
                                @endforeach
                                @endif
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        var element = $(".timeConvert");
        console.log(element)
        $.each(element, function (i, e) {
            var time = $(e).attr("created_time");
            var my_time = getTime(parseInt(time));
            $(e).html(my_time);
        });
    });

    function getTime(timestamp) {
        var unix_timestamp = timestamp;

        var date = new Date(unix_timestamp);
        var month = date.getMonth();
        var year = date.getFullYear();
        var daten = date.getDate();
        var hours = date.getHours();
        if (hours < 10) {
            hours = "0" + hours;
        }
        var minutes = date.getMinutes();
        if (minutes < 10) {
            minutes = "0" + minutes;
        }
        // var seconds = date.getSeconds();
        // if (seconds < 10) {
        //     seconds = "0" + date.getSeconds();
        // }

        var ampm = hours >= 12 ? 'PM' : 'AM';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        //var months = Array('01', '01', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12');

        var months = Array('Jan', 'Feb', 'Mar', 'April', 'May', 'Jun', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec');
        var standard_time = daten + ' ' + months[month] + ', ' + year + ' ' + hours + ':' + minutes + ' ' + ampm;
        return standard_time;

    }
</script>
@endsection