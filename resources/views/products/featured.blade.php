<meta name="csrf-token" content="{{ csrf_token() }}">
@extends('layouts.admin')
@section('content')
<div class="page-content-wrap">
    @include('partials.errors')
    @include('partials.success')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">                                
                    <h3 class="panel-title">Featured Products</h3> 
                    <ul class="panel-controls">
                        <li><a href="/admin/products/create" class="text-info"><span class="fa fa-plus"></span></a></li>
                        <li><a href="#" class="text-danger"><span class="fa fa-trash-o"></span></a></li>
                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    </ul>                
                </div>
                <div class="panel-body">
                    <table class="table datatable">
                        <thead>
                            <tr>
                                <th>ID</th>    
                                <th>Image</th>
                                <th style="max-width: 150px;">Product Name</th>
                                <th>Product Code</th>
                                <th style="min-width: 120px;">Price</th>
                                <th>Inventary</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($products)
                            @foreach($products as $key=>$product)
                            <tr>
                                <td>{{$product->id}}</td>
                                <td><img src="{{$product->fileDetail->file_path.$product->fileDetail->file_name}}" width="75" height="50"></td>
                                <td width="25%">{{$product->name}}</td>
                                <td>{{$product->sku_code}}</td>
                                <td width="20%"><i class="fa fa-inr"></i> <del class="text-danger">{{$product->price}} </del>  <i class="fa fa-inr"></i> <?= number_format($product->price - (($product->discount_percent * $product->price) / 100), 2, '.', ''); ?></td>
                                <td width="5%">{{$product->quantity}}</td>
                                <td>
                                    <label class="switch" style="top: 12px">
                                        <input type="checkbox" <?php
                                        if ($product->status == '1') {
                                            echo "checked";
                                        }
                                        ?> id="{{$product->id}}" onchange="checkStatus(this);" id="switch-{{$key}}}">
                                        <span class="slider round"></span>
                                    </label>
                                    <a href="/admin/products/{{$product->id}}" data-toggle="tooltip" title="View" class="btn btn-info btn-rounded text-center"><i class="fa fa-eye"></i> </a> 
                                    <a href="/admin/products/{{$product->id}}/edit" data-toggle="tooltip" title="Edit" class="btn btn-info btn-rounded text-center"><i class="fa fa-edit"></i> </a> 
                                    <a href="javascript:void(0)" delete_id="{{$product->id}}" table="products" class="deleterow" class="btn btn-info btn-rounded text-center"><i class="fa fa-trash-o"></i></a>
                                </td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function checkStatus(obj) {
        var id = $(obj).attr("id");
        var checked = $(obj).is(':checked');
        if (checked == true) {
            var status = 1;
        } else {
            var status = 0;
        }
        $.ajax({
            url: '/admin/change_status',
            type: 'post',
            dataType: 'json',
            data: {method: 'changeProductStatus', status: status, id: id},
            cache: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
        }).done(function (response) {
            if (response.status == '1') {
                //                location.reload();
            } else {
                alert("Some error occured, please try again");
            }
        });
    }
</script>
@endsection