<?php

namespace App\Http\Controllers;

use App\Model\Pages;
use Illuminate\Http\Request;

class PagesController extends Controller {

    public function __construct() {
      //  $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($slug="") {
        $pages = Pages::where('url_slug', $slug)->first();
        //$active = Pages::get();
        // print_r($active);
        return view('pages', ['pages' => $pages]);
    }
}