@extends('layouts.admin')
@section('content')
<style type="text/css">
    fieldset {    border: 1px solid #1caf9a!important;
                  padding: 20px!important;
                  margin: 20px!important;
    }
    legend {
        padding: 0.2em 0.5em!important;
        border:1px solid #1caf9a!important;
        color:#1caf9a!important;
        font-size: 21px!important;
        width:10%!important;
    }

</style>
<div class="page-content-wrap">
    @include('partials.errors')
    @include('partials.success')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-plus"></i> Add Product</h3>
                </div>
                <div class="tabs">
                    <form action="{{route('products.store')}}" method="post" enctype="multipart/form-data" id="productForm" class="form-horizontal">
                        {{csrf_field()}}
                        <ul class="nav nav-tabs">

                        </ul>
                        <div class="tab-content panel-body">
                            <div class="tab-pane active" id="tab-general">
                                <div class="tab-content">
                                    <fieldset>
                                        <legend>General</legend>
                                        <div class="form-group required">
                                            <label class="col-sm-2 control-label">Product Name</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="name" placeholder="Product Name" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="form-group required">
                                            <label class="col-sm-2 control-label">Product Price</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="price" placeholder="Product Price" class="form-control floatOnly" required>
                                            </div>
                                        </div>
                                        <div class="form-group required">
                                            <label class="col-sm-2 control-label">Quantity</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="quantity" placeholder="Quantity" class="form-control numericOnly" required>
                                            </div>
                                        </div>
                                        <div class="form-group required">
                                            <label class="col-sm-2 control-label">Delivery Charge</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="delivery_charge" placeholder="Delivery Charge" class="form-control numericOnly" required>
                                            </div>
                                        </div>
                                        <div class="form-group required">
                                            <label class="col-sm-2 control-label">Is Featured</label>
                                            <div class="col-sm-10">
                                                <select class="form-control" name="is_featured">
                                                    <option value="" selected disabled>Select</option>
                                                    <option value="1">YES</option>
                                                    <option value="0" selected>NO</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group required">
                                            <label class="col-sm-2 control-label">Is Recommend</label>
                                            <div class="col-sm-10">
                                                <select class="form-control" name="is_recommend">
                                                    <option value="" selected disabled>Select</option>
                                                    <option value="1">YES</option>
                                                    <option value="0" selected>NO</option>
                                                </select>
                                            </div>
                                        </div>
                                         <div class="form-group required">
                                            <label class="col-sm-2 control-label">Short Description</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="short_desc" placeholder="Short Description" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Description</label>
                                            <div class="col-sm-10">
                                                <textarea name="description" class="summernote" placeholder="Description"></textarea>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset>
                                        <legend  style="width:30%!important;padding-left: 50px!important;">Category & Attributes</legend>

                                        <div class="form-group required">
                                            <label class="col-sm-2 control-label" for="input-model">Category</label>
                                            <div class="col-sm-10">
                                                <select class="form-control" name="category_id" onchange="fetchAttributes(this);fetchCatBrands(this)" required>
                                                    <option disabled selected>Select</option>
                                                    @if($categories)
                                                    @foreach($categories as $key=>$category)
                                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                                    @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group required">
                                            <label class="col-sm-2 control-label" for="input-model">Manufacturer</label>
                                            <div class="col-sm-10">
                                                <select class="form-control brand_id" name="brand_id">
                                                    <option disabled selected>Select</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="attributediv">
                                            <div class="attribute_div">
                                                <p>Select Attributes</p>    
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset>
                                        <legend  style="width:30%!important;padding-left:60px!important;">Discount and Tax</legend>
                                        <div class="form-group required">
                                            <label class="col-sm-2 control-label">Tax (%)</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="tax" placeholder="Tax" class="form-control floatOnly percentage" value="0">
                                            </div>
                                        </div>
                                        <div class="form-group required">
                                            <label class="col-sm-2 control-label">Discount (%)</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="discount_percent" placeholder="Discount" class="form-control floatOnly percentage" value="0">
                                            </div>
                                        </div>
                                        <!-- <div class="form-group required">
                                            <label class="col-sm-2 control-label">Quantity </label>
                                            <div class="col-sm-10">
                                                <input type="text" name="discount_quantity" placeholder="Quantity" class="form-control numericOnly" value="0">
                                            </div>
                                        </div> -->
                                        <div class="form-group required">
                                            <label class="col-sm-2 control-label">Start Date</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="discount_start_date" placeholder="mm/dd/yyyy" class="form-control datepicker" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group required">
                                            <label class="col-sm-2 control-label">End Date</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="discount_end_date" placeholder="mm/dd/yyyy" class="form-control datepicker" readonly>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset>
                                        <legend  style="padding-left:28px!important;">Data</legend>

                                        <div class="form-group required">
                                            <label class="col-sm-2 control-label" >Sku Code</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="sku_code" placeholder="Sku Code" class="form-control" value="<?= 'SKU' . time(); ?>">
                                            </div>
                                        </div>
                                        <div class="form-group required">
                                            <label class="col-sm-2 control-label" >Hsn Code </label>
                                            <div class="col-sm-10">
                                                <input type="text" name="hsn_code" placeholder="Hsn Code" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group required">
                                            <label class="col-sm-2 control-label" >Other Code </label>
                                            <div class="col-sm-10">
                                                <input type="text" name="other_code" placeholder="Other Code" class="form-control">
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset>
                                        <legend style="padding-left:20px!important;">Image</legend>
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <td class="text-left">Image<span style="color: #ff0000; float: right;">* Image Height and Width Should be same.</span></td>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                    <tr>
                                                        <td class="">
                                                            <input type="file" name="image" class="file" data-preview-file-type="any"/>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="table-responsive">
                                            <table id="images" class="table table-striped table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <td class="text-left">Additional Images</td>
                                                        <td class="text-right">Preview</td>
                                                        <td></td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <td colspan="2"></td>
                                                        <td class="text-left">
                                                            <button type="button" onclick="addImage();" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Add Image">
                                                                <i class="fa fa-plus-circle"></i>
                                                            </button>
                                                        </td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">                                                                        
                            <button type="submit" class="btn btn-info pull-right">Publish <span class="fa fa-save"></span></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $("#productForm").validate();
//    $("#savePrd").click(function () {
//        jQuery.validator.defaults.ignore = ":hidden";
//        $("#productForm").data("validator").settings.ignore = "";
//        var validator = $("#productForm").validate();
//        var isValid = $("#productForm").valid();
//        if (!isValid) {
//            var tab_id = $(validator.errorList[0].element).closest('.tab-pane').attr('id');
//            $('ul li').removeClass('active');
//            $('.tab-pane').removeClass('active');
//            $('.' + tab_id).addClass('active');
//            $('#' + tab_id).addClass('active');
//        }
//    });

    $(document.body).on('click', function () {
        $(".text-left").find('input[type=file]').attr('name', 'product_images[]');

    });
    function fetchAttributes(objRef) {
        var obj = $(objRef);
        var category_id = obj.find('option:selected').val();

        $.ajax({
            url: '/admin/get_attributes',
            data: {category_id: category_id},
            type: 'POST',
            dataType: 'json',
            cache: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
        }).done(function (response) {
            var string = '<div class="row">';
            if (response.status == '1') {
                var k = 0;
                $.each(response.data, function (i, v) {

                    string += '  <div class="col-lg-4">'
                            + '<div class="col-sm-4">'
                            + '<label class="">' + v.attribute_name + '</label>    '
                            + '</div>'
                            + '<div class="col-sm-8">';

                    if (v.attribute_type === "INPUT") {
                        string += '   <input type="text" class="form-control" name="values[' + k + '][attr_val]" placeholder="Enter ' + v.attribute_name + '">';
                    } else if (v.attribute_type === "TOGGLE") {

                        string += '<select class="form-control" name="values[' + k + '][attr_val]">';
                        string += "<option value='YES'>YES</option>";
                        string += "<option value='NO'>NO</option>";
                        string += '</select>';
                    } else {

                        string += '<select class="form-control" name="values[' + k + '][attr_val]">';
                        $.each(v.attribute_values, function (index, value) {
                            string += "<option value='" + value.id + "'>" + value.attribute_value + "</option>";
                        });
                        string += '</select>';
                    }

                    string += '</div>';

                    /*
                     * hidden fields like attribtue id is stored at the end of the div
                     */
                    string += '   <input type="hidden" class="form-control" name="values[' + k + '][attr_id]" value=' + v.attribute_id + '>';
                    string += '   <input type="hidden" class="form-control" name="values[' + k + '][attr_type]" value=' + v.attribute_type + '>';
                    string += '</div>';

                    k++;

                });
                string += '</div>';
                $(".attribute_div").html(string);
            } else {
                string += "No Attribute Found";
                $(".attribute_div").html(string);
            }
        });

    }

    function fetchCatBrands(objRef) {
        var $obj = $(objRef);
        var category_id = $obj.find('option:selected').val();
        $.ajax({
            url: '/admin/get_brands',
            data: {category_id: category_id},
            type: 'POST',
            dataType: 'json',
            cache: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
        }).done(function (response) {
            var string = "";
            if (response.status == '1') {
                string += "<option disabled selected>Select Brand</option>";
                $.each(response.data, function (i, v) {
                    string += "<option value='" + v.brand_id + "'>" + v.name + "</option>";
                });
                $(".brand_id").html(string);

            } else {
                string += "<option value=''>No Brand Found</option>";
                $(".brand_id").html(string);
            }
        });
    }
</script>
@endsection