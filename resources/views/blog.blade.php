<style type="text/css">
  .owl-dot{
    display: none!important;
}
.owl-nav{
    display: none!important;
}
.thumb{
   margin-top: 10px;
}
</style>


@extends('layouts.default')
@section('content')
    <!-- =====  CONTAINER START  ===== -->
	
	 <div class="slider-section fullwidth">
        <div class="container-fluid">
          <div class="row">
			   <!-- =====  BANNER STRAT  ===== -->
          <div class="col-sm-12 col-md-12 col-lg-12 nopadding">
            <div class="banner">
              <div class="main-banner owl-carousel">
                <div class="item" style="background:url(<?= url('/'); ?>/frontassets/images/slider-30.jpg)">
                            <!-- <div class="caption">
                                <p>Exclusive Offers - 20% off this weak</p>
                                <h2>Handmade Jewellery</h2>
                                <p>Silk floral hairpieces have become the rage of contemporary weddings.</p>
                                <p><a href="#" class="banner-button">SHOP NOW</a></p>
                            </div> -->
                        </div>
              </div>
            </div>
          </div>
		  </div>
          <!-- =====  BANNER END  ===== -->
          </div>
        </div>
	  
    <div class="container">
	  
	   <!-- =====  Blog ===== -->
	   <div class="row white-bg mb_20">
        <div class="col-sm-12 col-md-12 col-lg-12">
       
          <div id="Blog" class="">
            <div class="heading-part mt_10 ">
              <h2 class="main_title">Blog</h2>
            </div>
          
              <div class="row">
                 @if($blogs)
                 @foreach($blogs as $key=>$value)
              <div class="col-sm-4">
                 <div class="item">
                  <div class="box-holder">
                    <div class="thumb post-img">
                      <a href="/blog_detail/{{$value->id}}"> 
                        <img src="<?= url('/'); ?>/images/testimonialsimages/{{$value->image}}" alt="HealthCare" style="    width: 410px;height: 218px;"> 
                      </a> </div>
                    <div class="post-info mtb_20 ">
                      <h6 class="mb_10 text-uppercase"> <a href="blogdetail.php">{{$value->name}}.</a> </h6>
                      <p>{{$value->description}}.</p>
                      <div class="date-time">
                        <div class="day"> 11</div>
                        <div class="month">Aug</div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              @endforeach
              @endif

              <!-- <div class="col-sm-4">
                 <div class="item">
                  <div class="box-holder">
                    <div class="thumb post-img"><a href="#"> <img src="images/blog/blog_img_02.jpg" alt="HealthCare"> </a></div>
                    <div class="post-info mtb_20 ">
                      <h6 class="mb_10 text-uppercase"> <a href="blogdetail.php">Unknown printer took a galley book.</a> </h6>
                      <p>Aliquam egestas pellentesque est. Etiam a orci Nulla id enim feugiat ligula ullamcorper scelerisque. Morbi eu luctus nisl.</p>
                      <div class="date-time">
                        <div class="day"> 11</div>
                        <div class="month">Aug</div>
                      </div>
                    </div>
                  </div>
                </div>
              </div> -->
              
              </div>
        </div>
	   </div>
      </div>
	    <!-- =====  Blog end ===== -->
	   
    </div>
    <!-- =====  CONTAINER END  ===== -->
    @endsection
	