<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Orderaddress extends Model {

    protected $table = 'order_address';
    protected $fillable = array(
        'order_id',
        'customer_id',
        'firstname',
        'lastname',
        'company',
        'address_1',
        'address_2',
        'city',
        'postcode',
        'country_id',
        'zone_id',
        'custom_field'
    );

}
