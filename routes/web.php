<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */
Route::get('/admin', function () {
    return view('auth/login');
});

Auth::routes();

Route::get('/', 'HomeController@index');
Route::get('/register', 'HomeController@register');
Route::get('/loginuser', 'HomeController@login');
Route::get('/loginuser/{id}', 'HomeController@login');
Route::get('/logoutuser', 'HomeController@logoutuser');
Route::get('/forgetpass', 'HomeController@forgetpass');
Route::get('/forgetpass/{id}', 'HomeController@forgetpass');
Route::post('/forgetpasswo', 'HomeController@frgtpassword');
Route::get('/pages/{slug}', 'PagesController@index');

Route::get('/mybusinessassociate', 'AccountsController@mybusinessassociate');
Route::get('/myaccount', 'AccountsController@myaccount');
Route::post('/update_profile/{id}', 'AccountsController@update_profile');
Route::get('/mywishlist', 'AccountsController@mywishlist');
Route::get('/myorder', 'AccountsController@myorder');
Route::get('/order_detail/{id}', 'AccountsController@order_detail');
Route::get('/return_request/{id}', 'AccountsController@return_request');

Route::get('/myreward', 'AccountsController@myreward');
Route::get('/myaddress', 'AccountsController@myaddress');
Route::post('/add_address', 'AccountsController@add_address');
Route::post('/update_address/{id}', 'AccountsController@update_address');
Route::post('/set_default_address/{id}', 'AccountsController@set_default_address');
Route::get('/mydownline', 'AccountsController@mydownline');
Route::get('/myreturn', 'AccountsController@myreturn');
Route::get('/mycart', 'AccountsController@mycart');

Route::get('/admin/all_orders', 'OrdersController@all_orders');
Route::get('/admin/orders/order_detail/{id}', 'OrdersController@order_detail');
Route::get('/admin/orders/invoice/{id}', 'OrdersController@invoice');

Route::post('/admin/filter_orders', 'OrdersController@filter_orders');
Route::get('/admin/filter_orders', 'OrdersController@filter_orders');

Route::get('/checkout', 'OrdersController@checkout');
Route::get('/success', 'OrdersController@order_success');
Route::get('/success/{id}', 'OrdersController@order_success');
Route::get('/failure', 'OrdersController@order_failure');
Route::get('/failure/{id}', 'OrdersController@order_failure');

Route::post('/registeruser', 'HomeController@registeruser');
Route::post('/loginuser', 'HomeController@loginuser');
Route::post('/loginuser/{id}', 'HomeController@loginuser');
Route::post('/phoneotp', 'HomeController@phoneotp');


Route::get('/products/{id}', 'HomeController@products');
Route::get('/searched_products/{id}', 'HomeController@searched_products');
Route::get('/product_detail/{id}', 'HomeController@product_detail');

Route::get('/blog', 'HomeController@blog');
Route::get('/blog_detail/{id}', 'HomeController@blog_detail');

Route::post('/admin/delete', 'AjaxController@delete');


Route::resources([
    '/admin/dashboard' => 'DashboardController',
    '/admin/categories' => 'CategoriesController',
    '/admin/brands' => 'BrandsController',
    '/admin/attributes' => 'AttributesController',
    '/admin/mappings' => 'MappingsController',
    '/admin/products' => 'ProductsController',
    '/orders' => 'OrdersController',
]);

Route::post('/admin/add_cat_brands', 'MappingsController@add_cat_brands');
Route::post('/admin/add_cat_attributes', 'MappingsController@add_cat_attributes');
Route::get('/admin/category_attribute_mapping', 'MappingsController@category_attribute_mapping');
Route::get('/admin/category_brand_mapping', 'MappingsController@category_brand_mapping');
Route::get('/admin/customers', 'CustomersController@index');
Route::get('/admin/customers/edit/{id}', 'CustomersController@edit');
Route::post('/admin/customers', 'CustomersController@filtered_customers');
Route::get('/admin/add-customer', 'CustomersController@addCustomer');

Route::get('/admin/samplepages', 'samplepagesController@add');
Route::post('/admin/add_sample_page', 'samplepagesController@store');
Route::get('/admin/view_sample_pages', 'samplepagesController@view');
Route::get('/admin/pages/edit/{id}', 'samplepagesController@edit');
Route::get('/admin/pages/delete/{id}', 'samplepagesController@delete');

Route::get('/admin/testimonials', 'TestimonialsController@index');
Route::post('/admin/testimonials/insert', 'TestimonialsController@insert');
Route::get('/admin/view_testimonials', 'TestimonialsController@view');
Route::get('/admin/testimonials/edit/{id}', 'TestimonialsController@edit');
Route::get('/admin/testimonials/delete/{id}', 'TestimonialsController@delete');

Route::get('/admin/advertisement', 'AdvertiseController@index');
Route::post('/admin/advert/insert', 'AdvertiseController@insert');
Route::get('/admin/view_advertisement', 'AdvertiseController@view');
Route::get('/admin/advert/edit/{id}', 'AdvertiseController@edit');
Route::get('/admin/advert/delete/{id}', 'AdvertiseController@delete');

Route::get('/admin/blog', 'BlogController@index');
Route::post('/admin/blog/insert', 'BlogController@insert');
Route::get('/admin/view_blog', 'BlogController@view');
Route::get('/admin/blog/edit/{id}', 'BlogController@edit');
Route::get('/admin/blog/delete/{id}', 'BlogController@delete');

Route::post('/admin/orders/order_history/', 'OrdersController@order_history');
Route::get('/admin/achiever', 'AchieverController@index');
Route::post('/admin/achiever/insert', 'AchieverController@insert');
Route::get('/admin/view_achiever', 'AchieverController@view');
Route::get('/admin/achiever/edit/{id}', 'AchieverController@edit');
Route::get('/admin/achiever/delete/{id}', 'AchieverController@delete');

Route::get('/admin/sponcered', 'SponceredController@index');
Route::post('/admin/sponcered/insert', 'SponceredController@insert');
Route::get('/admin/view_sponcered', 'SponceredController@view');
Route::get('/admin/sponcered/edit/{id}', 'SponceredController@edit');
Route::get('/admin/sponcered/delete/{id}', 'SponceredController@delete');

//Route::get('/admin/advertisement', 'AdvertisementController@view');
//Route::post('/admin/advert/insert', 'AdvertisementController@insert');
Route::get('/admin/advert/category', 'AdvertisementController@category');
Route::get('/admin/advert/category_insert', 'AdvertisementController@category_insert');
Route::post('/admin/advert/cat_store', 'AdvertisementController@cat_store');
Route::get('/admin/advert/update_category/{id}', 'AdvertisementController@update_category');
Route::get('/admin/advert/deleteCat/{id}', 'AdvertisementController@delete_category');

Route::post('/admin/filter_products', 'ProductsController@filter_products');
Route::get('/admin/filter_products', 'ProductsController@filter_products');
Route::get('/admin/featured_products', 'ProductsController@featured_products');
Route::get('/admin/top_products', 'ProductsController@top_products');
Route::post('/admin/remove_product_image', 'ProductsController@remove_product_image');
Route::post('/admin/remove_product_attribute', 'ProductsController@remove_product_attribute');
Route::get('/admin/reviews', 'ProductsController@reviews');

Route::post('/admin/change_status', 'AjaxController@change_status');
Route::post('/admin/get_brands', 'AjaxController@get_brands');
Route::post('/admin/get_attributes', 'AjaxController@get_attributes');
Route::post('/addtocart', 'AjaxController@addtocart');
Route::post('/updatecart', 'AjaxController@updatecart');
Route::post('/addtowishlist', 'AjaxController@addtowishlist');
Route::post('/removefromwishlist', 'AjaxController@removefromwishlist');
Route::post('/getaddress', 'AjaxController@getaddress');
Route::post('/deleteaddress', 'AjaxController@delete_address');
Route::post('/removefromcart', 'AjaxController@removefromcart');
Route::post('/addreview', 'AjaxController@addreview');
Route::post('/filterproducts', 'AjaxController@filterproducts');
Route::post('/admin/delete_cat_brands', 'AjaxController@delete_cat_brands');
Route::post('/admin/delete_cat_attr', 'AjaxController@delete_cat_attr');
Route::post('/do_search', 'AjaxController@do_search');
Route::post('/admin/approve_review', 'AjaxController@approve_review');
Route::post('/admin/disapprove_review', 'AjaxController@disapprove_review');

Route::post('/getproductsuggestion', 'AjaxController@getproductsuggestion');

Route::post('/admin/get_cat_brands', 'MappingsController@get_cat_brands');
Route::post('/admin/get_cat_attributes', 'MappingsController@get_cat_attributes');

Route::get('/cookie/set', 'CookieController@setCookie');
Route::get('/cookie/get', 'CookieController@getCookie');


// route for processing payment
Route::post('paypal', 'PaymentController@payWithpaypal');
// route for check status of the payment
Route::get('status', 'PaymentController@getPaymentStatus');
