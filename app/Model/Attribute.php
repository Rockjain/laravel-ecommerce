<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Attribute extends Model {

    protected $table = 'attributes';
    protected $fillable = array(
        'name',
        'type',
        'status',
        'created_at',
        'updated_at'
    );

    public function attributeValues() {
        return $this->hasMany('App\Model\Attributevalue', 'attribute_id');
    }

}
