@extends('layouts.account')
@section('content')
<div class="container mtb_30">

    <div class="row">
        <div class="col-sm-4 col-md-3" id="column-left">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="panel panel-default account-sidebar">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <?php
                                    $session = Session::get('user_data');
                                    if (!empty($session)):
                                        $customerInfo = getCustomer($session['customer_id']);
                                        if ($customerInfo):
                                            $fileInfo = getFile($customerInfo->photo);
                                            if ($fileInfo) {
                                                $photo = $fileInfo->file_path . $fileInfo->file_name;
                                            } else {
                                                $photo = url('/') . '/frontassets/images/no_img.png';
                                            }
                                        endif;
                                    endif;
                                    ?>
                                    <div class="pull-left"><p><img src="<?= $photo; ?>" alt="user" /></p></div>
                                    <div class="pull-left"><p>Welcome<br/> <strong><?= $customerInfo->firstname . ' ' . $customerInfo->lastname; ?></strong></p>
                                        <p>ID: <strong><?= 'AAIMA' . $customerInfo->id; ?></strong></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body account-sidebar-menu">
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                       <ul class="list-group">
                                        <li class="list-group-item"><a href="/myaccount"><i class="fa fa-user"></i> My Profile <span class="badge"><i class="fa fa-angle-right"></i></span></a></li>
                                        <li class="list-group-item"><a href="/myaddress"><i class="fa fa-map-marker"></i> Manage Addresses <span class="badge"><i class="fa fa-angle-right"></i></span></a></li>
                                     <!--    <li class="list-group-item"><a href="/myreward"><i class="fa fa-trophy"></i> My Rewards <span class="badge"><i class="fa fa-angle-right"></i></span></a></li> -->
                                        <li class="list-group-item"><a href="/mybusinessassociate"><i class="fa fa-sitemap"></i> My Business Associate <span class="badge"><i class="fa fa-angle-right"></i></span></a></li>
                                        <li class="list-group-item"><a href="/mywishlist" class="active"><i class="fa fa-heart"></i> My Wishlist <span class="badge"><i class="fa fa-angle-right"></i></span></a></li>
                                        <li class="list-group-item"><a href="/myorder" ><i class="fa fa-list"></i> My Orders <span class="badge"><i class="fa fa-angle-right"></i></span></a></li>
                                    <!--     <li class="list-group-item"><a href="/myreturn"><i class="fa fa-retweet"></i> My Returns <span class="badge"><i class="fa fa-angle-right"></i></span></a></li> -->
                                        <li class="list-group-item"><a href="/logoutuser" onclick="return confirm('Are you sure, you want to logout ?')" class="logout"><i class="fa fa-power-off"></i> Logout</a></li>
                                    </ul> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="col-sm-8 col-md-9" id="column-right">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="panel panel-default account-content">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <h3>My Wishlist</h3>   
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                @if($wishlist)
                                <div class="col-sm-12 col-md-12">
                                    <ul class="wishlist">
                                        @foreach($wishlist as $list)
                                        <li class="item product-layout-left mb_20">
                                            <div class="product-list col-xs-3 col-md-2">
                                                <div class="product-thumb">
                                                    <div class="image product-imageblock"> <a href="/product_detail/{{$list->product_id}}"> 
                                                            <img class="img-responsive" title="{{$list->productDetail->name}}" src="{{!empty($list->productDetail->fileDetail)?$list->productDetail->fileDetail->file_path.$list->productDetail->fileDetail->file_name:url('/').'frontassets/images/no_img.png'}}" style="height: 85px;"> 
                                                            <img class="img-responsive" title="{{$list->productDetail->name}}" src="{{!empty($list->productDetail->fileDetail)?$list->productDetail->fileDetail->file_path.$list->productDetail->fileDetail->file_name:url('/').'frontassets/images/no_img.png'}}" style="height: 85px;"> 
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-9 col-md-10">
                                                <div class="caption product-detail">
                                                    <h6 class="product-name"><a href="/product_detail/{{$list->product_id}}">{{$list->productDetail->name}}</a></h6>
                                                    <div class="rating">
                                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                    </div>
                                                    <span class="price"><span class="amount"><span class="currencySymbol">&#8377; </span>{{$list->productDetail->price}}</span>
                                                    </span>
                                                    <div class="add-to-cart"><a href="javascript:void(0)" id="{{$list->id}}" product_id="{{$list->product_id}}" onclick="movetocart(this)"><span>Move To Cart <i class="fa fa-shopping-cart"></i></span></a></div>
                                                    <p class="remove-wishlist" id="{{$list->id}}" onclick="removefromwishlist(this)"><i class="fa fa-trash" data-toggle="tooltip" title="Remove From Wishlist"></i></p>
                                                </div>
                                            </div>
                                        </li>
                                        @endforeach
                                    </ul>
                                </div>
                                @else
                                <div class="col-sm-12 col-md-12">
                                    <h3 style="text-align: center">No product in wishlist</h3>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection