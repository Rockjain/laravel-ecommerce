<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model {

    protected $table = 'product_images';
    protected $fillable = array(
        'product_id',
        'image',
        'created_at',
        'updated_at'
    );

    public function fileDetail() {
        return $this->belongsTo('App\Model\Appfiles', 'image');
    }

}
