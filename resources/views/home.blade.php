@extends('layouts.admin')
@section('content')
<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap" id="home-page">
    <div class="row">
        <div class="col-md-12">
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="widget widget-primary widget-item-icon">
                <a href="/admin/customers">
                    <div class="widget-item-left">
                        <span class="fa fa-user"></span>
                    </div>
                    <div class="widget-data">
                        <div class="widget-int num-count">{{$total_customer}}</div>
                        <div class="widget-title">Registred users</div>
                        <div class="widget-subtitle">On our website</div>
                    </div>
                </a>                      
            </div>

        </div>

        <div class="col-md-3">

            <div class="widget widget-success widget-item-icon">
                <a href="/admin/all_orders">
                    <div class="widget-item-left">
                        <span class="fa fa-globe"></span>
                    </div>
                    <div class="widget-data">
                        <div class="widget-int num-count">{{$total_order}}</div>
                        <div class="widget-title">Total Orders</div>
                        <div class="widget-subtitle">That Orders on our site </div>
                    </div>
                </a>                     
            </div>

        </div>

        <div class="col-md-3">

            <div class="widget widget-info widget-item-icon">
                <a href="">
                    <div class="widget-item-right">
                        <span class="fa fa-sitemap"></span>
                    </div>                             
                    <div class="widget-data-left">
                        <div class="widget-int num-count">0</div>
                        <div class="widget-title">Total Vendors</div>
                        <div class="widget-subtitle">On our website</div>
                    </div>   
                </a>                                  
            </div>

        </div>

        <div class="col-md-3">

            <div class="widget widget-danger widget-item-icon">
                <a href="/admin/products">
                    <div class="widget-item-right">
                        <span class="fa fa-sitemap"></span>
                    </div>                             
                    <div class="widget-data-left">
                        <div class="widget-int num-count">{{$total_product}}</div>
                        <div class="widget-title">Total Products</div>
                        <div class="widget-subtitle">in our website</div>
                    </div>  
                </a>                                   
            </div>
        </div>
    </div>
</div>
@endsection
