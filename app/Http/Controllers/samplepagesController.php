<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;




class samplepagesController extends Controller
{
    public function add() {
        // $pages = DB::table('pages')->select('*')->get();
        return view('samplepages.index');
    }
    public function store()
    {
    	extract($_POST);
    	$url_slug = str_replace(" ","_",$title);
    	if($form_type=='add'){
    	$data=array('title'=>$title,'url_slug'=>$url_slug,'description'=>$description,'meta_title'=>$meta_title,'meta_keyword'=>$meta_keyword,'meta_description'=>$meta_description,'status'=>$status,'priority'=>$priority);
    	$data = DB::table('pages')->insert($data);
    echo"$data";
    }
    if($form_type=='update'){
    	$data = DB::table('pages')->where('id', $id)->update([
                'title'=>$title,'url_slug'=>$url_slug,'description'=>$description,'meta_title'=>$meta_title,'meta_keyword'=>$meta_keyword,'meta_description'=>$meta_description,'status'=>$status,'priority'=>$priority
            ]);
    	if($data){
    		echo 'updated';
    	}else{
    		echo 2;
    	}
    }
	
    }

    public function delete(Request $request){
    	$id=$request->id;
    	$status = DB::table('pages')->delete($id);
    	if($status){
    		$pages = DB::table('pages')->select('*')->orderby('id', 'DESC')->get();
        return view('samplepages.view', ['pages' => $pages]);
    	}
    }



    public function view() {
        $pages = DB::table('pages')->select('*')->orderby('id', 'DESC')->get();
        return view('samplepages.view', ['pages' => $pages]);
    }
    public function edit(Request $request) {

    	$id=$request->id;
        $pages = DB::table('pages')->select('*')->where('id',$id)->get();
        return view('samplepages.index', ['pages' => $pages]);
    }
}
