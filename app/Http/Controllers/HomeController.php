<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use App\Model\Product;
use App\Model\State;
use App\Model\Country;
use App\Model\Customer;
use App\Model\Address;
use App\Model\Category;
use App\Model\Brand;
use App\Model\ProductReview;

class HomeController extends Controller {

    public function index(Request $request) {
        $user_data = $request->session()->get('user_data');
        $testimonials = DB::table('testimonials')->select('*')->where('status', '1')->get();

        $advertisements = DB::table('advertisements')->where('status', '1')->get();
                 $blogs = DB::table('blogs')->where('status', '1')->get();
              $achiever = DB::table('achievers')->where('status', '1')->get();
              $sponcered = DB::table('sponcered')->where('status', '1')->get();
              $banner = DB::table('banners')->where('status', '1')->get();

        $featured = Product::where(['is_featured' => '1', 'status' => '1'])->get();
        if ($featured):
            foreach ($featured as $key => $row):
                $rating = ProductReview::where(['product_id' => $row->id])->avg('rating');
                $featured[$key]['rating'] = round($rating);
            endforeach;
        endif;
        $products = Product::where(['status' => '1'])->orderBy('id', 'desc')->get();
        if ($products):
            foreach ($products as $key => $row):
                $rating = ProductReview::where(['product_id' => $row->id])->avg('rating');
                $products[$key]['rating'] = round($rating);
            endforeach;
        endif;

        $newitems = Product::where(['status' => '1'])->orderBy('updated_at', 'desc')->limit(12)->get();
        $newitemscount = count($newitems);
        if ($newitems):
            foreach ($newitems as $key => $row):
                $rating = ProductReview::where(['product_id' => $row->id])->avg('rating');
                $newitems[$key]['rating'] = round($rating);
            endforeach;
        endif;

        $recommenditems = Product::where(['status' => '1', 'is_recommend' => '1'])->orderBy('updated_at', 'desc')->limit(12)->get();
        $recommenditemscount = count($recommenditems);
        if ($recommenditems):
            foreach ($recommenditems as $key => $row):
                $rating = ProductReview::where(['product_id' => $row->id])->avg('rating');
                $recommenditems[$key]['rating'] = round($rating);
            endforeach;
        endif;

        $recentproducts = Product::where(['status' => '1'])->orderBy('visit_count', 'desc')->limit(10)->get();
        $recentproductcount = count($recentproducts);
        if ($recentproducts):
            foreach ($recentproducts as $key => $row):
                $rating = ProductReview::where(['product_id' => $row->id])->avg('rating');
                $recentproducts[$key]['rating'] = round($rating);
            endforeach;
        endif;
        $bestsellerproducts = Product::where(['status' => '1'])->orderBy('sold_count', 'desc')->limit(10)->get();
        $bestsellerproductcount = count($bestsellerproducts);
        if ($bestsellerproducts):
            foreach ($bestsellerproducts as $key => $row):
                $rating = ProductReview::where(['product_id' => $row->id])->avg('rating');
                $bestsellerproducts[$key]['rating'] = round($rating);
            endforeach;
        endif;
        $categories = Category::where(['status' => '1', 'parent_id' => '0'])->get();

        $featuredbrands = Brand::where(['is_featured' => '1', 'status' => '1'])->get();

        return view('index', ['testimonials' => $testimonials,
            'featured' => $featured,
            'products' => $products,
            'newitems' => $newitems,
            'newitemscount' => $newitemscount,
            'recentproducts' => $recentproducts,
            'recentproductcount' => $recentproductcount,
            'recommenditems' => $recommenditems,
            'recommenditemscount' => $recommenditemscount,
            'bestsellerproducts' => $bestsellerproducts,
            'bestsellerproductcount' => $bestsellerproductcount,
            'categories' => $categories,
            'featuredbrands' => $featuredbrands,
            'advertisements' => $advertisements,
            'blogs' => $blogs,
            'achiever' => $achiever,
            'sponcered' => $sponcered,
            'banner' => $banner,
            'user_data' => $user_data
        ]);
    }

    public function register(Request $request) {

        $user_data = $request->session()->get('user_data');
        if ($user_data) {
            return redirect('/');
        }

        $states = State::where(['country_id' => '1'])->get();
        $countries = Country::get();
        return view('register', ['states' => $states, 'countries' => $countries]);
    }

    public function registeruser(Request $request) {
           $users = Customer::get();
        // echo "<pre>";
        // print_r($users);
       
        $sponser_id= $request->input('sponser_id');
        $referred_by=substr($sponser_id, 5);  
       //$explode=explode("",$sponser_id);
     //  print_r($explode);

        // die;
     
        $insertArr = [
            'firstname' => $request->input('firstname'),
            'lastname' => $request->input('lastname'),
           // 'username' => $request->input('username'),
            'email' => $request->input('email'),
            'telephone' => $request->input('telephone'),
            'referred_by'=>$referred_by,
            //'sponser_id' => $request->input('sponser_id'),
            'password' => md5($request->input('password')),
            'pass' => $request->input('password'),
            'newsletter' => $request->input('newsletter'),
        ];
        $return = Customer::create($insertArr);
        if ($return) {
        	$id=$return->id;

        	$updateArr=[
        		"sponser_id"=>"AAIMA".$id,
        		"username"=>"aaima".$id
        	];

        	$array=[
			'firstname' => $request->input('firstname'),
            'lastname' => $request->input('lastname'),
        	"username"=>"aaima".$id,
            'email' => $request->input('email'),
            'telephone' => $request->input('telephone'),
            'referred_by'=>$referred_by,
        	"sponser_id"=>"AAIMA".$id,
            'password' => md5($request->input('password')),
            'pass' => $request->input('password'),
            'newsletter' => $request->input('newsletter'),
            'address' => $request->input('address'),
                'city' => $request->input('city'),
                'postcode' => $request->input('postcode'),
                'country_id' => $request->input('country_id'),
                'zone_id' => $request->input('zone_id')
        	];
        	$arr = urlencode(serialize($array)); 
        	
       // $sms_url = "http://ba.aaima.org/index.php?route=api/customer/register&details=".$arr;
       

       //  $ch = curl_init($sms_url);
       //  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
       //  curl_setopt($ch, CURLOPT_POST, 1);
       //  curl_setopt($ch, CURLOPT_POSTFIELDS, "");
       //  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 2);
       //  $data = curl_exec($ch);
       //  curl_close($ch);
        // echo $data;
        // exit;

        	        $update = Customer::where('id', $id)->update($updateArr);


            $addressArr = [
                'customer_id' => $return->id,
                'address_1' => $request->input('address'),
                'address_2' => '',
                'city' => $request->input('city'),
                'postcode' => $request->input('postcode'),
                'country_id' => $request->input('country_id'),
                'zone_id' => $request->input('zone_id'),
            ];
            $returnData = Address::create($addressArr);

            $loginData = [
                'customer_id' => $return->id,
                'username' => $return->username,
                'telephone' => $return->telephone,
            ];
            $request->session()->put('user_data', $loginData);

            return redirect('/')->with('success', 'Your account registered successfully');
        }
        return back()->withInput()->with('error', 'Some error occured, please try again');
    }

    public function login($id = null) {
        return view('login', ['id' => $id]);
    }

    public function loginuser(Request $request) {
        $isExist = Customer::where(['username' => $request->input('username'), 'password' => md5($request->input('password'))])->exists();
        if ($isExist) {
            $returnData = Customer::where(['username' => $request->input('username'), 'password' => md5($request->input('password'))])->first();

            $loginData = [
                'customer_id' => $returnData->id,
                'username' => $returnData->username,
                'telephone' => $returnData->telephone,
            ];
            $request->session()->put('user_data', $loginData);

            //move the cart data from cookie to user account
            if (!empty($_COOKIE['customer_cookie'])) {
                \App\Model\Cart::where(['customer_id' => $_COOKIE['customer_cookie']])->update(['customer_id' => $returnData->id]);
            }
            if (!empty($_COOKIE['customer_cookie'])) {
                unset($_COOKIE['customer_cookie']);
            }
            if ($request->input('id') == 'checkout') {
                return redirect('/checkout')->with('success', 'Logged in successfully');
            } else {
                return redirect('/')->with('success', 'Logged in successfully');
            }
        } else {
            return redirect('/loginuser')->with('error', 'Username or Password is incorrect');
        }
    }

    public function products(Request $request, $id = null) {
        $productcount = Product::where(['category_id' => $id])->exists();
        $products = Product::where(['category_id' => $id, 'status' => '1'])->get();
        $categories = Category::where(['status' => '1', 'parent_id' => '0'])->get();

        $category = Category::where([ 'id' => $id])->first();
        $max_price = Product::where(['category_id' => $id])->max('price');
        $min_price = Product::where(['category_id' => $id])->min('price');

        $attribites = \App\Model\CategoryAttribute::where(['category_id' => $id])->get();
//        echo "<pre>";
//        print_r($attribites[0]->attributeDetail->attributeValues);
//        die;
        return view('products', ['products' => $products,
            'productcount' => $productcount,
            'categories' => $categories,
            'min_price' => $min_price,
            'max_price' => $max_price,
            'attribites' => $attribites,
            'category_id' => $id,
            'name'=>$category
        ]);
    }

    public function searched_products(Request $request, $keyword = null) {
        $productcount = Product::where('name', 'like', '%' . $keyword . '%')->count();
        $products = Product::where('name', 'like', '%' . $keyword . '%')->get();
        return view('searched_products', [
            'products' => $products,
            'productcount' => $productcount,
        ]);
    }

    public function product_detail($id = null) {
        //update the view count of product
        $product = Product::where(['id' => $id])->first();
        Product::where('id', $id)->update(['visit_count' => $product->visit_count + 1]);

        $productInfo = Product::where(['id' => $id])->first();
        $relatedproducts = Product::where(['category_id' => $productInfo->category_id])->where('id', '!=', $id)->get();

        $reviewscount = \App\Model\ProductReview::where(['product_id' => $id, 'status' => '1'])->count();
        $reviews = \App\Model\ProductReview::where(['product_id' => $id, 'status' => '1'])->get();

        $attribites = DB::table('category_attribute_mapping')
            ->join('attributes', 'category_attribute_mapping.attribute_id', '=', 'attributes.id')
            //->join('attribute_values', 'attributes.id', '=', 'attribute_values.attribute_id')
            ->select('attributes.*')
            ->where('category_attribute_mapping.category_id', '=', $productInfo->category_id)
            ->get();
        //echo "<pre>";
        //print_r($attribites);
        //die;
        return view('product_detail', ['productInfo' => $productInfo, 'relatedproducts' => $relatedproducts, 'reviewscount' => $reviewscount, 'reviews' => $reviews, 'attribites' => $attribites]);
    }

    public function logoutuser(Request $request) {
        $request->session()->forget(['user_data']);
        return redirect('/')->with('success', 'Logged out successfully');
    }

    public function blog(Request $request) {
        //$products = Product::where(['category_id' => $id, 'status' => '1'])->get();
        $blogs = DB::table('blogs')->where('status', '1')->get();
        return view('blog', ['blogs' => $blogs]);
        
    }

    public function blog_detail($id = null) {
        //$product = Product::where(['id' => $id])->first();
        $blogs = DB::table('blogs')->where('id', $id)->first();
        //echo "<pre>";print_r($blogs);die;
        return view('blogdetail', ['blogs' => $blogs]);
    }
    
    public function forgetpass($id = null) {
        return view('forgetpassword', ['id' => $id]);
    }
    public function frgtpassword(Request $request) {
        
        $telephone = $request->input('telephone');
        $username = $request->input('username');
        $otp = $request->input('otp');
        $hidden_otp = $request->input('hidden_otp');

        if ($otp==$hidden_otp) {
        //$otp = rand(1000, 9999);
        $password = $this->generateRandomString();
         $sms = "Your forgot password send in your mobile $password ";
            $this->sendSms($telephone, $sms);
          $data =  Customer::where(['username' => $username])->update(['password' => md5($password) ]);
            if ($data) {
                return redirect('/forgetpass')->with('success', 'New password send in your mobile');
            } else {
                return redirect()->action('HomeController@forgetpass');
            }
            
        }else{
            return redirect('/forgetpass')->with('error', 'Your OTP is incorrect');
        }
       // return redirect('forgetpassword', ['id' => $id]);  
    }
    public function sendSms($mobile, $sms) {
        $sender = "AAIMAG";
        $workingkey = "nUxIBk39oEmU9tpcnmUmOg";

        $to = '91' . $mobile;

        $ms = rawurlencode($sms);
        $sms_url = "https://www.smsgatewayhub.com/api/mt/SendSMS?APIKey=" . $workingkey . "&senderid=" . $sender . "&channel=2&DCS=0&flashsms=0&number=" . $to . "&text=" . $ms . "&route=21";

        $ch = curl_init($sms_url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 2);
        $data = curl_exec($ch);
        curl_close($ch);
        return true;
    }
    public function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}
public function phoneotp(Request $request) {
    $telephone = $request->input('phone');
    $otp       = $request->input('phoneotp');

    $sms = "Your Phone No. OTP send in your mobile $otp ";
    $this->sendSms($telephone, $sms);
    echo json_encode(array('status'=>true));
    }

}
