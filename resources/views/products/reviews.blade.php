@extends('layouts.admin')
@section('content')
<div class="page-content-wrap" id="reviews-page">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default tabs">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="active"><a href="#review-new-tab" role="tab" data-toggle="tab">Pending Reviews</a></li>
                    <li><a href="#review-all-tab" role="tab" data-toggle="tab">All Approved Reviews</a></li>
                    <li><a href="#review-disapproved-tab" role="tab" data-toggle="tab">Unapproved</a></li>
                </ul>
                <div class="panel-body tab-content">
                    <div class="tab-pane active" id="review-new-tab">
                        <table class="table datatable table-bordered">
                            <thead>
                                <tr>
                                    <th>Review Details</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($pendingreviews)
                                @foreach($pendingreviews as $review)
                                <?php
                                $product = getProduct($review->product_id);
                                ?>
                                <tr>
                                    <td>
                                        <div>
                                            <h5>Product Name : <a href="/admin/products/{{$review->product_id}}" target="_blank"><?= $product->name; ?></a></h5>
                                            <h5>{{$review->name}}</h5>
                                            <p class="product-rating">
                                                <?php if ($review->rating == 1): ?>
                                                    <span class="float-right"></span>
                                                    <span class="float-right"></span>
                                                    <span class="float-right"></span>
                                                    <span class="float-right"></span>
                                                    <span class="float-right"></span>
                                                <?php elseif ($review->rating == 2): ?>
                                                    <span class="float-right"></span>
                                                    <span class="float-right"></span>
                                                    <span class="float-right"></span>
                                                    <span class="float-right"></span>
                                                    <span class="float-right"></span>
                                                <?php elseif ($review->rating == 3): ?>
                                                    <span class="float-right"></span>
                                                    <span class="float-right"></span>
                                                    <span class="float-right"></span>
                                                    <span class="float-right"></span>
                                                    <span class="float-right"></span>
                                                <?php elseif ($review->rating == 4): ?>
                                                    <span class="float-right"></span>
                                                    <span class="float-right"></span>
                                                    <span class="float-right"></span>
                                                    <span class="float-right"></span>
                                                    <span class="float-right"></span>
                                                <?php elseif ($review->rating == 5): ?>
                                                    <span class="float-right"></span>
                                                    <span class="float-right"></span>
                                                    <span class="float-right"></span>
                                                    <span class="float-right"></span>
                                                    <span class="float-right"></span>
                                                <?php endif; ?>
                                            </p>
                                            <p>{{$review->review}}</p>
                                        </div>
                                    </td>
                                    <td>
                                        <p><button class="btn btn-info btn-xs" onclick="approveReview(this,<?= $review->id; ?>)"><i class="fa fa-check"></i> Approve</button></p>
                                        <p><button class="btn btn-danger btn-xs" onclick="disapproveReview(this,<?= $review->id; ?>)"><i class="fa fa-times"></i> Disapprove</button></p>
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane" id="review-all-tab">
                        <table class="table datatable table-bordered">
                            <thead>
                                <tr>
                                    <th>Review Details</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($approvedreviews)
                                @foreach($approvedreviews as $review)
                                <?php
                                $product = getProduct($review->product_id);
                                ?>
                                <tr>
                                    <td>
                                        <div>
                                            <h5>Product Name : <a href="/admin/products/{{$review->product_id}}" target="_blank"><?= $product->name; ?></a></h5>
                                            <h5>{{$review->name}}</h5>
                                            <p class="product-rating">
                                                <?php if ($review->rating == 1): ?>
                                                    <span class="float-right"></span>
                                                    <span class="float-right"></span>
                                                    <span class="float-right"></span>
                                                    <span class="float-right"></span>
                                                    <span class="float-right"></span>
                                                <?php elseif ($review->rating == 2): ?>
                                                    <span class="float-right"></span>
                                                    <span class="float-right"></span>
                                                    <span class="float-right"></span>
                                                    <span class="float-right"></span>
                                                    <span class="float-right"></span>
                                                <?php elseif ($review->rating == 3): ?>
                                                    <span class="float-right"></span>
                                                    <span class="float-right"></span>
                                                    <span class="float-right"></span>
                                                    <span class="float-right"></span>
                                                    <span class="float-right"></span>
                                                <?php elseif ($review->rating == 4): ?>
                                                    <span class="float-right"></span>
                                                    <span class="float-right"></span>
                                                    <span class="float-right"></span>
                                                    <span class="float-right"></span>
                                                    <span class="float-right"></span>
                                                <?php elseif ($review->rating == 5): ?>
                                                    <span class="float-right"></span>
                                                    <span class="float-right"></span>
                                                    <span class="float-right"></span>
                                                    <span class="float-right"></span>
                                                    <span class="float-right"></span>
                                                <?php endif; ?>
                                            </p>
                                            <p>{{$review->review}}</p>
                                        </div>
                                    </td>
                                    <td>
                                        <p><button class="btn btn-danger btn-xs" onclick="disapproveReview(this,<?= $review->id; ?>)"><i class="fa fa-times"></i> Disapprove</button></p>
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane" id="review-disapproved-tab">
                        <table class="table datatable table-bordered">
                            <thead>
                                <tr>
                                    <th>Review Details</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($disapprovedreviews)
                                @foreach($disapprovedreviews as $review)
                                <?php
                                $product = getProduct($review->product_id);
                                ?>
                                <tr>
                                    <td>
                                        <div>
                                            <h5>Product Name : <a href="/admin/products/{{$review->product_id}}" target="_blank"><?= $product->name; ?></a></h5>
                                            <h5>{{$review->name}}</h5>
                                            <p class="product-rating">
                                                <?php if ($review->rating == 1): ?>
                                                    <span class="float-right"></span>
                                                    <span class="float-right"></span>
                                                    <span class="float-right"></span>
                                                    <span class="float-right"></span>
                                                    <span class="float-right"></span>
                                                <?php elseif ($review->rating == 2): ?>
                                                    <span class="float-right"></span>
                                                    <span class="float-right"></span>
                                                    <span class="float-right"></span>
                                                    <span class="float-right"></span>
                                                    <span class="float-right"></span>
                                                <?php elseif ($review->rating == 3): ?>
                                                    <span class="float-right"></span>
                                                    <span class="float-right"></span>
                                                    <span class="float-right"></span>
                                                    <span class="float-right"></span>
                                                    <span class="float-right"></span>
                                                <?php elseif ($review->rating == 4): ?>
                                                    <span class="float-right"></span>
                                                    <span class="float-right"></span>
                                                    <span class="float-right"></span>
                                                    <span class="float-right"></span>
                                                    <span class="float-right"></span>
                                                <?php elseif ($review->rating == 5): ?>
                                                    <span class="float-right"></span>
                                                    <span class="float-right"></span>
                                                    <span class="float-right"></span>
                                                    <span class="float-right"></span>
                                                    <span class="float-right"></span>
                                                <?php endif; ?>
                                            </p>
                                            <p>{{$review->review}}</p>
                                        </div>
                                    </td>
                                    <td>
                                        <p><button class="btn btn-info btn-xs" onclick="approveReview(this,<?= $review->id; ?>)"><i class="fa fa-check"></i> Approve</button></p>
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function approveReview(obj, id) {
        $.ajax({
            url: '/admin/approve_review',
            type: 'post',
            dataType: 'json',
            data: {id: id},
            cache: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
        }).done(function (response) {
            if (response.error_code == 200) {
                window.location.reload();
            } else {
                window.location.reload();
            }
        });
    }
    function disapproveReview(obj, id) {
        $.ajax({
            url: '/admin/disapprove_review',
            type: 'post',
            dataType: 'json',
            data: {id: id},
            cache: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
        }).done(function (response) {
            if (response.error_code == 200) {
                window.location.reload();
            } else {
                window.location.reload();
            }
        });
    }
</script>
@endsection