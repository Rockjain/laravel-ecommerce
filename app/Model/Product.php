<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Product extends Model {

    protected $table = 'products';
    protected $fillable = array(
        'seller_id',
        'sku_code',
        'hsn_code',
        'other_code',
        'name',
        'image',
        'price',
        'delivery_charge',
        'tax',
        'discount_percent',
        'discount_quantity',
        'discount_start_date',
        'discount_end_date',
        'description',
        'short_desc',
        'category_id',
        'brand_id',
        'quantity',
        'is_featured',
        'is_recommend',
        'visit_count',
        'sold_count',
        'status',
        'created_at',
        'updated_at'
    );

    public function fileDetail() {
        return $this->belongsTo('App\Model\Appfiles', 'image');
    }

    public function brandDetail() {
        return $this->belongsTo('App\Model\Brand', 'brand_id');
    }

    public function categoryDetail() {
        return $this->belongsTo('App\Model\Category', 'category_id');
    }

    public function productImages() {
        return $this->hasMany('App\Model\ProductImage', 'product_id');
    }

    public function productAttributes() {
        return $this->hasMany('App\Model\ProductAttribute', 'product_id');
    }

}
