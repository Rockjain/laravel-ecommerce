<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CategoryAttribute extends Model {

    protected $table = 'category_attribute_mapping';
    protected $fillable = array(
        'category_id',
        'attribute_id',
        'is_filter',
        'status',
        'created_at',
        'updated_at'
    );

    public function categoryDetail() {
        return $this->belongsTo('App\Model\Category', 'category_id');
    }
    
    public function attributeDetail() {
        return $this->belongsTo('App\Model\Attribute', 'attribute_id');
    }

}
