@extends('layouts.default')
@section('content')
<div class="container mtb_30">
    <div class="row">
        <div class="col-sm-12 col-sm-offset-1 col-md-10 col-md-offset-1 white-bg">
            <div class="confirmation-wrapper ptb_50 text-center">
                <div class="payment-success">
                    <div class="content">
                        <div class="icon">
                            <i class="fa fa-check-circle text-success"></i>
                        </div>
                        <h2 class="heading uppercase mt-0 text-success">Thank you, your Order is Successfully Placed!</h2>
                        <p>Your Order Id is <span class="text-primary font700">{{$order_id}}</span></p>
                    </div>
                </div>
                <a href="/">
                    <button class="btn btn-primary"><i class="fa fa-shopping-bag"></i>Continue Shopping</button>
                </a>
                <a href="/myorder">
                    <button class="btn btn-primary btn-inverse"><i class="fa fa-list"></i> My Orders</button>
                </a>                             
            </div>
        </div>
    </div>
</div>
@endsection