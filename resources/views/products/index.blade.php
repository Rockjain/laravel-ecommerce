


<meta name="csrf-token" content="{{ csrf_token() }}">
@extends('layouts.admin')
@section('content')
<div class="page-content-wrap">
    @include('partials.errors')
    @include('partials.success')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="well">
                        <div class="row">
                            <form action="/admin/filter_products" method="post">
                                {{csrf_field()}}
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label class="control-label">Product Name</label>
                                        <input type="text" name="name" placeholder="Product Name" class="form-control" value="<?= isset($_POST['name']) ? $_POST['name'] : ''; ?>">
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="control-label">Min Price</label>
                                        <input type="text" name="min_price" placeholder="Min Price" class="form-control floatOnly" value="<?= isset($_POST['min_price']) ? $_POST['min_price'] : ''; ?>">
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="control-label">Max Price</label>
                                        <input type="text" name="max_price" placeholder="Max Price" class="form-control floatOnly" value="<?= isset($_POST['max_price']) ? $_POST['max_price'] : ''; ?>">
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="control-label">Quantity</label>
                                        <input type="text" name="quantity" placeholder="Quantity" class="form-control numericOnly" value="<?= isset($_POST['quantity']) ? $_POST['quantity'] : ''; ?>">
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="control-label">Status</label>
                                        <select name="status" class="form-control">
                                            <option disabled selected>Select</option>
                                            <option value="1" <?php if (isset($_POST['status']) && $_POST['status'] == '1'): ?>selected<?php endif; ?>>Enabled</option>
                                            <option value="2" <?php if (isset($_POST['status']) && $_POST['status'] == '2'): ?>selected<?php endif; ?>>Disabled</option>
                                        </select>                                           
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-info pull-right"><i class="fa fa-filter"></i> Filter</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">                                
                    <h3 class="panel-title">All Products</h3> 
                    <ul class="panel-controls">
                        <li><a href="/admin/products/create" class="text-info"><span class="fa fa-plus"></span></a></li>                       
                    </ul>                
                </div>
                <div class="panel-body">
                    <table class="table datatable">
                        <thead>
                            <tr>
                                <th>S.No.</th>    
                                <th>Image</th>
                                <th style="max-width: 150px;">Product Name</th>
                                <th>Product Code</th>
                                <th style="min-width: 120px;">Price</th>
                                <th>Inventary</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($products)
                            @foreach($products as $key=>$product)
                            <?php
                            $fileInfo = getFile($product->image);
                            if ($fileInfo) {
                                $file = $fileInfo['file_path'] . $fileInfo['file_name'];
                            } else {
                                $file = url('/') . '/frontassets/images/no_img.png';
                            }
                            ?>
                            <tr>
                                <td>{{$key + 1}}</td>
                                <td><img src="<?= $file; ?>" width="75" height="50"></td>
                                <td width="25%">{{$product->name}}</td>
                                <td>{{$product->sku_code}}</td>
                        <td width="20%">
                             @if($product->discount_percent!=0)
                            <i class="fa fa-inr"></i> <del class="text-danger">{{$product->price}}</del>
                            @endif
                              &#x20b9;<?= number_format($product->price - (($product->discount_percent * $product->price) / 100), 2, '.', ''); ?></td>
                                <td width="5%">{{$product->quantity}}</td>
                                <td>
                                    <label class="switch" style="top: 12px">
                                        <input type="checkbox" <?php
                                        if ($product->status == '1') {
                                            echo "checked";
                                        }
                                        ?> id="{{$product->id}}" onchange="checkStatus(this);" id="switch-{{$key}}}">
                                        <span class="slider round"></span>
                                    </label>
                                    <a href="/admin/products/{{$product->id}}" data-toggle="tooltip" title="View" class="btn btn-info btn-rounded text-center"><i class="fa fa-eye"></i> </a> 
                                    <a href="/admin/products/{{$product->id}}/edit" data-toggle="tooltip" title="Edit" class="btn btn-info btn-rounded text-center"><i class="fa fa-edit"></i> </a> 
                                    <a href="javascript:void(0)" delete_id="{{$product->id}}" table="products" class="deleterow" class="btn btn-info btn-rounded text-center"><i class="fa fa-trash-o"></i></a>
                                </td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function checkStatus(obj) {
        var id = $(obj).attr("id");
        var checked = $(obj).is(':checked');
        if (checked == true) {
            var status = 1;
        } else {
            var status = 0;
        }
        $.ajax({
            url: '/admin/change_status',
            type: 'post',
            dataType: 'json',
            data: {method: 'changeProductStatus', status: status, id: id},
            cache: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
        }).done(function (response) {
            if (response.status == '1') {
                Command: toastr["success"]("Status has been updated successfully!")

                toastr.options = {
                    "closeButton": false,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": false,
                    "positionClass": "toast-top-right",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                }
            } else {
                alert("Some error occured, please try again");
            }
        });
    }
</script>
@endsection