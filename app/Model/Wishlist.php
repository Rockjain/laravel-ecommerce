<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Wishlist extends Model {

    protected $table = 'customer_wishlist';
    protected $fillable = array(
        'customer_id',
        'product_id',
        'status',
        'created_at',
        'updated_at'
    );

    public function productDetail() {
        return $this->belongsTo('App\Model\Product', 'product_id');
    }

}
