

<style type="text/css">
    .price{
        font-weight: 500!important;
    }
   
 .owl-carousel .owl-nav .owl-prev{
    height: 35px;
    width: 30px;
 }
.owl-nav .owl-next{
    height: 35px;
    width: 30px;
    right: 5px !important;
 }
 .blogpara{
     line-height: 1.5em;
    height:3em;
    overflow: hidden;
     text-overflow: ellipsis;
 }

.product-list .product-thumb .image {
   margin: 0 30px 0 0!important;
  
}
.product-list .product-thumb .caption {
  width: 70%!important;
  float: none!important;
}
owl-next:hover {
    margin-top: 5px!important;
}


</style>
@extends('layouts.default')
@section('content')
<?php
$categories = getParentCategory();
?>

<div class="slider-section fullwidth">
    <div class="container-fluid">
        <div class="row">
            <!-- =====  BANNER STRAT  ===== -->
            <div class="col-sm-12 col-md-12 col-lg-12 nopadding">
                <div class="banner">
                    <div class="main-banner owl-carousel">

                        @if($banner)
                        @foreach($banner as $key=>$value)
                    <div class="item" style="background:url(<?= url('/'); ?>/images/bannerimages/{{$value->image}})">
                            <div class="caption">
                                <p>Exclusive Offers - 20% off this weak</p>
                                <h2>{{$value->name}}</h2>
                                <p>{{$value->description}}.</p>
                                <p><a href="#" class="banner-button">SHOP NOW</a></p>
                            </div>
                        </div>
                        @endforeach
                        @endif

                        

                        <!-- <div class="item" style="background:url(<?= url('/'); ?>/frontassets/images/slider-31.jpg)">
                            <div class="caption">
                                <p>Exclusive Offers - 20% off this weak</p>
                                <h2>Rings for Men's</h2>
                                <p>Silk floral hairpieces have become the rage of contemporary weddings.</p>
                                <p><a href="#" class="banner-button">SHOP NOW</a></p>
                            </div>
                        </div>


                        <div class="item" style="background:url(<?= url('/'); ?>/frontassets/images/slider-32.jpg)">
                            <div class="caption">
                                <p>Exclusive Offers - 20% off this weak</p>
                                <h2>Wedding Hair Flowers</h2>
                                <p>Silk floral hairpieces have become the rage of contemporary weddings.</p>
                                <p><a href="#" class="banner-button">SHOP NOW</a></p>
                            </div>
                        </div> -->

                    </div>

                    
                      </div>
            </div>
            <!-- =====  BANNER END  ===== -->
        </div>
    </div>
</div>
<div class="banner-bottom">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="header-bottom-right offers">
                    <div class="marquee"><span><i class="fa fa-circle" aria-hidden="true"></i>It's Sexual Health Week!</span>
                        <span><i class="fa fa-circle" aria-hidden="true"></i>Our 5 Tips for a Healthy Summer</span>
                        <span><i class="fa fa-circle" aria-hidden="true"></i>Sugar health at risk?</span>
                        <span><i class="fa fa-circle" aria-hidden="true"></i>The Olay Ranges - What do they do?</span>
                        <span><i class="fa fa-circle" aria-hidden="true"></i>Body fat - what is it and why do we need it?</span>
                        <span><i class="fa fa-circle" aria-hidden="true"></i>Can a pillow help you to lose weight?</span></div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="container">
    <div class="row ">
        <div id="column-left" class="col-sm-4 col-md-4 col-lg-3 mt_30">
            <div class="">
                <div class="category">
                    <div class="menu-bar" data-target="#category-menu,#category-menu-responsive" data-toggle="collapse" aria-expanded="true" role="button">
                        <h4 class="category_text">Top category</h4>
                        <span class="i-bar"><i class="fa fa-bars"></i></span>
                    </div>
                </div>

                <div id="category-menu" class="navbar collapse mb_40 hidden-sm-down in" aria-expanded="true" style="" role="button">
                    <div class="nav-responsive">
                        <ul class="nav  main-navigation collapse in ">
                            <?php
                            if ($categories):
                                foreach ($categories as $key => $category):
                                    ?>
                                    <li><a href="/products/{{$category->id}}"><?= $category->name; ?></a></li>
                                    <?php
                                endforeach;
                            endif;
                            ?>
                        </ul>
                    </div>
                </div>
                <div class="left_banner left-sidebar-widget mt_20 mb_20"> <a href="https://play.google.com/store/apps/details?id=com.aaima"><img src="<?= url('/'); ?>/frontassets/images/appbanenr.png" alt="Left Banner" class="img-responsive" /></a> </div>
                <div class="left-cms left-sidebar-widget mb_20 white-bg">
                    <ul class="ptb_10">
                        <li>
                            <div class="feature-i-left ptb_40">
                                <div class="icon-right Shipping"></div>
                                <h6>Free Shipping</h6>
                                <p>Free delivery worldwide</p>
                            </div>
                        </li>
                        <li>
                            <div class="feature-i-left ptb_40">
                                <div class="icon-right Order"></div>
                                <h6>Order Online</h6>
                                <p>Hours : 8am - 11pm</p>
                            </div>
                        </li>
                        <li>
                            <div class="feature-i-left ptb_40">
                                <div class="icon-right Save"></div>
                                <h6>Shop And Save</h6>
                                <p>For All Spices & Herbs</p>
                            </div>
                        </li>
                        <li>
                            <div class="feature-i-left ptb_40">
                                <div class="icon-right Safe"></div>
                                <h6>Safe Shoping</h6>
                                <p>Ensure genuine 100%</p>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="left-special left-sidebar-widget mb_20 white-bg">
                    <div class="heading-part mt_10 ">
                        <h2 class="main_title">Top Products</h2>
                    </div>
                    <div id="left-special" class="owl-carousel">
                        <?php
                        $dataArr = [];
                        if ($bestsellerproducts):
                            foreach ($bestsellerproducts as $key => $prod) {
                                array_push($dataArr, $prod);
                            }
                            $item_count = (int) ($bestsellerproductcount / 5);
                            if (is_float($item_count) || $item_count == 0) {
                                $item_count = $item_count + 1;
                            }
                            for ($i = 0; $i < $item_count; $i++):
                                ?>
                                <ul class="row ">
                                    <?php
                                    for ($j = 0; $j < 5; $j++):
                                        $product = array_pop($dataArr);
                                        if ($product):
                                            ?>
                                            <li class="item product-layout-left mb_20">
                                                <div class="product-list col-xs-4">
                                                    <div class="product-thumb">
                                                        <div class="image product-imageblock"> 
                                                            <a href="/product_detail/{{$product->id}}"> 
                                                                <img class="img-responsive" title="{{$product->name}}" alt="product image" src="<?= !empty($product->fileDetail) ? $product->fileDetail->file_path . $product->fileDetail->file_name : ''; ?>" > 
                                                                <img class="img-responsive" title="{{$product->name}}" alt="product image" src="<?= !empty($product->fileDetail) ? $product->fileDetail->file_path . $product->fileDetail->file_name : ''; ?>"> 
                                                            </a> 
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-8">
                                                    <div class="caption product-detail">
                                                        <h6 class="product-name"><a href="/product_detail/{{$product->id}}"><?= substr($product->name, 0,10) . '...' ?></a></h6>
                                                        <div class="rating">
                                                            @if($product->rating == 0)
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            @elseif($product->rating == 1)
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            @elseif($product->rating == 2)
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            @elseif($product->rating == 3)
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            @elseif($product->rating == 4)
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            @elseif($product->rating == 5)
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            @endif

                                                        </div>
                                                        <!-- <span class="price">
                                                            <span class="amount">
                                                                <span class="currencySymbol">
                                                                    <i class="fa fa-inr"></i>
                                                                </span>{{$product->price}} 
                                                            </span>
                                                        </span> -->
                                                        @if($product->discount_percent!=0)
                                                    <span><i class="fa fa-inr"></i> <del class="text-danger">{{$product->price}}</del></span>
                                                    @endif
                                <span class="price"><span class="amount"><span class="currencySymbol">&#8377; </span><?= number_format($product->price - (($product->discount_percent * $product->price) / 100), 2, '.', ''); ?></span>
                                </span>
                                                    </div>
                                                </div>
                                            </li>
                                            <?php
                                        endif;
                                    endfor;
                                    ?>
                                </ul>
                                <?php
                            endfor;
                        endif;
                        ?>
                    </div>
                </div>
                <!--testimonial start -->
                <div class="left_banner left-sidebar-widget mb_20"> <a href="#"><img src="<?= url('/'); ?>/frontassets/images/left2.jpg" alt="Left Banner" class="img-responsive" /></a> </div>
                <div class="Testimonial left-sidebar-widget mb_20 white-bg">
                    <div class="heading-part mt_10 ">
                        <h2 class="main_title">Testimonial</h2>
                    </div>
                    <div class="client owl-carousel text-center pt_10">
                        @if($testimonials)
                        @foreach($testimonials as $key=>$value)
                        <div class="item client-detail">
                            <div class="client-avatar"> 
                                <img alt="" src="<?= url('/'); ?>/images/testimonialsimages/{{$value->image}}" style="width: 150px!important; height: 150px;"> 
                            </div>
                            <div class="client-title  mt_30"><strong>{{$value->name}}</strong></div>

                            <p style="margin-bottom: 10px;"><i class="fa fa-quote-left" aria-hidden="true"></i>{{$value->description}}</p>
                        </div>
                        @endforeach
                        @endif
                    </div>
                </div>
                <div class="left-special left-sidebar-widget mb_20 white-bg">
                    <div class="heading-part mt_10 ">
                        <h2 class="main_title">Recently Viewed</h2>
                    </div>
                    <div id="recentview-pro" class="owl-carousel">
                        <?php
                        $dataArr = [];
                        if ($recentproducts):
                            foreach ($recentproducts as $key => $prod) {
                                array_push($dataArr, $prod);
                            }
                            $item_count = (int) ($recentproductcount / 5);
                            if (is_float($item_count) || $item_count == 0) {
                                $item_count = $item_count + 1;
                            }
                            for ($i = 0; $i < $item_count; $i++):
                                ?>
                                <ul class="row ">
                                    <?php
                                    for ($j = 5; $j > 0; $j--):
                                        $product = array_pop($dataArr);
                                        if ($product):
                                            ?>
                                            <li class="item product-layout-left mb_20">
                                                <div class="product-list col-xs-4">
                                                    <div class="product-thumb">
                                                        <div class="image product-imageblock"> 
                                                            <a href="/product_detail/{{$product->id}}"> 
                                                                <img class="img-responsive" title="{{$product->name}}" alt="product image" src="<?= !empty($product->fileDetail) ? $product->fileDetail->file_path . $product->fileDetail->file_name : ''; ?>" > 
                                                                <img class="img-responsive" title="{{$product->name}}" alt="product image" src="<?= !empty($product->fileDetail) ? $product->fileDetail->file_path . $product->fileDetail->file_name : ''; ?>" > 
                                                            </a> 
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-8">
                                                    <div class="caption product-detail">
                                                        <h6 class="product-name"><a href="/product_detail/{{$product->id}}"> <?= substr($product->name, 0,24) . '...' ?></a></h6>
                                                        <div class="rating">
                                                            @if($product->rating == 0)
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            @elseif($product->rating == 1)
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            @elseif($product->rating == 2)
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            @elseif($product->rating == 3)
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            @elseif($product->rating == 4)
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            @elseif($product->rating == 5)
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            @endif

                                                        </div>
                                                        <!-- <span class="price">
                                                            <span class="amount">
                                                                <span class="currencySymbol">
                                                                    <i class="fa fa-inr"></i>
                                                                </span>{{$product->price}} 
                                                            </span>
                                                        </span> -->
                                                @if($product->discount_percent!=0)
                                                    <span><i class="fa fa-inr"></i> <del class="text-danger">{{$product->price}}</del></span>
                                                    @endif
                                <span class="price"><span class="amount"><span class="currencySymbol">&#8377; </span><?= number_format($product->price - (($product->discount_percent * $product->price) / 100), 2, '.', ''); ?></span>
                                </span>
                                                    </div>
                                                </div>
                                            </li>
                                            <?php
                                        endif;
                                    endfor;
                                    ?>
                                </ul>
                                <?php
                            endfor;
                        endif;
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div id="column-right" class="col-sm-8 col-md-8 col-lg-9">

            <!-- =====  SUB BANNER  STRAT ===== -->
            <div class="row mb_10">
                <div class="cms_banner mt_10 fullwidth">
                    <div class="col-xs-6 col-sm-12 col-md-6 mt_20 noleftpadding">
                        <div id="subbanner1" class="sub-hover"> <a href="#"><img src="<?= url('/'); ?>/frontassets/images/sub1.jpg" alt="Sub Banner1" class="img-responsive fullwidth"></a>
                            <div class="bannertext">
                                <h2>Full HD Camera</h2>
                                <p class="mt_10">Description here..</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-12 col-md-6 mt_20 norightpadding">
                        <div id="subbanner2" class="sub-hover"> <a href="<?= url('/'); ?>/frontassets/#"><img src="<?= url('/'); ?>/frontassets/images/sub2.jpg" alt="Sub Banner2" class="img-responsive fullwidth"></a>
                            <div class="bannertext">
                                <h2>Smart Watches</h2>
                                <p class="mt_10">Description here..</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- =====  SUB BANNER END  ===== -->

            <!-- =====  Featured TAB  ===== -->
            <div class="row white-bg">
                <div class="col-md-12">
                    <div class="heading-part text-center">
                        <h2 class="main_title mt_10">Featured Products</h2>
                    </div>
                </div>
            </div>
            <div class="row white-bg mb_20">
                <div class="product-layout  product-grid related-pro  owl-carousel mb_20">
                    @if($featured)
                    @foreach($featured as $key=>$data)
                    <div class="item">
                        <div class="product-thumb">
                            <div class="image product-imageblock"> 
                                <a href="/product_detail/{{$data->id}}">
                                    <img data-name="product_image" src="{{!empty($data->fileDetail)?$data->fileDetail->file_path.$data->fileDetail->file_name:url('/').'/frontassets/images/no_img.png'}}" alt="{{$data->name}}" title="{{$data->name}}" class="img-responsive" style="height: 234px;"> 
                                    <img src="{{!empty($data->fileDetail)?$data->fileDetail->file_path.$data->fileDetail->file_name:url('/').'/frontassets/images/no_img.png'}}" alt="{{$data->name}}" title="{{$data->name}}" class="img-responsive" style="height: 234px;"> 
                                </a> 
                            </div>
                            <div class="caption product-detail text-left">
                                <h6 data-name="product_name" class="product-name mt_20">
                                    <a href="/product_detail/{{$data->id}}" title="{{$data->name}}"><!-- {{$data->name}} --> <?= substr($data->name, 0, 30) . '...' ?></a>
                                </h6>
                                <div class="rating">
                                    @if($data->rating == 0)
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                    @elseif($data->rating == 1)
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                    @elseif($data->rating == 2)
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                    @elseif($data->rating == 3)
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                    @elseif($data->rating == 4)
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                    @elseif($data->rating == 5)
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                    @endif

                                </div>
                                @if($data->discount_percent!=0)
                                <span><i class="fa fa-inr"></i> <del class="text-danger">{{$data->price}}</del></span>
                                @endif
                                <span class="price"><span class="amount"><span class="currencySymbol">&#8377; </span><?= number_format($data->price - (($data->discount_percent * $data->price) / 100), 2, '.', ''); ?></span>
                                </span>
                                <div class="button-group text-center">
                                    <div class="wishlist" title="Add to wishlist"><a href="javascript:void(0)" product_id="{{$data->id}}" onclick="addtowishlist(this)"><span>wishlist</span></a></div>
                                    <div class="quickview" title="Quick View"><a href="/product_detail/{{$data->id}}"><span>Quick View</span></a></div>
                                    <div class="add-to-cart" title="Add to cart"><a href="javascript:void(0)" product_id="{{$data->id}}" onclick="addtocart(this)"><span>Add to cart</span></a></div>
                                </div>
                            </div>
                        </div>
                    </div>                 
                    @endforeach
                    @endif
                </div>
            </div>
            <!-- =====  Featured  END ===== -->
            <!-- =====  Bestseller TAB  ===== -->
            <div class="row white-bg">
                <div class="col-md-12">
                    <div class="heading-part text-center">
                        <h2 class="main_title mt_10">Bestseller Products</h2>
                    </div>
                </div>
            </div>
            <div class="row white-bg mb_20">
                <div class="product-layout  product-grid related-pro  owl-carousel mb_20">
                    @if($bestsellerproducts)
                    @foreach($bestsellerproducts as $data)
                    <div class="item">
                        <div class="product-thumb">
                            <div class="image product-imageblock"> 
                                <a href="/product_detail/{{$data->id}}">
                                    <img data-name="product_image" src="{{!empty($data->fileDetail)?$data->fileDetail->file_path.$data->fileDetail->file_name:url('/').'/frontassets/images/no_img.png'}}" alt="{{$data->name}}" title="{{$data->name}}" class="img-responsive" style="height: 234px;"> 
                                    <img src="{{!empty($data->fileDetail)?$data->fileDetail->file_path.$data->fileDetail->file_name:url('/').'/frontassets/images/no_img.png'}}" alt="{{$data->name}}" title="{{$data->name}}" class="img-responsive" style="height: 234px;"> 
                                </a> 
                            </div>
                            <div class="caption product-detail text-left">
                                <h6 data-name="product_name" class="product-name mt_20">
                                    <a href="/product_detail/{{$data->id}}" title="{{$data->name}}"><?= substr($data->name, 0, 30) . '...' ?></a>
                                </h6>
                                <div class="rating">
                                    @if($data->rating == 0)
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                    @elseif($data->rating == 1)
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                    @elseif($data->rating == 2)
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                    @elseif($data->rating == 3)
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                    @elseif($data->rating == 4)
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                    @elseif($data->rating == 5)
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                    @endif

                                </div>
                                @if($data->discount_percent!=0)
                                <span><i class="fa fa-inr"></i> <del class="text-danger">{{$data->price}}</del></span>
                                @endif
                                <span class="price"><span class="amount"><span class="currencySymbol">&#8377; </span><?= number_format($data->price - (($data->discount_percent * $data->price) / 100), 2, '.', ''); ?></span>
                                </span>
                                <div class="button-group text-center">
                                    <div class="wishlist" title="Add to wishlist"><a href="javascript:void(0)" product_id="{{$data->id}}" onclick="addtowishlist(this)"><span>wishlist</span></a></div>
                                    <div class="quickview" title="Quick View"><a href="/product_detail/{{$data->id}}"><span>Quick View</span></a></div>
                                    <div class="add-to-cart" title="Add to cart"><a href="javascript:void(0)" product_id="{{$data->id}}" onclick="addtocart(this)"><span>Add to cart</span></a></div>
                                </div>
                            </div>
                        </div>
                    </div>  
                    @endforeach
                    @endif
                </div>
            </div>
            <!-- =====  Bestseller  END ===== -->

            <!-- =====  Sponcered With Us TAB  ===== -->
            <div class="row white-bg">
                <div class="col-md-12">
                    <div class="heading-part text-center">
                        <h2 class="main_title mt_10">Sponcered With Us</h2>
                    </div>
                </div>
            </div>
            <div class="row white-bg mb_20">
                <div class="product-layout  product-grid related-pro  owl-carousel mb_20">

                     @if($sponcered)
                        @foreach($sponcered as $key=>$value)
                        <div class="item">
                        <div class="product-thumb">
                            <div class="image product-imageblock"> 
                            <a href="#"> 
                <img data-name="product_image" src="<?= url('/'); ?>/images/testimonialsimages/{{$value->image}}" alt="iPod Classic" title="iPod Classic" class="img-responsive" style="height: 270px;"> 
                <img src="<?= url('/'); ?>/images/testimonialsimages/{{$value->image}}" alt="iPod Classic" title="iPod Classic" class="img-responsive" style="height: 270px;"> 
                                </a> 
                            </div>
                            <!-- <div class="caption product-detail text-left">
                                <h6 data-name="product_name" class="product-name mt_20">
                                    <a href="#" title="Casual Shirt With Ruffle Hem">{{$value->description}}.</a>
                                </h6>
                            </div> -->
                        </div>
                    </div>
                        @endforeach
                        @endif

                </div>
            </div>
            <!-- =====  Sponcered With Us  END ===== -->

            <!-- =====  SUB BANNER  STRAT ===== -->
            <div class="row white-bg mb_20">
                <div class="col-md-12">
                    <div class="heading-part text-center mb_20">
                        <h2 class="main_title mt_10">Advertise With Us</h2>
                    </div>
                </div>
                <div class="cms_banner mb_20">
                        @if($advertisements)
                        @foreach($advertisements as $key=>$value)
                         @if($value->id=='1')
                    <div class="col-xs-12">
                        <div id="subbanner3" class="banner sub-hover"> <a href="#"><img src="<?= url('/'); ?>/images/testimonialsimages/{{$value->image}}" alt="Sub Banner3" class="img-responsive"></a> </div>
                    </div>
                    @endif
                    @if($value->id >'1')
                    <div class="col-xs-6 col-sm-12 col-md-6 mt_20">
                        <div id="subbanner1" class="sub-hover"> <a href="#">
                            <img src="<?= url('/'); ?>/images/testimonialsimages/{{$value->image}}" alt="Sub Banner1" class="img-responsive" style="width: 455px; height: 196px;">
                        </a>
                            <div class="bannertext">
                                <h2>{{$value->name}}</h2>
                                <p class="mt_10">{{$value->description}}</p>
                            </div>
                        </div>
                    </div>
                    @endif
                    <!-- <div class="col-xs-6 col-sm-12 col-md-6 mt_20">
                        <div id="subbanner2" class="sub-hover"> <a href="<?= url('/'); ?>/frontassets/#"><img src="<?= url('/'); ?>/frontassets/images/sub2.jpg" alt="Sub Banner2" class="img-responsive"></a>
                            <div class="bannertext">
                                <h2>Smart Watches</h2>
                                <p class="mt_10">Description here..</p>
                            </div>
                        </div>
                    </div> -->
                    @endforeach
                        @endif
                </div>
            </div>
            <!-- =====  SUB BANNER END  ===== -->

            <!-- =====  PRODUCT TAB  ===== -->
            <div class="row white-bg mb_20">
                <div class="col-md-12">
                    <div id="product-tab" class="mt_10">
                        <div class="heading-part mb_20 ">
                            <h2 class="main_title">Latest Products</h2>
                        </div>
                        <div class="tab-content clearfix box">
                            <div class="tab-pane active" id="nArrivals">
                                <div class="nArrivals owl-carousel">
                                    @if($products)
                                    @foreach($products as $key=>$data)
                                    <div class="product-grid">
                                        <div class="item">
                                            <div class="product-thumb">
                                                <div class="image product-imageblock"> 
                                                    <a href="/product_detail/{{$data->id}}">
                                                        <img data-name="product_image" src="{{!empty($data->fileDetail)?$data->fileDetail->file_path.$data->fileDetail->file_name:''}}"  title="{{$data->name}}" class="img-responsive" style="height: 234px;"> 
                                                        <img src="{{!empty($data->fileDetail)?$data->fileDetail->file_path.$data->fileDetail->file_name:''}}" title="{{$data->name}}" class="img-responsive" style="height: 234px;"> 
                                                    </a> 
                                                </div>
                                                <div class="caption product-detail text-left">
                                                    <h6 data-name="product_name" class="product-name mt_20">
                                                        <a href="/product_detail/{{$data->id}}" title="{{$data->name}}">
                                                           <?= substr($data->name, 0, 30) . '...' ?>

                                                        </a>
                                                    </h6>
                                                    <div class="rating">
                                                        @if($data->rating == 0)
                                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                        @elseif($data->rating == 1)
                                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                        @elseif($data->rating == 2)
                                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                        @elseif($data->rating == 3)
                                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                        @elseif($data->rating == 4)
                                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                        @elseif($data->rating == 5)
                                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                        @endif

                                                    </div>
                                                    <!-- <span><i class="fa fa-inr"></i> <del class="text-danger">14990</del></span>
                                        <span class="price"><span class="amount"><span class="currencySymbol">&#8377; </span>{{$data->price}}</span></span> -->

                                                    @if($data->discount_percent!=0)
                                                    <span><i class="fa fa-inr"></i> <del class="text-danger">{{$data->price}}</del></span>
                                                    @endif
                                <span class="price"><span class="amount"><span class="currencySymbol">&#8377; </span><?= number_format($data->price - (($data->discount_percent * $data->price) / 100), 2, '.', ''); ?></span>
                                </span>
                                                    <div class="button-group text-center">
                                                        <div class="wishlist" title="Add to wishlist"><a href="javascript:void(0)" product_id="{{$data->id}}" onclick="addtowishlist(this)"><span>wishlist</span></a></div>
                                                        <div class="quickview" title="Quick View"><a href="/product_detail/{{$data->id}}"><span>Quick View</span></a></div>
                                                        <div class="add-to-cart" title="Add to cart"><a href="javascript:void(0)" product_id="{{$data->id}}" onclick="addtocart(this)"><span>Add to cart</span></a></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>  
                                    </div>
                                    @endforeach
                                    @endif
                                </div>
                            </div>
                            <div class="tab-pane" id="Bestsellers">
                                <div class="Bestsellers owl-carousel">
                                    <div class="product-grid">
                                        <div class="item">
                                            <div class="product-thumb  mb_30">
                                                <div class="image product-imageblock"> <a href="<?= url('/'); ?>/frontassets/product_detail_page.php"> <img data-name="product_image" src="<?= url('/'); ?>/frontassets/images/product/product1.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive"> <img src="<?= url('/'); ?>/frontassets/images/product/product1-1.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive"> </a> </div>
                                                <div class="caption product-detail text-left">
                                                    <h6 data-name="product_name" class="product-name mt_20"><a href="<?= url('/'); ?>/frontassets/#" title="Casual Shirt With Ruffle Hem">Latin literature from 45 BC, making it over old.</a></h6>
                                                    <div class="rating"> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span> </div>
                                                    <span class="price"><span class="amount"><span class="currencySymbol">$</span>70.00</span>
                                                    </span>
                                                    <div class="button-group text-center">
                                                        <div class="wishlist"><a href="<?= url('/'); ?>/frontassets/#"><span>wishlist</span></a></div>
                                                        <div class="quickview"><a href="<?= url('/'); ?>/frontassets/#"><span>Quick View</span></a></div>
                                                        <div class="compare"><a href="<?= url('/'); ?>/frontassets/#"><span>Compare</span></a></div>
                                                        <div class="add-to-cart"><a href="<?= url('/'); ?>/frontassets/#"><span>Add to cart</span></a></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="product-thumb  mb_30">
                                                <div class="image product-imageblock"> <a href="<?= url('/'); ?>/frontassets/product_detail_page.php"> <img data-name="product_image" src="<?= url('/'); ?>/frontassets/images/product/product2.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive"> <img src="<?= url('/'); ?>/frontassets/images/product/product2-1.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive"> </a> </div>
                                                <div class="caption product-detail text-left">
                                                    <h6 data-name="product_name" class="product-name mt_20"><a href="<?= url('/'); ?>/frontassets/#" title="Casual Shirt With Ruffle Hem">Latin literature from 45 BC, making it over old.</a></h6>
                                                    <div class="rating"> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span> </div>
                                                    <span class="price"><span class="amount"><span class="currencySymbol">$</span>70.00</span>
                                                    </span>
                                                    <div class="button-group text-center">
                                                        <div class="wishlist"><a href="<?= url('/'); ?>/frontassets/#"><span>wishlist</span></a></div>
                                                        <div class="quickview"><a href="<?= url('/'); ?>/frontassets/#"><span>Quick View</span></a></div>
                                                        <div class="compare"><a href="<?= url('/'); ?>/frontassets/#"><span>Compare</span></a></div>
                                                        <div class="add-to-cart"><a href="<?= url('/'); ?>/frontassets/#"><span>Add to cart</span></a></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="product-grid">
                                        <div class="item">
                                            <div class="product-thumb  mb_30">
                                                <div class="image product-imageblock"> <a href="<?= url('/'); ?>/frontassets/product_detail_page.php"> <img data-name="product_image" src="<?= url('/'); ?>/frontassets/images/product/product3.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive"> <img src="<?= url('/'); ?>/frontassets/images/product/product3-1.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive"> </a> </div>
                                                <div class="caption product-detail text-left">
                                                    <h6 data-name="product_name" class="product-name mt_20"><a href="<?= url('/'); ?>/frontassets/#" title="Casual Shirt With Ruffle Hem">Latin literature from 45 BC, making it over old.</a></h6>
                                                    <div class="rating"> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span> </div>
                                                    <span class="price"><span class="amount"><span class="currencySymbol">$</span>70.00</span>
                                                    </span>
                                                    <div class="button-group text-center">
                                                        <div class="wishlist"><a href="<?= url('/'); ?>/frontassets/#"><span>wishlist</span></a></div>
                                                        <div class="quickview"><a href="<?= url('/'); ?>/frontassets/#"><span>Quick View</span></a></div>
                                                        <div class="compare"><a href="<?= url('/'); ?>/frontassets/#"><span>Compare</span></a></div>
                                                        <div class="add-to-cart"><a href="<?= url('/'); ?>/frontassets/#"><span>Add to cart</span></a></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="product-thumb  mb_30">
                                                <div class="image product-imageblock"> <a href="<?= url('/'); ?>/frontassets/product_detail_page.php"> <img data-name="product_image" src="<?= url('/'); ?>/frontassets/images/product/product4.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive"> <img src="<?= url('/'); ?>/frontassets/images/product/product4-1.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive"> </a> </div>
                                                <div class="caption product-detail text-left">
                                                    <h6 data-name="product_name" class="product-name mt_20"><a href="<?= url('/'); ?>/frontassets/#" title="Casual Shirt With Ruffle Hem">Latin literature from 45 BC, making it over old.</a></h6>
                                                    <div class="rating"> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span> </div>
                                                    <span class="price"><span class="amount"><span class="currencySymbol">$</span>70.00</span>
                                                    </span>
                                                    <div class="button-group text-center">
                                                        <div class="wishlist"><a href="<?= url('/'); ?>/frontassets/#"><span>wishlist</span></a></div>
                                                        <div class="quickview"><a href="<?= url('/'); ?>/frontassets/#"><span>Quick View</span></a></div>
                                                        <div class="compare"><a href="<?= url('/'); ?>/frontassets/#"><span>Compare</span></a></div>
                                                        <div class="add-to-cart"><a href="<?= url('/'); ?>/frontassets/#"><span>Add to cart</span></a></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="product-grid">
                                        <div class="item">
                                            <div class="product-thumb  mb_30">
                                                <div class="image product-imageblock"> <a href="<?= url('/'); ?>/frontassets/product_detail_page.php"> <img data-name="product_image" src="<?= url('/'); ?>/frontassets/images/product/product5.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive"> <img src="<?= url('/'); ?>/frontassets/images/product/product5-1.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive"> </a> </div>
                                                <div class="caption product-detail text-left">
                                                    <h6 data-name="product_name" class="product-name mt_20"><a href="<?= url('/'); ?>/frontassets/#" title="Casual Shirt With Ruffle Hem">Latin literature from 45 BC, making it over old.</a></h6>
                                                    <div class="rating"> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span> </div>
                                                    <span class="price"><span class="amount"><span class="currencySymbol">$</span>70.00</span>
                                                    </span>
                                                    <div class="button-group text-center">
                                                        <div class="wishlist"><a href="<?= url('/'); ?>/frontassets/#"><span>wishlist</span></a></div>
                                                        <div class="quickview"><a href="<?= url('/'); ?>/frontassets/#"><span>Quick View</span></a></div>
                                                        <div class="compare"><a href="<?= url('/'); ?>/frontassets/#"><span>Compare</span></a></div>
                                                        <div class="add-to-cart"><a href="<?= url('/'); ?>/frontassets/#"><span>Add to cart</span></a></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="product-thumb  mb_30">
                                                <div class="image product-imageblock"> <a href="<?= url('/'); ?>/frontassets/product_detail_page.php"> <img data-name="product_image" src="<?= url('/'); ?>/frontassets/images/product/product6.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive"> <img src="<?= url('/'); ?>/frontassets/images/product/product6-1.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive"> </a> </div>
                                                <div class="caption product-detail text-left">
                                                    <h6 data-name="product_name" class="product-name mt_20"><a href="<?= url('/'); ?>/frontassets/#" title="Casual Shirt With Ruffle Hem">Latin literature from 45 BC, making it over old.</a></h6>
                                                    <div class="rating"> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span> </div>
                                                    <span class="price"><span class="amount"><span class="currencySymbol">$</span>70.00</span>
                                                    </span>
                                                    <div class="button-group text-center">
                                                        <div class="wishlist"><a href="<?= url('/'); ?>/frontassets/#"><span>wishlist</span></a></div>
                                                        <div class="quickview"><a href="<?= url('/'); ?>/frontassets/#"><span>Quick View</span></a></div>
                                                        <div class="compare"><a href="<?= url('/'); ?>/frontassets/#"><span>Compare</span></a></div>
                                                        <div class="add-to-cart"><a href="<?= url('/'); ?>/frontassets/#"><span>Add to cart</span></a></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="product-grid">
                                        <div class="item">
                                            <div class="product-thumb  mb_30">
                                                <div class="image product-imageblock"> <a href="<?= url('/'); ?>/frontassets/product_detail_page.php"> <img data-name="product_image" src="<?= url('/'); ?>/frontassets/images/product/product7.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive"> <img src="<?= url('/'); ?>/frontassets/images/product/product7-1.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive"> </a> </div>
                                                <div class="caption product-detail text-left">
                                                    <h6 data-name="product_name" class="product-name mt_20"><a href="<?= url('/'); ?>/frontassets/#" title="Casual Shirt With Ruffle Hem">Latin literature from 45 BC, making it over old.</a></h6>
                                                    <div class="rating"> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span> </div>
                                                    <span class="price"><span class="amount"><span class="currencySymbol">$</span>70.00</span>
                                                    </span>
                                                    <div class="button-group text-center">
                                                        <div class="wishlist"><a href="<?= url('/'); ?>/frontassets/#"><span>wishlist</span></a></div>
                                                        <div class="quickview"><a href="<?= url('/'); ?>/frontassets/#"><span>Quick View</span></a></div>
                                                        <div class="compare"><a href="<?= url('/'); ?>/frontassets/#"><span>Compare</span></a></div>
                                                        <div class="add-to-cart"><a href="<?= url('/'); ?>/frontassets/#"><span>Add to cart</span></a></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="product-thumb  mb_30">
                                                <div class="image product-imageblock"> <a href="<?= url('/'); ?>/frontassets/product_detail_page.php"> <img data-name="product_image" src="<?= url('/'); ?>/frontassets/images/product/product8.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive"> <img src="<?= url('/'); ?>/frontassets/images/product/product8-1.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive"> </a> </div>
                                                <div class="caption product-detail text-left">
                                                    <h6 data-name="product_name" class="product-name mt_20"><a href="<?= url('/'); ?>/frontassets/#" title="Casual Shirt With Ruffle Hem">Latin literature from 45 BC, making it over old.</a></h6>
                                                    <div class="rating"> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span> </div>
                                                    <span class="price"><span class="amount"><span class="currencySymbol">$</span>70.00</span>
                                                    </span>
                                                    <div class="button-group text-center">
                                                        <div class="wishlist"><a href="<?= url('/'); ?>/frontassets/#"><span>wishlist</span></a></div>
                                                        <div class="quickview"><a href="<?= url('/'); ?>/frontassets/#"><span>Quick View</span></a></div>
                                                        <div class="compare"><a href="<?= url('/'); ?>/frontassets/#"><span>Compare</span></a></div>
                                                        <div class="add-to-cart"><a href="<?= url('/'); ?>/frontassets/#"><span>Add to cart</span></a></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="product-grid">
                                        <div class="item">
                                            <div class="product-thumb  mb_30">
                                                <div class="image product-imageblock"> <a href="<?= url('/'); ?>/frontassets/product_detail_page.php"> <img data-name="product_image" src="<?= url('/'); ?>/frontassets/images/product/product9.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive"> <img src="<?= url('/'); ?>/frontassets/images/product/product9-1.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive"> </a> </div>
                                                <div class="caption product-detail text-left">
                                                    <h6 data-name="product_name" class="product-name mt_20"><a href="<?= url('/'); ?>/frontassets/#" title="Casual Shirt With Ruffle Hem">Latin literature from 45 BC, making it over old.</a></h6>
                                                    <div class="rating"> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span> </div>
                                                    <span class="price"><span class="amount"><span class="currencySymbol">$</span>70.00</span>
                                                    </span>
                                                    <div class="button-group text-center">
                                                        <div class="wishlist"><a href="<?= url('/'); ?>/frontassets/#"><span>wishlist</span></a></div>
                                                        <div class="quickview"><a href="<?= url('/'); ?>/frontassets/#"><span>Quick View</span></a></div>
                                                        <div class="compare"><a href="<?= url('/'); ?>/frontassets/#"><span>Compare</span></a></div>
                                                        <div class="add-to-cart"><a href="<?= url('/'); ?>/frontassets/#"><span>Add to cart</span></a></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="product-thumb  mb_30">
                                                <div class="image product-imageblock"> <a href="<?= url('/'); ?>/frontassets/product_detail_page.php"> <img data-name="product_image" src="<?= url('/'); ?>/frontassets/images/product/product10.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive"> <img src="<?= url('/'); ?>/frontassets/images/product/product10-1.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive"> </a> </div>
                                                <div class="caption product-detail text-left">
                                                    <h6 data-name="product_name" class="product-name mt_20"><a href="<?= url('/'); ?>/frontassets/#" title="Casual Shirt With Ruffle Hem">Latin literature from 45 BC, making it over old.</a></h6>
                                                    <div class="rating"> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span> </div>
                                                    <span class="price"><span class="amount"><span class="currencySymbol">$</span>70.00</span>
                                                    </span>
                                                    <div class="button-group text-center">
                                                        <div class="wishlist"><a href="<?= url('/'); ?>/frontassets/#"><span>wishlist</span></a></div>
                                                        <div class="quickview"><a href="<?= url('/'); ?>/frontassets/#"><span>Quick View</span></a></div>
                                                        <div class="compare"><a href="<?= url('/'); ?>/frontassets/#"><span>Compare</span></a></div>
                                                        <div class="add-to-cart"><a href="<?= url('/'); ?>/frontassets/#"><span>Add to cart</span></a></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="Featured">
                                <div class="Featured owl-carousel">
                                    <div class="product-grid">
                                        <div class="item">
                                            <div class="product-thumb  mb_30">
                                                <div class="image product-imageblock"> <a href="<?= url('/'); ?>/frontassets/product_detail_page.php"> <img data-name="product_image" src="<?= url('/'); ?>/frontassets/images/product/product1.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive"> <img src="<?= url('/'); ?>/frontassets/images/product/product1-1.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive"> </a> </div>
                                                <div class="caption product-detail text-left">
                                                    <h6 data-name="product_name" class="product-name mt_20"><a href="<?= url('/'); ?>/frontassets/#" title="Casual Shirt With Ruffle Hem">Latin literature from 45 BC, making it over old.</a></h6>
                                                    <div class="rating"> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span> </div>
                                                    <span class="price"><span class="amount"><span class="currencySymbol">$</span>70.00</span>
                                                    </span>
                                                    <div class="button-group text-center">
                                                        <div class="wishlist"><a href="<?= url('/'); ?>/frontassets/#"><span>wishlist</span></a></div>
                                                        <div class="quickview"><a href="<?= url('/'); ?>/frontassets/#"><span>Quick View</span></a></div>
                                                        <div class="compare"><a href="<?= url('/'); ?>/frontassets/#"><span>Compare</span></a></div>
                                                        <div class="add-to-cart"><a href="<?= url('/'); ?>/frontassets/#"><span>Add to cart</span></a></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="product-thumb  mb_30">
                                                <div class="image product-imageblock"> <a href="<?= url('/'); ?>/frontassets/product_detail_page.php"> <img data-name="product_image" src="<?= url('/'); ?>/frontassets/images/product/product2.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive"> <img src="<?= url('/'); ?>/frontassets/images/product/product2-1.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive"> </a> </div>
                                                <div class="caption product-detail text-left">
                                                    <h6 data-name="product_name" class="product-name mt_20"><a href="<?= url('/'); ?>/frontassets/#" title="Casual Shirt With Ruffle Hem">Latin literature from 45 BC, making it over old.</a></h6>
                                                    <div class="rating"> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span> </div>
                                                    <span class="price"><span class="amount"><span class="currencySymbol">$</span>70.00</span>
                                                    </span>
                                                    <div class="button-group text-center">
                                                        <div class="wishlist"><a href="<?= url('/'); ?>/frontassets/#"><span>wishlist</span></a></div>
                                                        <div class="quickview"><a href="<?= url('/'); ?>/frontassets/#"><span>Quick View</span></a></div>
                                                        <div class="compare"><a href="<?= url('/'); ?>/frontassets/#"><span>Compare</span></a></div>
                                                        <div class="add-to-cart"><a href="<?= url('/'); ?>/frontassets/#"><span>Add to cart</span></a></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="product-grid">
                                        <div class="item">
                                            <div class="product-thumb  mb_30">
                                                <div class="image product-imageblock"> <a href="<?= url('/'); ?>/frontassets/product_detail_page.php"> <img data-name="product_image" src="<?= url('/'); ?>/frontassets/images/product/product3.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive"> <img src="<?= url('/'); ?>/frontassets/images/product/product3-1.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive"> </a> </div>
                                                <div class="caption product-detail text-left">
                                                    <h6 data-name="product_name" class="product-name mt_20"><a href="<?= url('/'); ?>/frontassets/#" title="Casual Shirt With Ruffle Hem">Latin literature from 45 BC, making it over old.</a></h6>
                                                    <div class="rating"> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span> </div>
                                                    <span class="price"><span class="amount"><span class="currencySymbol">$</span>70.00</span>
                                                    </span>
                                                    <div class="button-group text-center">
                                                        <div class="wishlist"><a href="<?= url('/'); ?>/frontassets/#"><span>wishlist</span></a></div>
                                                        <div class="quickview"><a href="<?= url('/'); ?>/frontassets/#"><span>Quick View</span></a></div>
                                                        <div class="compare"><a href="<?= url('/'); ?>/frontassets/#"><span>Compare</span></a></div>
                                                        <div class="add-to-cart"><a href="<?= url('/'); ?>/frontassets/#"><span>Add to cart</span></a></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="product-thumb  mb_30">
                                                <div class="image product-imageblock"> <a href="<?= url('/'); ?>/frontassets/product_detail_page.php"> <img data-name="product_image" src="<?= url('/'); ?>/frontassets/images/product/product4.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive"> <img src="<?= url('/'); ?>/frontassets/images/product/product4-1.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive"> </a> </div>
                                                <div class="caption product-detail text-left">
                                                    <h6 data-name="product_name" class="product-name mt_20"><a href="<?= url('/'); ?>/frontassets/#" title="Casual Shirt With Ruffle Hem">Latin literature from 45 BC, making it over old.</a></h6>
                                                    <div class="rating"> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span> </div>
                                                    <span class="price"><span class="amount"><span class="currencySymbol">$</span>70.00</span>
                                                    </span>
                                                    <div class="button-group text-center">
                                                        <div class="wishlist"><a href="<?= url('/'); ?>/frontassets/#"><span>wishlist</span></a></div>
                                                        <div class="quickview"><a href="<?= url('/'); ?>/frontassets/#"><span>Quick View</span></a></div>
                                                        <div class="compare"><a href="<?= url('/'); ?>/frontassets/#"><span>Compare</span></a></div>
                                                        <div class="add-to-cart"><a href="<?= url('/'); ?>/frontassets/#"><span>Add to cart</span></a></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="product-grid">
                                        <div class="item">
                                            <div class="product-thumb  mb_30">
                                                <div class="image product-imageblock"> <a href="<?= url('/'); ?>/frontassets/product_detail_page.php"> <img data-name="product_image" src="<?= url('/'); ?>/frontassets/images/product/product5.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive"> <img src="<?= url('/'); ?>/frontassets/images/product/product5-1.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive"> </a> </div>
                                                <div class="caption product-detail text-left">
                                                    <h6 data-name="product_name" class="product-name mt_20"><a href="<?= url('/'); ?>/frontassets/#" title="Casual Shirt With Ruffle Hem">Latin literature from 45 BC, making it over old.</a></h6>
                                                    <div class="rating"> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span> </div>
                                                    <span class="price"><span class="amount"><span class="currencySymbol">$</span>70.00</span>
                                                    </span>
                                                    <div class="button-group text-center">
                                                        <div class="wishlist"><a href="<?= url('/'); ?>/frontassets/#"><span>wishlist</span></a></div>
                                                        <div class="quickview"><a href="<?= url('/'); ?>/frontassets/#"><span>Quick View</span></a></div>
                                                        <div class="compare"><a href="<?= url('/'); ?>/frontassets/#"><span>Compare</span></a></div>
                                                        <div class="add-to-cart"><a href="<?= url('/'); ?>/frontassets/#"><span>Add to cart</span></a></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="product-thumb  mb_30">
                                                <div class="image product-imageblock"> <a href="<?= url('/'); ?>/frontassets/product_detail_page.php"> <img data-name="product_image" src="<?= url('/'); ?>/frontassets/images/product/product7.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive"> <img src="<?= url('/'); ?>/frontassets/images/product/product7-1.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive"> </a> </div>
                                                <div class="caption product-detail text-left">
                                                    <h6 data-name="product_name" class="product-name mt_20"><a href="<?= url('/'); ?>/frontassets/#" title="Casual Shirt With Ruffle Hem">Latin literature from 45 BC, making it over old.</a></h6>
                                                    <div class="rating"> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span> </div>
                                                    <span class="price"><span class="amount"><span class="currencySymbol">$</span>70.00</span>
                                                    </span>
                                                    <div class="button-group text-center">
                                                        <div class="wishlist"><a href="<?= url('/'); ?>/frontassets/#"><span>wishlist</span></a></div>
                                                        <div class="quickview"><a href="<?= url('/'); ?>/frontassets/#"><span>Quick View</span></a></div>
                                                        <div class="compare"><a href="<?= url('/'); ?>/frontassets/#"><span>Compare</span></a></div>
                                                        <div class="add-to-cart"><a href="<?= url('/'); ?>/frontassets/#"><span>Add to cart</span></a></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="product-grid">
                                        <div class="item">
                                            <div class="product-thumb  mb_30">
                                                <div class="image product-imageblock"> <a href="<?= url('/'); ?>/frontassets/product_detail_page.php"> <img data-name="product_image" src="<?= url('/'); ?>/frontassets/images/product/product8.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive"> <img src="<?= url('/'); ?>/frontassets/images/product/product8-1.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive"> </a> </div>
                                                <div class="caption product-detail text-left">
                                                    <h6 data-name="product_name" class="product-name mt_20"><a href="<?= url('/'); ?>/frontassets/#" title="Casual Shirt With Ruffle Hem">Latin literature from 45 BC, making it over old.</a></h6>
                                                    <div class="rating"> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span> </div>
                                                    <span class="price"><span class="amount"><span class="currencySymbol">$</span>70.00</span>
                                                    </span>
                                                    <div class="button-group text-center">
                                                        <div class="wishlist"><a href="<?= url('/'); ?>/frontassets/#"><span>wishlist</span></a></div>
                                                        <div class="quickview"><a href="<?= url('/'); ?>/frontassets/#"><span>Quick View</span></a></div>
                                                        <div class="compare"><a href="<?= url('/'); ?>/frontassets/#"><span>Compare</span></a></div>
                                                        <div class="add-to-cart"><a href="<?= url('/'); ?>/frontassets/#"><span>Add to cart</span></a></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="product-thumb  mb_30">
                                                <div class="image product-imageblock"> <a href="<?= url('/'); ?>/frontassets/product_detail_page.php"> <img data-name="product_image" src="<?= url('/'); ?>/frontassets/images/product/product9.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive"> <img src="<?= url('/'); ?>/frontassets/images/product/product9-1.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive"> </a> </div>
                                                <div class="caption product-detail text-left">
                                                    <h6 data-name="product_name" class="product-name mt_20"><a href="<?= url('/'); ?>/frontassets/#" title="Casual Shirt With Ruffle Hem">Latin literature from 45 BC, making it over old.</a></h6>
                                                    <div class="rating"> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span> </div>
                                                    <span class="price"><span class="amount"><span class="currencySymbol">$</span>70.00</span>
                                                    </span>
                                                    <div class="button-group text-center">
                                                        <div class="wishlist"><a href="<?= url('/'); ?>/frontassets/#"><span>wishlist</span></a></div>
                                                        <div class="quickview"><a href="<?= url('/'); ?>/frontassets/#"><span>Quick View</span></a></div>
                                                        <div class="compare"><a href="<?= url('/'); ?>/frontassets/#"><span>Compare</span></a></div>
                                                        <div class="add-to-cart"><a href="<?= url('/'); ?>/frontassets/#"><span>Add to cart</span></a></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="product-grid">
                                        <div class="item">
                                            <div class="product-thumb  mb_30">
                                                <div class="image product-imageblock"> <a href="<?= url('/'); ?>/frontassets/product_detail_page.php"> <img data-name="product_image" src="<?= url('/'); ?>/frontassets/images/product/product10.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive"> <img src="<?= url('/'); ?>/frontassets/images/product/product10-1.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive"> </a> </div>
                                                <div class="caption product-detail text-left">
                                                    <h6 data-name="product_name" class="product-name mt_20"><a href="<?= url('/'); ?>/frontassets/#" title="Casual Shirt With Ruffle Hem">Latin literature from 45 BC, making it over old.</a></h6>
                                                    <div class="rating"> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span> </div>
                                                    <span class="price"><span class="amount"><span class="currencySymbol">$</span>70.00</span>
                                                    </span>
                                                    <div class="button-group text-center">
                                                        <div class="wishlist"><a href="<?= url('/'); ?>/frontassets/#"><span>wishlist</span></a></div>
                                                        <div class="quickview"><a href="<?= url('/'); ?>/frontassets/#"><span>Quick View</span></a></div>
                                                        <div class="compare"><a href="<?= url('/'); ?>/frontassets/#"><span>Compare</span></a></div>
                                                        <div class="add-to-cart"><a href="<?= url('/'); ?>/frontassets/#"><span>Add to cart</span></a></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="product-thumb  mb_30">
                                                <div class="image product-imageblock"> <a href="<?= url('/'); ?>/frontassets/product_detail_page.php"> <img data-name="product_image" src="<?= url('/'); ?>/frontassets/images/product/product1.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive"> <img src="<?= url('/'); ?>/frontassets/images/product/product1-1.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive"> </a> </div>
                                                <div class="caption product-detail text-left">
                                                    <h6 data-name="product_name" class="product-name mt_20"><a href="<?= url('/'); ?>/frontassets/#" title="Casual Shirt With Ruffle Hem">Latin literature from 45 BC, making it over old.</a></h6>
                                                    <div class="rating"> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span> </div>
                                                    <span class="price"><span class="amount"><span class="currencySymbol">$</span>70.00</span>
                                                    </span>
                                                    <div class="button-group text-center">
                                                        <div class="wishlist"><a href="<?= url('/'); ?>/frontassets/#"><span>wishlist</span></a></div>
                                                        <div class="quickview"><a href="<?= url('/'); ?>/frontassets/#"><span>Quick View</span></a></div>
                                                        <div class="compare"><a href="<?= url('/'); ?>/frontassets/#"><span>Compare</span></a></div>
                                                        <div class="add-to-cart"><a href="<?= url('/'); ?>/frontassets/#"><span>Add to cart</span></a></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- =====  PRODUCT TAB  END ===== -->

            <!-- =====  SUB BANNER  STRAT ===== -->
            <div class="row">
                <div class="cms_banner mt_10 mb_20 fullwidth">
                    <div class="col-xs-12 nopadding">
                        <div id="subbanner4" class="banner sub-hover"> <a href="#"><img src="<?= url('/'); ?>/frontassets/images/sub4.jpg" alt="Sub Banner4" class="img-responsive fullwidth"></a>
                            <div class="bannertext"> </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- =====  SUB BANNER END  ===== -->
            <!-- =====  sale product  ===== -->
            <div class="row white-bg mb_20">
                <div class="col-xs-12">
                    <div id="sale-product">
                        <div class="heading-part mt_10 ">
                            <h2 class="main_title">Best Deals</h2>
                        </div>
                        <div class="Specials owl-carousel mb_20">
                            <div class="item product-layout product-list">
                                <div class="product-thumb">
                                    <div class="image product-imageblock"> <a href="<?= url('/'); ?>/frontassets/product_detail_page.php"> <img data-name="product_image" src="<?= url('/'); ?>/frontassets/images/product/product8.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive"> <img src="<?= url('/'); ?>/frontassets/images/product/product8-1.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive"> </a> </div>
                                    <div class="caption product-detail text-left">
                                        <h6 data-name="product_name" class="product-name"><a href="<?= url('/'); ?>/frontassets/#" title="Casual Shirt With Ruffle Hem">Latin literature from 45 BC, making it over old.</a></h6>
                                        <div class="rating"> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span> </div>
                                        <span class="price"><span class="amount"><span class="currencySymbol">$</span>70.00</span>
                                        </span>
                                        <p class="product-desc mt_20"> More room to move. With 80GB or 160GB of storage and up to 40 hours of battery life, the new iPod classic lets you enjoy up to 40,000 songs or up to 200 hours of video or any combination wherever you go.Cover Flow. Browse through your music collection by flipping..</p>
                                        <div class="timer mt_80">
                                            <div class="days"></div>
                                            <div class="hours"></div>
                                            <div class="minutes"></div>
                                            <div class="seconds"></div>
                                        </div>
                                       <!--  <div class="button-group text-center">
                                            <div class="wishlist"><a href="<?= url('/'); ?>/frontassets/#"><span>wishlist</span></a></div>
                                            <div class="quickview"><a href="<?= url('/'); ?>/frontassets/#"><span>Quick View</span></a></div>
                                            <div class="compare"><a href="<?= url('/'); ?>/frontassets/#"><span>Compare</span></a></div>
                                            <div class="add-to-cart"><a href="<?= url('/'); ?>/frontassets/#"><span>Add to cart</span></a></div>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                            <div class="item product-layout product-list">
                                <div class="product-thumb">
                                    <div class="image product-imageblock"> <a href="<?= url('/'); ?>/frontassets/product_detail_page.php"> <img data-name="product_image" src="<?= url('/'); ?>/frontassets/images/product/product7.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive"> <img src="<?= url('/'); ?>/frontassets/images/product/product7-1.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive"> </a> </div>
                                    <div class="caption product-detail text-left">
                                        <h6 data-name="product_name" class="product-name"><a href="<?= url('/'); ?>/frontassets/#" title="Casual Shirt With Ruffle Hem">Latin literature from 45 BC, making it over old.</a></h6>
                                        <div class="rating"> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span> </div>
                                        <span class="price"><span class="amount"><span class="currencySymbol">$</span>70.00</span>
                                        </span>
                                        <p class="product-desc mt_20"> More room to move. With 80GB or 160GB of storage and up to 40 hours of battery life, the new iPod classic lets you enjoy up to 40,000 songs or up to 200 hours of video or any combination wherever you go.Cover Flow. Browse through your music collection by flipping..</p>
                                        <div class="timer mt_80">
                                            <div class="days"></div>
                                            <div class="hours"></div>
                                            <div class="minutes"></div>
                                            <div class="seconds"></div>
                                        </div>
                                        <!-- <div class="button-group text-center">
                                            <div class="wishlist"><a href="<?= url('/'); ?>/frontassets/#"><span>wishlist</span></a></div>
                                            <div class="quickview"><a href="<?= url('/'); ?>/frontassets/#"><span>Quick View</span></a></div>
                                            <div class="compare"><a href="<?= url('/'); ?>/frontassets/#"><span>Compare</span></a></div>
                                            <div class="add-to-cart"><a href="<?= url('/'); ?>/frontassets/#"><span>Add to cart</span></a></div>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                            <div class="item product-layout product-list">
                                <div class="product-thumb">
                                    <div class="image product-imageblock"> <a href="<?= url('/'); ?>/frontassets/product_detail_page.php"> <img data-name="product_image" src="<?= url('/'); ?>/frontassets/images/product/product6.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive"> <img src="<?= url('/'); ?>/frontassets/images/product/product6-1.jpg" alt="iPod Classic" title="iPod Classic" class="img-responsive"> </a> </div>
                                    <div class="caption product-detail text-left">
                                        <h6 data-name="product_name" class="product-name"><a href="<?= url('/'); ?>/frontassets/#" title="Casual Shirt With Ruffle Hem">Latin literature from 45 BC, making it over old.</a></h6>
                                        <div class="rating"> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span> </div>
                                        <span class="price"><span class="amount"><span class="currencySymbol">$</span>70.00</span>
                                        </span>
                                        <p class="product-desc mt_20"> More room to move. With 80GB or 160GB of storage and up to 40 hours of battery life, the new iPod classic lets you enjoy up to 40,000 songs or up to 200 hours of video or any combination wherever you go.Cover Flow. Browse through your music collection by flipping..</p>
                                        <div class="timer mt_80">
                                            <div class="days"></div>
                                            <div class="hours"></div>
                                            <div class="minutes"></div>
                                            <div class="seconds"></div>
                                        </div>
                                        <!-- <div class="button-group text-center">
                                            <div class="wishlist"><a href="<?= url('/'); ?>/frontassets/#"><span>wishlist</span></a></div>
                                            <div class="quickview"><a href="<?= url('/'); ?>/frontassets/#"><span>Quick View</span></a></div>
                                            <div class="compare"><a href="<?= url('/'); ?>/frontassets/#"><span>Compare</span></a></div>
                                            <div class="add-to-cart"><a href="<?= url('/'); ?>/frontassets/#"><span>Add to cart</span></a></div>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- =====  sale product end ===== -->

           
        </div>
    </div> 
    <!--=========Logo slider========-->
    <div class="row white-bg mb_20">
         <div class="col-sm-12">
             <!-- =====  product ===== -->
            <div class="row white-bg mb_20">
                <div class="col-md-4">
                    <div class="heading-part mt_10 ">
                        <h2 class="main_title">Most Viewed</h2>
                    </div>
                    <div id="featu-pro" class="owl-carousel">
                        <?php
                        $dataArr = [];
                        if ($recentproducts):
                            foreach ($recentproducts as $key => $prod) {
                                array_push($dataArr, $prod);
                            }
                            $item_count = (int) ($recentproductcount / 3);
                            if (is_float($item_count) || $item_count == 0) {
                                $item_count = $item_count + 1;
                            }
                            for ($i = 0; $i < $item_count; $i++):
                                ?>
                                <ul class="row ">
                                    <?php
                                    for ($j = 0; $j < 3; $j++):
                                        $product = array_pop($dataArr);
                                        if ($product):
                                            ?>
                                            <li class="item product-layout-left mb_20">
                                                <div class="product-list col-xs-4">
                                                    <div class="product-thumb">
                                                        <div class="image product-imageblock"> 
                                                            <a href="/product_detail/{{$product->id}}"> 
                                                                <img class="img-responsive" title="{{$product->name}}" alt="product image" src="{{!empty($product->fileDetail)?$product->fileDetail->file_path.$product->fileDetail->file_name:''}}" style="height:75px"> 
                                                                <img class="img-responsive" title="{{$product->name}}" alt="product image" src="{{!empty($product->fileDetail)?$product->fileDetail->file_path.$product->fileDetail->file_name:''}}" style="height:75px">
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-8">
                                                    <div class="caption product-detail">
                                                        <h6 class="product-name"><a href="/product_detail/{{$product->id}}"><?= substr($product->name, 0, 24) . '...' ?></a></h6>
                                                        <div class="rating">
                                                            @if($prod->rating == 0)
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            @elseif($prod->rating == 1)
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            @elseif($prod->rating == 2)
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            @elseif($prod->rating == 3)
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            @elseif($prod->rating == 4)
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            @elseif($prod->rating == 5)
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            @endif

                                                        </div>
                                                        <!-- <span><i class="fa fa-inr"></i> <del class="text-danger">14990</del></span>
                                                        <span class="price"><span class="amount"><span class="currencySymbol"><i class="fa fa-inr"></i> </span>{{$product->price}}</span></span> -->

                                                @if($product->discount_percent!=0)
                                                    <span><i class="fa fa-inr"></i> <del class="text-danger">{{$product->price}}</del></span>
                                                    @endif
                                <span class="price"><span class="amount"><span class="currencySymbol"><i class="fa fa-inr"></i> </span><?= number_format($product->price - (($product->discount_percent * $product->price) / 100), 2, '.', ''); ?></span>
                                </span>
                                                    </div>
                                                </div>
                                            </li>
                                            <?php
                                        endif;
                                    endfor;
                                    ?>
                                </ul>
                                <?php
                            endfor;
                        endif;
                        ?>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="heading-part mt_10 ">
                        <h2 class="main_title">Recommended</h2>
                    </div>
                    <div id="bests-pro" class="owl-carousel">
                        <?php
                        $dataArr = [];
                        if ($recommenditems):
                            foreach ($recommenditems as $key => $prod) {
                                array_push($dataArr, $prod);
                            }
                            $item_count = (int) ($recommenditemscount / 3);
                            if (is_float($item_count) || $item_count == 0) {
                                $item_count = $item_count + 1;
                            }
                            for ($i = 0; $i < $item_count; $i++):
                                ?>
                                <ul class="row ">
                                    <?php
                                    for ($j = 0; $j < 3; $j++):
                                        $product = array_pop($dataArr);
                                        if ($product):
                                            ?>
                                            <li class="item product-layout-left mb_20">
                                                <div class="product-list col-xs-4">
                                                    <div class="product-thumb">
                                                        <div class="image product-imageblock"> 
                                                            <a href="/product_detail/{{$product->id}}"> 
                                                                <img class="img-responsive" title="{{$product->name}}" alt="product image" src="{{!empty($product->fileDetail)?$product->fileDetail->file_path.$product->fileDetail->file_name:''}}" style="height:75px"> 
                                                                <img class="img-responsive" title="{{$product->name}}" alt="product image" src="{{!empty($product->fileDetail)?$product->fileDetail->file_path.$product->fileDetail->file_name:''}}" style="height:75px">
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-8">
                                                    <div class="caption product-detail">
                                                        <h6 class="product-name"><a href="/product_detail/{{$product->id}}"><?= substr($product->name, 0, 24) . '...' ?></a></h6>
                                                        <div class="rating">
                                                            @if($data->rating == 0)
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            @elseif($data->rating == 1)
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            @elseif($data->rating == 2)
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            @elseif($data->rating == 3)
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            @elseif($data->rating == 4)
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            @elseif($data->rating == 5)
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            @endif

                                                        </div>
                                                        <!-- <span><i class="fa fa-inr"></i> <del class="text-danger">14990</del></span>
                                                        <span class="price"><span class="amount"><span class="currencySymbol"><i class="fa fa-inr"></i> </span>{{$product->price}}</span>
                                                        </span> -->
                                            @if($product->discount_percent!=0)
                                                    <span><i class="fa fa-inr"></i> <del class="text-danger">{{$product->price}}</del></span>
                                                    @endif
                                <span class="price"><span class="amount"><span class="currencySymbol"><i class="fa fa-inr"></i> </span><?= number_format($product->price - (($product->discount_percent * $product->price) / 100), 2, '.', ''); ?></span>
                                </span>
                                                    </div>
                                                </div>
                                            </li>
                                            <?php
                                        endif;
                                    endfor;
                                    ?>
                                </ul>
                                <?php
                            endfor;
                        endif;
                        ?>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="heading-part mt_10 ">
                        <h2 class="main_title">New Item’s</h2>
                    </div>
                    <div id="new-pro" class="owl-carousel">
                        <?php
                        $dataArr = [];
                        if ($newitems):
                            foreach ($newitems as $key => $prod) {
                                array_push($dataArr, $prod);
                            }
                            $item_count = (int) ($newitemscount / 3);
                            if (is_float($item_count) || $item_count == 0) {
                                $item_count = $item_count + 1;
                            }
                            for ($i = 0; $i < $item_count; $i++):
                                ?>
                                <ul class="row ">
                                    <?php
                                    for ($j = 0; $j < 3; $j++):
                                        $product = array_pop($dataArr);
                                        if ($product):
                                            ?>
                                            <li class="item product-layout-left mb_20">
                                                <div class="product-list col-xs-4">
                                                    <div class="product-thumb">
                                                        <div class="image product-imageblock"> 
                                                            <a href="/product_detail/{{$product->id}}"> 
                                                                <img class="img-responsive" title="{{$product->name}}" alt="product image" src="{{!empty($product->fileDetail)?$product->fileDetail->file_path.$product->fileDetail->file_name:''}}" style="height:75px"> 
                                                                <img class="img-responsive" title="{{$product->name}}" alt="product image" src="{{!empty($product->fileDetail)?$product->fileDetail->file_path.$product->fileDetail->file_name:''}}" style="height:75px">
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-8">
                                                    <div class="caption product-detail">
                                                        <h6 class="product-name"><a href="/product_detail/{{$product->id}}"><?= substr($product->name, 0, 24) . '...' ?></a></h6>
                                                        <div class="rating">
                                                            @if($data->rating == 0)
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            @elseif($data->rating == 1)
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            @elseif($data->rating == 2)
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            @elseif($data->rating == 3)
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            @elseif($data->rating == 4)
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                                            @elseif($data->rating == 5)
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                                            @endif

                                                        </div>
                                                        <!-- <span><i class="fa fa-inr"></i> <del class="text-danger">14990</del></span>
                                                        <span class="price"><span class="amount"><span class="currencySymbol"><i class="fa fa-inr"></i> </span>{{$product->price}}</span>
                                                        </span> -->
                                                @if($product->discount_percent!=0)
                                                    <span><i class="fa fa-inr"></i> <del class="text-danger">{{$product->price}}</del></span>
                                                    @endif
                                <span class="price"><span class="amount"><span class="currencySymbol"><i class="fa fa-inr"></i> </span><?= number_format($product->price - (($product->discount_percent * $product->price) / 100), 2, '.', ''); ?></span>
                                </span>
                                                    </div>
                                                </div>
                                            </li>
                                            <?php
                                        endif;
                                    endfor;
                                    ?>
                                </ul>
                                <?php
                            endfor;
                        endif;
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div id="brand_carouse" class="pb_30 text-center">
            <div class="type-01">
                <div class="col-sm-12">
                    <div class="heading-part pt_10">
                        <h2 class="main_title">Our Featured Brand</h2>
                    </div>
                <div class="brand owl-carousel pt_20"  > 
                        @if($featuredbrands)
                        @foreach($featuredbrands as $brand)
                        <div class="item text-center" style="width:150px!important;height:150px!important;"> 
                            <a href="#">
                                <img src="{{!empty($brand->fileDetail)?$brand->fileDetail->file_path.$brand->fileDetail->file_name:url('/').'/frontassets/images/no_img.png'}}" alt="{{$brand->title}}" class="img-responsive" / >
                            </a>
                        </div>                        
                        @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--=========Logo slider END========-->

    <!--=========Logo slider========-->
    <div class="row white-bg mb_20">


        <div id="brand_carouse" class="pb_30 text-center">
            <div class="type-01">
                <div class="col-sm-12">
                    <div class="heading-part pt_10">
                        <h2 class="main_title">Our Top Achievers</h2>
                    </div>


                    <div class="brand owl-carousel pt_20">
                         @if($achiever)
                        @foreach($achiever as $key=>$value)
                        <div class="item client-detail">
                            <div class="client-avatar"> 
                                <img alt="" src="<?= url('/'); ?>/images/testimonialsimages/{{$value->image}}" style="width: 101px!important;height: 101px;width: 101!important;"> 
                            </div>
                            <div class="client-title  mt_30"><strong>{{$value->name}}</strong></div>
                            <div class="client-designation mb_10">{{$value->description}}</div>
                        </div>
                        @endforeach
                        @endif
                        

                        <!-- <div class="item client-detail">
                            <div class="client-avatar"> <img alt="" src="<?= url('/'); ?>/frontassets/images/user1.jpg"> </div>
                            <div class="client-title  mt_30"><strong>joseph Lui</strong></div>
                            <div class="client-designation mb_10">php Developer</div>
                        </div>  -->

                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--=========Logo slider END========-->

    <!-- =====  Blog ===== -->
    <div class="row white-bg mb_20">
        <div class="col-sm-12 col-md-12 col-lg-12">

            <div id="Blog" class="">
                <div class="heading-part mt_10 ">
                    <h2 class="main_title">Latest from the Blog</h2>
                </div>
                <div class="blog-contain box">
                    <div class="blog owl-carousel ">
                        @if($blogs)
                        @foreach($blogs as $key=>$value)
                        <div class="item">
                            <div class="box-holder">
                                <div class="thumb post-img">
                                    <a href="/blog_detail/{{$value->id}}"> 
                                    <img src="<?= url('/'); ?>/images/testimonialsimages/{{$value->image}}" alt="HealthCare" style="width:410px;height: 218px;"> 
                                   </a> 
                                </div>
                                <div class="post-info mtb_20 ">
                                    <h6 class="mb_10 text-uppercase"> <a href="#">{{$value->name}}.</a> </h6>
                                    <p class="blogpara">
                                        <?= substr($value->description, 0, 130) . '...' ?></p>
                                    <div class="date-time">
                                        <div class="day"> 11</div>
                                        <div class="month">Aug</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        @endif
                       <!-- <div class="item">
                            <div class="box-holder">
                                <div class="thumb post-img"><a href="<?= url('/'); ?>/frontassets/#"> <img src="<?= url('/'); ?>/frontassets/images/blog/blog_img_02.jpg" alt="HealthCare"> </a></div>
                                <div class="post-info mtb_20 ">
                                    <h6 class="mb_10 text-uppercase"> <a href="<?= url('/'); ?>/frontassets/single_blog.php">Unknown printer took a galley book.</a> </h6>
                                    <p>Aliquam egestas pellentesque est. Etiam a orci Nulla id enim feugiat ligula ullamcorper scelerisque. Morbi eu luctus nisl.</p>
                                    <div class="date-time">
                                        <div class="day"> 11</div>
                                        <div class="month">Aug</div>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                       <!--  <div class="item">
                            <div class="box-holder">
                                <div class="thumb post-img"><a href="<?= url('/'); ?>/frontassets/#"> <img src="<?= url('/'); ?>/frontassets/images/blog/blog_img_03.jpg" alt="HealthCare"> </a></div>
                                <div class="post-info mtb_20 ">
                                    <h6 class="mb_10 text-uppercase"> <a href="<?= url('/'); ?>/frontassets/single_blog.php">Unknown printer took a galley book.</a> </h6>
                                    <p>Aliquam egestas pellentesque est. Etiam a orci Nulla id enim feugiat ligula ullamcorper scelerisque. Morbi eu luctus nisl.</p>
                                    <div class="date-time">
                                        <div class="day"> 11</div>
                                        <div class="month">Aug</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="box-holder">
                                <div class="thumb post-img"><a href="<?= url('/'); ?>/frontassets/#"> <img src="<?= url('/'); ?>/frontassets/images/blog/blog_img_04.jpg" alt="HealthCare"> </a></div>
                                <div class="post-info mtb_20 ">
                                    <h6 class="mb_10 text-uppercase"> <a href="<?= url('/'); ?>/frontassets/single_blog.php">Unknown printer took a galley book.</a> </h6>
                                    <p>Aliquam egestas pellentesque est. Etiam a orci Nulla id enim feugiat ligula ullamcorper scelerisque. Morbi eu luctus nisl.</p>
                                    <div class="date-time">
                                        <div class="day"> 11</div>
                                        <div class="month">Aug</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="box-holder">
                                <div class="thumb post-img"><a href="<?= url('/'); ?>/frontassets/#"> <img src="<?= url('/'); ?>/frontassets/images/blog/blog_img_05.jpg" alt="HealthCare"> </a></div>
                                <div class="post-info mtb_20 ">
                                    <h6 class="mb_10 text-uppercase"> <a href="<?= url('/'); ?>/frontassets/single_blog.php">Unknown printer took a galley book.</a> </h6>
                                    <p>Aliquam egestas pellentesque est. Etiam a orci Nulla id enim feugiat ligula ullamcorper scelerisque. Morbi eu luctus nisl.</p>
                                    <div class="date-time">
                                        <div class="day"> 11</div>
                                        <div class="month">Aug</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="box-holder">
                                <div class="thumb post-img"><a href="<?= url('/'); ?>/frontassets/#"> <img src="<?= url('/'); ?>/frontassets/images/blog/blog_img_06.jpg" alt="HealthCare"> </a></div>
                                <div class="post-info mtb_20 ">
                                    <h6 class="mb_10 text-uppercase"> <a href="<?= url('/'); ?>/frontassets/single_blog.php">Unknown printer took a galley book.</a> </h6>
                                    <p>Aliquam egestas pellentesque est. Etiam a orci Nulla id enim feugiat ligula ullamcorper scelerisque. Morbi eu luctus nisl.</p>
                                    <div class="date-time">
                                        <div class="day"> 11</div>
                                        <div class="month">Aug</div>
                                    </div>
                                </div>
                            </div>
                        </div>-->
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- =====  Blog end ===== -->
</div>
<!-- =====  CONTAINER END  ===== -->
@endsection

