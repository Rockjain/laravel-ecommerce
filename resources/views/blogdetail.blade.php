<style type="text/css">
.slider-section .item {
    min-height: 350px!important;
}
.post-outer {
    -moz-box-shadow: 0 0 10px -6px #999,0 0 0 1px #F8F8F8;
    -webkit-box-shadow: 0 0 10px -6px #999, 0 0 0 1px #F8F8F8;
    box-shadow: 0 0 10px -6px #999, 0 0 0 1px #F8F8F8;
    margin: 0 0 70px;
    padding-bottom: 0;
}
.post-body {
    background: #ffffff;
    font: normal normal 14px Lora, Helvetica, FreeSans, sans-serif;
    line-height: 1.74;
    word-wrap: break-word;
}
  .article_container {
    position: relative;
        margin-top: 50px;
}
.meta-wrapper {
    position: absolute;
    top: -28px;
    width: 100%;
}
.meta-wrapper > .meta-inner, .meta-wrapper > .meta-inner{
      background-color: #333333;
}
.meta-wrapper > .meta-inner {
    background: #333333;
    display: table;
    margin: 0 auto;
    padding: 5px 10px;
}
.article_date {
    font-size: 12px;
    position: relative;
    width: auto;
}
.sep {
    background: #fff;
    display: table;
    height: 1px;
    margin: 6px auto;
    width: 30px;
}
.meta-wrapper > .meta-inner, .meta-wrapper > .meta-inner a {
    color: #fff;
    font-family: montserrat,sans-serif;
   
    font-weight: 700;
    letter-spacing: .4px;
    text-align: center;
    text-transform: uppercase;
}
 .article_image {
    float: none;
    margin-bottom: 0;
    width: 100%;
}
a {
    color: #333333;
    outline: none;
    text-decoration: none;
}
.article_inner {
    clear: both;
    padding: 30px 40px 0;
    position: relative;
}
.post-body h1, .post-body h2, .post-body h3, .post-body h4, .post-body h5, .post-body h6 {
    margin-bottom: 15px;
    color: #2c3e50;
        font-family: Montserrat, Helvetica, FreeSans, sans-serif;
}
h2.post-title, h2.entry-title, h1.post-title {
    font-size: 36px;
    font-weight: 700;
    letter-spacing: -1.15px;
    line-height: 1.25;
    margin: 0 0 15px;
    text-transform: capitalize;
}
.post-title a {
    color: #333;
}
.article-meta, .article-meta a {
    color: #555;
    font-size: 12px;
}
.article-meta {
    border-color: #DDD;
    border-style: solid;
    border-width: 0;
    margin-bottom: 10px;
    padding: 5px 0;
    text-transform: lowercase;
}
.article_excerpt {
    margin-bottom: 30px;
}
.article_excerpt p {
    color: #555;
    font-weight: 400;
}
.article_footer {
    display: table;
    padding: 0 40px 35px;
    position: relative;
    width: 100%;
}
.cntinue_rdmre {
    float: left;
}
.clearfix:before, .clearfix:after {
    display: table;
    content: "";
    line-height: 0;
}
.cntinue_rdmre a {
    background-color: #333333;
    color: #fff;
    display: table;
    line-height: 1;
    margin: 0;
    padding: 12px 15px;
    position: relative;
    transition: all .2s ease-in-out;
}
.article_share_social {
    display: inline-block;
    float: right;
    text-align: right;
}
.widget .post-body ul, .widget .post-body ol {
    line-height: 1.5;
    font-weight: 400;
}

.article_share_social ul {
    float: right;
    margin: 0;
    padding: 0;
}
.widget .post-body li {
    margin: 5px 0;
    padding: 0;
    line-height: 1.5;
}

.post-body ul li {
    list-style: none;
}
.article_share_social ul li {
    display: inline-block;
    list-style: none;
    margin: 0 0 0 2px;
    padding: 0;
}
.sidebar-wrapper {
    margin-top: 50px;
}
#sidebar {
    display: block;
    margin: 0;
    padding: 0;
}
.sidebar .widget {
    background-color: #fff;
    box-shadow: 0 0 4px -1px #ccc, inset 0 2px 0 transparent;
    clear: both;
    line-height: 1.5;
    margin-bottom: 40px;
    padding: 20px 15px;
    position: relative;
}
.sidebar h2 {
    color: #333;
    font-family: Montserrat, sans-serif;
    font-size: 14px;
    font-weight: 700;
    line-height: 1.2;
    margin-bottom: 30px;
    padding: 0;
    position: relative;
    text-transform: uppercase;
}
#ArchiveList {
    text-align: left;
}
.BlogArchive #ArchiveList ul {
    margin: 0;
    padding: 0;
    list-style: none;
    list-style-image: none;
    border-width: 0;
}
.BlogArchive #ArchiveList ul li {
    background: none;
    list-style: none;
    list-style-image: none;
    list-style-position: outside;
    border-width: 0;
    padding-left: 15px;
    text-indent: -15px;
    margin: .25em 0;
    background-image: none;
}
.PopularPosts .item-title {
    font-weight: 400;
    padding-bottom: .2em;
}
.PopularPosts .item-thumbnail img {
    -moz-box-shadow: 0 0 10px -4px #999;
    -ms-box-shadow: 0 0 10px -4px #999;
    -o-box-shadow: 0 0 10px -4px #999;
    -webkit-box-shadow: 0 0 10px -4px #999;
    box-shadow: 0 0 10px -4px #999;
    display: block;
    float: left;
    height: 90px;
    margin-right: 10px;
    padding: 0;
    width: 90px;
}
.PopularPosts .item-thumbnail {
    float: left;
    margin: 0 5px 5px 0;
}
.PopularPosts .widget-content ul li {
    padding: .7em 0;
}

.sidebar li {
    border-bottom: 1px solid #F1F1F1;
    line-height: normal;
    list-style: none!important;
    margin: 8px 0;
    overflow: hidden;
    padding: 0 0 10px;
}
.cntinue_rdmre:hover a{
   color: #fff;
   background: #ff0000;
}
.owl-dot{
    display: none!important;
}
.owl-nav{
    display: none!important;
}
</style>

@extends('layouts.default')
@section('content')
    <!-- =====  CONTAINER START  ===== -->
	
	 <div class="slider-section fullwidth">
        <div class="container-fluid">
          <div class="row">
            
			   <!-- =====  BANNER STRAT  ===== -->
          <div class="col-sm-12 col-md-12 col-lg-12 nopadding">
            <div class="banner">
              <div class="main-banner owl-carousel">

                <div class="item" style="background:url(<?= url('/'); ?>/frontassets/images/slider-30.jpg)">
                </div>
              </div>


            </div>

          </div>
		  </div>
          <!-- =====  BANNER END  ===== -->
		
          </div>
        </div>
	  
	  
	  
    <div class="container">
	  
	   <!-- =====  Blog ===== -->
	   <div class="row white-bg mb_20">
        <div class="col-sm-12 col-md-12 col-lg-12">
       
          <div id="Blog" class="">
            <div class="heading-part mt_10 ">
              <h2 class="main_title">Blog</h2>
            </div>
          
              <div class="row">

                <div class="col-sm-7">
                  <div class="post-outer">
                    <div class=".post-body">
                  <div class="article_container">
                    <div class="meta-wrapper">
                      <div class="meta-inner">
                        <div class="article_date">
                          <span>15/02/2019</span>
                        </div>
                        <div class="sep">
                        </div>
                        <!-- <div class="article_comments meta">
                          <a href="">
                            <span>3 comments</span></a>
                          </div> -->
                      </div>
                          </div><div class="article_image">
                            <a title="" href="#">
                              <img src="<?= url('/'); ?>/images/testimonialsimages/{{$blogs->image}}" class="img-responsive" style="    max-height: 400px;height: 400px;">
                            </a></div><div class="article_inner"><div class="post-header">
                            <h2 class="post-title entry-title">
                              <a href="#">{{$blogs->name}}</a></h2><div class="article-meta">Posted by <span class="meta-author"><a href="#">Way2 Themes</a></span> | <span><a href="#" rel="tag">Photography, </a></span></div></div>
                          <div class="article_excerpt clearfix"><p>{{$blogs->description}}...</p>
                          </div>
                      </div>
                      <div class="article_footer clearfix">
                         <div class="cntinue_rdmre">
                            <a href="http://dev.aaima.org/blog"><< Back</a>
                        </div>
                        <!-- <div class="article_share_social">
                            <ul class="clearfix">
                            <span>Share: </span>
                            <li class="facebook-social">
                            <a data-title="Facebook" href="#"  rel="nofollow" target="_blank"><i class="fa fa-facebook ct-transition-up"></i>
                            </a>
                            </li>
                            <li class="twitter-social">
                            <a data-title="Twitter" href="#" rel="nofollow" target="_blank"><i class="fa fa-twitter ct-transition-up"></i></a>
                            </li>
                            <li class="pinterest-social">
                            <a data-title="Pinterest" rel="nofollow" target="_blank">
                            <i class="fa fa-pinterest ct-transition-up"></i>
                            </a>
                            </li>
                            </ul>
                        </div> -->
                      </div>
                  </div>
                 </div>
                 </div>
                </div>
                <div class="col-sm-5">


                  <div class="sidebar-wrapper">
<div class="sidebar col section" id="sidebar">
<div class="widget PopularPosts" data-version="1" id="PopularPosts100">
<h2>Popular Posts</h2>
<div class="widget-content popular-posts">
<ul>
<li>
<div class="item-content">
<div class="item-thumbnail">
<a href="#">
<img alt="" border="0" height="72" src="https://2.bp.blogspot.com/-bqMZCxn4GtE/WgbD2Q--ixI/AAAAAAAACb8/wViFnW2R-48egsjsJzKP2MikzopVhSTlACLcBGAs/s72-c/pexels-photo-239583.jpeg" width="72">
</a>
</div>
<div class="item-title"><a href="#">If You Can Believe You Can Achieve</a></div>
<div class="item-snippet">Occupy small batch Thundercats Portland 3 wolf moon distillery lo-fi, vegan scenester. Pop-up put a bird on it drinking vinegar craft beer, ...</div>
</div>
<div style="clear: both;"></div>
</li>
<li>
<div class="item-content">
<div class="item-thumbnail">
<a href="#" target="_blank">
<img alt="" border="0" height="72" src="https://1.bp.blogspot.com/-0PW6gteWHoU/WgbFLkOv9aI/AAAAAAAACcM/t-2RYVIBvxQcMKIoO4Wn7pT6A64um4xOgCLcBGAs/s72-c/pexels-photo-221438.jpeg" width="72">
</a>
</div>
<div class="item-title"><a href="#">Truth lies behind the bushes of Lies</a></div>
<div class="item-snippet">Vegan mlkshk hella, literally American Apparel Pitchfork taxidermy bitters kale chips kitsch PBR&amp;B Carles Bushwick gastropub. Umami stum...</div>
</div>
<div style="clear: both;"></div>
</li>

</ul>
</div>
</div>
<div class="widget HTML" data-version="1" id="HTML3">
<h2>Ad Banner</h2>
<div class="widget-content">
<div id="sidebar__ad">
<img src="//3.bp.blogspot.com/-vZnqSpZD1sU/VLkwb6SHh3I/AAAAAAAAAmw/SvL4siV_7x4/s1600/ad-1.jpg">
</div>
</div>
</div>
</div>
</div>
                </div>
              
              
             
              </div>
        </div>
	   </div>
      </div>
	    <!-- =====  Blog end ===== -->
	   
    </div>
    <!-- =====  CONTAINER END  ===== -->
	@endsection
	