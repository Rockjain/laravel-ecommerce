@extends('layouts.account')
@section('content')
<div class="container mtb_30">

    <div class="row">
        <div class="col-sm-4 col-md-3" id="column-left">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="panel panel-default account-sidebar">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <?php
                                   $session = Session::get('user_data');
                                    if (!empty($session)):
                                        $customerInfo = getCustomer($session['customer_id']);
                                        if ($customerInfo):
                                            $fileInfo = getFile($customerInfo->photo);
                                            if ($fileInfo) {
                                                $photo = $fileInfo->file_path . $fileInfo->file_name;
                                            } else {
                                                $photo = url('/') . '/frontassets/images/no_img.png';
                                            }
                                        endif;
                                    endif;
                                    ?>
                                    <div class="pull-left"><p><img src="<?= $photo; ?>" alt="user" /></p></div>
                                    <div class="pull-left"><p>Welcome<br/> <strong><?= $customerInfo->firstname . ' ' . $customerInfo->lastname; ?></strong></p>
                                        <p>ID: <strong><?= 'AAIMA' . $customerInfo->id; ?></strong></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body account-sidebar-menu">
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <ul class="list-group">
                                        <li class="list-group-item"><a href="/myaccount"><i class="fa fa-user"></i> My Profile <span class="badge"><i class="fa fa-angle-right"></i></span></a></li>
                                        <li class="list-group-item"><a href="/myaddress"><i class="fa fa-map-marker"></i> Manage Addresses <span class="badge"><i class="fa fa-angle-right"></i></span></a></li>
                                        <li class="list-group-item"><a href="/myreward" class="active"><i class="fa fa-trophy"></i> My Rewards <span class="badge"><i class="fa fa-angle-right"></i></span></a></li>
                                        <li class="list-group-item"><a href="/mybusinessassociate"><i class="fa fa-sitemap"></i> My Business Associate <span class="badge"><i class="fa fa-angle-right"></i></span></a></li>
                                        <li class="list-group-item"><a href="/mywishlist"><i class="fa fa-heart"></i> My Wishlist <span class="badge"><i class="fa fa-angle-right"></i></span></a></li>
                                        <li class="list-group-item"><a href="/myorder"><i class="fa fa-list"></i> My Orders <span class="badge"><i class="fa fa-angle-right"></i></span></a></li>
                                        <li class="list-group-item"><a href="/myreturn"><i class="fa fa-retweet"></i> My Returns <span class="badge"><i class="fa fa-angle-right"></i></span></a></li>
                                        <li class="list-group-item"><a href="/logoutuser" onclick="return confirm('Are you sure, you want to logout ?')" class="logout"><i class="fa fa-power-off"></i> Logout</a></li>
                                    </ul> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="col-sm-8 col-md-9" id="column-right">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="panel panel-default account-content">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <h3>My Rewards</h3>
                                </div>
                            </div>

                        </div>

                        <div class="panel-body manage-address-content">

                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <div class="border-content">
                                        <h5>Total Rewards Point : <strong>1260</strong></h5>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12 col-md-12">

                                    <div class="table-responsive">
                                        <table class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <td class="text-left">Date Added</td>
                                                    <td class="text-left">Description</td>
                                                    <td class="text-right">Points</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td class="text-left">01/01/2019</td>
                                                    <td class="text-left">Rewards Description</td>
                                                    <td class="text-right">1000</td>
                                                </tr>
                                                <tr>
                                                    <td class="text-center" colspan="3">No results!</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                            </div>



                        </div>
                    </div>
                </div>

            </div>
        </div>


    </div>

</div>
@endsection