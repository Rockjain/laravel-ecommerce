<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Category extends Model {

	protected $table = 'categories';
	protected $fillable = array(
		'name',
		'image',
		'parent_id',
		'description',
		'status',
		'created_at',
		'updated_at',
	);

	public function parentDetail() {
		return $this->belongsTo('App\Model\Category', 'parent_id');
	}

	public function fileDetail() {
		return $this->belongsTo('App\Model\Appfiles', 'image');
	}

}
