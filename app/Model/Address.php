<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Address extends Model {

    protected $table = 'customer_address';
    protected $fillable = array(
        'customer_id',
        'firstname',
        'lastname',
        'company',
        'address_1',
        'address_2',
        'city',
        'postcode',
        'country_id',
        'zone_id',
        'custom_field'
    );

}
