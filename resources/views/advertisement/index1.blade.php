<?php
if(!empty($advert)){
  $id = $advert[0]->id;
  $title = $advert[0]->title;
  $link = $advert[0]->link;
  $image = $advert[0]->image;
  $form_type = 'update';
}else{
  $name = '';
$description = '';
$image  = '';
$form_type = 'add';
$id = '';
}
?>
<meta name="csrf-token" content="{{ csrf_token() }}">
@extends('layouts.admin')
@section('content')

<script src="https://cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>
                <div class="page-content-wrap">
                
                    <div class="row">
                        <div class="col-md-12">
                            
                          
                            <div class="panel panel-default">
       <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-plus"></i> Add Classified</h3>
      </div>
      <div class="tabs">
        <form  enctype="multipart/form-data" id="advert-form" class="form-horizontal">
           @csrf
           <input type="hidden" name="form_type" value="add">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#tab-general" data-toggle="tab">General</a></li>
            <li><a href="#tab-data" data-toggle="tab">Data</a></li>
            <li><a href="#tab-image" data-toggle="tab">Image</a></li>
          </ul>
          <div class="tab-content panel-body">
            <div class="tab-pane active" id="tab-general">
              
              <div class="tab-content">
                                <div class="tab-pane active" id="language1">
                  <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-name1">Ad Category</label>
                    <div class="col-sm-10">
                      <select class="form-control" name="category">
                        <option>Category 1</option>
                        <option>Category 2</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-name1">Ad Title</label>
                    <div class="col-sm-10">
                      <input type="text" name="title" placeholder="Ad Title" id="input-name1" class="form-control">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-description1">Description</label>
                    <div class="col-sm-10">
          <textarea class="summernote" name="description"><h4>Also subhearder available too</h4>
      
           </textarea>
                      </div>
                  </div>
                  <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-meta-title1">Meta Tag Title</label>
                    <div class="col-sm-10">
                      <input type="text" name="meta_title" placeholder="Meta Tag Title" id="input-meta-title1" class="form-control">
                                          </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-meta-description1">Meta Tag Description</label>
                    <div class="col-sm-10">
                      <textarea name="meta_description" rows="5" placeholder="Meta Tag Description" id="input-meta-description1" class="form-control"></textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-meta-keyword1">Meta Tag Keywords</label>
                    <div class="col-sm-10">
                      <textarea name="meta_keyword" rows="5" placeholder="Meta Tag Keywords" id="input-meta-keyword1" class="form-control"></textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-tag1"><span data-toggle="tooltip" title="" data-original-title="Comma separated">Product Tags</span></label>
                    <div class="col-sm-10">
                      <input type="text" name="tag" placeholder="Product Tags" id="input-tag1" class="form-control">
                    </div>
                  </div>
                </div>
               </div>
            </div>
            <div class="tab-pane" id="tab-data">
             
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-customer">Customer</label>
                <div class="col-sm-10">
                  <input type="text" name="customer" placeholder="Customer" id="input-customer" class="form-control">
                </div>
              </div>
        
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-location">Location</label>
                <div class="col-sm-10">
                  <input type="text" name="location" placeholder="Location" id="input-location" class="form-control">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-price">Price</label>
                <div class="col-sm-10">
                  <input type="text" name="price" placeholder="Price" id="input-price" class="form-control">
                </div>
              </div>
             
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-minimum"><span data-toggle="tooltip" title="" data-original-title="Force a minimum ordered amount">Minimum Quantity</span></label>
                <div class="col-sm-10">
                  <input type="text" name="minimum" value="1" placeholder="Minimum Quantity" id="input-minimum" class="form-control">
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-maximum"><span data-toggle="tooltip" title="" data-original-title="Force a maximum ordered amount">Maximum Quantity</span></label>
                <div class="col-sm-10">
                  <input type="text" name="maximum" value="0" placeholder="Maximum Quantity" id="input-maximum" class="form-control">
                </div>
              </div>
            

              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-keyword"><span data-toggle="tooltip" title="" data-original-title="Do not use spaces, instead replace spaces with - and make sure the SEO URL is globally unique.">SEO URL</span></label>
                <div class="col-sm-10">
                  <input type="text" name="seo_url" placeholder="SEO URL" id="input-keyword" class="form-control">
                                  </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-date-available">Date Available</label>
                <div class="col-sm-3">
                  <div class="input-group date">
                    <input type="text" name="date_available" value="" placeholder="" data-date-format="yyyy-mm-dd" id="" class="form-control datepicker">
                    <span class="input-group-btn">
                    <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                    </span></div>
                </div>
              </div>
             
       <div class="form-group">
                <label class="col-sm-2 control-label" for="input-link">Link to External</label>
                <div class="col-sm-10">
                  <input type="text" name="link" placeholder="External Link" id="input-link" class="form-control">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-status">Status</label>
                <div class="col-sm-10">
                  <select name="status" id="input-status" class="form-control">
                                        <option value="1" selected="selected">Enabled</option>
                    <option value="0">Disabled</option>
                                      </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-sort-order">Sort Order</label>
                <div class="col-sm-10">
                  <input type="text" name="sort_order" value="1" placeholder="Sort Order" id="input-sort-order" class="form-control">
                </div>
              </div>
            </div>
            
           
            <div class="tab-pane" id="tab-image">
              <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                  <thead>
                    <tr>
                      <td class="text-left">Image</td>
                    </tr>
                  </thead>
                  
                  <tbody>
                    <tr>
                      <td class="text-left">
           <input type="file" class="file" data-preview-file-type="any"/>
                  </tr>
                  </tbody>
                </table>
              </div>
              <div class="table-responsive">
                <table id="images" class="table table-striped table-bordered table-hover">
                  <thead>
                    <tr>
                      <td class="text-left">Additional Images</td>
                      <td class="text-right">Sort Order</td>
                      <td></td>
                    </tr>
                  </thead>
                  <tbody>
                                                          </tbody>
                  <tfoot>
                    <tr>
                      <td colspan="2"></td>
                      <td class="text-left"><button type="button" onclick="addImage();" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Add Image"><i class="fa fa-plus-circle"></i></button></td>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
            
          </div>
      <div class="panel-footer">                                                                        
                                        <button type="submit" class="btn btn-info pull-right add">Publish <span class="fa fa-save"></span></button>
                                    </div>
                  
        </form>
      </div>
    </div>
                        
                          
                            
                        </div>

                    </div>                    
                    
                </div>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
<script>
   
    $("#advert-form").validate({
   rules:{
        address:"required",
        name:"required"
    },
submitHandler: function(form) {
var formData = new FormData(form);
$.ajax({
    type: "POST",
    url: "/admin/advert/insert",
    data: formData,
    type: "POST", 
    //use contentType, processData for sure.
    contentType: false,
    processData: false,
    beforeSend: function() {
        },
    success: function(msg) {
       if(msg == 1){
Command: toastr["success"]("Testimonial has been added Successfully. Redirecting you to testimonial lists!")

toastr.options = {
  "closeButton": false,
  "debug": false,
  "newestOnTop": false,
  "progressBar": false,
  "positionClass": "toast-top-right",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}
                 window.setTimeout(function() {
                    window.location.replace("/admin/view_advert");
                 }, 3000);
}else if(msg=='updated'){
                Command: toastr["success"]("Testimonial has been updated Successfully. Redirecting you to testimonial lists!")
                toastr.options = {
                  "closeButton": false,
                  "debug": false,
                  "newestOnTop": false,
                  "progressBar": false,
                  "positionClass": "toast-top-right",
                  "preventDuplicates": false,
                  "onclick": null,
                  "showDuration": "300",
                  "hideDuration": "1000",
                  "timeOut": "5000",
                  "extendedTimeOut": "1000",
                  "showEasing": "swing",
                  "hideEasing": "linear",
                  "showMethod": "fadeIn",
                  "hideMethod": "fadeOut"
                }
                 window.setTimeout(function() {
                    window.location.replace("/admin/view_advert");
                 }, 3000);
            }else{
              alert('something went wrong with server! Please try again later!')
            }
    },
    error: function() {

    }
});
      
        return false;
    },


});
</script>
@endsection