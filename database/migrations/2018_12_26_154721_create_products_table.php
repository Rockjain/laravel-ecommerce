<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('seller_id')->unsigned()->default(0)->comment("0:Admin,>0:Seller");
            $table->string('sku_code')->nullable();
            $table->string('hsn_code')->nullable();
            $table->string('other_code')->nullable();
            $table->string('name')->nullable();
            $table->integer('image')->default(0);
            $table->double('price')->default(0);
            $table->double('delivery_charge')->default(0);
            $table->integer('tax')->default(0)->comment("in percentage");
            $table->integer('discount_percent')->unsigned()->default(0)->comment("in percentage");
            $table->integer('discount_quantity')->unsigned()->default(0);
            $table->timestamp('discount_start_date')->nullable();
            $table->timestamp('discount_end_date')->nullable();
            $table->text('description')->nullable();
            $table->integer('category_id')->unsigned()->nullable();
            $table->integer('brand_id')->unsigned()->nullable();
            $table->integer('quantity')->nullable();
            $table->enum('is_featured', ['0', '1'])->default(0);
            $table->enum('status', ['0', '1'])->default(0)->comment("0:inactive,1:active");

            $table->foreign('category_id')->references('id')->on('categories');
            $table->foreign('brand_id')->references('id')->on('brands');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('products');
    }

}
