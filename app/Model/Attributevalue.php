<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Attributevalue extends Model {

    protected $table = 'attribute_values';
    protected $fillable = array(
        'attribute_id',
        'attribute_value',
        'status',
        'created_at',
        'updated_at'
    );

}
