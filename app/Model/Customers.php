<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Customers extends Model {

    protected $table = 'customers';
    protected $fillable = [
        'customer_group_id',
        'store_id',
        'language_id',
        'firstname',
        'lastname',
        'photo',
        'aadhar_front',
        'aadhar_back',
        'pan_card',
        'cheque_photo',
        'email',
        'telephone',
        'fax',
        'password',
        'pass',
        'salt',
        'cart',
        'wishlist',
        'newsletter',
        'address_id',
        'custom_field',
        'ip',
        'status',
        'approved',
        'safe',
        'token',
        'code',
        'date_added',
        'payment',
        'cheque',
        'utr',
        'amount_pay',
        'cbname_pay',
        'cbnum_pay',
        'date_pay',
        'amount',
        'cbname',
        'cbnum',
        'date',
        'paypal',
        'bank_name',
        'bank_branch_number',
        'bank_swift_code',
        'bank_account_name',
        'bank_account_number',
        'sponser_id',
        'referred_by',
        'star',
        'regbal',
    ];

}
