<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoryAttributeMappingTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('category_attribute_mapping', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id')->unsigned()->nullable();
            $table->integer('attribute_id')->unsigned()->nullable();
            $table->enum('is_filter', ['0', '1'])->default(0);
            $table->integer('status')->unsigned()->nullable(1)->comment('1:active,0:inactive');

            $table->foreign('attribute_id')->references('id')->on('attributes');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('category_attribute_mapping');
    }

}
