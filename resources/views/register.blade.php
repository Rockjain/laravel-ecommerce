@extends('layouts.default')
@section('content')

<div class="container fullwidth">
    <div class="row">
        <div class="col-md-10 col-md-offset-1 mtb_30" id="register-page">

            <div class="col-sm-12 col-md-12 col-lg-12 white-bg">
                <!-- contact  -->
                <div class="panel-register ptb_30">
                    <div class="panel-heading">
                        <h3 class="title-heading text-center">Register</h3>
                    </div>
                    <div class="panel-body">
                        <form action="/registeruser" id="register-form" method="post" enctype="multipart/form-data" class="form-horizontal">
                            {{csrf_field()}}
                         <!--    <div class="form-group">
                                <label class="col-sm-9 col-sm-offset-3">
                                    <input type="checkbox" id="iboregi" value="1" name="iboregi">&nbsp; Do you want to become Business Associate ?(Rs.0.00)
                                </label>
                            </div> -->
                            <div class="form-group required has-feedback">
                                <label class="col-sm-4 control-label" for="input-sponsor">Referral Code</label>
                                <div class="col-sm-8">
                                    <input type="text" name="sponser_id" placeholder="Referral Code" id="input-payment-sponser-id" class="form-control">
                                    <span class="fa fa-check form-control-feedback"></span>
                                    <div class="username_availability_result" id="username_availability_result"></div>
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="col-sm-4 control-label" for="input-firstname">First Name</label>
                                <div class="col-sm-8">
                                    <input type="text" name="firstname" value="" placeholder="First Name" id="input-firstname" class="form-control characterOnly" maxlength="20" minlength="2" required>
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="col-sm-4 control-label" for="input-lastname">Last Name</label>
                                <div class="col-sm-8">
                                    <input type="text" name="lastname" value="" placeholder="Last Name" id="input-lastname" class="form-control characterOnly" maxlength="20" minlength="2" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="input-email"> &nbsp;E-Mail</label>
                                <div class="col-sm-8">
                                    <input type="email" pattern="[^ @]*@[^ @]*" name="email" value="" placeholder="E-Mail" id="input-email" class="form-control" maxlength="80" required>
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="col-sm-4 control-label" for="input-telephone">Mobile</label>
                                <div class="col-sm-6">
                                    <input type="tel" name="telephone" value="" placeholder="Mobile" id="telephone" class="form-control telephone numericOnly" minlength="10" maxlength="10" required>
                                </div>
                                <div class="col-sm-2">
                                    <input type="tel" name="otp" placeholder="OTP" id="otp" class="form-control" required>
                                    <input type="hidden" name="hidden_otp" id="hidden_otp" class="form-control">
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-4 control-label" for="input-address-1">Address</label>
                                <div class="col-sm-8">
                                    <input type="text" name="address" value="" placeholder="Address" id="input-address" class="form-control" maxlength="128" required>
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-4 control-label" for="input-city">City</label>
                                <div class="col-sm-8">
                                    <input type="text" name="city" value="" placeholder="City" id="input-city" class="form-control" maxlength="128" required>
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="col-sm-4 control-label" for="input-postcode" style="">Post Code</label>
                                <div class="col-sm-8">
                                    <input type="text" name="postcode" value="" placeholder="Post Code" id="input-postcode" class="form-control numericOnly" maxlength="6" minlength="6" required>
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="col-sm-4 control-label" for="input-zone">Region / State</label>
                                <div class="col-sm-8">
                                    <select name="zone_id" id="input-zone" class="form-control" required>
                                        <option value="0" selected disabled>Select</option>
                                        @if($states)
                                        @foreach($states as $state)
                                        <option value="{{$state->state_id}}">{{$state->state_name}}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="col-sm-4 control-label" for="input-country">Country</label>
                                <div class="col-sm-8">
                                    <select name="country_id" id="input-country" class="form-control" required>
                                        <option value="0" selected disabled>Select</option>
                                        @if($countries)
                                        @foreach($countries as $country)
                                        <option value="{{$country->country_id}}">{{$country->country_name}}</option>
                                        @endforeach
                                        @endif
                                    </select> 
                                </div>
                            </div>
                        <!--     <div class="form-group required">
                                <label class="col-sm-4 control-label" style="">Username</label>
                                <div class="col-sm-8">
                                    <input type="text" name="username" value="" placeholder="Choose username" class="form-control" maxlength="10" minlength="2" required>
                                </div>
                            </div> -->
                            <div class="form-group required">
                                <label class="col-sm-4 control-label" for="input-password">Password</label>
                                <div class="col-sm-8">
                                    <input type="password" name="password" value="" placeholder="Password" id="input-password" class="form-control" maxlength="20" required>
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="col-sm-4 control-label" for="input-confirm">Password Confirm</label>
                                <div class="col-sm-8">
                                    <input type="password" name="confirm" value="" placeholder="Password Confirm" id="input-confirm" class="form-control" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label">Subscribe Newsletter</label>
                                <div class="col-sm-8">
                                    <label class="radio-inline">
                                        <input type="radio" name="newsletter" value="1">
                                        Yes</label>
                                    <label class="radio-inline">
                                        <input type="radio" name="newsletter" value="0" checked="checked">
                                        No</label>
                                </div>
                            </div>
                            <div class="buttons">
                                <div class="pull-right">I have read and agree to the <a href="#" target="_blank"><b>Privacy Policy</b></a>                     
                                    <input type="checkbox" name="agree" value="1" checked>
                                    &nbsp;
                                    <input type="submit" value="Register" class="btn register-button">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $("#telephone").blur(function(){
      var telephone=$(this).val();
      var otp = Math.floor(1000 + Math.random() * 9000);
      
      $("#hidden_otp").val(otp);
      var workingkey="nUxIBk39oEmU9tpcnmUmOg";
      var sender="AAIMAG";
      var sms="Use this OTP "+otp+" for reset your password";
     // var ms= rawurlencode(sms);

       $.ajax({
            type: "POST",
            url : "/phoneotp",
            data: {phone: telephone, phoneotp: otp},
         success: function(result){
            data=JSON.parse(result);
            if (data.status) {
                alert('Your Phone number OTP send in your mobile');
            }
    }
});

});

$("#otp").blur(function(){
      var maxLength = 4;
      var otp = $(this).val();
      var length=otp.length;
      if (length > maxLength){
     alert('Your OTP is incorrect');
       return false;
    }
      //alert(length);
      if(length == 4){
      var hiddenotp = $("#hidden_otp").val();
      //alert(hiddenotp);
      if (otp != hiddenotp) {
        alert('Your OTP is incorrect')
        return false;
      }
  }
     

});
  </script>
@endsection