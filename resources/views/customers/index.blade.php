<meta name="csrf-token" content="{{ csrf_token() }}">
@extends('layouts.admin')
@section('content')
<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">


    <div class="row">
        <div class="col-md-12">


            <!-- START  Filter -->
            <div class="panel panel-default">
                <div class="panel-body">

                    <div class="well">

                        <div class="row">
                            <form action="/admin/customers" method="post">
                                {{csrf_field()}}
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="control-label" for="input-name">Customer Name</label>
                                        <input type="text" name="filter_name" value="{{old('filter_name')}}" placeholder="Customer Name" id="input-name" class="form-control" autocomplete="off"><ul class="dropdown-menu"></ul>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="input-email">E-Mail</label>
                                        <input type="text" name="filter_email" value="{{old('filter_email')}}" placeholder="E-Mail" id="input-email" class="form-control" autocomplete="off"><ul class="dropdown-menu"></ul>
                                    </div>
                                </div>
                   <!--              <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="control-label" for="input-customer-group">Customer Group</label>
                                        <select name="filter_customer_group_id" id="input-customer-group" class="form-control">
                                            <option value="">All</option>
                                            <option value="1">Default</option>
                                            <option value="4">Distributor</option>
                                            <option value="3">Guest</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="input-status">Status</label>
                                        <select name="filter_status" id="input-status" class="form-control">
                                            <option value="">All</option>
                                            <option value="1">Enabled</option>
                                            <option value="0">Disabled</option>
                                        </select>
                                    </div>
                                </div> -->
                                <div class="col-sm-3">
                                 <!--    <div class="form-group">
                                        <label class="control-label" for="input-approved">Approved</label>
                                        <select name="filter_approved" id="input-approved" class="form-control">
                                            <option value="">All</option>
                                            <option value="1">Yes</option>
                                            <option value="0">No</option>
                                        </select>
                                    </div> -->
                                    <div class="form-group">
                                        <label class="control-label" for="input-sponser_id">Sponser ID</label>
                                        <input type="text" name="filter_sponser_id" value="" placeholder="Sponser ID" id="input-sponser_id" class="form-control">
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="control-label" for="input-date-added">Date Added</label>
                                        <div class="input-group date">
                                            <input type="text" name="filter_date_added" value="" placeholder="Date Added" data-date-format="YYYY-MM-DD" id="" class="form-control">
                                            <span class="input-group-btn">
                                                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                            </span></div>
                                    </div>
                                    <div class="form-group">
                                        <label>&nbsp; </label>
                                        <p><button type="submit" id="button-filter" name="filter_customer" value="filter" class="btn btn-info pull-right"><i class="fa fa-filter"></i> Filter</button></p>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12">


            <!-- START Customer DATATABLE -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">All Users</h3>
                    <ul class="panel-controls">
                        <li><a href="/admin/add_customer" class="text-info"><span class="fa fa-plus"></span></a></li>
                        <li><a href="#" class="text-danger"><span class="fa fa-trash-o"></span></a></li>
                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <table class="table datatable table-bordered">
                        <thead>
                            <tr>
                                <th style="max-width: 20px;"><input type="checkbox" name="" id=""/></th>
                                <th style="max-width: 150px;">Customer Name</th>
                                <th>Customer Mobile</th>
                                <th>Customer Email</th>
<!--                                 <th>Status</th>
 -->                                <th>Password</th>
                                <th>Added Date</th>
                                <th>Unique Id</th>
<!--                                 <th>Points</th>
 -->                                <th style="min-width: 200px;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($customers as $value)
                            <tr>
                                <td><input type="checkbox" name="" id=""/> </td>
                                <td>{{ $value->firstname.' '.$value->lastname }}</td>
                                <td>{{ $value->telephone }}</td>
                                <td>{{ $value->email }}</td>
                               <!--  <td>@if($value->status=='1')
                                    <span class="text-info">Enabled</span>
                                    @else
                                    <span class="text-danger">Disabled</span>
                                    @endif
                                </td> -->

                                <td>{{ $value->pass }}</td>
                                <?php $created_at=$value->created_at;
                                    $explode=explode(" ",$created_at);
                                ?>
                                <td>{{ $explode[0] }}</td>
                                <td>{{ $value->sponser_id }}</td>
<!--                                 <td>{{ $value->regbal }}</td>
 -->                                <td>
                                  <!--   <button disabled data-toggle="tooltip" title="Change Status" class="btn btn-warning btn-rounded text-center"><i class="fa fa-thumbs-up"></i> </button> -->
                                   <a href="#" target="_blank" data-toggle="tooltip" title="Edit" class="btn btn-info btn-rounded text-center"><i class="fa fa-edit"></i> </a>
                                   <!--  <a href="/admin/customers/edit/{{ $value->id }}" target="_blank" data-toggle="tooltip" title="Edit" class="btn btn-info btn-rounded text-center"><i class="fa fa-edit"></i> </a> -->
                                    <!-- <a href="view-user.php" target="_blank" data-toggle="tooltip" title="View User" class="btn btn-info btn-rounded text-center"><i class="fa fa-eye"></i> </a>
                                    <button data-toggle="tooltip" title="Delete" class="btn btn-danger btn-rounded text-center"><i class="fa fa-trash-o"></i> </button> -->
                                </td>
                            </tr>

                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END STATE DATATABLE -->

        </div>
    </div>

</div>
<!-- END PAGE CONTENT WRAPPER -->
@endsection