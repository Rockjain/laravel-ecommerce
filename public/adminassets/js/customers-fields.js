var address_row = 2;

function addAddress() {
	html  = '<div class="tab-pane" id="tab-address' + address_row + '">';
	html += '  <input type="hidden" name="address[' + address_row + '][address_id]" value="" />';

	html += '  <div class="form-group required">';
	html += '    <label class="col-sm-2 control-label" for="input-firstname' + address_row + '">First Name</label>';
	html += '    <div class="col-sm-10"><input type="text" name="address[' + address_row + '][firstname]" value="" placeholder="First Name" id="input-firstname' + address_row + '" class="form-control" /></div>';
	html += '  </div>';

	html += '  <div class="form-group required">';
	html += '    <label class="col-sm-2 control-label" for="input-lastname' + address_row + '">Last Name</label>';
	html += '    <div class="col-sm-10"><input type="text" name="address[' + address_row + '][lastname]" value="" placeholder="Last Name" id="input-lastname' + address_row + '" class="form-control" /></div>';
	html += '  </div>';

	html += '  <div class="form-group">';
	html += '    <label class="col-sm-2 control-label" for="input-company' + address_row + '">Company</label>';
	html += '    <div class="col-sm-10"><input type="text" name="address[' + address_row + '][company]" value="" placeholder="Company" id="input-company' + address_row + '" class="form-control" /></div>';
	html += '  </div>';

	html += '  <div class="form-group required">';
	html += '    <label class="col-sm-2 control-label" for="input-address-1' + address_row + '">Address 1</label>';
	html += '    <div class="col-sm-10"><input type="text" name="address[' + address_row + '][address_1]" value="" placeholder="Address 1" id="input-address-1' + address_row + '" class="form-control" /></div>';
	html += '  </div>';

	html += '  <div class="form-group">';
	html += '    <label class="col-sm-2 control-label" for="input-address-2' + address_row + '">Address 2</label>';
	html += '    <div class="col-sm-10"><input type="text" name="address[' + address_row + '][address_2]" value="" placeholder="Address 2" id="input-address-2' + address_row + '" class="form-control" /></div>';
	html += '  </div>';

	html += '  <div class="form-group required">';
	html += '    <label class="col-sm-2 control-label" for="input-city' + address_row + '">City</label>';
	html += '    <div class="col-sm-10"><input type="text" name="address[' + address_row + '][city]" value="" placeholder="City" id="input-city' + address_row + '" class="form-control" /></div>';
	html += '  </div>';

	html += '  <div class="form-group required">';
	html += '    <label class="col-sm-2 control-label" for="input-postcode' + address_row + '">Postcode</label>';
	html += '    <div class="col-sm-10"><input type="text" name="address[' + address_row + '][postcode]" value="" placeholder="Postcode" id="input-postcode' + address_row + '" class="form-control" /></div>';
	html += '  </div>';

	html += '  <div class="form-group required">';
	html += '    <label class="col-sm-2 control-label" for="input-country' + address_row + '">Country</label>';
	html += '    <div class="col-sm-10"><select name="address[' + address_row + '][country_id]" id="input-country' + address_row + '" onchange="country(this, \'' + address_row + '\', \'0\');" class="form-control">';
    html += '         <option value=""> --- Please Select --- </option>';
        html += '         <option value="244">Aaland Islands</option>';
        html += '         <option value="1">Afghanistan</option>';
        html += '         <option value="2">Albania</option>';
        html += '         <option value="3">Algeria</option>';
        html += '         <option value="4">American Samoa</option>';
        html += '         <option value="5">Andorra</option>';
        html += '         <option value="6">Angola</option>';
        html += '         <option value="7">Anguilla</option>';
        html += '         <option value="8">Antarctica</option>';
        html += '         <option value="9">Antigua and Barbuda</option>';
        html += '         <option value="10">Argentina</option>';
        html += '         <option value="11">Armenia</option>';
        html += '         <option value="12">Aruba</option>';
        html += '         <option value="252">Ascension Island (British)</option>';
        html += '         <option value="13">Australia</option>';
        html += '         <option value="14">Austria</option>';
        html += '         <option value="15">Azerbaijan</option>';
        html += '         <option value="16">Bahamas</option>';
        html += '         <option value="17">Bahrain</option>';
        html += '         <option value="18">Bangladesh</option>';
        html += '         <option value="19">Barbados</option>';
        html += '         <option value="20">Belarus</option>';
        html += '         <option value="21">Belgium</option>';
        html += '         <option value="22">Belize</option>';
        html += '         <option value="23">Benin</option>';
        html += '         <option value="24">Bermuda</option>';
        html += '         <option value="25">Bhutan</option>';
        html += '         <option value="26">Bolivia</option>';
        html += '         <option value="245">Bonaire, Sint Eustatius and Saba</option>';
        html += '         <option value="27">Bosnia and Herzegovina</option>';
        html += '         <option value="28">Botswana</option>';
        html += '         <option value="29">Bouvet Island</option>';
        html += '         <option value="30">Brazil</option>';
        html += '         <option value="31">British Indian Ocean Territory</option>';
        html += '         <option value="32">Brunei Darussalam</option>';
        html += '         <option value="33">Bulgaria</option>';
        html += '         <option value="34">Burkina Faso</option>';
        html += '         <option value="35">Burundi</option>';
        html += '         <option value="36">Cambodia</option>';
        html += '         <option value="37">Cameroon</option>';
        html += '         <option value="38">Canada</option>';
        html += '         <option value="251">Canary Islands</option>';
        html += '         <option value="39">Cape Verde</option>';
        html += '         <option value="40">Cayman Islands</option>';
        html += '         <option value="41">Central African Republic</option>';
        html += '         <option value="42">Chad</option>';
        html += '         <option value="43">Chile</option>';
        html += '         <option value="44">China</option>';
        html += '         <option value="45">Christmas Island</option>';
        html += '         <option value="46">Cocos (Keeling) Islands</option>';
        html += '         <option value="47">Colombia</option>';
        html += '         <option value="48">Comoros</option>';
        html += '         <option value="49">Congo</option>';
        html += '         <option value="50">Cook Islands</option>';
        html += '         <option value="51">Costa Rica</option>';
        html += '         <option value="52">Cote D\'Ivoire</option>';
        html += '         <option value="53">Croatia</option>';
        html += '         <option value="54">Cuba</option>';
        html += '         <option value="246">Curacao</option>';
        html += '         <option value="55">Cyprus</option>';
        html += '         <option value="56">Czech Republic</option>';
        html += '         <option value="237">Democratic Republic of Congo</option>';
        html += '         <option value="57">Denmark</option>';
        html += '         <option value="58">Djibouti</option>';
        html += '         <option value="59">Dominica</option>';
        html += '         <option value="60">Dominican Republic</option>';
        html += '         <option value="61">East Timor</option>';
        html += '         <option value="62">Ecuador</option>';
        html += '         <option value="63">Egypt</option>';
        html += '         <option value="64">El Salvador</option>';
        html += '         <option value="65">Equatorial Guinea</option>';
        html += '         <option value="66">Eritrea</option>';
        html += '         <option value="67">Estonia</option>';
        html += '         <option value="68">Ethiopia</option>';
        html += '         <option value="69">Falkland Islands (Malvinas)</option>';
        html += '         <option value="70">Faroe Islands</option>';
        html += '         <option value="71">Fiji</option>';
        html += '         <option value="72">Finland</option>';
        html += '         <option value="74">France, Metropolitan</option>';
        html += '         <option value="75">French Guiana</option>';
        html += '         <option value="76">French Polynesia</option>';
        html += '         <option value="77">French Southern Territories</option>';
        html += '         <option value="126">FYROM</option>';
        html += '         <option value="78">Gabon</option>';
        html += '         <option value="79">Gambia</option>';
        html += '         <option value="80">Georgia</option>';
        html += '         <option value="81">Germany</option>';
        html += '         <option value="82">Ghana</option>';
        html += '         <option value="83">Gibraltar</option>';
        html += '         <option value="84">Greece</option>';
        html += '         <option value="85">Greenland</option>';
        html += '         <option value="86">Grenada</option>';
        html += '         <option value="87">Guadeloupe</option>';
        html += '         <option value="88">Guam</option>';
        html += '         <option value="89">Guatemala</option>';
        html += '         <option value="256">Guernsey</option>';
        html += '         <option value="90">Guinea</option>';
        html += '         <option value="91">Guinea-Bissau</option>';
        html += '         <option value="92">Guyana</option>';
        html += '         <option value="93">Haiti</option>';
        html += '         <option value="94">Heard and Mc Donald Islands</option>';
        html += '         <option value="95">Honduras</option>';
        html += '         <option value="96">Hong Kong</option>';
        html += '         <option value="97">Hungary</option>';
        html += '         <option value="98">Iceland</option>';
        html += '         <option value="99">India</option>';
        html += '         <option value="100">Indonesia</option>';
        html += '         <option value="101">Iran (Islamic Republic of)</option>';
        html += '         <option value="102">Iraq</option>';
        html += '         <option value="103">Ireland</option>';
        html += '         <option value="254">Isle of Man</option>';
        html += '         <option value="104">Israel</option>';
        html += '         <option value="105">Italy</option>';
        html += '         <option value="106">Jamaica</option>';
        html += '         <option value="107">Japan</option>';
        html += '         <option value="257">Jersey</option>';
        html += '         <option value="108">Jordan</option>';
        html += '         <option value="109">Kazakhstan</option>';
        html += '         <option value="110">Kenya</option>';
        html += '         <option value="111">Kiribati</option>';
        html += '         <option value="253">Kosovo, Republic of</option>';
        html += '         <option value="114">Kuwait</option>';
        html += '         <option value="115">Kyrgyzstan</option>';
        html += '         <option value="116">Lao People\'s Democratic Republic</option>';
        html += '         <option value="117">Latvia</option>';
        html += '         <option value="118">Lebanon</option>';
        html += '         <option value="119">Lesotho</option>';
        html += '         <option value="120">Liberia</option>';
        html += '         <option value="121">Libyan Arab Jamahiriya</option>';
        html += '         <option value="122">Liechtenstein</option>';
        html += '         <option value="123">Lithuania</option>';
        html += '         <option value="124">Luxembourg</option>';
        html += '         <option value="125">Macau</option>';
        html += '         <option value="127">Madagascar</option>';
        html += '         <option value="128">Malawi</option>';
        html += '         <option value="129">Malaysia</option>';
        html += '         <option value="130">Maldives</option>';
        html += '         <option value="131">Mali</option>';
        html += '         <option value="132">Malta</option>';
        html += '         <option value="133">Marshall Islands</option>';
        html += '         <option value="134">Martinique</option>';
        html += '         <option value="135">Mauritania</option>';
        html += '         <option value="136">Mauritius</option>';
        html += '         <option value="137">Mayotte</option>';
        html += '         <option value="138">Mexico</option>';
        html += '         <option value="139">Micronesia, Federated States of</option>';
        html += '         <option value="140">Moldova, Republic of</option>';
        html += '         <option value="141">Monaco</option>';
        html += '         <option value="142">Mongolia</option>';
        html += '         <option value="242">Montenegro</option>';
        html += '         <option value="143">Montserrat</option>';
        html += '         <option value="144">Morocco</option>';
        html += '         <option value="145">Mozambique</option>';
        html += '         <option value="146">Myanmar</option>';
        html += '         <option value="147">Namibia</option>';
        html += '         <option value="148">Nauru</option>';
        html += '         <option value="149">Nepal</option>';
        html += '         <option value="150">Netherlands</option>';
        html += '         <option value="151">Netherlands Antilles</option>';
        html += '         <option value="152">New Caledonia</option>';
        html += '         <option value="153">New Zealand</option>';
        html += '         <option value="154">Nicaragua</option>';
        html += '         <option value="155">Niger</option>';
        html += '         <option value="156">Nigeria</option>';
        html += '         <option value="157">Niue</option>';
        html += '         <option value="158">Norfolk Island</option>';
        html += '         <option value="112">North Korea</option>';
        html += '         <option value="159">Northern Mariana Islands</option>';
        html += '         <option value="160">Norway</option>';
        html += '         <option value="161">Oman</option>';
        html += '         <option value="162">Pakistan</option>';
        html += '         <option value="163">Palau</option>';
        html += '         <option value="247">Palestinian Territory, Occupied</option>';
        html += '         <option value="164">Panama</option>';
        html += '         <option value="165">Papua New Guinea</option>';
        html += '         <option value="166">Paraguay</option>';
        html += '         <option value="167">Peru</option>';
        html += '         <option value="168">Philippines</option>';
        html += '         <option value="169">Pitcairn</option>';
        html += '         <option value="170">Poland</option>';
        html += '         <option value="171">Portugal</option>';
        html += '         <option value="172">Puerto Rico</option>';
        html += '         <option value="173">Qatar</option>';
        html += '         <option value="174">Reunion</option>';
        html += '         <option value="175">Romania</option>';
        html += '         <option value="176">Russian Federation</option>';
        html += '         <option value="177">Rwanda</option>';
        html += '         <option value="178">Saint Kitts and Nevis</option>';
        html += '         <option value="179">Saint Lucia</option>';
        html += '         <option value="180">Saint Vincent and the Grenadines</option>';
        html += '         <option value="181">Samoa</option>';
        html += '         <option value="182">San Marino</option>';
        html += '         <option value="183">Sao Tome and Principe</option>';
        html += '         <option value="184">Saudi Arabia</option>';
        html += '         <option value="185">Senegal</option>';
        html += '         <option value="243">Serbia</option>';
        html += '         <option value="186">Seychelles</option>';
        html += '         <option value="187">Sierra Leone</option>';
        html += '         <option value="188">Singapore</option>';
        html += '         <option value="189">Slovak Republic</option>';
        html += '         <option value="190">Slovenia</option>';
        html += '         <option value="191">Solomon Islands</option>';
        html += '         <option value="192">Somalia</option>';
        html += '         <option value="193">South Africa</option>';
        html += '         <option value="194">South Georgia &amp; South Sandwich Islands</option>';
        html += '         <option value="113">South Korea</option>';
        html += '         <option value="248">South Sudan</option>';
        html += '         <option value="195">Spain</option>';
        html += '         <option value="196">Sri Lanka</option>';
        html += '         <option value="249">St. Barthelemy</option>';
        html += '         <option value="197">St. Helena</option>';
        html += '         <option value="250">St. Martin (French part)</option>';
        html += '         <option value="198">St. Pierre and Miquelon</option>';
        html += '         <option value="199">Sudan</option>';
        html += '         <option value="200">Suriname</option>';
        html += '         <option value="201">Svalbard and Jan Mayen Islands</option>';
        html += '         <option value="202">Swaziland</option>';
        html += '         <option value="203">Sweden</option>';
        html += '         <option value="204">Switzerland</option>';
        html += '         <option value="205">Syrian Arab Republic</option>';
        html += '         <option value="206">Taiwan</option>';
        html += '         <option value="207">Tajikistan</option>';
        html += '         <option value="208">Tanzania, United Republic of</option>';
        html += '         <option value="209">Thailand</option>';
        html += '         <option value="210">Togo</option>';
        html += '         <option value="211">Tokelau</option>';
        html += '         <option value="212">Tonga</option>';
        html += '         <option value="213">Trinidad and Tobago</option>';
        html += '         <option value="255">Tristan da Cunha</option>';
        html += '         <option value="214">Tunisia</option>';
        html += '         <option value="215">Turkey</option>';
        html += '         <option value="216">Turkmenistan</option>';
        html += '         <option value="217">Turks and Caicos Islands</option>';
        html += '         <option value="218">Tuvalu</option>';
        html += '         <option value="219">Uganda</option>';
        html += '         <option value="220">Ukraine</option>';
        html += '         <option value="221">United Arab Emirates</option>';
        html += '         <option value="222">United Kingdom</option>';
        html += '         <option value="223">United States</option>';
        html += '         <option value="224">United States Minor Outlying Islands</option>';
        html += '         <option value="225">Uruguay</option>';
        html += '         <option value="226">Uzbekistan</option>';
        html += '         <option value="227">Vanuatu</option>';
        html += '         <option value="228">Vatican City State (Holy See)</option>';
        html += '         <option value="229">Venezuela</option>';
        html += '         <option value="230">Viet Nam</option>';
        html += '         <option value="231">Virgin Islands (British)</option>';
        html += '         <option value="232">Virgin Islands (U.S.)</option>';
        html += '         <option value="233">Wallis and Futuna Islands</option>';
        html += '         <option value="234">Western Sahara</option>';
        html += '         <option value="235">Yemen</option>';
        html += '         <option value="238">Zambia</option>';
        html += '         <option value="239">Zimbabwe</option>';
        html += '      </select></div>';
	html += '  </div>';

	html += '  <div class="form-group required">';
	html += '    <label class="col-sm-2 control-label" for="input-zone' + address_row + '">Region / State</label>';
	html += '    <div class="col-sm-10"><select name="address[' + address_row + '][zone_id]" id="input-zone' + address_row + '" class="form-control"><option value=""> --- None --- </option></select></div>';
	html += '  </div>';

	// Custom Fields
					
	
	
	
	
		html += '	  <div class="form-group custom-field custom-field5" data-sort="8">';
	html += '		<label class="col-sm-2 control-label">Aadhar Card(front scanned)</label>';
	html += '		<div class="col-sm-10">';
	html += '		  <input type="file" class="file" data-preview-file-type="any"/>';
	html += '		  <input type="hidden" name="address[' + address_row + '][5]" value="" id="input-address' + address_row + '-custom-field5" />';
	html += '		</div>';
	html += '	  </div>';
	
	
	
	
								
	
	
	
	
		html += '	  <div class="form-group custom-field custom-field12" data-sort="9">';
	html += '		<label class="col-sm-2 control-label">Aadhar Card(back scanned)</label>';
	html += '		<div class="col-sm-10">';
	html += '		  <input type="file" class="file" data-preview-file-type="any"/>';
	html += '		  <input type="hidden" name="address[' + address_row + '][12]" value="" id="input-address' + address_row + '-custom-field12" />';
	html += '		</div>';
	html += '	  </div>';
	
	
	
	
												
	
	
	
	
		html += '	  <div class="form-group custom-field custom-field11" data-sort="14">';
	html += '		<label class="col-sm-2 control-label">Bank Account Copy(scanned)</label>';
	html += '		<div class="col-sm-10">';
	html += '		  <input type="file" class="file" data-preview-file-type="any"/>';
	html += '		  <input type="hidden" name="address[' + address_row + '][11]" value="" id="input-address' + address_row + '-custom-field11" />';
	html += '		</div>';
	html += '	  </div>';
	
	
	
	
		
	html += '  <div class="form-group">';
	html += '    <label class="col-sm-2 control-label">Default Address</label>';
	html += '    <div class="col-sm-10"><label class="radio"><input type="radio" name="address[' + address_row + '][default]" value="1" /></label></div>';
	html += '  </div>';

    html += '</div>';

	$('#tab-general .tab-content').append(html);

	$('select[name=\'customer_group_id\']').trigger('change');

	$('select[name=\'address[' + address_row + '][country_id]\']').trigger('change');

	$('#address-add').before('<li><a href="#tab-address' + address_row + '" data-toggle="tab"><i class="fa fa-minus-circle" onclick="$(\'#address a:first\').tab(\'show\'); $(\'a[href=\\\'#tab-address' + address_row + '\\\']\').parent().remove(); $(\'#tab-address' + address_row + '\').remove();"></i> Address ' + address_row + '</a></li>');

	$('#address a[href=\'#tab-address' + address_row + '\']').tab('show');

	$('.date').datetimepicker({
		pickTime: false
	});

	$('.datetime').datetimepicker({
		pickDate: true,
		pickTime: true
	});

	$('.time').datetimepicker({
		pickDate: false
	});

	$('#tab-address' + address_row + ' .form-group[data-sort]').detach().each(function() {
		if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('#tab-address' + address_row + ' .form-group').length) {
			$('#tab-address' + address_row + ' .form-group').eq($(this).attr('data-sort')).before(this);
		}

		if ($(this).attr('data-sort') > $('#tab-address' + address_row + ' .form-group').length) {
			$('#tab-address' + address_row + ' .form-group:last').after(this);
		}

		if ($(this).attr('data-sort') < -$('#tab-address' + address_row + ' .form-group').length) {
			$('#tab-address' + address_row + ' .form-group:first').before(this);
		}
	});

	address_row++;
}

function country(element, index, zone_id) {
	$.ajax({
		/*url: 'index.php?route=localisation/country/country&token=Fmug3hnLjO88GaNKMtPpb8B8TIUF1dcF&country_id=' + element.value,
		dataType: 'json',
		beforeSend: function() {
			$('select[name=\'address[' + index + '][country_id]\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
		},
		complete: function() {
			$('.fa-spin').remove();
		},*/
		success: function(json) {
			if (json['postcode_required'] == '1') {
				$('input[name=\'address[' + index + '][postcode]\']').parent().parent().addClass('required');
			} else {
				$('input[name=\'address[' + index + '][postcode]\']').parent().parent().removeClass('required');
			}

			html = '<option value=""> --- Please Select --- </option>';

			if (json['zone'] && json['zone'] != '') {
				for (i = 0; i < json['zone'].length; i++) {
					html += '<option value="' + json['zone'][i]['zone_id'] + '"';

					if (json['zone'][i]['zone_id'] == zone_id) {
						html += ' selected="selected"';
					}

					html += '>' + json['zone'][i]['name'] + '</option>';
				}
			} else {
				html += '<option value="0"> --- None --- </option>';
			}

			$('select[name=\'address[' + index + '][zone_id]\']').html(html);
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
}

$('select[name$=\'[country_id]\']').trigger('change');


$('#reward').delegate('.pagination a', 'click', function(e) {
	e.preventDefault();

	$('#reward').load(this.href);
});

// Sort the custom fields
$('#tab-address1 .form-group[data-sort]').detach().each(function() {
	if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('#tab-address1 .form-group').length) {
		$('#tab-address1 .form-group').eq($(this).attr('data-sort')).before(this);
	}

	if ($(this).attr('data-sort') > $('#tab-address1 .form-group').length) {
		$('#tab-address1 .form-group:last').after(this);
	}

	if ($(this).attr('data-sort') < -$('#tab-address1 .form-group').length) {
		$('#tab-address1 .form-group:first').before(this);
	}
});


$('#tab-customer .form-group[data-sort]').detach().each(function() {
	if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('#tab-customer .form-group').length) {
		$('#tab-customer .form-group').eq($(this).attr('data-sort')).before(this);
	}

	if ($(this).attr('data-sort') > $('#tab-customer .form-group').length) {
		$('#tab-customer .form-group:last').after(this);
	}

	if ($(this).attr('data-sort') < -$('#tab-customer .form-group').length) {
		$('#tab-customer .form-group:first').before(this);
	}
});