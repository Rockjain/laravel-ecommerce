<?php

namespace App\Http\Controllers;

use App\Model\Attribute;
use App\Model\Attributevalue;
use Illuminate\Http\Request;

class AttributesController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $attributes = Attribute::get();
//        echo "<pre>";print_r($attributes[0]->attributValues);die;
        return view('attributes.index', ['attributes' => $attributes]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $addArr = [
            'name' => $request->input('name'),
            'type' => $request->input('type'),
            'status' => '1',
        ];
        $return = Attribute::create($addArr);
        if ($return) {
            if ($request->input('attribute_values')[0]) {
                foreach ($request->input('attribute_values') as $value) {
                    Attributevalue::create([
                        'attribute_id' => $return->id,
                        'attribute_value' => $value,
                        'status' => '1'
                    ]);
                }
            }
            return redirect()->route('attributes.index')->with('success', 'Attribute created successfully');
        }
        return back()->withInput()->with('error', 'Error creating new attribute');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Attribute  $attribute
     * @return \Illuminate\Http\Response
     */
    public function show(Attribute $attribute) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Attribute  $attribute
     * @return \Illuminate\Http\Response
     */
    public function edit(Attribute $attribute) {
        $attribute = Attribute::find($attribute->id);
        return view('attributes.edit', ['attribute' => $attribute]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Attribute  $attribute
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Attribute $attribute) {
        $updateArr = [
            'name' => $request->input('name'),
            'type' => $request->input('type')
        ];

        $return = Attribute::where('id', $attribute->id)->update($updateArr);
//        echo "<pre>";
//        print_r($request->input('attribute_values'));
//        die;
        if ($return) {
            if ($request->input('attribute_values')) {
                foreach ($request->input('attribute_values') as $key => $value) {
                    Attributevalue::where('id', $key)->update([
                        'attribute_value' => $value,
                    ]);
                }
            }
            return redirect()->route('attributes.index')->with('success', 'Attribute updated successfully');
        }
        return back()->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Attribute  $attribute
     * @return \Illuminate\Http\Response
     */
    public function destroy(Attribute $attribute) {
        //
    }

}
