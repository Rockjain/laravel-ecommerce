<?php

namespace App\Http\Controllers;

use App\Model\Category;
use Illuminate\Http\Request;

class CategoriesController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $parents = Category::where('parent_id', '0')->get();
        $categories = Category::get();
        return view('categories.index', ['categories' => $categories, 'parents' => $parents]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $adArr = [
            'name' => $request->input('name'),
            'parent_id' => $request->input('parent_id'),
            'description' => $request->input('description'),
            'status' => '1',
        ];
        $path = "/images/categoryimages/";
        $check = $this->uploadFile($request, 'image', $path);
        if ($check):
            $nameArray = explode('.', $check);
            $ext = end($nameArray);

            $insertParam['file_path'] = $path;
            $insertParam['file_name'] = $check;
            $insertParam['file_type'] = $ext;

            $getImgData = $this->insertFile($insertParam);

            $adArr['image'] = $getImgData['id'];

        endif;

        $return = Category::create($adArr);
        if ($return) {
            return redirect()->route('categories.index')->with('success', 'Category created successfully');
        }
        return back()->withInput()->with('error', 'Error creating new category');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category) {
        $parents = Category::where('parent_id', '0')->get();
        $category = Category::find($category->id);
        return view('categories.edit', ['category' => $category, 'parents' => $parents]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category) {
        $updateArr = [
            'name' => $request->input('name'),
            'parent_id' => $request->input('parent_id'),
            'description' => $request->input('description'),
        ];

        $path = "/images/categoryimages/";
        $check = $this->uploadFile($request, 'image', $path);
        if ($check):
            $nameArray = explode('.', $check);
            $ext = end($nameArray);

            $insertParam['file_path'] = $path;
            $insertParam['file_name'] = $check;
            $insertParam['file_type'] = $ext;

            $getImgData = $this->insertFile($insertParam);

            $updateArr['image'] = $getImgData['id'];

        endif;

        $return = Category::where('id', $category->id)->update($updateArr);

        if ($return) {
            return redirect()->route('categories.index')->with('success', 'Category updated successfully');
        }
        return back()->withInput();
    }

}
