@extends('layouts.admin')
@section('content')
<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Edit Manufacturer</h3>
                </div>
                @include('partials.errors')
                @include('partials.success')
                <div class="panel-body">
                    <form method="post" id="brandForm" class="form-horizontal" action="{{route('brands.update',[$brand->id])}}" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <input type="hidden" name="_method" value="put">
                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Brand Title</label>
                            <div class="col-md-6 col-xs-12">  
                                <input type="text" name="title" class="form-control" placeholder="Title" value="{{$brand->title}}" required/>                                       
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Link</label>
                            <div class="col-md-6 col-xs-12">  
                                <input type="text" name="link" class="form-control" placeholder="Link" value="{{$brand->link}}" required/>                                       
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Description</label>
                            <div class="col-md-6 col-xs-12">  
                                <textarea class="form-control" name="description" placeholder="Description" rows="4">{{$brand->description}}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Is Featured</label>
                            <div class="col-md-6 col-xs-12">  
                                <select class="form-control" name="is_featured">
                                    <option value="1"<?= $brand->is_featured == '1' ? 'selected' : ''; ?>>Yes</option>
                                    <option value="0"<?= $brand->is_featured == '0' ? 'selected' : ''; ?>>No</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Upload Category Image</label>
                            <p><code>Recommended size 1660 X 550</code> </p>
                            <label class="" style="margin-left: 300px">
                                <?php if ($brand->fileDetail): ?>
                                    <img src="<?= $brand->fileDetail->file_path . $brand->fileDetail->file_name; ?>" class="appendImage" width="75" height="75">
                                <?php else: ?>
                                    <img src="<?= url('/'); ?>/adminassets/img/upload-icon.png" class="appendImage" width="75" height="75">
                                <?php endif; ?>
                                <input type="file" name="image" onchange="readURL(this);" accept="image/*" style="display: none"/>
                            </label>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12 col-xs-12">  
                                <p class="text-center">
                                    <input type="submit" class="btn btn-info" value="Update Brand"/> 
                                </p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection