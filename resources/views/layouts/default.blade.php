

<!DOCTYPE html>
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
    <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <title>Aaima Jewellers</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="viewport" content="width=device-width">

        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link rel="stylesheet" type="text/css" href="<?= url('/'); ?>/frontassets/css/font-awesome.min.css" />
        <link rel="stylesheet" type="text/css" href="<?= url('/'); ?>/frontassets/css/bootstrap.css" />
        <link rel="stylesheet" type="text/css" href="<?= url('/'); ?>/frontassets/css/style.css">
        <link rel="stylesheet" type="text/css" href="<?= url('/'); ?>/frontassets/css/custom.css">
        <link rel="stylesheet" type="text/css" href="<?= url('/'); ?>/frontassets/css/magnific-popup.css">
        <link rel="stylesheet" type="text/css" href="<?= url('/'); ?>/frontassets/css/owl.carousel.css">
        <link rel="stylesheet" type="text/css" href="<?= url('/'); ?>/frontassets/css/jquery-ui.css">
        <link rel="stylesheet" type="text/css" href="<?= url('/'); ?>/frontassets/css/product-zoomifier.css">

        <link rel="shortcut icon" href="<?= url('/'); ?>/frontassets/images/favicon.png">
        <link rel="apple-touch-icon" href="<?= url('/'); ?>/frontassets/images/apple-touch-icon.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?= url('/'); ?>/frontassets/images/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<?= url('/'); ?>/frontassets/images/apple-touch-icon-114x114.png">
        <style>
            .error{
                color:red;
            }
          #loader-wrapper {
  position: fixed;
  top: 0;
  left: 0;
  width: 50%;
  height: 50%;
  z-index: 1000;
  overflow: hidden;
}
.loder {
  position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url('http://dev.aaima.org/images/cartloader.gif') 50% 50% center no-repeat #fff;
    opacity: 1;
      background-size:200px 200px;

}


@-webkit-keyframes spin {
  0% {
    -webkit-transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
  }
}
@keyframes spin {
  0% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@-webkit-keyframes spin-reverse {
  0% {
    -webkit-transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(-360deg);
  }
}
@keyframes spin-reverse {
  0% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(-360deg);
    transform: rotate(-360deg);
  }
}
#loader-wrapper .loader-section {
  position: fixed;
  top: 0;
  width: 100%;
  height: 100%;
  background: #fff;
  z-index: 10;
}
.loaded .loder {
  opacity: 0;
  transition: all 0.1s ease-in-out;
}
.loaded #loader-wrapper {
  visibility: hidden;
  transition: all 0.2s ease-in-out;
}
[style*='--text-hover']:hover {
  color: var(--text-hover) !important;
}

        </style>
        <script src="<?= url('/'); ?>/frontassets/js/jQuery_v3.1.1.min.js"></script>
        <script src="<?= url('/'); ?>/adminassets/js/jquery-validate.min.js" type="text/javascript"></script>

        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

          <script type="text/javascript">
    $(document).ready(function() {
 
  // Fakes the loading setting a timeout
    setTimeout(function() {
         $('body').addClass('loaded');
         $(".loader").fadeOut("slow");
    }, 100);
 
});
</script>
    </head>
    <body>
         <div id="loader-wrapper">
  <!-- <div id="loader"></div> -->
  <div class="loder"></div>
</div>

<!-- <div class="loder">

</div> -->
        <div class="ajax-loader">
            <img src="<?= url('/'); ?>/frontassets/images/loader.gif" class="img-responsive" />
        </div>
        <?php
        $categories = getParentCategory();
        ?>
        <!--<div class="loder"></div>-->
        <div class="toastr"></div>
        <div class="wrapper">
         <!--    <div id="subscribe-me" class="modal animated fade in" role="dialog" data-keyboard="true" tabindex="-1">
                <div class="newsletter-popup">
                    <img class="offer" src="<?= url('/'); ?>/frontassets/images/newsbg.jpg" alt="offer">
                    <div class="newsletter-popup-static newsletter-popup-top">
                        <div class="popup-text">
                            <div class="popup-title">50% <span>off</span></div>
                            <div class="popup-desc">
                                <div>Sign up and get 50% off your next Order</div>
                            </div>
                        </div>
                        <form onsubmit="return  validatpopupemail();" method="post">
                            <div class="form-group required">
                                <input type="email" name="email-popup" id="email-popup" placeholder="Enter Your Email" class="form-control input-lg" required />
                                <button type="submit" class="btn btn-default btn-lg" id="email-popup-submit">Subscribe</button>
                                <label class="checkme">
                                    <input type="checkbox" value="" id="checkme" />Dont show again</label>
                            </div>
                        </form>
                    </div>
                </div>
            </div> -->
            <!-- =====  HEADER START  ===== -->
            <header id="header">

                <div class="header">
                    <div class="container">
                        <nav class="navbar">
                            <div class="navbar-header"> 
                                <a class="navbar-brand" href="<?= url('/'); ?>"> 
                                    <img alt="" src="<?= url('/'); ?>/frontassets/images/logo.png"> 
                                </a> 
                            </div>
                            <div class="header-right pull-right mtb_30">
                                <div class="dropdown pull-right signin-menu">
                                    <a href="" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user-o"></i>
                                        <?php
                                        if (Session::get('user_data')):
                                            $user_data = Session::get('user_data');
                                            $customer = getCustomer($user_data['customer_id']);
                                            if ($customer) {
                                                $firstname = $customer->firstname;
                                            } else {
                                                $firstname = 'Guest';
                                            }
                                            ?>
                                            Welcome <?= $firstname; ?> 
                                        <?php else: ?>
                                            Login/Register
                                        <?php endif; ?>
                                        <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <?php
                                        if (Session::get('user_data')):
                                            ?>
                                            <li><a href="/myaccount"><i class="fa fa-user"></i> My Account</a></li>
                                            <li><a href="/myorder"><i class="fa fa-list"></i> Orders</a></li>
                                            <li><a href="/mywishlist"><i class="fa fa-heart"></i> My Wishlist</a></li>
                                            <li><a href="/mycart"><i class="fa fa-shopping-cart"></i> Go To Cart</a></li>
                                            <li><a href="/mybusinessassociate"><i class="fa fa-sitemap"></i> My Business Associate</a></li>
                                            <li><a href="/myreward"><i class="fa fa-trophy"></i> My Rewards</a></li>
                                            <li><a href=""><i class="fa fa-retweet"></i> Returns</a></li>
                                            <li><a href=""><i class="fa fa-lock"></i> Change Password</a></li>
                                            <li class="logout"><a href="/logoutuser" onclick="return confirm('Are you sure, you want to logout ?')"><i class="fa fa-power-off"></i> Logout</a></li>
                                        <?php else: ?>
                                            <li><a href="/loginuser">Login</a></li>
                                            <li><a href="/register">Register</a></li>
                                            <li><a href="/mycart">Go To Cart</a></li>
                                        <?php endif; ?>
                                        <?php
                                        //if (!Session::get('user_data')):
                                        ?>
                                        <!--  <li class="divider"></li>  
                                         <li class="register"><a href="/register"><small>New User? </small>Register</a></li>
                                         <li class="login"><a href="/loginuser">Login</a></li> -->
                                        <?php //endif;  ?>
                                    </ul>
                                </div>
                                <?php
                                if (!Session::get('user_data')) {
                                    if (!empty($_COOKIE['customer_cookie'])) {
                                        $customer_id = $_COOKIE['customer_cookie'];
                                    } else {
                                        $customer_id = '';
                                    }
                                } else {
                                    $user_data = Session::get('user_data');
                                    $customer_id = $user_data['customer_id'];
                                }
//                                echo $customer_id;die;
                                $usercart = getCart($customer_id);
                                $cartCount = getCartCount($customer_id);

                                if ($usercart):
                                    $cartArr = [];
                                    $s_total = '0';
                                    foreach ($usercart as $key => $row):
                                        $productInfo = getProduct($row->product_id);

                                        $cartArr[$key] = $row;
                                        $price=$productInfo->price - (($productInfo->discount_percent * $productInfo->price) / 100);
                                        $s_total = $s_total + ($row->quantity * $price);
                                    endforeach;
                                endif;
                                ?>
                                <button class="navbar-toggle pull-left" type="button" data-toggle="collapse" data-target=".js-navbar-collapse"> <span class="i-bar"><i class="fa fa-bars"></i></span></button>
                                <div class="shopping-icon aa" data-target="#cart-dropdown" data-toggle="collapse" aria-expanded="true" role="button">
                                    <div class="cart-item " data-target="#cart-dropdown" data-toggle="collapse" aria-expanded="true" role="button">Cart <span class="cart-qty"><?= $cartCount != '' ? $cartCount : '0'; ?></span></div>
                                    <div id="cart-dropdown" class="cart-menu collapse">
                                        <ul class="cart">
                                            <li>
                                                <div class="row">
                                                         <div class="scrollbar" id="style-2">
                                                            
                                                        <table class="table table-striped">
                                                            <tbody class="append_cart">
                                                                @if(!$cartArr)
                                                            <p class="noproduct" style="position: absolute;left: 90px;">No Product Found</p>
                                                            @else
                                                            @foreach($cartArr as $cart)
                                                            <?php
                                                            $productInfo = getProduct($cart->product_id);
                                                            if ($productInfo) {
                                                                $image = $productInfo->fileDetail->file_path . $productInfo->fileDetail->file_name;
                                                            } else {
                                                                $image = url('/') . '/frontassets/images/no_img.png';
                                                            }
                                                            ?>
                                                            <tr class="cartlist<?= $cart->id; ?>">
                                                                <td class="text-center col-xs-2 col-sm-4 col-md-4"><a href="/product_detail/{{$cart->product_id}}"><img src="{{$image}}" title="{{$productInfo->name}}" style="height: 75px;"></a></td>
                                                            <?php 
                                                            $price=$productInfo->price - (($productInfo->discount_percent * $productInfo->price) / 100);
                                                            ?>
                                                                <td class="text-left product-name col-xs-4 col-sm-6 col-md-6"><a href="/product_detail/{{$cart->product_id}}">{{$productInfo->name}}</a>
                                                                    <span class="text-left price"><i class="fa fa-inr"></i> {{$price}} x {{$cart->quantity}}</span>                                                                
                                                                </td>
                                                                <td class="text-center col-xs-2 col-sm-2 col-md-2">
                                                                    <a class="close-cart" id="{{$cart->id}}" onclick="removefromcart(this,<?= $cart->id; ?>)"><i class="fa fa-times-circle"></i></a>
                                                                </td>
                                                            </tr>
                                                            @endforeach
                                                            @endif
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <table class="table">
                                                    <tbody>
                                                        <tr>
                                                            <td class="text-right"><strong>Sub-Total</strong></td>
                                                            <td class="text-right subtotal"><i class="fa fa-inr"></i> <?= number_format($s_total, '2'); ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-right"><strong>Total</strong></td>
                                                            <td class="text-right subtotal"><i class="fa fa-inr"></i> <?= number_format($s_total, '2'); ?></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </li>
                                            <li class="mobile_cart">
                                                <a href="/mycart">
                                                    <input class="btn pull-left mt_10" value="View cart" type="submit">
                                                </a>
                                                <a <?php if (!empty($user_data)): ?>href="/checkout"<?php else: ?>href="/loginuser/checkout"<?php endif; ?>>
                                                    <input class="btn pull-right mt_10" value="Checkout" type="submit">
                                                </a>
                                            </li>

                                        </ul>
                                    </div>
                                </div>
                                <div class="main-search pull-right">
                                    <form method="post" id="searchForm">
                                        {{csrf_field()}}                                       
                                        <div class="header-search">
                                            <div class="main-seacrhbar hidden-xs hidden-sm">
                                                <div class="input-group">
                                                    <input type="text" name="keyword" class="form-control keyword" placeholder="Search here..." onkeyup="getsuggestions(this)" value="<?php isset($_POST['keyword']) ? $_POST['keyword'] : ''; ?>">
                                                    <span class="input-group-btn">
                                                        <input type="submit" class="btn" data-loading-text="Loading..." id="button-search" value="Search" disabled>
                                                    </span>    
                                                </div>
                                                <div class="infosearchlist">

                                                </div>
                                            </div>
                                            <a id="search-overlay-btn" class="visible-xs visible-sm"></a> 
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <!-- /.nav-collapse -->
                        </nav>
                    </div>
                </div>
                <div class="header-bottom">
                    <div class="container">
                        <div class="collapse navbar-collapse js-navbar-collapse">
                            <ul id="menu" class="nav navbar-nav">
                                <li><a href="<?= url('/'); ?>">Home</a></li>


                               
                                @if($categories)
                                @foreach($categories as $category)

  <?php 
                                    $child_categories = getChildCategories($category->id);
                            $count=count($child_categories);
                                    ?>
                                   
                                <li <?php  if($count!="0"){?>class="dropdown"<?php }?>><a href="/products/{{$category->id}}" <?php  if($count!="0"){?>class="dropdown-toggle" <?php }?>>{{$category->name}}
<?php  if($count!="0"){?>
                                 <span class="caret"></span>
<?php }?>
                             </a>
<?php  if($count!="0"){?>
                                  <ul class="dropdown-menu">
                                    <?php foreach($child_categories as $val){?>
                                        <li> <a href="/products/{{$val->id}}">{{$val->name}}</a></li>
                                <?php }?>
                                    </ul>
<?php  }?>
                                </li>


                                @endforeach
                                @endif
                               
                                <li> <a href="/blog">Blog</a></li> 
                               <!-- <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown">Pages <span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li> <a href="#">Contact us</a></li>
                                        <li> <a href="#">Cart</a></li>
                                        <li> <a href="#">Checkout</a></li>
                                        <li> <a href="#">Product Detail Page</a></li>
                                        <li> <a href="#">Single Post</a></li>
                                    </ul>
                                </li>
                                 <li> <a href="#">About us</a></li> -->
                            </ul>
                        </div>
                    </div>
                </div>
            </header>
            <!-- =====  HEADER END  ===== -->
            @yield('content')

            <!-- =====  FOOTER START  ===== -->
            <div class="footer pt_60 pb_20 white-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3 footer-block">
                            <div class="content_footercms_right">
                                <div class="footer-contact">
                                    <div class="footer-logo mb_40">
                                        <a href="<?= url('/'); ?>"> 
                                            <img src="<?= url('/'); ?>/frontassets/images/logo.png" alt="HealthCare"> 
                                        </a> 
                                    </div>
                                    <ul>
                                        <li>1st Floor, Anand Colony.Near Chintpurni Mata Mandir,Railway Road,Gurugram(Haryana)</li>
                                        <li><span style="font-weight: bold;">Custmer Care:-</span>01244241987</li>
                                        <li><span style="font-weight: bold;">Whatsapp No:-</span>8447951987</li>
                                    </ul>
                                    <div class="social_icon">
                                        <ul>
                                            <!-- <li><a href=""><i class="fa fa-facebook"></i></a></li>
                                            <li><a href=""><i class="fa fa-google"></i></a></li>
                                            <li><a href=""><i class="fa fa-linkedin"></i></a></li>
                                            <li><a href=""><i class="fa fa-twitter"></i></a></li> -->
                                            <!-- <li><a href=""><i class="fa fa-rss"></i></a></li> -->
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 footer-block">
                            <h6 class="footer-title ptb_20">Categories</h6>
                            <ul>
                                <?php
                                if ($categories):
                                    foreach ($categories as $cat):
                                        ?>
                                        <li><a href="/products/<?= $cat->id; ?>"><?= $cat['name']; ?></a></li>
                                        <?php
                                    endforeach;
                                endif
                                ?>
                            </ul>
                        </div>
                        <div class="col-md-2 footer-block">
                            <h6 class="footer-title ptb_20">Information</h6>
                            <ul>
                                <li><a href="">Contact Us</a></li>
                                <li><a href="/pages/about_us">About Us</a></li>
                                <li><a href="/pages/privacy_policy">Privacy Policy</a></li>
                                <li><a href="/pages/return_policy">Return Refund Policy</a></li>
                                <!-- <li><a href="">Best Sellers</a></li>
                                <li><a href="">Our Stores</a></li> -->
                            </ul>
                        </div>
                        <div class="col-md-2 footer-block">
                            <h6 class="footer-title ptb_20">My Account</h6>
                            <ul>
                                <?php
                                if (Session::get('user_data')):
                                    ?>
                                    <li><a href="/checkout">Checkout</a></li>
                                    <li><a href="/myaccount">My Account</a></li>
                                    <li><a href="/myorder">My Orders</a></li>
                                    <!-- <li><a href="">My Credit Slips</a></li> -->
                                    <li><a href="/myaddress">My Addresses</a></li>
                                    <!-- <li><a href="">My Personal Info</a></li> -->
                                <?php else: ?>
                                    <li><a href="/loginuser">Checkout</a></li>
                                    <li><a href="/loginuser">My Account</a></li>
                                    <li><a href="/loginuser">My Orders</a></li>
                                    <!-- <li><a href="/loginuser">My Credit Slips</a></li> -->
                                    <li><a href="/loginuser">My Addresses</a></li>
                                    <!-- <li><a href="/loginuser">My Personal Info</a></li> -->
                                <?php endif; ?>
                            </ul>
                        </div>
                        <div class="col-md-3">
                            <h6 class="ptb_20">SIGN UP OUR NEWSLETTER</h6>
                            <p class="mt_10 mb_20">For get offers from our favorite brands & get 20% off for next </p>
                            <form action="#" id="register-form" method="#" enctype="multipart/form-data" class="form-horizontal">
                            <div class="form-group" style="width: 70%;">
                                <input class="mb_20" type="email" pattern="[^ @]*@[^ @]*" placeholder="Enter Your Email Address" required="" style="width: 300px;"> 
                                <button class="btn">Subscribe</button>
                            </div>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer">
                <div class="footer-bottom ptb_10">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="copyright-part">@ 2017 All Rights AAIMA Pvt Ltd.</div>
                            </div>
                            <div class="col-sm-4">
                                <div class="copyright-part">Designed & Developed by <a href="http://shadowtechnology.in"  style="--text-hover: red">Shadow technology</a></div>
                            </div>
                            <div class="col-sm-4">
                                <div class="payment-icon text-right">
                                    <ul>
                                        <li><i class="fa fa-cc-paypal "></i></li>
                                        <li><i class="fa fa-cc-stripe"></i></li>
                                        <li><i class="fa fa-cc-visa"></i></li>
                                        <li><i class="fa fa-cc-discover"></i></li>
                                        <li><i class="fa fa-cc-mastercard"></i></li>
                                        <li><i class="fa fa-cc-amex"></i></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- =====  FOOTER END  ===== -->

        </div> <!-- wrapper class ends here started in header -->

        <a id="scrollup">Scroll</a>
        <script>

            $(".aa").on('click', function () {
            var aa = $('.cart-qty').text();
            if (aa > 0) {
                $('.cart-menu').toggle();
            }else{
               $('.cart-menu').hide(); 
            }

                });

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $(document).ready(function () {
                $(document)
                        .ajaxStart(function () {
                            $('.ajax-loader').css("visibility", "visible");
                        })
                        .ajaxStop(function () {
                            $('.ajax-loader').css("visibility", "hidden");
                        });
            });

            function getsuggestions(obj) {
                var keyword = $(obj).val();
                $.ajax({
                    url: '/getproductsuggestion',
                    type: 'post',
                    dataType: 'json',
                    data: {keyword: keyword},
                    cache: false
                }).done(function (response) {
                    var string = '<ul>';
                    if (response.error_code == 200) {
                        $.each(response.result, function (i, v) {
                            string += '<li><a href="javascript:void(0)" name="' + v.name + '" onclick="appendText(this)">' + v.name.substr(0, 65) + '...</a></li>';
                        });
                        string += '</ul>';
                        $('.infosearchlist').show();
                        $(".infosearchlist").html(string);
                    } else {
                        string = '<p>No Product Found</p>';
                        $(".infosearchlist").html(string);
                    }
                })
            }
            function appendText(obj) {
                var text = $(obj).attr('name');
                $('input[name = keyword]').val(text);
                $('.infosearchlist').hide();
                $("#button-search").attr("disabled", false);
            }

            $("#searchForm").on('submit', function (e) {
                e.preventDefault();

                var keyword = $(".keyword").val();
                if (keyword != '') {
                    $.ajax({
                        url: '/do_search',
                        type: 'post',
                        dataType: 'json',
                        data: $(this).serialize(),
                        cache: false
                    }).done(function (response) {
                        if (response.error_code == 200) {
                            window.location.href = '/searched_products/' + response.result.keyword
                        } else {
                            Command: toastr["error"]("Some error found, please try again")

                            toastr.options = {
                                "closeButton": false,
                                "debug": false,
                                "newestOnTop": false,
                                "progressBar": false,
                                "positionClass": "toast-top-right",
                                "preventDuplicates": false,
                                "onclick": null,
                                "showDuration": "200",
                                "hideDuration": "200",
                                "timeOut": "200",
                                "extendedTimeOut": "200",
                                "showEasing": "swing",
                                "hideEasing": "linear",
                                "showMethod": "fadeIn",
                                "hideMethod": "fadeOut"
                            }
                        }
                    });
                } else {
                    alert("Please enter keyword");
                }
            });

            function addtocart(obj) {
                var quantity = $("#quantity1").val();
                if (quantity == undefined) {
                    quantity = 1;
                }
                var product_id = $(obj).attr('product_id');
                $.ajax({
                    url: '/addtocart',
                    type: 'post',
                    dataType: 'json',
                    data: {quantity: quantity, product_id: product_id},
                    cache: false
                }).done(function (response) {
                    if (response.error_code == 200) {
                        console.log(response);
                        $(".cart-qty").text(response.result.cartCount);
                        $(".subtotal").html("<i class='fa fa-inr'></i> " + response.result.totalprice.toFixed(2));

                        if (response.result.product != '') {
                            var pricee = response.result.product.price - ( response.result.product.price * response.result.product.discount_percent / 100 );
                            var string = ' <tr class="cartlist' + response.result.cart_id + '">'
                                    + '<td class="text-center"><a href="/product_detail/' + product_id + '"><img src="' + response.result.product.image + '" title="' + response.result.product.name + '" style="height:100px;"></a></td>'
                                    + '<td class="text-left product-name"><a href="/product_detail/' + product_id + '">' + response.result.product.name + '</a>'
                                    + '<span class="text-left price"><i class="fa fa-inr"></i> ' + pricee + 'x' + response.result.quantity + '</span>'
                                    + '</td>'
                                    + '<td class="text-center">'
                                    + '<a class="close-cart" id="' + response.result.cart_id + '" onclick="removefromcart(this,' + response.result.cart_id + ')"><i class="fa fa-times-circle"></i></a>'
                                    + '</td>'
                                    + '</tr>';
                            $(".append_cart").append(string);
                           // alert($('.append_cart tr').length);
                            if ($('.append_cart tr').length == '0') {
                                $(".noproduct").css("display","block");
                            } else {
                                $(".noproduct").css("display","none");
                            }
                        }

                        Command: toastr["success"](response.message)

                        toastr.options = {
                            "closeButton": false,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": false,
                            "positionClass": "toast-top-right",
                            "preventDuplicates": false,
                            "onclick": null,
                            "showDuration": "200",
                            "hideDuration": "200",
                            "timeOut": "500",
                            "extendedTimeOut": "200",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        }
                    } else if (response.error_code == 201) {
                        Command: toastr["error"](response.message)

                        toastr.options = {
                            "closeButton": false,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": false,
                            "positionClass": "toast-top-right",
                            "preventDuplicates": false,
                            "onclick": null,
                            "showDuration": "200",
                            "hideDuration": "200",
                            "timeOut": "200",
                            "extendedTimeOut": "200",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        }
                    } else {
                        Command: toastr["error"](response.message)

                        toastr.options = {
                            "closeButton": false,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": false,
                            "positionClass": "toast-top-right",
                            "preventDuplicates": false,
                            "onclick": null,
                            "showDuration": "200",
                            "hideDuration": "200",
                            "timeOut": "200",
                            "extendedTimeOut": "200",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        }
                    }
                });
            }

            function addtowishlist(obj) {
                var product_id = $(obj).attr('product_id');
                $.ajax({
                    url: '/addtowishlist',
                    type: 'post',
                    dataType: 'json',
                    data: {product_id: product_id},
                    cache: false
                }).done(function (response) {
                    if (response.error_code == 200) {

                        Command: toastr["success"](response.message)
                        toastr.options = {
                            "closeButton": false,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": false,
                            "positionClass": "toast-top-right",
                            "preventDuplicates": false,
                            "onclick": null,
                            "showDuration": "200",
                            "hideDuration": "200",
                            "timeOut": "200",
                            "extendedTimeOut": "200",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        }
                    } else if (response.error_code == 201) {
                        window.location.href = '/loginuser'
                    } else {
                        Command: toastr["error"](response.message)
                        toastr.options = {
                            "closeButton": false,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": false,
                            "positionClass": "toast-top-right",
                            "preventDuplicates": false,
                            "onclick": null,
                            "showDuration": "200",
                            "hideDuration": "200",
                            "timeOut": "200",
                            "extendedTimeOut": "200",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        }
                    }
                });
            }

            function updatecart(obj, type) {
                var id = $(obj).attr('id');
                var quantity = $(".cart_count" + id).val();
                $.ajax({
                    url: '/updatecart',
                    type: 'post',
                    dataType: 'json',
                    data: {quantity: quantity, id: id, type: type},
                    cache: false
                }).done(function (response) {
                    if (response.error_code == 200) {
                        $(".cart-qty").text(response.result.t_quantity);

                        $(".totalprice" + id).html("<i class='fa fa-inr'></i> " + response.result.totalprice.toFixed(2));
                        $(".subtotal").html("<i class='fa fa-inr'></i> " + response.result.s_total.toFixed(2));

                        if (type == 'plus') {
                            $(".cart_count" + id).val(parseInt(quantity) + 1);
                        } else if (type == 'minus') {
                            $(".cart_count" + id).val(parseInt(quantity) - 1);
                        }

                        Command: toastr["success"](response.message)
                        toastr.options = {
                            "closeButton": false,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": false,
                            "positionClass": "toast-top-right",
                            "preventDuplicates": false,
                            "onclick": null,
                            "showDuration": "200",
                            "hideDuration": "200",
                            "timeOut": "200",
                            "extendedTimeOut": "200",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        }
                    } else {
                        Command: toastr["error"](response.message)

                        toastr.options = {
                            "closeButton": false,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": false,
                            "positionClass": "toast-top-right",
                            "preventDuplicates": false,
                            "onclick": null,
                            "showDuration": "200",
                            "hideDuration": "200",
                            "timeOut": "200",
                            "extendedTimeOut": "200",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        }
                    }
                });
            }

            function removefromcart(obj, number) {
                var id = $(obj).attr('id');
                if (confirm("Are you sure, you want to remove ?")) {
                    $.ajax({
                        url: '/removefromcart',
                        type: 'post',
                        dataType: 'json',
                        data: {id: id},
                        cache: false
                    }).done(function (response) {
                        if (response.error_code == 200) {
                            $(".cartlist" + number).remove();

                            $(".cart-qty").text(response.result.t_quantity);

                            if (response.result.s_total == '0') {
                                var subtotal = '0.00';
                            } else {
                                var subtotal = response.result.s_total.toFixed(2);
                            }

                            $(".subtotal").html("<i class='fa fa-inr'></i>" + subtotal);
                            $(".totalprice").html("<i class='fa fa-inr'></i>" + subtotal);


                            if ($('.append_cart tr').length == '0') {
                                $(".noproduct").css("display","block");
                            } else {
                                $(".noproduct").css("display","none");
                            }

                            Command: toastr["success"](response.message)

                            toastr.options = {
                                "closeButton": false,
                                "debug": false,
                                "newestOnTop": false,
                                "progressBar": false,
                                "positionClass": "toast-top-right",
                                "preventDuplicates": false,
                                "onclick": null,
                                "showDuration": "200",
                                "hideDuration": "200",
                                "timeOut": "200",
                                "extendedTimeOut": "200",
                                "showEasing": "swing",
                                "hideEasing": "linear",
                                "showMethod": "fadeIn",
                                "hideMethod": "fadeOut"
                            }
                        } else {
                            Command: toastr["error"](response.message)

                            toastr.options = {
                                "closeButton": false,
                                "debug": false,
                                "newestOnTop": false,
                                "progressBar": false,
                                "positionClass": "toast-top-right",
                                "preventDuplicates": false,
                                "onclick": null,
                                "showDuration": "200",
                                "hideDuration": "200",
                                "timeOut": "200",
                                "extendedTimeOut": "200",
                                "showEasing": "swing",
                                "hideEasing": "linear",
                                "showMethod": "fadeIn",
                                "hideMethod": "fadeOut"
                            }
                        }
                    });
                }
            }

            $("#register-form").validate({
                rules: {
                    password: {
                        minlength: 5
                    },
                    confirm: {
                        minlength: 5,
                        equalTo: "#input-password"
                    }
                }
            });
            $("#login-form").validate();
            $('.characterOnly').keypress(function (e) {
                var regex = new RegExp(/^[a-zA-Z\s]+$/);
                var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
                if (regex.test(str)) {
                    return true;
                } else {
                    e.preventDefault();
                    return false;
                }
            });
            /*Below code for prevent to write character*/
            $('.floatOnly').keypress(function (event) {
                if (event.which != 46 && (event.which < 47 || event.which > 59))
                {
                    event.preventDefault();
                    if ((event.which == 46) && ($(this).indexOf('.') != -1)) {
                        event.preventDefault();
                    }
                }
            });
            $(".numericOnly").keypress(function (e) {
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    return false;
                }
            });
            /*Below code prevent the percentage till 100*/
            $('.percentage').on('keyup keydown', function (e) {
                if ($(this).val() > 100
                        && e.keyCode != 46
                        && e.keyCode != 8
                        ) {
                    e.preventDefault();
                    $(this).val(100);
                }
            });
        </script>
        <script src="<?= url('/'); ?>/frontassets/js/jquery-ui.js"></script>
        <script src="<?= url('/'); ?>/frontassets/js/owl.carousel.min.js"></script>
        <script src="<?= url('/'); ?>/frontassets/js/bootstrap.min.js"></script>
        <script src="<?= url('/'); ?>/frontassets/js/jquery.magnific-popup.js"></script>
        <script src="<?= url('/'); ?>/frontassets/js/jquery.firstVisitPopup.js"></script>
        <script src="<?= url('/'); ?>/frontassets/js/custom.js"></script>
        <script src="<?= url('/'); ?>/frontassets/js/product-zoomifier.js"></script>
     
    </body>

</html>