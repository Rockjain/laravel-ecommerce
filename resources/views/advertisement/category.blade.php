<?php use \App\Http\Controllers\AdvertisementController; ?>
<meta name="csrf-token" content="{{ csrf_token() }}">
@extends('layouts.admin')
@section('content')
<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">

            <!-- START Category DATATABLE -->
            <div class="panel panel-default">
                <div class="panel-heading">                                
                    <h3 class="panel-title">All Categories</h3>  
                    <a href="/admin/advert/category_insert" class="btn btn-danger pull-right">Add Category</a>                       
                </div>
                <div class="panel-body">
                    <table class="table datatable">
                        <thead>
                            <tr>
                                <th>Sr.</th>
                                <th>Parent</th>
                                <th>name</th>
                                <th>Status</th>
                                <th>Date Time</th>
                                 <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $sr=1;?>
                            @if($category)
                            @foreach($category as $key=>$value)
                           
                            <tr>
                                <td>{{ $sr}}</td>
                                <td><?php if($value->parent==0){echo 'Yes';}else{} ?></td>
                                <td data-toggle="tooltip" title="{{$value->name}}" >{{substr($value->name,0,30)}}</td>
                               <td>{{$value->status}}</td>
                               <td>{{$value->date_time}}</td>
                                <td style="min-width: 100px;">
                                    <a href="/admin/advert/update_category/{{$value->id}}" data-toggle="tooltip" title="Edit" class="btn btn-info btn-rounded text-center"><i class="fa fa-edit"></i></a>  
                                    <label class="switch" style="top: 12px">
                                        <input type="checkbox" <?php
                                        if ($value->status == 1) {
                                            echo "checked";
                                        }
                                        ?> id="{{$value->id}}" onchange="checkStatus(this);" id="switch-{{$key}}}">
                                        <span class="slider round"></span>
                                    </label>
                                    <a href="/admin/advert/deleteCat/{{$value->id}}" data-toggle="tooltip" title="Delete" class="btn btn-danger btn-rounded text-center" onclick="if (confirm('Delete selected Category?')){return true;}else{event.stopPropagation(); event.preventDefault();};" ><i class="fa fa-minus-circle"></i></a>  
                                </td>
                            </tr>
                            <?php $sr++;?>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END Category DATATABLE -->
        </div>
    </div>
</div>
<script>
    function checkStatus(obj) {
        var id = $(obj).attr("id");
        var checked = $(obj).is(':checked');
        if (checked == true) {
            var status = 1;
        } else {
            var status = 0;
        }
        $.ajax({
            url: '/admin/change_status',
            type: 'post',
            dataType: 'json',
            data: {method: 'changeAdvCatStatus', status: status, id: id},
            cache: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
        }).done(function (response) {
            if (response.status == '1') {

Command: toastr["success"]("Status has been updated successfully!")

toastr.options = {
  "closeButton": false,
  "debug": false,
  "newestOnTop": false,
  "progressBar": false,
  "positionClass": "toast-top-right",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}
            } else {
                alert("Some error occured, please try again");
            }
        });
    }
</script>
@endsection