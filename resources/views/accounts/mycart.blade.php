@extends('layouts.default')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-8 col-md-8 col-lg-9 mtb_30">
            <div class="white-bg">
                <div class="ptb_20">
                    <form enctype="multipart/form-data" method="post" action="#">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <td class="text-center">Image</td>
                                        <td class="text-left">Product Name</td>
                                         <td class="text-left"></td> 
                                        <td class="text-left">Price</td> 

                                        <td class="text-left">Quantity</td>
                                        <td class="text-left">Total</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if($usercart)
                                    @foreach($usercart as $cart)
                                    <?php
                                    $productInfo = getProduct($cart->product_id);
                                    if ($productInfo) {
                                        $image = $productInfo->fileDetail->file_path . $productInfo->fileDetail->file_name;
                                    } else {
                                        $image = url('/') . '/frontassets/images/no_img.png';
                                    }
                                    
                   
                                    $price=$productInfo->price - (($productInfo->discount_percent * $productInfo->price) / 100);
                                    ?>
                                    <tr class="cartlist<?= $cart->id; ?>">
                                        <td width="15%" class="text-center">
                                            <a href="/product_detail/{{$cart->product_id}}">
                                                <img src="{{$image}}" title="{{$productInfo->name}}" width="75" style="height: 85px;">
                                            </a>
                                        </td>
                                        <td width="35%" class="text-left">
                                            <h4><a href="/prorduct_detail/{{$cart->product_id}}">{{$productInfo->name}}</a></h4>
                                        </td>
                                        <?php if($productInfo->discount_percent!=0) { 
                                            $totalprice = $productInfo->price ;
                                             ?>
                                        <td width="10%" class="text-left">
                                           
                                        <span><i class="fa fa-inr"></i> <del class="text-danger">{{$totalprice}}</del></span>
                                         
                                        </td>
                                        <?php }else{ ?>
                                        <td width="10%" class="text-left">
                                        </td>
                                         <?php } ?>
                                        <td width="15%" class="text-left">
                                        <span><i class="fa fa-inr"></i>{{$price}}<span>
                                        </td>
                                   
                                        <td width="20%" class="text-left">
                                            <div class="cartqty mt-5">
                                                <span class="minus bg-dark" id="{{$cart->id}}" onclick="updatecart(this, 'minus')">-</span>
                                                <input type="number" class="cart_count{{$cart->id}}" value="<?= $cart->quantity; ?>">
                                                <span class="plus bg-dark" id="{{$cart->id}}" onclick="updatecart(this, 'plus')">+</span>
                                                <a onclick="removefromcart(this,<?= $cart->id; ?>)" id="{{$cart->id}}" title="Remove" data-toggle="tooltip">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            </div>                                           
                                        </td>
                                        <td width="15%" class="totalprice{{$cart->id}}">
                                            <i class="fa fa-inr"></i> <?= number_format($cart->quantity * $price, '2'); ?>
                                        </td>
                                    </tr>
                                    @endforeach
                                    @else
                                    <tr><td></td><td></td><td align="center">Cart Empty</td><td></td><td></td></tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </form>
                    <div class="panel-group mt_20" id="accordion">
                        <div class="panel panel-default pull-left">
                            <div id="collapseOne" class="panel-collapse collapse in">
                                <div class="panel-footer">
                                    <label for="input-coupon" class="col-sm-4 control-label"></label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="input-coupon" placeholder="Enter your coupon here" value="" name="coupon">
                                        <span class="input-group-btn">
                                            <input type="button" class="btn btn-blue" data-loading-text="Loading..." id="button-coupon" value="Apply Coupon">
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <form action="/">
                                <input class="btn pull-left btn-info mt_30" type="submit" value="Continue Shopping" />
                            </form>
                            <a <?php if (!empty($user_data)): ?>href="/checkout"<?php else: ?>href="/loginuser/checkout"<?php endif; ?>>
                                <button class="btn pull-right btn-primary mt_30">Checkout</button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="" class="col-sm-4 col-md-4 col-lg-3 mtb_30">
            <div class="white-bg">
                <div class="ptb_20">

                    <div class="row">
                        <div class="col-sm-12">
                            <table class="table table-striped">
                                <tbody>
                                    <tr>
                                        <td class="text-right"><strong>Sub-Total:</strong></td>
                                        <td class="text-right subtotal"><i class="fa fa-inr"></i> <?= number_format($s_total, '2'); ?></td>
                                    </tr>
                                    <tr>
                                        <td class="text-right"><strong>Total:</strong></td>
                                        <td class="text-right subtotal"><i class="fa fa-inr"></i> <?= number_format($s_total, '2'); ?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
            <div class="white-bg mt_10 hidden-xs">
                <div class="ptb_20">

                    <div class="row">
                        <div class="col-sm-12 cart-information-box">
                            <p><i class="fa fa-check"></i> Safe and secure payments, easy returns, 100% Authentic products</p>
                            <p><i class="fa fa-phone"></i> For support call us: +91-8447951987</p>
                        </div>
                    </div>

                </div>
            </div>

        </div>

    </div>
</div>
<script>
    $(document).ready(function () {
        $('.count').prop('disabled', true);
        $(document).on('click', '.plus', function () {
            $('.count').val(parseInt($('.count').val()) + 1);
        });
        $(document).on('click', '.minus', function () {
            $('.count').val(parseInt($('.count').val()) - 1);
            if ($('.count').val() == 0) {
                $('.count').val(1);
            }
        });
    });
</script>
@endsection