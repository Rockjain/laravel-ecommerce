@if (session()->has('success'))
<div class="alert alert-success alert-dismissible">
    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
    <strong>{!! session()->get('success') !!}</strong>.
</div>
@endif