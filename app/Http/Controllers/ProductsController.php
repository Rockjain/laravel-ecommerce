<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Model\Product;
use App\Model\Category;
use App\Model\ProductAttribute;
use Illuminate\Http\Request;
use App\Model\ProductImage;

class ProductsController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $products = Product::orderBy('id', 'desc')->get();
        return view('products.index', ['products' => $products]);
    }

    public function filter_products(Request $request) {
        extract($_POST);
        if (!empty($name)) {
            $where = [
                    ['name', 'like', '%' . $name . '%'],
            ];
        }
        if (!empty($min_price)) {
            $where = [
                    ['price', '>', $min_price],
            ];
        }
        if (!empty($max_price)) {
            $where = [
                    ['price', '<', $max_price],
            ];
        }
        if (!empty($quantity)) {
            $where['quantity'] = $quantity;
        }
        if (!empty($status)) {
            if ($status == 2) {
                $where['status'] = '0';
            } elseif ($status == 1) {
                $where['status'] = '1';
            }
        }
        if (empty($_POST)) {
            $where = [
                    ['id', '>', '0'],
            ];
        }
        if (!empty($name) || !empty($min_price) || !empty($max_price) || !empty($quantity) || !empty($status)) {
            $products = Product::where($where)->orderBy('id', 'desc')->get();
        } else {
            $products = Product::orderBy('id', 'desc')->get();
        }

        return view('products.index', ['products' => $products]);
    }

    public function featured_products(Request $request) {
        $products = Product::where(['is_featured' => '1'])->get();
        return view('products.featured', ['products' => $products]);
    }

    public function top_products(Request $request) {
        $products = Product::where(['status' => '1'])->orderBy('sold_count', 'desc')->limit(10)->get();
        return view('products.top', ['products' => $products]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $categories = Category::where(['status' => '1'])->get();
        return view('products.create', ['categories' => $categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $addArr = [
            'sku_code' => $request->input('sku_code'),
            'hsn_code' => $request->input('hsn_code'),
            'other_code' => $request->input('other_code'),
            'name' => $request->input('name'),
            'price' => $request->input('price'),
            'quantity' => $request->input('quantity'),
            'delivery_charge' => $request->input('delivery_charge'),
            'short_desc' => $request->input('short_desc'),
            'description' => $request->input('description'),
            'category_id' => $request->input('category_id'),
            'brand_id' => $request->input('brand_id'),
            'tax' => $request->input('tax') != '' ? $request->input('tax') : '0',
            'discount_percent' => $request->input('discount_percent') != '' ? $request->input('discount_percent') : '0',
            'discount_quantity' => $request->input('discount_quantity') != '' ? $request->input('discount_quantity') : '0',
            'discount_start_date' => date('Y-m-d', strtotime($request->input('discount_start_date'))),
            'discount_end_date' => date('Y-m-d', strtotime($request->input('discount_end_date'))),
            'is_featured' => $request->input('is_featured') != '' ? $request->input('is_featured') : '0',
            'is_recommend' => $request->input('is_recommend') != '' ? $request->input('is_recommend') : '0',
            'status' => '1'
        ];

        if ($request->hasFile('image')):
            $check = $this->uploadFile($request, 'image', '/images/productimages/');
            if ($check):
                $nameArray = explode('.', $check);
                $ext = end($nameArray);

                $insertParam['file_path'] = '/images/productimages/';
                $insertParam['file_name'] = $check;
                $insertParam['file_type'] = $ext;

                $getImgData = $this->insertFile($insertParam);
                $addArr['image'] = $getImgData['id'];
            endif;
        endif;

        $return = Product::create($addArr);
        if ($return) {
            if ($request->hasFile('product_images')):
                foreach ($request->file()['product_images'] as $file):
                    $path = "/images/productimages/";

                    $fullName = $file->getClientOriginalName();
                    $stringName = $this->my_random_string(explode('.', $fullName)[0]);
                    $fileName = $stringName . time() . '.' . $file->getClientOriginalExtension();
                    $destinationPath = public_path($path);
                    $check = $file->move($destinationPath, $fileName);

                    if ($check):
                        $nameArray = explode('.', $check);
                        $ext = end($nameArray);

                        $insertParam['file_path'] = $path;
                        $insertParam['file_name'] = $fileName;
                        $insertParam['file_type'] = $ext;

                        $getImgData = $this->insertFile($insertParam);

                        ProductImage::create(['product_id' => $return->id, 'image' => $getImgData->id]);

                    endif;
                endforeach;
            endif;

            if ($request->input('values')):
                foreach ($request->input('values') as $value):
                    $attrArr = [
                        'product_id' => $return->id,
                        'attribute_id' => $value['attr_id'],
                        'attribute_value' => $value['attr_val'],
                        'type' => $value['attr_type'],
                    ];
                    ProductAttribute::create($attrArr);
                endforeach;
            endif;
            return redirect()->route('products.index')->with('success', 'Product created successfully');
        }
        return back()->withInput()->with('error', 'Error creating new product');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product) {
        $product = Product::find($product->id);
//      echo "<pre>";print_r($product->productAttributes);die;
        return view('products.show', ['product' => $product]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product) {
        $categories = Category::where(['status' => '1'])->get();
        $productInfo = Product::find($product->id);
        $catbrands = \App\Model\CategoryBrand::where(['category_id' => $productInfo->category_id])->get();
//        echo "<pre>";
//        print_r($productInfo->productImages[0]->fileDetail);
//        die;
        $productAttributes = ProductAttribute::where(['product_id' => $product->id])->get();
        return view('products.edit', ['productInfo' => $productInfo, 'categories' => $categories, 'brands' => $catbrands, 'productAttributes' => $productAttributes]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product) {
        $updateArr = [
            'sku_code' => $request->input('sku_code'),
            'hsn_code' => $request->input('hsn_code'),
            'other_code' => $request->input('other_code'),
            'name' => $request->input('name'),
            'price' => $request->input('price'),
            'quantity' => $request->input('quantity'),
            'description' => $request->input('description'),
            'short_desc'=>$request->input('short_desc'),
            'category_id' => $request->input('category_id'),
            'brand_id' => $request->input('brand_id'),
            'tax' => $request->input('tax') != '' ? $request->input('tax') : '0',
            'discount_percent' => $request->input('discount_percent') != '' ? $request->input('discount_percent') : '0',
            //'discount_quantity' => $request->input('discount_quantity') != '' ? $request->input('discount_quantity') : '0',
            'discount_start_date' => date('Y:m:d', strtotime($request->input('discount_start_date'))),
            'discount_end_date' => date('Y:m:d', strtotime($request->input('discount_end_date'))),
            'is_featured' => $request->input('is_featured'),
            'is_recommend' => $request->input('is_recommend'),
        ];

        //code for update first existing image
        if ($request->hasFile('image')):
            $path = "/images/productimages/";
            $check = $this->uploadFile($request, 'image', $path);
            if ($check):
                $nameArray = explode('.', $check);
                $ext = end($nameArray);

                $insertParam['file_path'] = $path;
                $insertParam['file_name'] = $check;
                $insertParam['file_type'] = $ext;

                $getImgData = $this->insertFile($insertParam);

                $updateArr['image'] = $getImgData['id'];
            endif;
        endif;

        $return = Product::where('id', $product->id)->update($updateArr);
        if ($return) {
            //code for update existing more images
            if ($request->hasFile('product_images')):
                foreach ($request->file()['product_images'] as $key => $file):
                    $path = "/images/product_images/";
                    $fullName = $file->getClientOriginalName();
                    $stringName = $this->my_random_string(explode('.', $fullName)[0]);
                    $fileName = $stringName . time() . '.' . $file->getClientOriginalExtension();
                    $destinationPath = public_path($path);
                    $check = $file->move($destinationPath, $fileName);

                    if ($check):
                        $nameArray = explode('.', $check);
                        $ext = end($nameArray);

                        $insertParam['file_path'] = $path;
                        $insertParam['file_name'] = $fileName;
                        $insertParam['file_type'] = $ext;

                        $getImgData = $this->updateFile($insertParam, $key);
                        ProductImage::where(['id' => $key])->update(['product_id' => $product->id, 'image' => $getImgData->id]);
                    endif;
                endforeach;
            endif;

            //code to add new images into products images
            if ($request->hasFile('more_product_images')):
                foreach ($request->file()['more_product_images'] as $file):
                    $path = "/images/product_images/";
                    $fullName = $file->getClientOriginalName();
                    $stringName = $this->my_random_string(explode('.', $fullName)[0]);
                    $fileName = $stringName . time() . '.' . $file->getClientOriginalExtension();
                    $destinationPath = public_path($path);
                    $check = $file->move($destinationPath, $fileName);

                    if ($check):
                        $nameArray = explode('.', $check);
                        $ext = end($nameArray);

                        $insertParam['file_path'] = url('/') . $path;
                        $insertParam['file_name'] = $fileName;
                        $insertParam['file_type'] = $ext;
                        $getImgData = $this->insertFile($insertParam);

                        ProductImage::create(['product_id' => $product->id, 'image' => $getImgData->id]);
                    endif;
                endforeach;
            endif;

            if ($request->input('values')):
                foreach ($request->input('values') as $value):
                    $attrArr = [
                        'product_id' => $product->id,
                        'attribute_value' => $value['attr_val'],
                        'type' => $value['attr_type'],
                    ];
                    ProductAttribute::where('id', $value['attr_id'])->update($attrArr);
                endforeach;
            endif;
            return redirect()->route('products.index')->with('success', 'Product updated successfully');
        }
        return back()->withInput();
    }

    /* Function for removing product image */

    public function remove_product_image(Request $request) {
        $id = $request->input('id');
        $delete = ProductImage::find($id)->delete();
        if ($delete) {
            $res = ['status' => '1', 'message' => 'Image deleted successfully'];
        } else {
            $res = ['status' => '0', 'message' => 'Some error found, please try again'];
        }
        echo json_encode($res);
        die;
    }

    public function remove_product_attribute(Request $request) {
        $id = $request->input('id');
        $delete = ProductAttribute::find($id)->delete();
        if ($delete) {
            $res = ['status' => '1', 'message' => 'Attribute deleted successfully'];
        } else {
            $res = ['status' => '0', 'message' => 'Some error found, please try again'];
        }
        echo json_encode($res);
        die;
    }

    public function reviews() {
        $pendingreviews = \App\Model\ProductReview::where(['status' => '0'])->get();
        $approvedreviews = \App\Model\ProductReview::where(['status' => '1'])->get();
        $disapprovedreviews = \App\Model\ProductReview::where(['status' => '2'])->get();
        return view('products.reviews', ['approvedreviews' => $approvedreviews, 'pendingreviews' => $pendingreviews, 'disapprovedreviews' => $disapprovedreviews]);
    }

}
