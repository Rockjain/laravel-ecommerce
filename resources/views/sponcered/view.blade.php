<meta name="csrf-token" content="{{ csrf_token() }}">
@extends('layouts.admin')
@section('content')
<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">

            <!-- START Category DATATABLE -->
            <div class="panel panel-default">
                <div class="panel-heading">                                
                    <h3 class="panel-title">All Sponcered</h3>                         
                </div>
                <div class="panel-body">
                    <table class="table datatable">
                        <thead>
                            <tr>
                                <th>Sr.</th>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Image</th>
                                <th>Date</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $sr = 1; ?>
                            @if($sponcered)
                            @foreach($sponcered as $key=>$value)
                            <tr>
                                <td>{{ $sr}}</td>
                                <td>{{$value->name}}</td>
                                <td data-toggle="tooltip" title="{{$value->description}}" >{{substr($value->description,0,30)}}</td>
                                <td><img src="/images/testimonialsimages/{{$value->image}}" style="border-radius: 50%;" width="75" height="75"></td>
                                <td>{{substr($value->created_at, 0, 10)}}</td>

                                <td style="min-width: 100px;">
                                    <a href="/admin/sponcered/edit/{{$value->id}}" data-toggle="tooltip" title="Edit" class="btn btn-info btn-rounded text-center"><i class="fa fa-edit"></i></a>  
                                     <!-- <label class="switch" style="top: 12px">
                                        <input type="checkbox" <?php
                                        if ($value->status == 1) {
                                            //echo "checked";
                                        }
                                        ?> id="{{$value->id}}" onchange="checkStatus(this);" id="switch-{{$key}}}">
                                        <span class="slider round"></span>
                                    </label> -->
                                    <a href="/admin/sponcered/delete/{{$value->id}}" data-toggle="tooltip" title="Delete" class="btn btn-danger btn-rounded text-center" onclick="if (confirm('Delete selected sponcered?')) {
                                                return true;
                                            } else {
                                                event.stopPropagation();
                                                event.preventDefault();
                                            }
                                            ;" ><i class="fa fa-minus-circle"></i>
                                    </a>  
                                </td>
                            </tr>
                            <?php $sr++; ?>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END Category DATATABLE -->
        </div>
    </div>
</div>
<script>
    function checkStatus(obj) {
        var id = $(obj).attr("id");
        var checked = $(obj).is(':checked');
        if (checked == true) {
            var status = 1;
        } else {
            var status = 0;
        }
        $.ajax({
            url: '/admin/change_status',
            type: 'post',
            dataType: 'json',
            data: {method: 'changeTestStatus', status: status, id: id},
            cache: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
        }).done(function (response) {
            if (response.status == '1') {

                Command: toastr["success"]("Status has been updated successfully!")

                toastr.options = {
                    "closeButton": false,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": false,
                    "positionClass": "toast-top-right",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                }
            } else {
                alert("Some error occured, please try again");
            }
        });
    }
</script>
@endsection