<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class OrderHistory extends Model {

    protected $table = 'order_history';
    protected $fillable = array(
        'order_id',
            'status',
            'comment',
            'date_added'
    );

 
}
