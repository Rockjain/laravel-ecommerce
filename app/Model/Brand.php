<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model {

    protected $table = 'brands';
    protected $fillable = array(
        'title',
        'image',
        'link',
        'description',
        'status',
        'created_at',
        'updated_at'
    );

    public function fileDetail() {
        return $this->belongsTo('App\Model\Appfiles', 'image');
    }

}
