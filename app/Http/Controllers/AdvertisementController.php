<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;




class AdvertisementController extends Controller
{
    //
    public function index() {
        return view('advertisement.index');
    }

     public function view() {
        $advert = DB::table('advertisements')->select('*')->get();

        return view('advertisement.index');
    }

    public function parent_cat($parent_id) {
        $parent = DB::table('adv_category')->select('*')->where('parent',$parent_id)->get();
        return $parent;
    }

    public function insert(Request $request)
    {
    	extract($_POST);
        if($form_type=='add'){
        // $path = "/images/advertisementimages/";
        // $check = $this->uploadFile($request, 'image', $path);
        // if ($check):
        //     $nameArray = explode('.', $check);
        //     $ext = end($nameArray);

        //     $insertParam['file_path'] = url('/') . $path;
        //     $insertParam['file_name'] = $check;
        //     $insertParam['file_type'] = $ext;

        //     $getImgData = $this->insertFile($insertParam);

        //     $adArr['image'] = $getImgData['id'];

        // endif;
    	$data=array('name'=>$title);
    	$data = DB::table('testimonials')->insert($data);
    	
        
        }else if($form_type=='update'){

            $path = "/images/testimonialsimages/";
        $check = $this->uploadFile($request, 'image', $path);
        if ($check){
            $nameArray = explode('.', $check);
            $ext = end($nameArray);

            $insertParam['file_path'] = url('/') . $path;
            $insertParam['file_name'] = $check;
            $insertParam['file_type'] = $ext;

            $getImgData = $this->insertFile($insertParam);

            $adArr['image'] = $getImgData['id'];
            }else{
                $check = $image_update;
            }


        $data = DB::table('testimonials')->where('id', $id)->update([
            'name'=>$name,'description'=>$description,'status'=>$status,'image'=>$check
            ]);
        if($data){
            echo 'updated';
        }else{
            echo 2;
        }
    }
    }

    public function category() {
        $category = DB::table('adv_category')->select('*')->get();


        // $parent_id = $category->parent;
        // $parent = DB::table('adv_category')->select('*')->where('parent',$parent_id)->get();
        return view('advertisement.category',['category' => $category]);
    }

    public function category_insert() {
    $parent = DB::table('adv_category')->select('*')->where('parent',0)->get();
        return view('advertisement.category_insert', ['parents' => $parent]);
    }

    public function update_category($id) {

    $parent = DB::table('adv_category')->select('*')->where('parent',0)->get();
    $category = DB::table('adv_category')->select('*')->where('id',$id)->get();
        return view('advertisement.category_insert', ['parents' => $parent],['category' => $category]);
    }

     public function cat_store() {
    extract($_POST);
         if($form_type=='add'){
        $data=array('parent'=>$parent,'name'=>$name,'description'=>$description,'status'=>$status);
        $data = DB::table('adv_category')->insert($data);
    echo"$data";
    }
    if($form_type=='update'){
        $data = DB::table('adv_category')->where('id', $id)->update([
                'parent'=>$parent,'name'=>$name,'description'=>$description,'status'=>$status
            ]);
        if($data){
            echo 'updated';
        }else{
            echo 2;
        }
    }
    }

    public function edit(Request $request) {

        $id=$request->id;
        $testimonials = DB::table('testimonials')->select('*')->where('id',$id)->get();
        return view('testimonials.index', ['testimonials' => $testimonials]);
    }
    public function delete_category(Request $request){
        $id=$request->id;
        $status = DB::table('adv_category')->delete($id);
        if($status){
            return redirect()->action('AdvertisementController@category');
             }
    }
}
