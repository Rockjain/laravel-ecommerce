<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class State extends Model {

    protected $table = 'states';
    protected $fillable = array(
        'state_name',
        'country_id',
    );

}
