<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- META SECTION -->
        <title>AAIMA Admin</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />

        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link rel="icon" href="<?= url('/'); ?>/adminassets/favicon.png" type="image/x-icon" />
        <!-- END META SECTION -->

        <!-- CSS INCLUDE -->
        <link rel="stylesheet" type="text/css" id="theme" href="<?= url('/'); ?>/adminassets/css/theme-default.css"/>
        <link rel="stylesheet" type="text/css" id="theme" href="<?= url('/'); ?>/adminassets/css/custom.css"/>
        <link rel="stylesheet" type="text/css" id="theme" href="<?= url('/'); ?>/adminassets/css/downline.css"/>
        <!-- EOF CSS INCLUDE -->
        <link rel="stylesheet" href="<?= url('/'); ?>/select2/css/select2.min.css" />

        <script type="text/javascript" src="<?= url('/'); ?>/adminassets/js/plugins/jquery/jquery.min.js"></script>
        <script src="<?= url('/'); ?>/adminassets/js/jquery-validate.min.js"></script>
        <script type="text/javascript" src="<?= url('/'); ?>/adminassets/js/plugins/jquery/jquery-ui.min.js"></script>
        <script src="<?= url('/'); ?>/select2/js/select2.min.js"></script>

        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    </head>
    <body>
        <?php
        Route::getCurrentRoute()->getAction();
        $fullroute = Route::currentRouteAction();
        $actionname = Route::currentRouteName();
//        echo $actionname;
//        die;
        ?>
        <div class="page-container">
            <div class="page-sidebar">
                <ul class="x-navigation">
                    <li class="xn-logo">
                        <a href="/admin/dashboard"><img src="<?= url('/'); ?>/adminassets/img/logo.png"/></a>
                        <a href="#" class="x-navigation-control"></a>
                    </li>
                    <li class="xn-title">Navigation</li>
                    <li class="<?php if ($actionname == 'dashboard.index'): ?>active<?php endif; ?>">
                        <a href="/admin/dashboard"><span class="fa fa-desktop"></span> <span class="xn-text">Dashboard</span></a>
                    </li>
                    <li class="xn-openable <?php if ($actionname == 'categories.index' || $actionname == 'categories.edit' || $actionname == 'brands.index' || $actionname == 'brands.edit' || $actionname == 'attributes.index' || $actionname == 'attributes.edit' || $actionname == 'products.index' || $actionname == 'products.create' || $actionname == 'products.edit' || $actionname == 'products.show' || $fullroute == 'App\Http\Controllers\ProductsController@featured_products' || $fullroute == 'App\Http\Controllers\ProductsController@filter_products' || $fullroute == 'App\Http\Controllers\MappingsController@category_attribute_mapping' || $fullroute == 'App\Http\Controllers\MappingsController@category_brand_mapping' || $fullroute == 'App\Http\Controllers\ProductsController@top_products'): ?>active<?php endif; ?>">
                        <a href="#"><span class="fa fa-list"></span> Manage Catalog</a>
                        <ul>
                            <li class="<?php if ($actionname == 'products.create'): ?>active<?php endif; ?>"><a href="/admin/products/create"><span class="fa fa-inbox"></span> Add New Products</a></li>
                            <li class="<?php if ($actionname == 'products.index' || $fullroute == 'App\Http\Controllers\ProductsController@filter_products'): ?>active<?php endif; ?>"><a href="/admin/products"><span class="fa fa-file-text"></span> All Products</a></li>
                            <li class="<?php if ($actionname == 'categories.index' || $actionname == 'categories.edit'): ?>active<?php endif; ?>"><a href="/admin/categories"><span class="fa fa-plus"></span>Manage Categories</a></li>
                            <li class="xn-openable <?php if ($actionname == 'attributes.index' || $actionname == 'attributes.edit'): ?>active<?php endif; ?>">
                                <a href="#"><span class="fa fa-tags"></span> Manage Attributes</a>
                                <ul>
                                    <li><a href="/admin/attributes"><span class="fa fa-angle-double-right"></span> Attributes</a></li>
                                </ul>
                            </li>
                            <li class="xn-openable <?php if ($fullroute == 'App\Http\Controllers\MappingsController@category_attribute_mapping' || $fullroute == 'App\Http\Controllers\MappingsController@category_brand_mapping'): ?>active<?php endif; ?>">
                                <a href=""><span class="fa fa-tags"></span> Manage Mappings</a>
                                <ul>
                                    <li><a href="/admin/category_attribute_mapping"><span class="fa fa-angle-double-right"></span>Category Attributes</a></li>
                                    <li><a href="/admin/category_brand_mapping"><span class="fa fa-angle-double-right"></span>Category Brands</a></li>
                                </ul>
                            </li>
                            <li class="<?php if ($actionname == 'brands.index' || $actionname == 'brands.edit'): ?>active<?php endif; ?>"><a href="/admin/brands"><span class="fa fa-chain"></span> Manufacturer</a></li>

                            <li class="<?php if ($fullroute == 'App\Http\Controllers\ProductsController@top_products'): ?>active<?php endif; ?>"><a href="/admin/top_products"><span class="fa fa-thumbs-up"></span> Top Products</a></li>
                            <li class="<?php if ($fullroute == 'App\Http\Controllers\ProductsController@featured_products'): ?>active<?php endif; ?>"><a href="/admin/featured_products"><span class="fa fa-thumbs-up"></span> Featured Products</a></li>
                        </ul>
                    </li>

                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-users"></span> Customers</a>
                        <ul>
                            <li><a href="/admin/customers"><span class="fa fa-angle-double-right"></span> <span class="xn-text">Customers</span></a></li>
                            <li><a href=""><span class="fa fa-angle-double-right"></span> <span class="xn-text">Customer Groups</span></a></li>
                        </ul>
                    </li>

                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-tags"></span> Coupons</a>
                        <ul>
                            <li><a href=""><span class="fa fa-angle-double-right"></span> <span class="xn-text">Create Coupons</span></a></li>
                            <li><a href=""><span class="fa fa-angle-double-right"></span> <span class="xn-text">All Coupons</span></a></li>
                        </ul>
                    </li>
                    <li class="xn-openable <?php if ($fullroute == 'App\Http\Controllers\OrdersController@all_orders'): ?>active<?php endif; ?>">
                        <a href="#"><span class="fa fa-shopping-cart"></span> Sales</a>
                        <ul>
                            <li>
                                <a href="/admin/all_orders"><span class="fa fa-angle-double-right"></span> Orders</a></li>                            
                        </ul>
                    </li>
                    <li class="xn-openable <?php if ($fullroute == 'App\Http\Controllers\ProductsController@reviews'): ?>active<?php endif; ?>">
                        <a href="/admin/reviews"><span class="fa fa-star"></span> <span class="xn-text">Manage Reviews</span></a>
                    </li>
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-list"></span> Manage CMS Content</a>
                        <ul>
                            <li><a href="/admin/advertisement/"><span class="fa fa-angle-double-right"></span> <span class="xn-text">Add Advertisement</span></a></li>
                            <li><a href="/admin/view_advertisement"><span class="fa fa-angle-double-right"></span> <span class="xn-text">View Advertisement</span></a></li>
                            <li><a href="/admin/achiever/"><span class="fa fa-angle-double-right"></span> <span class="xn-text">Add achiever</span></a></li>
                            <li><a href="/admin/view_achiever"><span class="fa fa-angle-double-right"></span> <span class="xn-text">View Achiever</span></a></li>

                            <li><a href="/admin/sponcered/"><span class="fa fa-angle-double-right"></span> <span class="xn-text">Add Sponcered</span></a></li>
                            <li><a href="/admin/view_sponcered"><span class="fa fa-angle-double-right"></span> <span class="xn-text">View Sponcered</span></a></li>
                            <li><a href="#"><span class="fa fa-angle-double-right"></span> <span class="xn-text">News Headlines</span></a></li>
                            <li><a href="#"><span class="fa fa-angle-double-right"></span> <span class="xn-text">CMS Pages</span></a></li>
                            <li><a href=""><span class="fa fa-angle-double-right"></span> <span class="xn-text">Manage Slider</span></a></li>
                        </ul>
                    </li>
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-list"></span> Manage Blogs</a>
                        <ul>
                            <li><a href="/admin/blog/"><span class="fa fa-angle-double-right"></span> <span class="xn-text">Add Blogs</span></a></li>
                            <li><a href="/admin/view_blog"><span class="fa fa-angle-double-right"></span> <span class="xn-text">View Blogs</span></a></li>
                        </ul>
                    </li>
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-cogs"></span> Manage Sub-Admin</a>
                        <ul>
                            <li><a href=""><span class="fa fa-plus"></span> Create Sub Admin</a></li>
                            <li><a href=""><span class="fa fa-tags"></span> All Sub Admins</a></li>
                        </ul>
                    </li>

                    <li class="xn-openable">
                        <a href=""><span class="fa fa-cogs"></span> Manage Sample-Pages</a>
                        <ul>
                            <li><a href="/admin/samplepages/"><span class="fa fa-plus"></span> Create Sample Page</a></li>
                            <li><a href="/admin/view_sample_pages"><span class="fa fa-tags"></span> All Sample Pages</a></li>
                        </ul>
                    </li>

                    <li class="xn-openable <?php if ($fullroute == 'App\Http\Controllers\TestimonialsController@view' || $fullroute == 'App\Http\Controllers\TestimonialsController@edit' || $fullroute == 'App\Http\Controllers\TestimonialsController@index'): ?>active<?php endif; ?>">
                        <a href=""><span class="fa fa-cogs"></span> Manage Testimonials</a>
                        <ul>
                            <li><a href="/admin/testimonials/"><span class="fa fa-plus"></span> Create Testimonials</a></li>
                            <li><a href="/admin/view_testimonials"><span class="fa fa-tags"></span> View Testimonials</a></li>
                        </ul>
                    </li>
                    <!-- <li class="xn-openable">
                        <a href=""><span class="fa fa-cogs"></span> Manage Advertisement</a>
                        <ul>
                            <li><a href="/admin/advertisement/"><span class="fa fa-plus"></span> Create Advertisement</a></li>
                            <li><a href="/admin/view_advertisement"><span class="fa fa-tags"></span> View Advertisement</a></li>
                            <li><a href="/admin/advert/category"><span class="fa fa-plus"></span> Category</a></li>
                        </ul>
                    </li> -->

                </ul>
                <!-- END X-NAVIGATION -->
            </div>
            <!-- END PAGE SIDEBAR -->
            <!-- PAGE CONTENT -->
            <div class="page-content">

                <!-- START X-NAVIGATION VERTICAL -->
                <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                    <!-- TOGGLE NAVIGATION -->
                    <li class="xn-icon-button">
                        <a href="" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
                    </li>
                    <!-- END TOGGLE NAVIGATION -->
                    <!-- SIGN OUT -->
                    <li class="xn-icon-button pull-right">
                        <a href="javascript:void(0)" id="showlogoutmodal">
                            <span class="fa fa-sign-out"></span>
                        </a>
                    </li>
                    <!-- END SIGN OUT -->
                </ul>
                <!-- END X-NAVIGATION VERTICAL -->
                @yield('content')
            </div>
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->
        <!-- MESSAGE BOX-->
        <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                    <div class="mb-content">
                        <p>Are you sure you want to log out?</p>
                        <p>Press No if you want to continue work. Press Yes to logout current user.</p>
                    </div>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST">
                        @csrf
                        <div class="mb-footer">
                            <div class="pull-right">
                                <button type="submit" class="btn btn-success btn-lg">Yes</button>
                                <button id="closelogoutmodal" class="btn btn-default btn-lg mb-control-close">No</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- END MESSAGE BOX-->
        <script type="text/javascript">
        </script>
        <!-- START SCRIPTS -->
        <!-- START PLUGINS -->
        <script type="text/javascript" src="<?= url('/'); ?>/adminassets/js/plugins/bootstrap/bootstrap.min.js"></script>
        <!-- END PLUGINS -->

        <!-- THIS PAGE PLUGINS -->
        <script type='text/javascript' src='<?= url('/'); ?>/adminassets/js/plugins/icheck/icheck.min.js'></script>
        <script type="text/javascript" src="<?= url('/'); ?>/adminassets/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
        <script type="text/javascript" src="<?= url('/'); ?>/adminassets/js/plugins/scrolltotop/scrolltopcontrol.js"></script>

        <script type="text/javascript" src="<?= url('/'); ?>/adminassets/js/plugins/morris/raphael-min.js"></script>
        <script type="text/javascript" src="<?= url('/'); ?>/adminassets/js/plugins/rickshaw/d3.v3.js"></script>
        <script type="text/javascript" src="<?= url('/'); ?>/adminassets/js/plugins/rickshaw/rickshaw.min.js"></script>

        <script type="text/javascript" src="<?= url('/'); ?>/adminassets/js/plugins/moment.min.js"></script>
        <script type="text/javascript" src="<?= url('/'); ?>/adminassets/js/plugins/daterangepicker/daterangepicker.js"></script>

        <script type="text/javascript" src="<?= url('/'); ?>/adminassets/js/plugins/bootstrap/bootstrap-datepicker.js"></script>
        <script type="text/javascript" src="<?= url('/'); ?>/adminassets/js/plugins/bootstrap/bootstrap-file-input.js"></script>
        <script type="text/javascript" src="<?= url('/'); ?>/adminassets/js/plugins/bootstrap/bootstrap-select.js"></script>
        <script type="text/javascript" src="<?= url('/'); ?>/adminassets/js/plugins/tagsinput/jquery.tagsinput.min.js"></script>

        <script type="text/javascript" src="<?= url('/'); ?>/adminassets/js/plugins/dropzone/dropzone.min.js"></script>
        <script type="text/javascript" src="<?= url('/'); ?>/adminassets/js/plugins/fileinput/fileinput.min.js"></script>
        <script type="text/javascript" src="<?= url('/'); ?>/adminassets/js/plugins/filetree/jqueryFileTree.js"></script>

        <script type="text/javascript" src="<?= url('/'); ?>/adminassets/js/plugins/summernote/summernote.js"></script>
        <script type="text/javascript" src="<?= url('/'); ?>/adminassets/js/plugins/datatables/jquery.dataTables.min.js"></script>
        <!-- END THIS PAGE PLUGINS -->
        <!-- START TEMPLATE -->
        <script type="text/javascript" src="<?= url('/'); ?>/adminassets/js/plugins.js"></script>
        <script type="text/javascript" src="<?= url('/'); ?>/adminassets/js/actions.js"></script>
        <script type="text/javascript" src="<?= url('/'); ?>/adminassets/js/demo_dashboard.js"></script>
        <script type="text/javascript" src="<?= url('/'); ?>/adminassets/js/products.js"></script>
        <script type="text/javascript" src="<?= url('/'); ?>/adminassets/js/customers-fields.js"></script>
        <script type="text/javascript" src="<?= url('/'); ?>/adminassets/js/custom.js"></script>
        <!-- END TEMPLATE -->
        <!-- END SCRIPTS -->

        <!---coupon code-add products-->
        <script language="javascript">
$("#showlogoutmodal").on('click', function () {
    $("#mb-signout").modal('show');
});
$("#closelogoutmodal").on('click', function () {
    $("#mb-signout").modal('hide');
});

function readProUrl(input, image_no) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $(".appendImage" + image_no)
                    .attr('src', e.target.result)
                    .width(75)
                    .height(50);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('.appendImage')
                    .attr('src', e.target.result)
                    .width(100);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

$("#categoryForm").validate();
$("#brandForm").validate();
$("#attributeForm").validate();
$("#catAttrForm").validate();
$("#catBrandForm").validate();
//$("#productForm").validate();

$('.multiselect').select2();

$('.characterOnly').keypress(function (e) {
    var regex = new RegExp(/^[a-zA-Z\s]+$/);
    var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
    if (regex.test(str)) {
        return true;
    } else {
        e.preventDefault();
        return false;
    }
});
/*Below code for prevent to write character*/
$('.floatOnly').keypress(function (event) {
    if (event.which != 46 && (event.which < 47 || event.which > 59))
    {
        event.preventDefault();
        if ((event.which == 46) && ($(this).indexOf('.') != -1)) {
            event.preventDefault();
        }
    }
});
$(".numericOnly").keypress(function (e) {
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        return false;
    }
});
$(document.body).on('keypress', function (e) {
    if (e.key === ";") { // disallow semicolon
        return false;
    }
});
/*Below code prevent the percentage till 100*/
$('.percentage').on('keyup keydown', function (e) {
    if ($(this).val() > 100
            && e.keyCode != 46
            && e.keyCode != 8
            ) {
        e.preventDefault();
        $(this).val(100);
    }
});

//delete data by primary key
$(".deleterow").on('click', function () {
    var delete_id = $(this).attr("delete_id");
    var table = $(this).attr("table");
    if (confirm("Are you sure, you want to delete this ?"))
        $.ajax({
            url: '/admin/delete',
            type: 'post',
            dataType: 'json',
            data: {delete_id: delete_id, table: table},
            cache: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
        }).done(function (response) {
            if (response.status == '1') {
                window.location.reload();
            }
        });
});

$('.coupon-products').each(function () {
    var default_value = this.value;
    $(this).focus(function () {
        if (this.value == default_value) {
            this.value = '';
        }
    });
    $(this).blur(function () {
        if (this.value == '') {
            this.value = default_value;
        }
    });
});
function coupon_products_append() {
    $('#coupon-products-list ul').append('<li class="list-group-item">' + $("#coupon-products").val() + ' <i class="remove-prod fa fa-times"></i></li>');
}
;

jQuery("#coupon-products-list ul").on("click", ".remove-prod", function (event) {
    $(this).closest("li").remove();
});


$('.coupon-categories').each(function () {
    var default_value = this.value;
    $(this).focus(function () {
        if (this.value == default_value) {
            this.value = '';
        }
    });
    $(this).blur(function () {
        if (this.value == '') {
            this.value = default_value;
        }
    });
});
function coupon_categories_append() {
    $('#coupon-categories-list ul').append('<li class="list-group-item">' + $("#coupon-categories").val() + ' <i class="remove-prod fa fa-times"></i></li>');
}
;

jQuery("#coupon-categories-list ul").on("click", ".remove-prod", function (event) {
    $(this).closest("li").remove();
});

        </script>
        <!---//coupon code-add products-->
        <script>
            $(".date").datepicker();

            var dateToday = new Date();
            var dates = $("#payoutdatepicker").datepicker({
                changeMonth: true,
                minDate: dateToday
            });
            ;
        </script>


    </body>
</html>