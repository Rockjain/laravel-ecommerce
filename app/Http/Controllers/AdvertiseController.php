<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;




class AdvertiseController extends Controller
{
    //
    public function index() {
        return view('advertisement.index');
    }

     public function view() {
        $advertisement = DB::table('advertisements')->select('*')->get();

        return view('advertisement.view', ['advertisement' => $advertisement]);
    }

    public function parent_cat($parent_id) {
        $parent = DB::table('adv_category')->select('*')->where('parent',$parent_id)->get();
        return $parent;
    }

    public function insert(Request $request) {
        extract($_POST);
        if ($form_type == 'add') {
            $path = "/images/testimonialsimages/";
            $check = $this->uploadFile($request, 'image', $path);
            if ($check):
                $nameArray = explode('.', $check);
                $ext = end($nameArray);

                $insertParam['file_path'] = $path;
                $insertParam['file_name'] = $check;
                $insertParam['file_type'] = $ext;

                $getImgData = $this->insertFile($insertParam);

                $adArr['image'] = $getImgData['id'];

            endif;
            $data = array('name' => $name, 'description' => $description, 'status' => $status, 'image' => $check);
            $data = DB::table('advertisements')->insert($data);
            echo"$data";
        }else if ($form_type == 'update') {
            $path = "/images/testimonialsimages/";
            $check = $this->uploadFile($request, 'image', $path);
            if ($check) {
                $nameArray = explode('.', $check);
                $ext = end($nameArray);

                $insertParam['file_path'] = $path;
                $insertParam['file_name'] = $check;
                $insertParam['file_type'] = $ext;

                $getImgData = $this->insertFile($insertParam);

                $adArr['image'] = $getImgData['id'];
            } else {
                $check = $image_update;
            }


            $data = DB::table('advertisements')->where('id', $id)->update([
                'name' => $name, 'description' => $description, 'status' => $status, 'image' => $check
            ]);
            if ($data) {
                echo 'updated';
            } else {
                echo 'updated';
            }
        }
    }

    public function edit(Request $request) {
        $id = $request->id;
        $advertisements = DB::table('advertisements')->select('*')->where('id', $id)->get();
        return view('advertisement.index', ['advertisements' => $advertisements]);
    }

    public function delete(Request $request) {
        $id = $request->id;
        $status = DB::table('blogs')->delete($id);
        if ($status) {
            return redirect()->action('AdvertiseController@view');
        }
    }

    
}
