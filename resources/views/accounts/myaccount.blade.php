@extends('layouts.account')
@section('content')
<div class="container mtb_30">

    <div class="row">
        <div class="col-sm-4 col-md-3" id="column-left">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="panel panel-default account-sidebar">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <?php
                                    $session = Session::get('user_data');
                                    if (!empty($session)):
                                        $customerInfo = getCustomer($session['customer_id']);
                                        if ($customerInfo):
                                            $fileInfo = getFile($customerInfo->photo);
                                            if ($fileInfo) {
                                                $photo = $fileInfo->file_path . $fileInfo->file_name;
                                            } else {
                                                $photo = url('/') . '/frontassets/images/no_img.png';
                                            }
                                        endif;
                                    endif;
                                    ?>
                                    <div class="pull-left"><p><img src="<?= $photo; ?>" alt="user" /></p></div>
                                    <div class="pull-left"><p>Welcome<br/> <strong><?= $customerInfo->firstname . ' ' . $customerInfo->lastname; ?></strong></p>
                                        <p>ID: <strong><?= 'AAIMA' . $customerInfo->id; ?></strong></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body account-sidebar-menu">
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                         <ul class="list-group">
                                        <li class="list-group-item"><a href="/myaccount" class="active"><i class="fa fa-user"></i> My Profile <span class="badge"><i class="fa fa-angle-right"></i></span></a></li>
                                        <li class="list-group-item"><a href="/myaddress"><i class="fa fa-map-marker"></i> Manage Addresses <span class="badge"><i class="fa fa-angle-right"></i></span></a></li>
                                     <!--    <li class="list-group-item"><a href="/myreward"><i class="fa fa-trophy"></i> My Rewards <span class="badge"><i class="fa fa-angle-right"></i></span></a></li> -->
                                        <li class="list-group-item"><a href="/mybusinessassociate"><i class="fa fa-sitemap"></i> My Business Associate <span class="badge"><i class="fa fa-angle-right"></i></span></a></li>
                                        <li class="list-group-item"><a href="/mywishlist"><i class="fa fa-heart"></i> My Wishlist <span class="badge"><i class="fa fa-angle-right"></i></span></a></li>
                                        <li class="list-group-item"><a href="/myorder" ><i class="fa fa-list"></i> My Orders <span class="badge"><i class="fa fa-angle-right"></i></span></a></li>
                                    <!--     <li class="list-group-item"><a href="/myreturn"><i class="fa fa-retweet"></i> My Returns <span class="badge"><i class="fa fa-angle-right"></i></span></a></li> -->
                                        <li class="list-group-item"><a href="/logoutuser" onclick="return confirm('Are you sure, you want to logout ?')" class="logout"><i class="fa fa-power-off"></i> Logout</a></li>
                                    </ul> 
                      
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="col-sm-8 col-md-9" id="column-right">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="panel panel-default account-content">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <div class="pull-left"><h3>My Profile</h3></div>
                                    <div class="pull-right"><p><button type="button" class="edit-button" id="edit-profile" onclick="undisableTxt()" data-toggle="tooltip" title="edit profile"><i class="fa fa-edit"></i> Edit</button></p></div>
                                </div>
                            </div>
                        </div>
                        @include('partials.errors')
                        @include('partials.success')
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-12 col-md-6">
                                    <form action="/update_profile/{{$customerInfo->id}}" id="profile-form" method="post" enctype="multipart/form-data" class="form-horizontal">
                                        {{csrf_field()}}
                                        <div class="row">
                                            <div class="form-group required col-sm-12">
                                                <label class="col-sm-4 col-md-4 control-label" for="input-firstname">Profile Pic</label>
                                                <div class="col-sm-4 col-md-4">
                                                    <input type="file" name="photo" style="width: 200px;">
                                                </div>
                                                <div class="col-sm-4 col-md-4">
                                                    <?php
                                                    $fileInfo = getFile($customerInfo->photo);
                                                    if ($fileInfo) {
                                                        $image = $fileInfo->file_path . $fileInfo->file_name;
                                                    } else {
                                                        $image = url('/') . '/frontassets/images/no_img.png';
                                                    }
                                                    ?>
                                                   <img src="<?= $image; ?>"  style="float:right;width:50px; height:50px; border-radius: 50%; border: 2px solid #ff0000">
                                                  
                                                </div>
                                            </div>
                                            <div class="form-group required col-sm-12">
                                                <label class="col-sm-4 control-label" for="input-firstname">First Name</label>
                                                <div class="col-sm-8">
                                                    <input type="text" name="firstname" placeholder="First Name" id="input-firstname" class="form-control" value="{{$customerInfo->firstname}}" maxlength="25" required disabled>
                                                </div>
                                            </div>
                                            <div class="form-group required col-sm-12">
                                                <label class="col-sm-4 control-label" for="input-lastname">Last Name</label>
                                                <div class="col-sm-8">
                                                    <input type="text" name="lastname" placeholder="Last Name" id="input-lastname" class="form-control" value="{{$customerInfo->lastname}}" maxlength="25" disabled>
                                                </div>
                                            </div>
                                            <div class="form-group col-sm-12">
                                                <label class="col-sm-4 control-label" for="input-email"> &nbsp;E-Mail</label>
                                                <div class="col-sm-8">
                                                    <input type="text" name="email"  placeholder="E-Mail" id="input-email" class="form-control" value="{{$customerInfo->email}}" maxlength="80" required disabled>
                                                </div>
                                            </div>
                                            <div class="form-group required col-sm-12">
                                                <label class="col-sm-4 control-label" for="input-telephone">Mobile</label>
                                                <div class="col-sm-8">
                                                    <input type="tel" name="telephone" placeholder="Mobile" id="input-telephone" class="form-control telephone" value="{{$customerInfo->telephone}}" maxlength="15" required disabled>
                                                </div>
                                            </div>
                                            <div class="form-group required col-sm-12">
                                                <label class="col-sm-4 control-label" for="input-username">Username</label>
                                                <div class="col-sm-8">
                                                    <input type="text" placeholder="Username" id="input-username" class="form-control" value="{{$customerInfo->username}}" disabled>
                                                </div>
                                            </div>

                                            <div class="form-group required col-sm-12">
                                                <label class="col-sm-4 control-label" for="input-username">Aadhar Card</label>
                                                <div class="col-sm-8">
                                                    <input type="text" name="aadharcard" placeholder="Aadhar card" id="input-aadharcard" class="form-control" value="{{$customerInfo->aadhar_card}}" disabled>
                                                </div>
                                            </div>

                                            <div class="form-group required col-sm-12">
                                                <label class="col-sm-4 control-label" for="input-username">Bank Account Number</label>
                                                <div class="col-sm-8">
                                                    <input type="text" name="bankAccount" placeholder="Bank Account" id="input-bankAccount" class="form-control" value="{{$customerInfo->bank_account}}" disabled>
                                                </div>
                                            </div>

                                            <div class="form-group required col-sm-12">
                                                <label class="col-sm-4 control-label" for="input-username">IFSC Code</label>
                                                <div class="col-sm-8">
                                                    <input type="text" name="IfscCode" placeholder="IFSC Code" id="input-IfscCode" class="form-control" value="{{$customerInfo->ifsc_code}}" disabled>
                                                </div>
                                            </div>

                                            <div class="form-group required col-sm-12">
                                                <label class="col-sm-4 control-label" for="input-username">Pan Card</label>
                                                <div class="col-sm-8">
                                                    <input type="text" name="panCard" placeholder="Pan Card" id="input-panCard" class="form-control" value="{{$customerInfo->pan_card}}" disabled>
                                                </div>
                                            </div>

                                            <div class="form-group required col-sm-12">
                                                <label class="col-sm-4 control-label" for="input-username">Nominee Name</label>
                                                <div class="col-sm-8">
                                                    <input type="text" name="nomineeName" placeholder="Nominee Name" id="input-nomineeName" class="form-control" value="{{$customerInfo->nominee_name}}" disabled>
                                                </div>
                                            </div>

                                            <div class="form-group required col-sm-12">
                                                <label class="col-sm-4 control-label" for="input-username">Relation of Nominee</label>
                                                <div class="col-sm-8">
                                                    <input type="text" name="relationOfNominee" placeholder="Relation of Nominee" id="input-relationOfNominee" class="form-control" value="{{$customerInfo->nominee_relation}}" disabled>
                                                </div>
                                            </div>

                                            <div class="form-group required col-sm-12">
                                                <label class="col-sm-4 control-label" for="input-username">Aadhar Card of Nominee</label>
                                                <div class="col-sm-8">
                                                    <input type="text" name="aadharNominee" placeholder="Aadhar Card of Nominee" id="input-aadharNominee" class="form-control" value="{{$customerInfo->aadhar_nominee}}" disabled>
                                                </div>
                                            </div>

                                            <div class="buttons col-sm-6">
                                                <div class="pull-right"><input type="button" id="cancelbutton" value="Cancel" class="btn btn-default" onclick="disableTxt()" >
                                                </div>
                                            </div>
                                            <div class="buttons col-sm-6">
                                                <div class="pull-right"><input type="submit" value="Update" class="btn btn-blue" id="updateprofile">
                                                </div>
                                            </div>

                                            <div class="form-group required col-sm-12">
                                                <label class="col-sm-4 control-label">Newsletter</label>
                                                <div class="col-sm-8">
                                                    <label class="radio-inline">
                                                        <input type="radio" name="newsletter" value="1" checked="checked">
                                                        Yes</label>
                                                    <label class="radio-inline">
                                                        <input type="radio" name="newsletter" value="0">
                                                        No</label>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection