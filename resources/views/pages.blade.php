@extends('layouts.default')
@section('content')
	<div class="container mb_30">
      
	   <div class="row white-bg">
      <?php 
      // echo "<pre>";
      // print_r($pages);
      // die;
      ?>
          
            <div class="col-md-12 pb_30">
              <div class="about-text">
                <div class="about-heading-wrap">
                  <h2 class="about-heading mb_20 mt_40 ptb_10"><?php echo $pages->title;?></h2>
                </div>
                <p><?php echo $pages->description;?></p>
               
              </div>
            </div>
          </div>
		  
    </div>
    @endsection