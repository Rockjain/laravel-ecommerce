<meta name="csrf-token" content="{{ csrf_token() }}">
@extends('layouts.admin')
@section('content')
<div class="page-content-wrap">
    @include('partials.errors')
    @include('partials.success')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">                                
                    <h3 class="panel-title">Product Detail</h3>                
                </div>
                <div class="panel-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th width="20%">ID</th>
                                <td>{{$product->id}}</td>                          
                            </tr>
                            <tr>
                                <th width="20%">Name</th>
                                <td>{{$product->name}}</td>                          
                            </tr>
                            <tr>
                                <th width="20%">Price</th>
                                <td>{{$product->price}}</td>                          
                            </tr>
                          
                            <tr>
                                <th width="20%">Inventary</th>
                                <td>{{$product->quantity}}</td>                          
                            </tr>
                            <tr>
                                <th width="20%">Image</th>
                                <td><img src="{{!empty($product->fileDetail)?$product->fileDetail->file_path.$product->fileDetail->file_name:url('/').'/frontassets/images/no_img.png'}}" width="75" height="50"></td>                          
                            </tr>
                            <tr>
                                <th width="20%">More Images</th>
                                <td>
                                    @if($product->productImages)
                                    @foreach($product->productImages as $image)
                                    <img src="{{!empty($image->fileDetail)?$image->fileDetail->file_path.$image->fileDetail->file_name:url('/').'/frontassets/images/no_img.png'}}" width="75" height="50">
                                    @endforeach
                                    @endif
                                </td>                          
                            </tr>
                            <tr>
                                <th width="20%">Category</th>
                                <td>{{!empty($product->categoryDetail)?$product->categoryDetail->name:'N/A'}}</td>                          
                            </tr>
                            <tr>
                                <th width="20%">Brand</th>
                                <td>{{!empty($product->brandDetail)?$product->brandDetail->title:'N/A'}}</td>                          
                            </tr>
                            <tr>
                                <th width="20%">Sku Code</th>
                                <td>{{$product->sku_code}}</td>                          
                            </tr>
                            <tr>
                                <th width="20%">Hsn Code</th>
                                <td>{{$product->hsn_code!=''?$product->hsn_code:'N/A'}}</td>                          
                            </tr>
                            <tr>
                                <th width="20%">Other Code</th>
                                <td>{{$product->other_code!=''?$product->other_code:'N/A'}}</td>                          
                            </tr>
                            <tr>
                                <th width="20%">Short Description</th>
                                <td><?= $product->short_desc; ?></td>                          
                            </tr>
                            <tr>
                                <th width="20%">Description</th>
                                <td><?= $product->description; ?></td>                          
                            </tr>
                            <tr>
                                <th width="20%">Discount</th>
                                <td>
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th width="20%">Tax</th>
                                                <td>: {{$product->tax}}</td>                          
                                            </tr>
                                            <tr>
                                                <td width="20%">Discount Percent</td>
                                                <td>: {{$product->discount_percent}}</td>
                                            </tr>
                                            <tr>
                                                <td width="20%">Discount Quantity</td>
                                                <td>: {{$product->discount_quantity}}</td>
                                            </tr>
                                            <tr>
                                                <td width="20%">Start Date</td>
                                                <td>: {{date('d-m-Y',strtotime($product->discount_start_date))}}</td>
                                            </tr>
                                            <tr>
                                                <td width="20%">End Date</td>
                                                <td>: {{date('d-m-Y',strtotime($product->discount_end_date))}}</td>
                                            </tr>
                                        </thead>                                    
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <th width="20%">Attributes</th>
                                <td>
                                    <table class="table">
                                        <thead>
                                            @if($product->productAttributes)
                                            @foreach($product->productAttributes as $attribute)
                                            <tr>
                                                <td width="20%">{{$attribute->attributeDetail->name}}</td>
                                                <td>: <?php
                                                    if ($attribute->attributeDetail->type == 'SELECT'):
                                                        echo $attribute->attributeValue->attribute_value;
                                                    else:
                                                        echo $attribute->attribute_value;
                                                    endif;
                                                    ?>
                                                </td>
                                            </tr>
                                            @endforeach
                                            @endif
                                        </thead>                                    
                                    </table>
                                </td>
                            </tr>
                        </thead>                        
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
