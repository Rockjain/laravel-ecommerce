<meta name="csrf-token" content="{{ csrf_token() }}">
@extends('layouts.admin')
@section('content')
<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Add New Attribute</h3>
                </div>
                @include('partials.errors')
                @include('partials.success')
                <div class="panel-body">
                    <form method="post" id="attributeForm" class="form-horizontal" action="{{route('attributes.store')}}" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Attribute Name</label>
                            <div class="col-md-6 col-xs-12">  
                                <input type="text" name="name" class="form-control" placeholder="Attribute Name" required/>                                       
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Type</label>
                            <div class="col-md-6 col-xs-12">                                                                                
                                <select class="form-control" onchange="appendValueDiv(this)" name="type" required />
                                <option value="" disabled selected>select type</option>
                                <option value="SELECT">SELECT</option>
                                <option value="INPUT">INPUT</option>
                                <option value="TOGGLE">TOGGLE</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12 toggleDiv appendDiv form-row" style="display: none">
                            <div class="row">
                                <label class="col-md-3 col-xs-12 control-label">Attribute Value</label>
                                <div class="col-md-6 col-xs-12">  
                                    <input type="text" name="attribute_values[]" class="form-control" placeholder="Attribute Value" required/>                                       
                                </div>
                                <a style="cursor: pointer" onclick="appendMoreValue(this)"><i class="fa fa-plus"></i></a>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12 col-xs-12" style="margin-top: 10px;">  
                                <p class="text-center">
                                    <input type="submit" class="btn btn-info" value="Submit"/> 
                                </p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <!-- START Category DATATABLE -->
            <div class="panel panel-default">
                <div class="panel-heading">                                
                    <h3 class="panel-title">All Attributes</h3>                         
                </div>
                <div class="panel-body">
                    <table class="table datatable">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Type</th>
                                <th>Values</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($attributes)
                            @foreach($attributes as $key=>$attribute)
                            <tr>
                                <td>{{$attribute->name}}</td>
                                <td>{{$attribute->type}}</td>
                                <td>
                                    @if($attribute->attributeValues)                                    
                                    <ul>
                                        @foreach($attribute->attributeValues as $key=>$value)
                                        <li>{{$value->attribute_value}}</li>
                                        @endforeach
                                    </ul>
                                    @endif
                                </td>
                                <td>
                                    <a href="/admin/attributes/{{$attribute->id}}/edit"><i class="fa fa-edit"></i></a>
                                    <label class="switch" style="top: 12px">
                                        <input type="checkbox" <?php
                                        if ($attribute->status == '1') {
                                            echo "checked";
                                        }
                                        ?> id="{{$attribute->id}}" onchange="checkStatus(this);" id="switch-{{$key}}}">
                                        <span class="slider round"></span>
                                    </label>
                                </td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END Category DATATABLE -->
        </div>
    </div>
</div>
<script>
    function checkStatus(obj) {
        var id = $(obj).attr("id");
        var checked = $(obj).is(':checked');
        if (checked == true) {
            var status = 1;
        } else {
            var status = 0;
        }
        $.ajax({
            url: '/admin/change_status',
            type: 'post',
            dataType: 'json',
            data: {method: 'changeAttributeStatus', status: status, id: id},
            cache: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
        }).done(function (response) {
            if (response.status == '1') {
 Command: toastr["success"]("Status updated successfully")

                        toastr.options = {
                            "closeButton": false,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": false,
                            "positionClass": "toast-top-right",
                            "preventDuplicates": false,
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "timeOut": "5000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        }
            } else {
                Command: toastr["error"]("Some error found, please try again")

                        toastr.options = {
                            "closeButton": false,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": false,
                            "positionClass": "toast-top-right",
                            "preventDuplicates": false,
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "timeOut": "5000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        }
            }
        });
    }

    function appendValueDiv(objRef) {
        var obj = $(objRef);
        var type = obj.find('option:selected').val();
        if (type == 'SELECT') {
            $(".toggleDiv").slideDown();
        } else {
            $(".toggleDiv").slideUp();
        }
    }

    function appendMoreValue(objRef) {
        var obj = $(objRef);
        var string = '<div class="row">'
                + '<label class="col-md-3 col-xs-12 control-label">Attribute Value</label>'
                + '<div class="col-md-6 col-xs-12">'
                + '<input type="text" name="attribute_values[]" class="form-control" placeholder="Attribute Value" required/>'
                + '</div>'
                + '<a style="cursor: pointer" onclick="removeDiv(this)"><i class="fa fa-trash-o"></i></a>'
                + '</div>';
        obj.parent().parent('.appendDiv').append(string);
    }
    function removeDiv(objRef) {
        var obj = $(objRef);
        obj.parent().remove();
    }
</script>
@endsection