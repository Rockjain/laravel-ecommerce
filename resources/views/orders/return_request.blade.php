@extends('layouts.account')
@section('content')
<div class="container mtb_30">
    <div class="row">
        <div class="col-sm-4 col-md-3" id="column-left">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="panel panel-default account-sidebar">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <?php
                                    $session = Session::get('user_data');
                                    if (!empty($session)):
                                        $customerInfo = getCustomer($session['customer_id']);
                                        if ($customerInfo):
                                            $fileInfo = getFile($customerInfo->photo);
                                            if ($fileInfo) {
                                                $photo = $fileInfo->file_path . $fileInfo->file_name;
                                            } else {
                                                $photo = url('/') . '/frontassets/images/no_img.png';
                                            }
                                        endif;
                                    endif;
                                    ?>
                                    <div class="pull-left"><p><img src="<?= $photo; ?>" alt="user" /></p></div>
                                    <div class="pull-left"><p>Welcome<br/> <strong><?= $customerInfo->firstname . ' ' . $customerInfo->lastname; ?></strong></p>
                                        <p>ID: <strong><?= 'AAIMA' . $customerInfo->id; ?></strong></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body account-sidebar-menu">
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <ul class="list-group">
                                        <li class="list-group-item"><a href="/myaccount"><i class="fa fa-user"></i> My Profile <span class="badge"><i class="fa fa-angle-right"></i></span></a></li>
                                        <li class="list-group-item"><a href="/myaddress"><i class="fa fa-map-marker"></i> Manage Addresses <span class="badge"><i class="fa fa-angle-right"></i></span></a></li>
                                        <li class="list-group-item"><a href="/myreward"><i class="fa fa-trophy"></i> My Rewards <span class="badge"><i class="fa fa-angle-right"></i></span></a></li>
                                        <li class="list-group-item"><a href="/mydownline"><i class="fa fa-sitemap"></i> My Downline <span class="badge"><i class="fa fa-angle-right"></i></span></a></li>
                                        <li class="list-group-item"><a href="/mywishlist"><i class="fa fa-heart"></i> My Wishlist <span class="badge"><i class="fa fa-angle-right"></i></span></a></li>
                                        <li class="list-group-item"><a href="/myorder" class="active"><i class="fa fa-list"></i> My Orders <span class="badge"><i class="fa fa-angle-right"></i></span></a></li>
                                        <li class="list-group-item"><a href="/myreturn"><i class="fa fa-retweet"></i> My Returns <span class="badge"><i class="fa fa-angle-right"></i></span></a></li>
                                        <li class="list-group-item"><a href="/logoutuser" class="logout"><i class="fa fa-power-off"></i> Logout</a></li>
                                    </ul> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-8 col-md-9" id="column-right">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="panel panel-default account-content">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <h3>Return Request</h3>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="row" id="">
                                <div class="col-sm-12 col-md-12">
                                    <form action="" id="" method="post" enctype="multipart/form-data" class="form-horizontal">
                                        <div class="row">        
                                            <div class="form-group col-sm-6">
                                                <label class="col-sm-4 control-label" for="input-firstname">First Name</label>
                                                <div class="col-sm-8">
                                                    <input type="text" name="firstname" value="Balram" placeholder="First Name" id="input-firstname" class="form-control" maxlength="25">
                                                </div>
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label class="col-sm-4 control-label" for="input-lastname">Last Name</label>
                                                <div class="col-sm-8">
                                                    <input type="text" name="lastname" value="Agarwal" placeholder="Last Name" id="input-lastname" class="form-control" maxlength="25">
                                                </div>
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label class="col-sm-4 control-label" for="input-email">E-Mail</label>
                                                <div class="col-sm-8">
                                                    <input type="text" name="email" value="balramagarwal07@gmail.com" placeholder="E-Mail" id="input-email" class="form-control" maxlength="80">
                                                </div>
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label class="col-sm-4 control-label" for="input-telephone">Mobile</label>
                                                <div class="col-sm-8">
                                                    <input type="tel" name="telephone" value="9313484654" placeholder="Mobile" id="input-telephone" class="form-control telephone" maxlength="15">
                                                </div>
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label class="col-sm-4 control-label" for="input-orderid">Order ID</label>
                                                <div class="col-sm-8">
                                                    <input type="text" name="orderid" value="123" placeholder="Order ID" id="input-orderid" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label class="col-sm-4 control-label" for="input-orderdate">Order Date</label>
                                                <div class="col-sm-8">
                                                    <input type="tel" name="orderdate" value="" placeholder="Order Date" id="input-orderdate" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label class="col-sm-4 control-label" for="input-productname">Product Name</label>
                                                <div class="col-sm-8">
                                                    <input type="text" name="productname" value="test product" placeholder="Product Name" id="input-productname" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label class="col-sm-4 control-label" for="input-productcode">Product Code</label>
                                                <div class="col-sm-8">
                                                    <input type="text" name="productcode" value="test345" placeholder="Product Code" id="input-productcode" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label class="col-sm-4 control-label" for="input-quantity">Quantity</label>
                                                <div class="col-sm-8">
                                                    <input type="number" name="quantity" value="1" placeholder="" id="input-quantity" class="form-control" min="1">
                                                </div>
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label class="col-sm-4 control-label" for="productopened">Product is opened</label>
                                                <div class="col-sm-8">
                                                    <div class="radio">
                                                        <label><input type="radio" name="productopened" value="1">Yes</label>
                                                    </div>
                                                    <div class="radio">
                                                        <label><input type="radio" name="productopened" value="2" checked="">No</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-sm-8">
                                                <label class="col-sm-4 control-label" for="input-reason">Reason for Return</label>
                                                <div class="col-sm-8">
                                                    <div class="radio">
                                                        <label><input type="radio" name="returnreason" value="1">Dead On Arrival</label>
                                                    </div>
                                                    <div class="radio">
                                                        <label><input type="radio" name="returnreason" value="2">Faulty, please supply details</label>
                                                    </div>
                                                    <div class="radio">
                                                        <label><input type="radio" name="returnreason" value="3">Order Error</label>
                                                    </div>
                                                    <div class="radio">
                                                        <label><input type="radio" name="returnreason" value="4">Other, please supply details</label>
                                                    </div>
                                                    <div class="radio">
                                                        <label><input type="radio" name="returnreason" value="5">Received Wrong Item</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-sm-12">
                                                <label class="col-sm-4 col-md-3 control-label" for="input-otherdetails">Faulty or other details</label>
                                                <div class="col-sm-8 col-md-9">
                                                    <textarea name="otherdetails" value="" placeholder="Faulty or other details" id="input-otherdetails" class="form-control"></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group col-sm-12">
                                                <div class="buttons">
                                                    <div class="pull-right col-sm-6">I have read and agree to the <a href="#" target="_blank"><b>Privacy Policy</b></a>                        <input type="checkbox" name="agree" value="1">
                                                        &nbsp;
                                                        <input type="submit" value="Register" class="btn register-button">
                                                    </div>
                                                    <div class="pull-left col-sm-6"><a href="/myorder" class="btn btn-default button"><i class="fa fa-arrow-left"></i> Back To Orders</a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection