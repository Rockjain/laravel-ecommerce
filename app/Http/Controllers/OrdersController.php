<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Model\Order;
use App\Model\Cart;
use App\Model\Address;
use App\Model\OrderItem;
use App\Model\OrderHistory;
use App\Model\State;
use App\Model\Country;
use App\Model\Product;
use App\Model\Orderaddress;
use Illuminate\Http\Request;

class OrdersController extends Controller {
    /* Function for Checkout Page */

    public function checkout(Request $request) {
        $user_data = $request->session()->get('user_data');
        if (!$user_data) {
            return redirect('/loginuser')->with('error', 'Please login first');
        }
        $states = State::where(['country_id' => '1'])->get();
        $countries = Country::get();

        $addressInfo = Address::where(['customer_id' => $user_data['customer_id']])->get();

        $usercart = Cart::where(['customer_id' => $user_data['customer_id']])->get();
        if ($usercart):
            $cartArr = [];
            $s_total = '0';
            foreach ($usercart as $key => $row):
                $productInfo = \App\Model\Product::where(['id' => $row->product_id])->first();
                $cartArr[$key] = $row;
                $price=$productInfo->price - (($productInfo->discount_percent * $productInfo->price) / 100);
                $s_total = $s_total + ($row->quantity * $price);
                //$deliverycharge + =$row->delivery_charge;
                
            endforeach;
        endif;

        return view('accounts/checkout', ['addressInfo' => $addressInfo, 'states' => $states, 'countries' => $countries, 'usercart' => $cartArr, 's_total' => $s_total != '' ? $s_total : '0']);
    }

    /* Function to place new order */

    public function store(Request $request) {
        $user_data = $request->session()->get('user_data');

        $orderArr = [
            'customer_id' => $user_data['customer_id'],
            'order_no' => 'ORD' . rand(00000, 99999),
            'payment_mode' => $request->input('payment_mode'),
            'grand_total' => $request->input('grand_total'),
            'status' => '1'
        ];

        $return = Order::create($orderArr);
        if ($return) {
            //start saving address
            if ($request->input('address_type') == '2') {  //for new address
                $newAdd = [
                    'order_id' => $return->id,
                    'customer_id' => $user_data['customer_id'],
                    'firstname' => $request->input('firstname'),
                    'lastname' => $request->input('lastname'),
                    'address_1' => $request->input('address_1'),
                    'address_2' => $request->input('address_2'),
                    'city' => $request->input('city'),
                    'zone_id' => $request->input('zone_id'),
                    'country_id' => $request->input('country_id'),
                    'postcode' => $request->input('postcode'),
                ];
                OrderAddress::create($newAdd);
            } else {
                $address = Address::find($request->input('address_id'));
                if ($address):
                    $existAdd = [
                        'order_id' => $return->id,
                        'customer_id' => $user_data['customer_id'],
                        'firstname' => $address->firstname,
                        'lastname' => $address->lastname,
                        'address_1' => $address->address_1,
                        'address_2' => $address->address_2,
                        'city' => $address->city,
                        'postcode' => $address->postcode,
                        'country_id' => $address->country_id,
                        'zone_id' => $address->zone_id
                    ];
                    OrderAddress::create($existAdd);
                endif;
            }
            //end saving address

            $cartData = Cart::where(['customer_id' => $user_data['customer_id']])->get();
            if ($cartData):
                foreach ($cartData as $cart):
                    $productInfo = getProduct($cart->product_id);
                    if ($productInfo) {
                        $total_amount = $productInfo->price * $cart->quantity;
                    } else {
                        $total_amount = '0';
                    }
                    $itemsArr = [
                        'order_id' => $return->id,
                        'customer_id' => $user_data['customer_id'],
                        'product_id' => $cart->product_id,
                        'sku_code' => $productInfo->sku_code,
                        'hsn_code' => $productInfo->hsn_code,
                        'other_code' => $productInfo->other_code,
                        'name' => $productInfo->name,
                        'image' => $productInfo->image,
                        'price' => $productInfo->price,
                        'delivery_charge' => $productInfo->delivery_charge,
                        'tax' => $productInfo->tax,
                        'discount_percent' => $productInfo->discount_percent,
                        'discount_quantity' => $productInfo->discount_quantity,
                        'discount_start_date' => $productInfo->discount_start_date,
                        'discount_end_date' => $productInfo->discount_end_date,
                        'description' => $productInfo->description,
                        'category_id' => $productInfo->category_id,
                        'brand_id' => $productInfo->brand_id,
                        'quantity' => $cart->quantity,
                        'unit_price' => $productInfo->price,
                        'total_amount' => $total_amount,
                        'status' => '1'
                    ];
                    OrderItem::create($itemsArr);
                    $quantity = $productInfo->quantity - $cart->quantity;

                    $sold_count = $productInfo->sold_count + 1;
                    Product::where('id', $cart->product_id)->update(['sold_count' => $sold_count, 'quantity' => $quantity]);
                endforeach;
            endif;
            //delete cart data after order successfull placed
            Cart::where(['customer_id' => $user_data['customer_id']])->delete();

            //order info after placed
            $order = Order::find($return->id);

            //send order placed message
            $orderurl = url('/') . '/myorder';
            $grand_total = $order->grand_total +50;
            $order_id = $order->order_no;
            $sms = "Order Placed: Your order for ProductName with order ID $order_id amounting to Rs $grand_total has been received. We will send you an update when order is packed/shipped. Manage your order here $orderurl";
            $this->sendSms($user_data['telephone'], $sms);

//            $name = trim('Tahir Ali');
//            $email = trim('tahirali2k19@gmail.com');
//            $title = trim('Title');
//            $content = trim('Order Placed');
//
//            $check = \Mail::send('emails.order_place_mail', ['name' => $name, 'email' => $email, 'title' => $title, 'content' => $content], function ($message) {
//                        $message->from('ali.tahir04@gmail.com', 'Aaima');
//                        $message->to('tahirali2k19@gmail.com');
//                        $message->subject('Order Placed');
//                    });

            return redirect('/success/' . $order->order_no)->with('success', 'Order placed successfully');
        } else {
            return redirect('/failure/' . $order->order_no)->with('error', 'Order failed, please contact admin');
        }
    }

        public function order_detail(Request $request, $id = null) {
        $user_data = $request->session()->get('user_data');
      
        $order = Order::find($id);

        if ($order):
            $address = Orderaddress::where(['order_id' => $id])->first();

            $history = OrderHistory::where(['order_id'=>$id])->get();
            $order['history']=$history;
       //   echo "<pre>";
       // print_r($history);
       // die;
            if ($address) {
                $state = State::where(['state_id' => $address->zone_id])->first();
                if ($state) {
                    $address['state'] = $state->state_name;
                } else {
                    $address['state'] = '';
                }
                $country = Country::where(['country_id' => $address->country_id])->first();
                if ($country) {
                    $address['country'] = $country->country_name;
                } else {
                    $address['country'] = '';
                }
                $order['address'] = $address;
            } else {
                $order['address'] = [];
            }
        endif;

        return view('orders.order_detail', ['orderInfo' => $order]);
    }

    public function order_success($order_id = null) {
        return view('orders.order_success', ['order_id' => $order_id]);
    }
 public function invoice(Request $request,$id = null) {
 $user_data = $request->session()->get('user_data');
      
        $order = Order::find($id);

        if ($order):
            $address = Orderaddress::where(['order_id' => $id])->first();
//          echo "<pre>";
//        print_r($address);
//        die;
            if ($address) {
                $state = State::where(['state_id' => $address->zone_id])->first();
                if ($state) {
                    $address['state'] = $state->state_name;
                } else {
                    $address['state'] = '';
                }
                $country = Country::where(['country_id' => $address->country_id])->first();
                if ($country) {
                    $address['country'] = $country->country_name;
                } else {
                    $address['country'] = '';
                }
                $order['address'] = $address;
            } else {
                $order['address'] = [];
            }
        endif;

        return view('orders.invoice',  ['orderInfo' => $order]);
    }

    public function order_failure($order_id = null) {
        return view('orders.order_failure', ['order_id' => $order_id]);
    }

    public function all_orders() {
        $orders = Order::orderBy('id', 'desc')->get();
        return view('orders.all_orders', ['orders' => $orders]);
    }
    public function filter_orders(Request $request) {
        extract($_POST);
        if (!empty($order_no)) {
           $where['order_no'] = $order_no;
        }
        if (!empty($name)) {
            $where = [
                    ['firstname', 'like', '%' . $name . '%'],
            ];
        }
        if (!empty($grand_total)) {
            $where['grand_total'] = $grand_total;
        }
        if (!empty($created_at)) {
            $date = date("Y-m-d", strtotime($created_at));
            //$where[DATE('orders.created_at')] = date("Y-m-d", strtotime($created_at));
            $where = [
                    ['orders.created_at', 'like', '%' . $date . '%'],
            ];
        }
        if (!empty($updated_at)) {
            $date = date("Y-m-d", strtotime($updated_at));
            $where = [
                    ['orders.updated_at', 'like', '%' . $date . '%'],
            ];
        }
        if (!empty($status)) {
            if ($status == 1) {
                $where['orders.status'] = '1';
            } elseif ($status == 2) {
                $where['orders.status'] = '2';
            }elseif ($status == 3) {
                $where['orders.status'] = '3';
            }elseif ($status == 4) {
                $where['orders.status'] = '4';
            }
        }
        if (empty($_POST)) {
            $where = [
                    ['id', '>', '0'],
            ];
        }
        if (!empty($name) || !empty($order_no) || !empty($created_at) || !empty($updated_at) || !empty($grand_total) || !empty($status)) {
            //$orders = Order::where($where)->orderBy('id', 'desc')->get();
            $orders = DB::table('orders')
            ->leftJoin('customers', 'orders.customer_id', '=', 'customers.id')
            ->select('orders.*', 'customers.firstname', 'customers.lastname')
            ->where($where)
            ->orderBy('orders.id', 'desc')
            ->get();
        }
         else {
            $orders = Order::orderBy('id', 'desc')->get();
        }

        return view('orders.all_orders', ['orders' => $orders]);
    }

    //Send Mobile OTP
    public function sendSms($mobile, $sms) {
        $sender = "AAIMAG";
        $workingkey = "nUxIBk39oEmU9tpcnmUmOg";

        $to = '91' . $mobile;

        $ms = rawurlencode($sms);
        $sms_url = "https://www.smsgatewayhub.com/api/mt/SendSMS?APIKey=" . $workingkey . "&senderid=" . $sender . "&channel=2&DCS=0&flashsms=0&number=" . $to . "&text=" . $ms . "&route=21";

        $ch = curl_init($sms_url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 2);
        $data = curl_exec($ch);
        curl_close($ch);
        return true;
    }
 public function order_history(Request $request) {
     $order_id=$request->input('order_id');
             $orderInfo = Order::find($order_id);

 $customerInfo = getCustomer($orderInfo->customer_id);
// echo "<pre>";
// print_r($customerInfo);
// echo $customerInfo->telephone;
// die;
        $newAdd = [
                    'order_id' => $request->input('order_id'),
                    'status' => $request->input('status'),
                    'comment' => $request->input('comment'),
                  
                ];
              $status=  OrderHistory::create($newAdd);
              // echo $status->status;
              // die;
              $status_1="";
             if($status->status ==1){
    $status_1 ="Request";
}
elseif($status->status==2){
    $status_1="Process";
}elseif($status->status==3){
    $status_1= "Dispatched";
}
elseif($status->status==4){
    $status_1= "Delivered";
}
elseif($status->status==5){
    $status_1= "Cancel";
}
              if($status){
                   $sms = "Order Status Updated: Your order status with order ID $order_id has been updated to $status_1 .";
            $this->sendSms($customerInfo->telephone, $sms);
                  return redirect()->action('OrdersController@order_detail', $order_id);
              }
    }


}
