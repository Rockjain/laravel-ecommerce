<?php

use Illuminate\Support\Facades\DB;
use App\Model\Category;
use App\Model\Appfiles;
use App\Model\Customer;
use App\Model\Product;
use App\Model\Cart;
use App\Model\State;
use App\Model\Country;

function getParentCategory() {
    $categories = Category::where(['parent_id' => '0', 'status' => '1'])->get();
    if ($categories) {
        return $categories;
    } else {
        return array();
    }
}
function getSamplePages() {
    $pages = Pages::where(['status' => '1'])->get();
    if ($pages) {
        return $pages;
    } else {
        return array();
    }
}
function getChildCategories($id) {
    $categories = Category::where(['parent_id' => $id, 'status' => '1'])->get();
    if ($categories) {
        return $categories;
    } else {
        return array();
    }
}

function getFile($id) {
    $file = Appfiles::find($id);
    if ($file) {
        return $file;
    } else {
        return array();
    }
}

function getCustomer($id) {
    $userInfo = Customer::find($id);
    if ($userInfo) {
        return $userInfo;
    } else {
        return array();
    }
}

function getProduct($id) {
    $prodInfo = Product::find($id);
    if ($prodInfo) {
        return $prodInfo;
    } else {
        return array();
    }
}

function getCart($customer_id) {
    $cartInfo = Cart::where(['customer_id' => $customer_id])->orderBy('id', 'desc')->get();
    if ($cartInfo) {
        return $cartInfo;
    } else {
        return array();
    }
}

function getCartCount($customer_id) {
    $countInfo = DB::table('customer_cart')->select(DB::raw('SUM(quantity) as total_count'))->where(['customer_id' => $customer_id])->first();
    if ($countInfo) {
        return $countInfo->total_count;
    } else {
        return array();
    }
}

function getState($id) {
    $stateInfo = State::where(['state_id' => $id])->first();
    if ($stateInfo) {
        return $stateInfo;
    } else {
        return array();
    }
}

function getCountry($id) {
    $countryInfo = Country::where(['country_id' => $id])->first();
    if ($countryInfo) {
        return $countryInfo;
    } else {
        return array();
    }
}
