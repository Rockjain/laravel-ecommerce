@extends('layouts.account')
@section('content')
<link rel="stylesheet" href="<?= url('/'); ?>/frontassets/css/downline/hierarchy-view.css">
<link rel="stylesheet" href="<?= url('/'); ?>/frontassets/css/downline/main.css">
<!-- =====  CONTAINER START  ===== -->
<div class="container-fluid white-bg mb_30">
    <div class="row nomargin">

        <div class="col-sm-12 col-md-12 col-lg-12 nopadding">

            <!--Management Hierarchy-->
            <section class="management-hierarchy">

                <div class="hv-container">
                    <div class="hv-wrapper">

                        <!-- Key component -->
                        <div class="hv-item">

                            <div class="hv-item-parent">
                                <div class="person">
                                    <img src="https://pbs.twimg.com/profile_images/762654833455366144/QqQhkuK5.jpg" alt="">
                                    <p class="name">
                                        Ziko Sichi <b>/ CEO</b>
                                        <br/>1 <i class="fa fa-star"></i>
                                    </p>
                                </div>
                            </div>

                            <div class="hv-item-children">

                                <!---------- firt level parent----->
                                <div class="hv-item-child">
                                    <!-- Key component -->
                                    <div class="hv-item">

                                        <div class="hv-item-parent">
                                            <a href="#">
                                                <div class="person">
                                                    <img src="https://randomuser.me/api/portraits/women/50.jpg" alt="">
                                                    <p class="name">
                                                        Annie Wilner <b>/ Creative Director</b>
                                                        <br/>1 <i class="fa fa-star"></i>
                                                    </p>
                                                </div>
                                            </a>
                                        </div>

                                        <div class="hv-item-children">

                                            <div class="hv-item-child">
                                                <a href="#">
                                                    <div class="person">
                                                        <img src="https://randomuser.me/api/portraits/women/68.jpg" alt="">
                                                        <p class="name">
                                                            Anne Potts <b>/ UI Designer</b>
                                                            <br/>1 <i class="fa fa-star"></i>
                                                        </p>
                                                    </div>
                                                </a>
                                            </div>


                                            <div class="hv-item-child">
                                                <a href="#">
                                                    <div class="person">
                                                        <img src="https://randomuser.me/api/portraits/men/81.jpg" alt="">
                                                        <p class="name">
                                                            Dan Butler <b>/ UI Designer</b>
                                                            <br/>1 <i class="fa fa-star"></i>
                                                        </p>
                                                    </div>
                                                </a>
                                            </div>

                                            <div class="hv-item-child">
                                                <a href="#">
                                                    <div class="person">
                                                        <img src="https://randomuser.me/api/portraits/women/18.jpg" alt="">
                                                        <p class="name">
                                                            Mary Bower <b>/ UX Designer</b>
                                                            <br/>1 <i class="fa fa-star"></i>
                                                        </p>
                                                    </div>
                                                </a>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                                <!----------end firt level parent----->
                                <!---------- firt level parent----->
                                <div class="hv-item-child">
                                    <!-- Key component -->
                                    <div class="hv-item">

                                        <div class="hv-item-parent">
                                            <a href="#">
                                                <div class="person">
                                                    <img src="https://randomuser.me/api/portraits/men/3.jpg" alt="">
                                                    <p class="name">
                                                        Gordon Clark <b>/ Senior Developer</b>
                                                        <br/>1 <i class="fa fa-star"></i>
                                                    </p>
                                                </div>
                                            </a>
                                        </div>

                                        <div class="hv-item-children">

                                            <div class="hv-item-child">
                                                <a href="#">
                                                    <div class="person">
                                                        <img src="https://randomuser.me/api/portraits/men/41.jpg" alt="">
                                                        <p class="name">
                                                            Harry Bell <b>/ Front-end</b>
                                                            <br/>1 <i class="fa fa-star"></i>
                                                        </p>
                                                    </div>
                                                </a>
                                            </div>


                                            <div class="hv-item-child">
                                                <a href="#">
                                                    <div class="person">
                                                        <img src="https://randomuser.me/api/portraits/men/90.jpg" alt="">
                                                        <p class="name">
                                                            Matt Davies <b>/ Back-end</b>
                                                            <br/>1 <i class="fa fa-star"></i>
                                                        </p>
                                                    </div>
                                                </a>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                                <!----------end firt level parent----->

                                <!---------- firt level parent----->

                                <div class="hv-item-child">
                                    <!-- Key component -->
                                    <div class="hv-item">

                                        <div class="hv-item-parent">
                                            <a href="#">
                                                <div class="person">
                                                    <img src="https://randomuser.me/api/portraits/men/3.jpg" alt="">
                                                    <p class="name">
                                                        Gordon Clark <b>/ Senior Developer</b>
                                                        <br/>1 <i class="fa fa-star"></i>
                                                    </p>
                                                </div>
                                            </a>
                                        </div>

                                        <div class="hv-item-children">

                                            <div class="hv-item-child">
                                                <a href="#">
                                                    <div class="person">
                                                        <img src="https://randomuser.me/api/portraits/men/41.jpg" alt="">
                                                        <p class="name">
                                                            Harry Bell <b>/ Front-end</b>
                                                            <br/>1 <i class="fa fa-star"></i>
                                                        </p>
                                                    </div>
                                                </a>
                                            </div>


                                            <div class="hv-item-child">
                                                <a href="#">
                                                    <div class="person">
                                                        <img src="https://randomuser.me/api/portraits/men/90.jpg" alt="">
                                                        <p class="name">
                                                            Matt Davies <b>/ Back-end</b>
                                                            <br/>1 <i class="fa fa-star"></i>
                                                        </p>
                                                    </div>
                                                </a>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                                <!----------end firt level parent----->


                                <!---------- firt level parent----->
                                <div class="hv-item-child">
                                    <!-- Key component -->
                                    <div class="hv-item">

                                        <div class="hv-item-parent">
                                            <a href="#">
                                                <div class="person">
                                                    <img src="https://randomuser.me/api/portraits/women/50.jpg" alt="">
                                                    <p class="name">
                                                        Annie Wilner <b>/ Creative Director</b>
                                                        <br/>1 <i class="fa fa-star"></i>
                                                    </p>
                                                </div>
                                            </a>
                                        </div>

                                        <div class="hv-item-children">

                                            <div class="hv-item-child">
                                                <a href="#">
                                                    <div class="person">
                                                        <img src="https://randomuser.me/api/portraits/women/68.jpg" alt="">
                                                        <p class="name">
                                                            Anne Potts <b>/ UI Designer</b>
                                                            <br/>1 <i class="fa fa-star"></i>
                                                        </p>
                                                    </div>
                                                </a>
                                            </div>


                                            <div class="hv-item-child">
                                                <a href="#">
                                                    <div class="person">
                                                        <img src="https://randomuser.me/api/portraits/men/81.jpg" alt="">
                                                        <p class="name">
                                                            Dan Butler <b>/ UI Designer</b>
                                                            <br/>1 <i class="fa fa-star"></i>
                                                        </p>
                                                    </div>
                                                </a>
                                            </div>

                                            <div class="hv-item-child">
                                                <a href="#">
                                                    <div class="person">
                                                        <img src="https://randomuser.me/api/portraits/women/18.jpg" alt="">
                                                        <p class="name">
                                                            Mary Bower <b>/ UX Designer</b>
                                                            <br/>1 <i class="fa fa-star"></i>
                                                        </p>
                                                    </div>
                                                </a>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                                <!----------end firt level parent----->

                            </div>

                        </div>

                    </div>
                </div>
            </section>

        </div>
    </div>
</div>
<!-- =====  CONTAINER END  ===== -->
@endsection