<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('coupons', function (Blueprint $table) {
            $table->increments('id');
            $table->string('coupon_name')->nullable();
            $table->string('code')->nullable();
            $table->enum('type', ['1', '2'])->comment('1:percentage,2:fixed');
            $table->double('discount')->default(0);
            $table->double('total_amount')->default(0);
            $table->string('categories')->default(0)->comment('comma sepated category ids');
            $table->timestamp('start_date')->nullable();
            $table->timestamp('end_date')->nullable();
            $table->integer('uses_per_coupon')->unsigned();
            $table->integer('uses_per_user')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('coupons');
    }

}
