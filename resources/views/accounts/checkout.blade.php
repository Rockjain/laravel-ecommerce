@extends('layouts.default')
@section('content')
<div class="container">
    <div class="row">
        <div id="column-left" class="col-sm-8 col-md-8 col-lg-8 mtb_20">
            <section class="white-bg">
                <div class="wizard ptb_20">
                    <div class="wizard-inner">
                        <!-- <div class="connecting-line"></div> -->
                        <ul class="nav  breadcrumb_checkout" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#shippingstep" data-toggle="tab" aria-controls="shippingstep" role="tab" title="Shipping">
                                    <h3>Shipping</h3>
                                </a>
                            </li>
                            <li role="presentation" class="disabled">
                                <a href="#paymentstep" data-toggle="tab" aria-controls="paymentstep" role="tab" title="Payment">
                                    <h3>Payment</h3>
                                </a>
                            </li>
                            <li role="presentation" class="disabled">
                                <a href="#confirmationstep" data-toggle="tab" aria-controls="confirmationstep" role="tab" title="Confirmation">
                                    <h3>Confirmations</h3>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <form method="post" id="checkOutForm" action="{{route('orders.store')}}" class="form-horizontal">
                        {{csrf_field()}}
                        <div class="tab-content">
                            <div class="tab-pane active" role="tabpanel" id="shippingstep">
                                <div class="shippingstep">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" checked="checked" value="1" name="address_type" onclick="addresstype(this)"> I want to use an existing address
                                                </label>
                                            </div>
                                            <div id="shipping-existing">
                                                <select class="form-control" name="address_id">
                                                    <?php
                                                    if ($addressInfo):
                                                        foreach ($addressInfo as $address):
                                                            $stateInfo = getState($address->zone_id);
                                                            if ($stateInfo) {
                                                                $state = $stateInfo->state_name;
                                                            } else {
                                                                $state = '';
                                                            }
                                                            $countryInfo = getCountry($address->country_id);
                                                            if ($countryInfo) {
                                                                $country = $countryInfo->country_name;
                                                            } else {
                                                                $country = '';
                                                            }
                                                            ?>
                                                            <option selected="selected" value="<?= $address->id; ?>"><?= $address->firstname . ' ' . $address->lastname . ', ' . $address->address_1 . ' ' . $address->address_2 . ' ' . $address->city . ' ' . $state . ' ' . $country; ?></option>
                                                            <?php
                                                        endforeach;
                                                    endif;
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" value="2" name="address_type" onclick="addresstype(this)"> I want to use a new address
                                                </label>
                                            </div>
                                            <br>
                                            <div id="shipping-new" style="display: none;">
                                                <div class="form-group required">
                                                    <label class="col-sm-4 control-label">First Name</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="firstname" value="" placeholder="First Name"  class="form-control" maxlength="25">
                                                    </div>
                                                </div>
                                                <div class="form-group required">
                                                    <label class="col-sm-4 control-label">Last Name</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="lastname" value="" placeholder="Last Name" class="form-control" maxlength="25">
                                                    </div>
                                                </div>
                                                <div class="form-group required">
                                                    <label class="col-sm-4 control-label">Address 1</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="address_1" value="" placeholder="Address 1"  class="form-control" maxlength="128">
                                                    </div>
                                                </div>
                                                <div class="form-group required">
                                                    <label class="col-sm-4 control-label">Address 2</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="address_2" value="" placeholder="Address 2" class="form-control" maxlength="128">
                                                    </div>
                                                </div>
                                                <div class="form-group required">
                                                    <label class="col-sm-4 control-label">City</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="city" value="" placeholder="City" id="input-city" class="form-control" maxlength="128">
                                                    </div>
                                                </div>
                                                <div class="form-group required">
                                                    <label class="col-sm-4 control-label">Region / State</label>
                                                    <div class="col-sm-8">
                                                        <select name="zone_id" id="input-zone" class="form-control">
                                                            <option disabled selected value="">Select State</option>
                                                            @if($states)
                                                            @foreach($states as $state)
                                                            <option value="{{$state->state_id}}">{{$state->state_name}}</option>
                                                            @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group required">
                                                    <label class="col-sm-4 control-label" style="">Post Code</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="postcode" placeholder="Post Code" class="form-control numericOnly" maxlength="10">
                                                    </div>
                                                </div>
                                                <div class="form-group required">
                                                    <label class="col-sm-4 control-label">Country</label>
                                                    <div class="col-sm-8">
                                                        <select name="country_id" class="form-control">
                                                            <option disabled selected value="">Select Country</option>
                                                            @if($countries)
                                                            @foreach($countries as $country)
                                                            <option value="{{$country->country_id}}">{{$country->country_name}}</option>
                                                            @endforeach
                                                            @endif
                                                        </select> 
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div> 
                                </div><!-- step 1 end div tag -->
                                <ul class="list-inline pull-right">
                                    <li><button type="button" class="btn btn-primary next-step btn-theme">Next <i class="fa fa-angle-right"></i></button></li>
                                </ul>
                            </div>
                            <div class="tab-pane" role="tabpanel" id="paymentstep"> <!-- step 2 start -->
                                <div class="paymentstep">    
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="radio">
                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                    <label>Select Payment Method :</label>
                                                </div>
                                                <div class="col-xs-12 col-sm-5 col-md-3">
                                                    <div class="funkyradio">
                                                        <div class="funkyradio-default">
                                                            <input type="radio" name="payment_mode" value="1" id="paymentmethod1" checked/>
                                                            <label for="paymentmethod1">Cash On Delivery</label>
                                                        </div>
                                                         <div class=funkyradio-primary>
                                  
                                                        </div>
                                                        <div class="funkyradio-primary" id="payment_mode">
                                                            <input type="radio" name="payment_mode" value="2" id="paymentmethod2" />
                                                            <label for="paymentmethod2">Pay with Paypal</label>
                                                        </div>
                                                      <!--   <div class="funkyradio-primary">
                                                            <input type="radio" name="payment_mode" value="3" id="paymentmethod3" />
                                                            <label for="paymentmethod3">Payu MOney</label>
                                                        </div> -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <ul class="list-inline pull-right">
                                    <li><button type="button" class="btn btn-info prev-step btn-theme"><i class="fa fa-angle-left"></i> Previous</button></li>
                                    <li><button type="button" class="btn btn-primary next-step btn-theme">Next <i class="fa fa-angle-right"></i></button></li>
                                </ul>
                            </div><!-- step 2 end -->
                            <div class="tab-pane" role="tabpanel" id="confirmationstep">
                                <ul class="order-item">
                                    @if($usercart)
                                    @foreach($usercart as $cart)
                                    <?php
                                    $productInfo = getProduct($cart->product_id);
                                     $delivery_charge[] = $productInfo->delivery_charge;
                                    if ($productInfo) {
                                        $image = $productInfo->fileDetail->file_path . $productInfo->fileDetail->file_name;
                                    } else {
                                        $image = url('/') . '/frontassets/images/no_img.png';
                                    }
                                    ?>
                                    <li>
                                        <div class="thumb-image"><img src="{{$image}}" title="{{$productInfo->name}}"></a></div>
                                        <div class="confirmorder-info">
                                            <h4>{{$productInfo->name}}</h4>
                                            <p><b><i class="fa fa-inr"></i> {{$productInfo->price}}</b>x{{$cart->quantity}}</p>
                                            <p></p>
                                        </div>
                                    </li>
                                    @endforeach
                                    @endif
                                    
                                    <li>
                                        <div class="final-total pull-right">
                                            <table class="table table-stripped table-hover">
                                                <tr>
                                                    <td class="text-right"><strong>Sub-Total:</strong></td>
                                                    <td class="text-right">&#x20B9;{{$s_total}}</td>
                                                </tr>
                                           <!--      <tr>
                                                    <td class="text-right"><strong>Flat Shipping Rate:</strong></td>
                                                    <td class="text-right">&#x20B9; 0</td>
                                                </tr>  -->                       
                                                <tr>
                                                     <?php
                                                     // print_r($delivery_charge);die;
                                                      $sum = array_sum($delivery_charge);
                                                      ?>
                                                    <td class="text-right"><strong>Delivery Charges:</strong></td>
                                                    <td class="text-right">&#x20B9; {{$sum}}</td>
                                                </tr>
                                                <tr>
                                                    <td class="text-right"><strong>Total:</strong></td>
                                                    <td class="text-right">&#x20B9;{{$s_total+$sum}}</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </li>
                                </ul>
                                <input type="hidden" value="{{$s_total}}" name="grand_total">
                                <div class="clearfix"></div>
                                <ul class="list-inline pull-right">
                                    <li><button type="button" class="btn btn-info prev-step btn-theme"><i class="fa fa-angle-left"></i> Previous</button></li>
                                    <li><button type="submit" class="btn btn-primary btn-info-full next-step btn-theme">Place Order</button></li>
                                </ul>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </form>
                    <div id="paymentform" style="display: none;">
                      <form class="w3-display-middle w3-card-4 w3-padding-16" method="POST" id="payment-form" action="{!! URL::to('paypal') !!}">
                                  <!-- <div class="w3-container w3-teal w3-padding-16">Paywith Paypal</div> -->
                                  {{ csrf_field() }}
                                  <input class="w3-input w3-border" id="amount" type="hidden" value={{$s_total+$sum}} name="amount"></p> 
                                   <!-- <input type="radio" name="payment_mode" value="2" id="paymentmethod2" /> -->
                                  <button class="w3-btn w3-blue">Pay</button>
<!--                                   <label for="paymentmethod2"></label>
 -->                      </form>
                     </div>
                </div>
               <!--  <form class="w3-display-middle w3-card-4 w3-padding-16" method="POST" id="payment-form" action="{!! URL::to('paypal') !!}">
                                  <div class="w3-container w3-teal w3-padding-16">Paywith Paypal</div>
                                  {{ csrf_field() }}
                                   <label class="w3-text-blue"><b>Enter Amount</b></label>
                                  <input class="w3-input w3-border" id="amount" type="text" name="amount"></p> 
                                  <button class="w3-btn w3-blue">Pay with PayPal</button>
                                </form> -->
            </section>
        </div>
        <div id="column-right" class="col-sm-4 col-md-4 col-lg-3 mtb_20">
            <div class="white-bg nopadding">
                <div class="">
                    <div class="row nomargin">
                        <div class="col-sm-12 nopadding">
                            <div class="col-sm-12">
                                <p class="hidden-xs viewcart-button">
                                    Cart Items <span class="pull-right"><i class="fa fa-angle-down"></i></span>
                                </p>
                                <p class="hidden visible-xs viewcart-button"  data-toggle="collapse" data-target="#viewcart" aria-expanded="false" aria-controls="collapseExample">
                                    View Cart Items <span class="pull-right"><i class="fa fa-angle-right"></i></span>
                                </p>
                            </div>
                            <div class="collapse dont-collapse-sm" id="viewcart">
                                <div class="col-md-12 pt_20">
                                    @if($usercart)
                                    @foreach($usercart as $cart)
                                    <?php
                                    $productInfo = getProduct($cart->product_id);
                                     $delivery_charges[] = $productInfo->delivery_charge;
                                    //print_r($cart);
                                   // die;
                                    if ($productInfo) {
                                        $image = $productInfo->fileDetail->file_path . $productInfo->fileDetail->file_name;
                                    } else {
                                        $image = url('/') . '/frontassets/images/no_img.png';
                                    }
                                    $price=$productInfo->price - (($productInfo->discount_percent * $productInfo->price) / 100);
                                    ?>
                                   
                                    <div class="cart-item-info">
                                        <h4>{{$productInfo->name}} x {{$cart->quantity}} <small>= &#x20B9; {{$price}}</small> 
                                             <?php if($productInfo->discount_percent!=0) { 
                                              $totalprice = $productInfo->price ;
                                              ?>
                                              <del class="text-danger">{{$totalprice}}</del>
                                            <?php }else{?>
                                             <del class="text-danger"></del>
                                            <?php } ?>
                                        </h4>
                                    </div>
                                    @endforeach
                                    @endif
                                </div>
                                <div class="col-md-12 nopadding">
                                    <div class="cart-final-total">
                                        <?php
                                        //print_r($delivery_charges);die;
                                         $sum2 = array_sum($delivery_charges);
                                        ?>

                                        <p class="text-right"> <strong>Delievery Charges:</strong> &#x20B9; {{$sum2}}</p>
                                        <p class="text-right"> <strong>Total Amount:</strong> &#x20B9; <?php echo $s_total+$sum2;?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function(){
  $("#paymentmethod2").click(function(){
    $("#paymentform").show();
    $("checkOutForm").hide();
    $("#payment_mode").hide();
  });
});
$(document).ready(function(){
  $(".prev-step").click(function(){
    $("#paymentform").hide();
    // $("checkOutForm").hide();
    $("#payment_mode").show();
  });
});

</script>
<script type="text/javascript">
    $('input[name=\'payment_address\']').on('change', function () {
        if (this.value == 'new') {
            $('#payment-existing').hide();
            $('#payment-new').show();
        } else {
            $('#payment-existing').show();
            $('#payment-new').hide();
        }
    });
    function addresstype(obj) {
        var radioval = $(obj).val();
        if (radioval == '1') {
            $("#shipping-existing").show();
            $("#shipping-new").hide();
        } else if (radioval == '2') {
            $("#shipping-new").show();
            $("#shipping-existing").hide();
        }
    }
</script>
<style type="text/css">
    div#paymentform {
    width: 44%;
    text-align: right;
    position: absolute;
    /* top: 0; */
    bottom: 30px;
    left: 5px;
}
</style>
@endsection