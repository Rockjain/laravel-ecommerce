<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Pages extends Model {

	protected $table = 'pages';
	protected $fillable = array(
		'title',
		'url_slug',
		'description',
		'meta_title',
		'meta_keyword',
		'meta_description',
		'priority',
		'status',
		'date_time'
	);

	

	public function fileDetail() {
		return $this->belongsTo('App\Model\Appfiles', 'image');
	}

}
