<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Category;
use App\Model\Attribute;
use App\Model\Brand;
use App\Model\CategoryBrand;
use App\Model\CategoryAttribute;
use App\Model\Product;
use App\Model\Wishlist;
use App\Model\Cart;
use App\Model\Address;
use App\Model\ProductReview;
use App\Model\ProductAttribute;
use App\Model\ProductImage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cookie;

class AjaxController extends Controller {

    protected $error = true;
    protected $error_code = 101;
    protected $message = "Invalid request format";
    protected $result;

    public function delete(Request $request) {
        $delete_id = $request->input('delete_id');
        $table = $request->input('table');
        if ($table == 'products') {
            ProductAttribute::where(['product_id' => $delete_id])->delete();
            ProductImage::where(['product_id' => $delete_id])->delete();
            Wishlist::where(['product_id' => $delete_id])->delete();
            Cart::where(['product_id' => $delete_id])->delete();
            Product::where(['id' => $delete_id])->delete();
        }
        $res = ['status' => '1', 'message' => 'Success'];
        echo json_encode($res);
        die;
    }

    public function get_brands(Request $request) {
        $brands = CategoryBrand::where(['category_id' => $request->input('category_id')])->get();
        $data = [];
        if ($brands) {
            foreach ($brands as $key => $brand) {
                $data[$key]['id'] = $brand->id;
                $data[$key]['brand_id'] = $brand->brand_id;
                $data[$key]['name'] = $brand->brandDetail->title;
            }
            $res = ['status' => '1', 'message' => 'Success', 'data' => $data];
        } else {
            $res = ['status' => '0', 'message' => 'Error', 'data' => []];
        }
        echo json_encode($res);
        die;
    }

    public function get_attributes(Request $request) {
        $attributes = CategoryAttribute::where(['category_id' => $request->input('category_id')])->get();
        $data = [];
        if ($attributes) {
            foreach ($attributes as $key => $attr) {
                $data[$key]['attribute_name'] = $attr->attributeDetail->name;
                $data[$key]['attribute_type'] = $attr->attributeDetail->type;
                $data[$key]['attribute_id'] = $attr->attribute_id;

                foreach ($attr->attributeDetail->attributeValues as $key1 => $value):
                    $valueArr[$key1]['id'] = $value->id;
                    $valueArr[$key1]['attribute_value'] = $value->attribute_value;

                    $data[$key]['attribute_values'] = $valueArr;
                endforeach;
            }
            $res = ['status' => '1', 'message' => 'Success', 'data' => $data];
        } else {
            $res = ['status' => '0', 'message' => 'Error', 'data' => []];
        }
        echo json_encode($res);
        die;
    }

    public function change_status(Request $request) {
        $method = $request->input('method');
        $status = $request->input('status');
        $id = $request->input('id');

        if ($method == 'changeCategoryStatus') {
            $updateArr = Category::where('id', $id)->update([
                'status' => $status,
            ]);
            if ($updateArr) {
                $res = ['status' => '1', 'message' => 'Success'];
            } else {
                $res = ['status' => '0', 'message' => 'Error'];
            }
            echo json_encode($res);
            die;
        }
        if ($method == 'changeAttributeStatus') {
            $updateArr = Attribute::where('id', $id)->update([
                'status' => $status,
            ]);
            if ($updateArr) {
                $res = ['status' => '1', 'message' => 'Success'];
            } else {
                $res = ['status' => '0', 'message' => 'Error'];
            }
            echo json_encode($res);
            die;
        }
        if ($method == 'changeBrandStatus') {
            $updateArr = Brand::where('id', $id)->update([
                'status' => $status,
            ]);
            if ($updateArr) {
                $res = ['status' => '1', 'message' => 'Success'];
            } else {
                $res = ['status' => '0', 'message' => 'Error'];
            }
            echo json_encode($res);
            die;
        }
        if ($method == 'changeProductStatus') {
            $updateArr = Product::where('id', $id)->update([
                'status' => $status,
            ]);
            if ($updateArr) {
                $res = ['status' => '1', 'message' => 'Success'];
            } else {
                $res = ['status' => '0', 'message' => 'Error'];
            }
            echo json_encode($res);
            die;
        }
        if ($method == 'changeCatAttrStatus') {
            $updateArr = CategoryAttribute::where('id', $id)->update([
                'status' => $status,
            ]);
            if ($updateArr) {
                $res = ['status' => '1', 'message' => 'Success'];
            } else {
                $res = ['status' => '0', 'message' => 'Error'];
            }
            echo json_encode($res);
            die;
        }
        if ($method == 'changeCatBrandStatus') {
            $updateArr = CategoryBrand::where('id', $id)->update([
                'status' => $status,
            ]);
            if ($updateArr) {
                $res = ['status' => '1', 'message' => 'Success'];
            } else {
                $res = ['status' => '0', 'message' => 'Error'];
            }
            echo json_encode($res);
            die;
        }
        if ($method == 'changePageStatus') {
            $updateArr = DB::table('pages')->where('id', $id)->update([
                'status' => $status,
            ]);
            if ($updateArr) {
                $res = ['status' => '1', 'message' => 'Success'];
            } else {
                $res = ['status' => '0', 'message' => 'Error'];
            }
            echo json_encode($res);
            die;
        }
        if ($method == 'changeTestStatus') {
            $updateArr = DB::table('testimonials')->where('id', $id)->update([
                'status' => $status,
            ]);
            if ($updateArr) {
                $res = ['status' => '1', 'message' => 'Success'];
            } else {
                $res = ['status' => '0', 'message' => 'Error'];
            }
            echo json_encode($res);
            die;
        }
        if ($method == 'changeAdvCatStatus') {
            $updateArr = DB::table('adv_category')->where('id', $id)->update([
                'status' => $status,
            ]);
            if ($updateArr) {
                $res = ['status' => '1', 'message' => 'Success'];
            } else {
                $res = ['status' => '0', 'message' => 'Error'];
            }
            echo json_encode($res);
            die;
        }
    }

    public function addtocart(Request $request) {
        $user_data = $request->session()->get('user_data');
        if ($user_data) {
            $customer_id = $user_data['customer_id'];
        } else {
            if (!empty($_COOKIE['customer_cookie'])) {
                $customer_id = $_COOKIE['customer_cookie'];
            } else {
                $customer_cookie = time() . $request->input('product_id') . $request->input('quantity');
                setcookie('customer_cookie', $customer_cookie, time() + (86400 * 30), "/");
                $customer_id = $customer_cookie;
            }
        }

        $quantity = $request->input('quantity');
        $product_id = $request->input('product_id');

        $product = Product::find($product_id);
        if ($product->quantity < $quantity) {
            $this->error_code = 201;
            $this->message = "Maximum quantity reached";
            $this->result = [];
        } else {
            $isExist = Cart::where(['customer_id' => $customer_id, 'product_id' => $product_id])->exists();
            if ($isExist) {
                $prevData = Cart::where(['customer_id' => $customer_id, 'product_id' => $product_id])->first();
                $updateArr = [
                    'quantity' => $prevData->quantity + 1
                ];
                $update = Cart::where(['id' => $prevData->id])->update($updateArr);
                if ($update) {
                    $cartCount = DB::table('customer_cart')->select(DB::raw('SUM(quantity) as total_count'))->where(['customer_id' => $customer_id])->first();

                    //gettimg sub total
                    $usercart = Cart::where(['customer_id' => $customer_id])->get();
                    if ($usercart):
                        $s_total = '0';
                        $t_quantity = '0';
                        foreach ($usercart as $key => $row):
                            $productInfo = \App\Model\Product::where(['id' => $row->product_id])->first();
                            $s_total = $s_total + ($row->quantity * $productInfo->price);
                            $t_quantity = $t_quantity + $row->quantity;
                        endforeach;
                    endif;

                    $this->error_code = 200;
                    $this->message = "Product added to cart";
                    $this->result = ['cartCount' => $t_quantity, 'totalprice' => $s_total, 'product' => [], 'cart_id' => $prevData->id, 'quantity' => $prevData->quantity + 1];
                } else {
                    $this->error_code = 99;
                    $this->message = "Some Error Found, please try again";
                    $this->result = [];
                }
            } else {
                $insertArr = [
                    'customer_id' => $customer_id,
                    'product_id' => $product_id,
                    'quantity' => $quantity,
                    'status' => '1'
                ];
                $add = Cart::create($insertArr);
                if ($add):
                    $cartCount = DB::table('customer_cart')->select(DB::raw('SUM(quantity) as total_count'))->where(['customer_id' => $customer_id])->first();

                    //gettimg sub total
                    $usercart = Cart::where(['customer_id' => $customer_id])->get();
                    if ($usercart):
                        $s_total = '0';
                        $t_quantity = '0';
                        foreach ($usercart as $key => $row):
                            $productInfo = \App\Model\Product::where(['id' => $row->product_id])->first();
                            $s_total = $s_total + ($row->quantity * $productInfo->price);
                            $t_quantity = $t_quantity + $row->quantity;
                        endforeach;
                    endif;

                    $product = Product::find($product_id);
                    if ($product->image) {
                        $file = \App\Model\Appfiles::find($product->image);

                        if ($file) {
                            $product->image = $file->file_path . $file->file_name;
                        } else {
                            $product->image = url('/') . '/frontassets/images/no_img.png';
                        }
                    }

                    $this->error_code = 200;
                    $this->message = "Product added to cart";
                    $this->result = ['cartCount' => $t_quantity, 'totalprice' => $s_total, 'product' => $product, 'cart_id' => $add->id, 'quantity' => $add->quantity];
                else:
                    $this->error_code = 99;
                    $this->message = "Some Error Found, please try again";
                    $this->result = [];
                endif;
            }
        }
        $response = $this->makeJson();
        return $response;
    }

    public function updatecart(Request $request) {
        $user_data = $request->session()->get('user_data');
        if ($user_data) {
            $customer_id = $user_data['customer_id'];
        } else {
            $customer_id = $_COOKIE['customer_cookie'];
        }

        $cartRow = Cart::find($request->input('id'));

        if ($cartRow->quantity == 1 && $request->input('type') == 'minus') {
            $this->error_code = 201;
            $this->message = "Minimum quantity reached";
            $this->result = [];
        } else {
            if ($request->input('type') == 'plus') {
                $quantity = $request->input('quantity') + 1;
            } elseif ($request->input('type') == 'minus') {
                $quantity = $request->input('quantity') - 1;
            }
            $product = Product::find($cartRow->product_id);
            $productPrice=$product->price - (($product->discount_percent * $product->price) / 100);
            if ($quantity > $product->quantity) {
                $this->error_code = 202;
                $this->message = "Maximum quantity reached";
                $this->result = [];
            } else {
                $updateArr = [
                    'quantity' => $quantity
                ];
                $update = Cart::find($request->input('id'))->update($updateArr);
                if ($update) {
                    //gettimg sub total
                    $usercart = Cart::where(['customer_id' => $customer_id])->get();
                    if ($usercart):
                        $s_total = '0';
                        $t_quantity = '0';
                        foreach ($usercart as $key => $row):
                            $productInfo = \App\Model\Product::where(['id' => $row->product_id])->first();
                            $price=$productInfo->price - (($productInfo->discount_percent * $productInfo->price) / 100);
                            $s_total = $s_total + ($row->quantity * $price);
                            $t_quantity = $t_quantity + $row->quantity;
                        endforeach;
                    endif;

                    $this->error_code = 200;
                    $this->message = "Cart has been updated";
                    $this->result = ['s_total' => $s_total, 't_quantity' => $t_quantity, 'totalprice' => $quantity * $productPrice];
                } else {
                    $this->error_code = 99;
                    $this->message = "Some Error Found, please try again";
                    $this->result = [];
                }
            }
        }
        $response = $this->makeJson();
        return $response;
    }

    public function addtowishlist(Request $request) {
        $user_data = $request->session()->get('user_data');
        $product_id = $request->input('product_id');

        $isExist = Wishlist::where(['customer_id' => $user_data['customer_id'], 'product_id' => $product_id])->exists();
        if ($isExist) {
            $this->error_code = 201;
            $this->message = "Product already in your wishlist";
        } else {
            if (!empty($user_data)) {
                $insertArr = [
                    'customer_id' => $user_data['customer_id'],
                    'product_id' => $product_id,
                    'status' => '1'
                ];
                $update = Wishlist::create($insertArr);
                if ($update):
                    $this->error_code = 200;
                    $this->message = "Product added to wishlist";
                else:
                    $this->error_code = 99;
                    $this->message = "Some Error Found, please try again";
                endif;
            }else {
                $this->error_code = 201;
                $this->message = "Please login first";
            }
        }
        $response = $this->makeJson();
        return $response;
    }

    public function removefromwishlist(Request $request) {
        $user_data = $request->session()->get('user_data');

        $id = $request->input('id');

        $isExist = Wishlist::where(['id' => $id])->exists();
        if ($isExist) {
            $wishlist = Wishlist::find($id);
            $wishlist->delete();

            $this->error_code = 200;
            $this->message = "Product removed from wishlist";
        } else {
            $this->error_code = 99;
            $this->message = "Some Error Found, please try again";
        }
        $response = $this->makeJson();
        return $response;
    }

    public function getaddress(Request $request) {
        $user_data = $request->session()->get('user_data');

        $address_id = $request->input('address_id');

        $address = Address::where(['id' => $address_id])->first();
        if ($address) {
            $this->error_code = 200;
            $this->message = "Success";
            $this->result = $address;
        } else {
            $this->error_code = 99;
            $this->message = "Some Error Found, please try again";
            $this->result = [];
        }
        $response = $this->makeJson();
        return $response;
    }

    public function delete_address(Request $request) {
        $id = $request->input('address_id');

        $isExist = Address::where(['id' => $id])->exists();
        if ($isExist) {
            $address = Address::find($id);
            $address->delete();

            $this->error_code = 200;
            $this->message = "Address deleted successfully";
        } else {
            $this->error_code = 99;
            $this->message = "Some Error Found, please try again";
        }
        $response = $this->makeJson();
        return $response;
    }

    public function removefromcart(Request $request) {
        $id = $request->input('id');

        $isExist = Cart::where(['id' => $id])->exists();
        if ($isExist) {
            $cart = Cart::find($id);
            $cart->delete();

            $user_data = $request->session()->get('user_data');
            if ($user_data) {
                $customer_id = $user_data['customer_id'];
            } else {
                $customer_id = $_COOKIE['customer_cookie'];
            }
            $usercart = Cart::where(['customer_id' => $customer_id])->get();
            if ($usercart):
                $cartArr = [];
                $s_total = '0';
                $t_quantity = '0';
                foreach ($usercart as $key => $row):
                    $productInfo = \App\Model\Product::where(['id' => $row->product_id])->first();
                    $cartArr[$key] = $row;

                    $t_quantity = $t_quantity + $row->quantity;
                    $s_total = $s_total + ($row->quantity * $productInfo->price);
                endforeach;
            endif;

            $this->error_code = 200;
            $this->message = "Product removed successfully";
            $this->result = ['s_total' => $s_total, 't_quantity' => $t_quantity];
        } else {
            $this->error_code = 99;
            $this->message = "Some Error Found, please try again";
            $this->result = [];
        }
        $response = $this->makeJson();
        return $response;
    }

    public function addreview(Request $request) {
        $user_data = $request->session()->get('user_data');

        if ($user_data) {
            $insertArr = [
                'customer_id' => !empty($user_data) ? $user_data['customer_id'] : '',
                'product_id' => $request->input('product_id'),
                'name' => $request->input('name'),
                'review' => $request->input('review'),
                'rating' => $request->input('rating'),
                'status' => '0'
            ];
            $productReview = ProductReview::create($insertArr);
            if ($productReview) {
                $this->error_code = 200;
                $this->message = "Reviewed successfully";
            } else {
                $this->error_code = 99;
                $this->message = "Some Error Found, please try again";
            }
        } else {
            $this->error_code = 201;
            $this->message = "Please login first";
        }
        $response = $this->makeJson();
        return $response;
    }

    public function delete_cat_brands(Request $request) {
        if (CategoryBrand::find($request->input('id'))->delete()) {
            $this->error_code = 200;
            $this->message = "Unmapped successfully";
        } else {
            $this->error_code = 99;
            $this->message = "Some Error Found";
        }
        $response = $this->makeJson();
        return $response;
    }

    public function delete_cat_attr(Request $request) {
        if (CategoryAttribute::find($request->input('id'))->delete()) {
            $this->error_code = 200;
            $this->message = "Unmapped successfully";
        } else {
            $this->error_code = 99;
            $this->message = "Some Error Found";
        }
        $response = $this->makeJson();
        return $response;
    }

    public function filterproducts(Request $request) {
        $sort_by = $request->input('sort_by');
        $min_price = $request->input('min_price');
        $max_price = $request->input('max_price');
        $category_id = $request->input('category_id');
        $attributes = $request->input('attributes');

        $sqlfilter = "select prd.id,prd.name,prd.price,prd.image,prd.category_id,prd.description from products prd where prd.status = '1' ";
        if ($min_price != '' && $max_price != '') {
            $pricefilter = "AND (prd.price >= $min_price AND prd.price <= $max_price) ";
        } else {
            $pricefilter = "";
        }
        if ($category_id != '') {
            $catfilter = "AND prd.category_id = $category_id ";
        } else {
            $catfilter = "";
        }

        $attrfilter = "";
        if ($attributes):

            $sqlfilter = "select prd.id,prd.name,prd.price,prd.image,prd.category_id,prd.description,attr.product_id,attr.attribute_id,attr.attribute_value,attr.type from products as prd LEFT JOIN product_attributes as attr on prd.id = attr.product_id where prd.status = '1' ";

            foreach ($attributes as $key => $attribute):
                $attrfilter .= " AND (attr.attribute_id = $key AND attr.attribute_value IN ('" . implode(',', $attribute) . "')) ";
            endforeach;
        else:
            $attrfilter = "";
        endif;

        if ($sort_by == 'name_asc') {
            $sortfilter = "order by prd.name asc ";
        } elseif ($sort_by == 'name_desc') {
            $sortfilter = "order by prd.name desc ";
        } elseif ($sort_by == 'price_asc') {
            $sortfilter = "order by prd.price asc ";
        } elseif ($sort_by == 'price_desc') {
            $sortfilter = "order by prd.price desc ";
        } else {
            $sortfilter = "";
        }

        $sql = $sqlfilter . $attrfilter . $pricefilter . $catfilter . $sortfilter;
//        echo $sql;
//        die;
        $filtered = DB::select($sql);
//        print_r($filtered);
//        die;
        if (!empty($filtered)):
            foreach ($filtered as $key => $filter):
                $filtered[$key] = $filter;

                $imageInfo = getFile($filter->image);
                if ($imageInfo) {
                    $filtered[$key]->image = $imageInfo->file_path . $imageInfo->file_name;
                } else {
                    $filtered[$key]->image = url('/') . '/frontassets/images/no_img.png';
                }
            endforeach;
        endif;
//        echo "<pre>";
//        print_r($filtered);
//        die;
        if ($filtered) {
            $this->error_code = 200;
            $this->message = "Success";
            $this->result = $filtered;
        } else {
            $this->error_code = 99;
            $this->message = "Some Error Found";
            $this->result = [];
        }
        $response = $this->makeJson();
        return $response;
    }

    public function do_search(Request $request) {
        $keyword = $request->input('keyword');
        if ($keyword):
            $this->error_code = 200;
            $this->message = "Success";
            $this->result = array('keyword' => $keyword);
        else:
            $this->error_code = 99;
            $this->message = "No product found";
            $this->result = [];
        endif;
        $response = $this->makeJson();
        return $response;
    }

    public function approve_review(Request $request) {
        $id = $request->input('id');
        $update = ProductReview::where(['id' => $id])->update(['status' => '1']);
        if ($update) {
            $this->error_code = 200;
            $this->message = "Review Approved Successfully";
        } else {
            $this->error_code = 99;
            $this->message = "Some error found, please try again";
        }
        $response = $this->makeJson();
        return $response;
    }

    public function disapprove_review(Request $request) {
        $id = $request->input('id');
        $update = ProductReview::where(['id' => $id])->update(['status' => '2']);
        if ($update) {
            $this->error_code = 200;
            $this->message = "Review Disapproved Successfully";
        } else {
            $this->error_code = 99;
            $this->message = "Some error found, please try again";
        }
        $response = $this->makeJson();
        return $response;
    }

    public function getproductsuggestion(Request $request) {
        $exists = Product::where('name', 'like', '%' . $request->input('keyword') . '%')->exists();
        if ($exists) {
            $products = Product::where('name', 'like', '%' . $request->input('keyword') . '%')->get();
            if ($products) {
                $this->error_code = 200;
                $this->message = "Success";
                $this->result = $products;
            } else {
                $this->error_code = 99;
                $this->message = 'Some error found, please try again';
            }
        } else {
            $this->error_code = 99;
            $this->message = 'Some error found, please try again';
        }
        $response = $this->makeJson();
        return $response;
    }

    //TO Create JSON Response
    private function makeJson() {
        return response()->json([
                    'error' => $this->error,
                    'error_code' => $this->error_code,
                    'message' => $this->message,
                    'result' => $this->result
        ]);
    }

}
