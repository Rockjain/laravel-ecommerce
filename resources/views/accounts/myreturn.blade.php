@extends('layouts.account')
@section('content')
<div class="container mtb_30">

    <div class="row">
        <div class="col-sm-4 col-md-3" id="column-left">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="panel panel-default account-sidebar">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                     <?php
                                 $session = Session::get('user_data');
                                    if (!empty($session)):
                                        $customerInfo = getCustomer($session['customer_id']);
                                        if ($customerInfo):
                                            $fileInfo = getFile($customerInfo->photo);
                                            if ($fileInfo) {
                                                $photo = $fileInfo->file_path . $fileInfo->file_name;
                                            } else {
                                                $photo = url('/') . '/frontassets/images/no_img.png';
                                            }
                                        endif;
                                    endif;
                                    ?>
                                    <div class="pull-left"><p><img src="<?= $photo; ?>" alt="user" /></p></div>
                                    <div class="pull-left"><p>Welcome<br/> <strong><?= $customerInfo->firstname . ' ' . $customerInfo->lastname; ?></strong></p>
                                        <p>ID: <strong><?= 'AAIMA' . $customerInfo->id; ?></strong></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body account-sidebar-menu">
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <ul class="list-group">
                                        <li class="list-group-item"><a href="/myaccount"<i class="fa fa-user"></i> My Profile <span class="badge"><i class="fa fa-angle-right"></i></span></a></li>
                                        <li class="list-group-item"><a href="/myaddress"><i class="fa fa-map-marker"></i> Manage Addresses <span class="badge"><i class="fa fa-angle-right"></i></span></a></li>
                                        <li class="list-group-item"><a href="/myreward"><i class="fa fa-trophy"></i> My Rewards <span class="badge"><i class="fa fa-angle-right"></i></span></a></li>
                                         <li class="list-group-item"><a href="/mybusinessassociate"><i class="fa fa-sitemap"></i> My Business Associate <span class="badge"><i class="fa fa-angle-right"></i></span></a></li>
                                        <li class="list-group-item"><a href="/mywishlist"><i class="fa fa-heart"></i> My Wishlist <span class="badge"><i class="fa fa-angle-right"></i></span></a></li>
                                        <li class="list-group-item"><a href="/myorder"><i class="fa fa-list"></i> My Orders <span class="badge"><i class="fa fa-angle-right"></i></span></a></li>
                                        <li class="list-group-item"><a href="/myreturn"  class="active">><i class="fa fa-retweet"></i> My Returns <span class="badge"><i class="fa fa-angle-right"></i></span></a></li>
                                        <li class="list-group-item"><a href="/logoutuser" onclick="return confirm('Are you sure, you want to logout ?')" class="logout"><i class="fa fa-power-off"></i> Logout</a></li>
                                    </ul> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-8 col-md-9" id="column-right">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="panel panel-default account-content order-content">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <h3>My Returns History</h3>
                                </div>
                            </div>

                        </div>

                        <div class="panel-body order-history">


                            <div class="row">
                                <div class="col-sm-12 col-md-12">

                                    <!--////////order start//////--->

                                    <div class="panel panel-default">
                                        <div class="panel-heading">

                                            <div class="row">
                                                <div class="col-xs-6 col-sm-6 col-md-9">
                                                    <a href="order-details.php" class="order-number">OD113801264501183000</a>
                                                </div>
                                                <div class="col-xs-6 col-sm-6 col-md-3">
                                                    <a href="#" class="order-help"><i class="fa fa-question"></i> Need Help</a>
                                                    <a href="#" class="order-track"><i class="fa fa-map-marker"></i> Track</a>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="panel-body order-short-info">

                                            <div class="row">
                                                <div class="col-xs-4 col-sm-2 col-md-2">
                                                    <p class="text-center"><a href="product_detail_page.php"><img src="<?= url('/'); ?>/frontassets/images/product/product2.jpg"/></a></p>
                                                </div>
                                                <div class="col-xs-8 col-sm-6 col-md-6">
                                                    <h5 class="order-item-name"><a href="product_detail_page.php">Mi 20000 mAh Power Bank (PLM06ZM, 2i)</a></h5>
                                                    <p class="order-item-attribute">Color: Black</p>
                                                    <p class="order-item-attribute">Size: Small</p>
                                                    <h4 class="order-item-price">&#x20B9; 1269</h4>
                                                </div>
                                                <div class="col-xs-12 col-sm-4 col-md-4">
                                                    <p class="order-item-attribute order-status">Return Processing</p>
                                                    <p class="order-item-attribute">Return policy ended on Nov 18th '18</p>
                                                    <p class=""><strong>Rewards Points:</strong> 0</p>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="panel-footer">

                                            <div class="row">
                                                <div class="col-xs-6 col-sm-6 col-md-9">
                                                    <p>Return Request On Fri, Nov 2nd '18</p>
                                                </div>
                                                <div class="col-xs-6 col-sm-6 col-md-3">
                                                    <p class="text-right">Order Total <strong>&#x20B9; 1269</strong></p>
                                                </div>
                                            </div>

                                        </div>

                                    </div>

                                    <!--////////order Ends//////--->


                                    <!--////////order start//////--->

                                    <div class="panel panel-default">
                                        <div class="panel-heading">

                                            <div class="row">
                                                <div class="col-xs-6 col-sm-6 col-md-9">
                                                    <a href="order-details.php" class="order-number">OD113801264501183000</a>
                                                </div>
                                                <div class="col-xs-6 col-sm-6 col-md-3">
                                                    <a href="#" class="order-help"><i class="fa fa-question"></i> Need Help</a>
                                                    <a href="#" class="order-track"><i class="fa fa-map-marker"></i> Track</a>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="panel-body order-short-info">

                                            <div class="row">
                                                <div class="col-xs-4 col-sm-2 col-md-2">
                                                    <p class="text-center"><a href="product_detail_page.php"><img src="<?= url('/'); ?>/frontassets/images/product/product2.jpg"/></a></p>
                                                </div>
                                                <div class="col-xs-8 col-sm-6 col-md-6">
                                                    <h5 class="order-item-name"><a href="product_detail_page.php">Mi 20000 mAh Power Bank (PLM06ZM, 2i)</a></h5>
                                                    <p class="order-item-attribute">Color: Black</p>
                                                    <p class="order-item-attribute">Size: Small</p>
                                                    <h4 class="order-item-price">&#x20B9; 1269</h4>
                                                </div>
                                                <div class="col-xs-12 col-sm-4 col-md-4">
                                                    <p class="order-item-attribute order-status">Returned completed on 15 Dec 2018</p>
                                                    <p class="order-item-attribute">Return policy ended</p>
                                                    <p class=""><strong>Rewards Points:</strong> 0</p>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="panel-footer">

                                            <div class="row">
                                                <div class="col-xs-6 col-sm-6 col-md-9">
                                                    <p>Return Request, Nov 2nd '18</p>
                                                </div>
                                                <div class="col-xs-6 col-sm-6 col-md-3">
                                                    <p class="text-right">Order Total <strong>&#x20B9; 1269</strong></p>
                                                </div>
                                            </div>

                                        </div>

                                    </div>

                                    <!--////////order Ends//////--->


                                </div>
                            </div>



                        </div>
                    </div>
                </div>

            </div>
        </div>


    </div>

</div>
@endsection