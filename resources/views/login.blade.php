@extends('layouts.default')
@section('content')
<div class="container fullwidth">
    <div class="row">
        <div class="col-md-10 col-md-offset-1 white-bg nopadding mtb_30" id="">
            <div id="column-left" class="col-sm-6 col-md-6 col-lg-6 hidden-xs nopadding">
                <a href="#"><img src="<?= url('/'); ?>/frontassets/images/login-page.jpg" alt="Left Banner" class="img-responsive" /></a> 
            </div>
            <div id="" class="col-sm-6 col-md-6 col-lg-6">
                <!-- contact  -->
                <div class="panel-login pt_30">
                    <div class="panel-heading">
                        <h3 class="title-heading text-center">Login</h3>
                    </div>
                    @include('partials.errors')
                    @include('partials.success')
                    <div class="panel-body">
                        <form id="login-form" action="/loginuser" method="post">
                            {{csrf_field()}}
                            <div class="form-group">
                                <input type="text" name="username" tabindex="1" class="form-control" placeholder="Your Business Associate Id" minlength="6" maxlength="100" required>
                            </div>
                            <div class="form-group">
                                <input type="password" name="password" tabindex="2" class="form-control" placeholder="Password" required>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <input type="hidden" name="id" value="<?= $id; ?>">
                                        <input type="submit" tabindex="4" class="form-control btn btn-login" value="Log In">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="text-center">
                                            <a href="/forgetpass" tabindex="5" class="forgot-password">Forgot Password?</a>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="text-center">
                                            <a href="/register" tabindex="5" class="forgot-password">Register Now</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection