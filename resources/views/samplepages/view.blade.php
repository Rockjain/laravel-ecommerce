<meta name="csrf-token" content="{{ csrf_token() }}">
@extends('layouts.admin')
@section('content')
<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">

            <!-- START Category DATATABLE -->
            <div class="panel panel-default">
                <div class="panel-heading">                                
                    <h3 class="panel-title">All Pages</h3>                         
                </div>
                <div class="panel-body">
                    <table class="table datatable">
                        <thead>
                            <tr>
                                <th>Sr.</th>
                                <th>Page Title</th>
                                <th>Url_Slug</th>
                                <th>Description</th>
                                <th>M_Title</th>
                                 <th>M_Keyword</th>
                                <th>M_Description</th>
                                <th>Priority</th>
                                 <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $sr=1;?>
                            @if($pages)
                            @foreach($pages as $key=>$value)
                            <tr>
                                <td>{{ $sr}}</td>
                                <td>{{$value->title}}</td>
                                <td>{{$value->url_slug}}</td>
                                <td data-toggle="tooltip" title="{{$value->description}}" >{{substr($value->description,0,30)}}</td>
                                <td>{{$value->meta_title}}</td>
                                <td>{{$value->meta_keyword}}</td>
                                <td style="max-width: 50px;" data-toggle="tooltip" title="{{$value->meta_description}}" >{{substr($value->meta_description,0,30)}}</td>
                                <td>{{$value->priority}}</td>
                              
                                <td style="min-width: 100px;">
                                    <a href="/admin/pages/edit/{{$value->id}}" data-toggle="tooltip" title="Edit" class="btn btn-info btn-rounded text-center"><i class="fa fa-edit"></i></a>  
                                    <label class="switch" style="top: 12px">
                                        <input type="checkbox" <?php
                                        if ($value->status == 1) {
                                            echo "checked";
                                        }
                                        ?> id="{{$value->id}}" onchange="checkStatus(this);" id="switch-{{$key}}}">
                                        <span class="slider round"></span>
                                    </label>
                                    <a href="/admin/pages/delete/{{$value->id}}" data-toggle="tooltip" title="Delete" class="btn btn-danger btn-rounded text-center" onclick="if (confirm('Delete selected Page?')){return true;}else{event.stopPropagation(); event.preventDefault();};" ><i class="fa fa-minus-circle"></i></a>  
                                </td>
                            </tr>
                            <?php $sr++;?>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END Category DATATABLE -->
        </div>
    </div>
</div>
<script>
    function checkStatus(obj) {
        var id = $(obj).attr("id");
        var checked = $(obj).is(':checked');
        if (checked == true) {
            var status = 1;
        } else {
            var status = 0;
        }
        $.ajax({
            url: '/admin/change_status',
            type: 'post',
            dataType: 'json',
            data: {method: 'changePageStatus', status: status, id: id},
            cache: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
        }).done(function (response) {
            if (response.status == '1') {

Command: toastr["success"]("Status has been updated successfully!")

toastr.options = {
  "closeButton": false,
  "debug": false,
  "newestOnTop": false,
  "progressBar": false,
  "positionClass": "toast-top-right",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}
            } else {
                alert("Some error occured, please try again");
            }
        });
    }
</script>
@endsection