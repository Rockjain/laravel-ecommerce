@extends('layouts.admin')
@section('content')
<style type="text/css">

    fieldset {    border: 1px solid #1caf9a!important;
                  padding: 20px!important;
                  margin: 20px!important;
    }
    legend {
        padding: 0.2em 0.5em!important;
        border:1px solid #1caf9a!important;
        color:#1caf9a!important;
        font-size: 21px!important;
        width:10%!important;
    }

</style>
<div class="page-content-wrap">
    @include('partials.errors')
    @include('partials.success')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-plus"></i> Edit Product</h3>
                </div>
                <div class="tabs">
                    <form action="{{route('products.update',[$productInfo->id])}}" method="post" enctype="multipart/form-data" id="productForm" class="form-horizontal">
                        {{csrf_field()}}
                        <input type="hidden" name="_method" value="put">
                        <ul class="nav nav-tabs">
                          
                        </ul>
                        <div class="tab-content panel-body">
                            <div class="tab-pane active" id="tab-general">
                                <div class="tab-content">
                                    <fieldset>
                                        <legend>General</legend>
                                        <div class="form-group required">
                                            <label class="col-sm-2 control-label">Product Name</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="name" placeholder="Product Name" class="form-control" value="{{$productInfo->name}}" required>
                                            </div>
                                        </div>
                                        <div class="form-group required">
                                            <label class="col-sm-2 control-label">Product Price</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="price" placeholder="Product Price" class="form-control floatOnly" value="{{$productInfo->price}}" required>
                                            </div>
                                        </div>
                                        <div class="form-group required">
                                            <label class="col-sm-2 control-label">Quantity</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="quantity" placeholder="Quantity" class="form-control numericOnly" value="{{$productInfo->quantity}}" required>
                                            </div>
                                        </div>
                                        <div class="form-group required">
                                            <label class="col-sm-2 control-label">Is Featured</label>
                                            <div class="col-sm-10">
                                                <select class="form-control" name="is_featured">
                                                    <option value="" selected disabled>Select</option>
                                                    <option value="1" <?= ($productInfo->is_featured == '1') ? 'selected' : ''; ?>>YES</option>
                                                    <option value="0" <?= ($productInfo->is_featured == '0') ? 'selected' : ''; ?>>NO</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group required">
                                            <label class="col-sm-2 control-label">Is Recommend</label>
                                            <div class="col-sm-10">
                                                <select class="form-control" name="is_recommend">
                                                    <option value="" selected disabled>Select</option>
                                                    <option value="1" <?= ($productInfo->is_recommend == '1') ? 'selected' : ''; ?>>YES</option>
                                                    <option value="0" <?= ($productInfo->is_recommend == '0') ? 'selected' : ''; ?>>NO</option>
                                                </select>
                                            </div>
                                        </div>

                                            <div class="form-group required">
                                            <label class="col-sm-2 control-label">Short Description</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="short_desc" value="{{$productInfo->short_desc}}" placeholder="Short Description" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Description</label>
                                            <div class="col-sm-10">
                                                <textarea name="description" class="summernote" placeholder="Description">{{$productInfo->description}}</textarea>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset>
                                        <legend  style="width:30%!important;padding-left: 50px!important;">Category & Attributes</legend>

                                        <div class="form-group required">
                                            <label class="col-sm-2 control-label" for="input-model">Category</label>
                                            <div class="col-sm-10">
                                                <input type="hidden" name="category_id" value="{{$productInfo->category_id}}">
                                                <select class="form-control" name="category_id" disabled required>
                                                    <option disabled selected>Select</option>
                                                    @if($categories)
                                                    @foreach($categories as $key=>$category)
                                                    <option value="{{$category->id}}"<?php if ($productInfo->category_id == $category->id): ?>selected<?php endif; ?>>{{$category->name}}</option>
                                                    @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group required">
                                            <label class="col-sm-2 control-label" for="input-model">Manufacturer</label>
                                            <div class="col-sm-10">
                                                <select class="form-control brand_id" name="brand_id">
                                                    <option disabled selected>Select</option>
                                                    @if($brands)
                                                    @foreach($brands as $key=>$brand)
                                                    <option value="{{$brand->brand_id}}"<?php if ($productInfo->brand_id == $brand->brand_id): ?>selected<?php endif; ?>>{{$brand->brandDetail->title}}</option>
                                                    @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                        <div class="attributediv">
                                            <div class="attribute_div">
                                                <div class="row">
                                                    @if($productAttributes)
                                                    @foreach($productAttributes as $key1=>$attr)
                                                    <div class="col-lg-4">
                                                        <div class="col-sm-4">
                                                            <label><a href="javascript:void(0)" onclick="removeProductAttribute(this,<?= $attr->id; ?>)"><i class="fa fa-trash-o"></i></a> {{$attr->attributeDetail->name}} </label>
                                                        </div>
                                                        <div class="col-sm-8">
                                                            @if($attr->type == 'INPUT')
                                                            <input type="text" class="form-control" name="values[2][attr_val]" value="{{$attr->attribute_value}}" placeholder="Enter Processor">
                                                            @elseif($attr->type == 'TOGGLE')
                                                            <select class="form-control" name="values[{{$key1}}][attr_val]">
                                                                <option value="YES"<?php if ($attr->attribute_value == 'YES'): ?>selected<?php endif; ?>>YES</option>
                                                                <option value="NO"<?php if ($attr->attribute_value == 'NO'): ?>selected<?php endif; ?>>NO</option>
                                                            </select>
                                                            @else
                                                            <select class="form-control" name="values[{{$key1}}][attr_val]">
                                                                @if($attr->attributeDetail->attributeValues)
                                                                @foreach($attr->attributeDetail->attributeValues as $key2=>$detail)
                                                                <option value="{{$detail->id}}"<?php if ($attr->attribute_value == $detail->id): ?>selected<?php endif; ?>>{{$detail->attribute_value}}</option>
                                                                @endforeach
                                                                @endif
                                                            </select>
                                                            @endif
                                                        </div>
                                                        <input type="hidden" class="form-control" name="values[{{$key1}}][attr_id]" value="{{$attr->id}}">
                                                        <input type="hidden" class="form-control" name="values[{{$key1}}][attr_type]" value="{{$attr->type}}">
                                                    </div>
                                                    @endforeach
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset>
                                        <legend  style="width:30%!important;padding-left:60px!important;">Discount and Tax</legend>
                                        <div class="form-group required">
                                            <label class="col-sm-2 control-label">Tax (%)</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="tax" placeholder="Tax" class="form-control floatOnly percentage" value="{{$productInfo->tax}}">
                                            </div>
                                        </div>
                                        <div class="form-group required">
                                            <label class="col-sm-2 control-label">Discount (%)</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="discount_percent" placeholder="Discount" value="{{$productInfo->discount_percent}}" class="form-control floatOnly percentage">
                                            </div>
                                        </div>
                                        <!-- <div class="form-group required">
                                            <label class="col-sm-2 control-label">Quantity </label>
                                            <div class="col-sm-10">
                                                <input type="text" name="discount_quantity" placeholder="Quantity" value="{{$productInfo->discount_quantity}}" class="form-control numericOnly">
                                            </div>
                                        </div> -->
                                        <div class="form-group required">
                                            <label class="col-sm-2 control-label">Start Date</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="discount_start_date" placeholder="mm/dd/yyyy" value="{{date('d-m-Y',strtotime($productInfo->discount_start_date))}}" class="form-control datepicker" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group required">
                                            <label class="col-sm-2 control-label">End Date</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="discount_end_date" placeholder="mm/dd/yyyy" value="{{date('d-m-Y',strtotime($productInfo->discount_end_date))}}" class="form-control datepicker" readonly>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset>
                                        <legend  style="padding-left:28px!important;">Data</legend>
                                        <div class="form-group required">
                                            <label class="col-sm-2 control-label">Sku Code</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="sku_code" placeholder="Sku Code" class="form-control" value="<?= $productInfo->sku_code != '' ? $productInfo->sku_code : 'SKU' . time(); ?>">
                                            </div>
                                        </div>
                                        <div class="form-group required">
                                            <label class="col-sm-2 control-label">Hsn Code </label>
                                            <div class="col-sm-10">
                                                <input type="text" name="hsn_code" placeholder="Hsn Code" class="form-control" value="<?= $productInfo->hsn_code != '' ? $productInfo->hsn_code : ''; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group required">
                                            <label class="col-sm-2 control-label">Other Code </label>
                                            <div class="col-sm-10">
                                                <input type="text" name="other_code" placeholder="Other Code" class="form-control" value="<?= $productInfo->other_code != '' ? $productInfo->other_code : ''; ?>">
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset>
                                        <legend style="padding-left:20px!important;">Image</legend>
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <td class="text-left">Image</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td class="text-left">
                                                            <span class="file-input">
                                                                <div class="file-preview">
                                                                    <!--<div class="close fileinput-remove text-right">×</div>-->
                                                                    <div class="file-preview-thumbnails">
                                                                        <div class="file-preview-frame" >
                                                                            <img class="appendFirstImage" src="{{!empty($productInfo->fileDetail)?$productInfo->fileDetail->file_path.$productInfo->fileDetail->file_name:url('/').'/frontassets/images/no_img.png'}}" title="{{$productInfo->name}}" alt="{{$productInfo->name}}" style="width:auto;height:160px;">
                                                                        </div>
                                                                    </div>
                                                                    <div class="clearfix"></div>   
                                                                </div>
                                                                <div class="input-group ">
                                                                    <div tabindex="-1" class="form-control file-caption  kv-fileinput-caption" title="">
                                                                        <span class="glyphicon glyphicon-file kv-caption-icon" style="display: inline;"></span>                                                                
                                                                    </div>
                                                                    <div class="input-group-btn">
                                                                        <div class="btn btn-primary btn-file"> 
                                                                            <i class="glyphicon glyphicon-folder-open"></i> &nbsp;Browse …
                                                                            <input type="file" name="image" onchange="readFirstImgUrl(this)">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </span>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="table-responsive">
                                            <table id="images" class="table table-striped table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <td class="text-left">Additional Images</td>
                                                        <td class="text-center">Preview</td>
                                                    </tr>
                                                </thead>
                                                <tbody class="appendnewrow">
                                                    @if($productInfo->productImages)
                                                    @foreach($productInfo->productImages as $key=>$image)
                                                    <tr>  
                                                        <td class="text-left">
                                                            <input type="file" name="product_images[{{$image->image}}]" onchange="readProUrl(this,<?= $key; ?>);">
                                                        </td>  
                                                        <td align="center">
                                                            <img class="appendImage{{$key}}" src="{{$image->fileDetail->file_path.$image->fileDetail->file_name}}" style="height: 50px;width: 75px">
                                                        </td>
                                                        <td class="text-left">
                                                            <button type="button" onclick="removeProImage(this,{{$image->id}})" title="Remove" class="btn btn-danger">
                                                                <i class="fa fa-minus-circle"></i>
                                                            </button>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                    @endif
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <td colspan="2"></td>
                                                        <td class="text-left">
                                                            <button type="button" onclick="addNextImage(this);" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Add Image">
                                                                <i class="fa fa-plus-circle"></i>
                                                            </button>
                                                        </td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">                                                                        
                            <button type="submit" class="btn btn-info pull-right">Publish <span class="fa fa-save"></span></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $("#productForm").validate();
    function readFirstImgUrl(input) {
    if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function (e) {
    $('.appendFirstImage')
            .attr('src', e.target.result)
            .width(100);
    };
    reader.readAsDataURL(input.files[0]);
    }
    }
    $.ajaxSetup({
    headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
    });
    function addNextImage(obj) {
    var str = '<tr id="image-row">'
            + '<td class="text-left">'
            + '<input type="file" name="more_product_images[]" onchange="readProUrl(this,1);">'
            + '</td>'
            + '<td align="center">'
            + '<img class="appendImage{{$key}}" src="<?= url('/') . '/frontassets/images/upload_icon.png'; ?>" style="height: 50px;width: 75px">'
            + '</td>'
            + '<td class="text-left">'
            + '<button type="button" onclick="" title="Remove" class="btn btn-danger">'
            + '<i class="fa fa-minus-circle"></i>'
            + '</button>'
            + '</td>'
            + '</tr>';
    $(".appendnewrow").append(str);
    }

    function removeProImage(obj, id){
    if (confirm("Are you sure you want to remove this image ?")){
    $.ajax({
    url:'/admin/remove_product_image',
            type:'post',
            dataType:'json',
            data:{id:id},
            cache:false
    }).done(function(response){
    if (response.status == '1'){
    $(obj).parent().parent("tr").remove();
    }
    });
    }
    }

    function removeProductAttribute(obj, id){
    if (confirm("Are you sure you want to remove this attribute ?")){
    $.ajax({
    url:'/admin/remove_product_attribute',
            type:'post',
            dataType:'json',
            data:{id:id},
            cache:false
    }).done(function(response){
    if (response.status == '1'){
    $(obj).parent().parent().parent(".col-lg-4").remove();
    }
    });
    }
    }


    function fetchAttributes(objRef) {
    var obj = $(objRef);
    var category_id = obj.find('option:selected').val();
    $.ajax({
    url: '/admin/get_attributes',
            data: {category_id: category_id},
            type: 'POST',
            dataType: 'json',
            cache: false,
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
    }).done(function (response) {
    var string = '<div class="row">';
    if (response.status == '1') {
    var k = 0;
    $.each(response.data, function (i, v) {

    string += '  <div class="col-lg-4">'
            + '<div class="col-sm-4">'
            + '<label class="">' + v.attribute_name + '</label>    '
            + '</div>'
            + '<div class="col-sm-8">';
    if (v.attribute_type === "INPUT") {
    string += '   <input type="text" class="form-control" name="values[' + k + '][attr_val]" placeholder="Enter ' + v.attribute_name + '">';
    } else if (v.attribute_type === "TOGGLE") {

    string += '<select class="form-control" name="values[' + k + '][attr_val]">';
    string += "<option value='YES'>YES</option>";
    string += "<option value='NO'>NO</option>";
    string += '</select>';
    } else {

    string += '<select class="form-control" name="values[' + k + '][attr_val]">';
    $.each(v.attribute_values, function (index, value) {
    string += "<option value='" + value.id + "'>" + value.attribute_value + "</option>";
    });
    string += '</select>';
    }

    string += '</div>';
    /*
     * hidden fields like attribtue id is stored at the end of the div
     */
    string += '   <input type="hidden" class="form-control" name="values[' + k + '][attr_id]" value=' + v.attribute_id + '>';
    string += '   <input type="hidden" class="form-control" name="values[' + k + '][attr_type]" value=' + v.attribute_type + '>';
    string += '</div>';
    k++;
    });
    string += '</div>';
    $(".attribute_div").html(string);
    } else {
    string += "No Attribute Found";
    $(".attribute_div").html(string);
    }
    });
    }

    function fetchCatBrands(objRef) {
    var $obj = $(objRef);
    var category_id = $obj.find('option:selected').val();
    $.ajax({
    url: '/admin/get_brands',
            data: {category_id: category_id},
            type: 'POST',
            dataType: 'json',
            cache: false,
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
    }).done(function (response) {
    var string = "";
    if (response.status == '1') {
    string += "<option disabled selected>Select Brand</option>";
    $.each(response.data, function (i, v) {
    string += "<option value='" + v.brand_id + "'>" + v.name + "</option>";
    });
    $(".brand_id").html(string);
    } else {
    string += "<option value=''>No Brand Found</option>";
    $(".brand_id").html(string);
    }
    });
    }
</script>
@endsection