<?php
if(!empty($pages)){
  $id = $pages[0]->id;
  $title = $pages[0]->title;
  $description = $pages[0]->description;
  $priority = $pages[0]->priority;
  $status = $pages[0]->status;
  $meta_title = $pages[0]->meta_title;
  $meta_keyword = $pages[0]->meta_keyword;
  $meta_description = $pages[0]->meta_description;
  $form_type = 'update';
}else{
  $title = '';
$description = '';
$priority  = '';
$status = '';
$meta_title = '';
$meta_keyword = '';
$meta_description = '';
$form_type = 'add';
$id = '';
}
?>
<meta name="csrf-token" content="{{ csrf_token() }}">
@extends('layouts.admin')
@section('content')

<script src="https://cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>
<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Add New Page</h3>
                </div>
                @include('partials.errors')
                @include('partials.success')
                <div class="panel-body">
                    <form method="post" id="page_form" class="form-horizontal" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Page Title</label>
                            <div class="col-md-6 col-xs-12">                                                                                
                               <input type="text" name="title" class="form-control" placeholder="Page Title"  value="{{$title}}" required/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Description</label>
                            <div class="col-md-6 col-xs-12">  
                                <textarea class="form-control summernote" id="desc" name="description" placeholder="Description" rows="4" required="">{{$description}}</textarea>
                            </div>
                            <input type="hidden" name="form_type" value="{{$form_type}}">
                            <input type="hidden" name="id" value="{{$id}}">
                        </div>
                         <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Page Priority</label>
                            <div class="col-md-6 col-xs-12">                                                                                
                               <input type="num" name="priority" class="form-control" min="1" max="50" value="{{$priority}}" placeholder="1 To 50" required/>
                            </div>
                        </div>
                         <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Page Status</label>
                            <div class="col-md-6 col-xs-12">                                                                                
                               <select class="form-control" name="status">
                                   <option value="1">Active</option>
                                   <option value="2">Inactive</option>
                               </select>
                            </div>
                        </div>
                       
                        
                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Meta Title</label>
                            <div class="col-md-6 col-xs-12">                                                                                
                               <input type="text" name="meta_title" class="form-control" placeholder="Meta Title" value="{{$meta_title}}" required/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Meta Keyword</label>
                            <div class="col-md-6 col-xs-12">                                                                                
                               <input type="text" name="meta_keyword" class="form-control" placeholder="Meta Keyword" value="{{$meta_keyword}}" required/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Meta Description</label>
                            <div class="col-md-6 col-xs-12">                                                                                
                               <textarea class="form-control" name="meta_description" placeholder="Meta Description" rows="4">{{$meta_description}}</textarea>
                            </div>
                        </div>
                       
                        <div class="form-group">
                            <div class="col-md-12 col-xs-12">  
                                <p class="text-center">
                                    <input type="submit" class="btn btn-info" id="submit" value="Submit Page"/> 
                                </p>
                            </div>
                        
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
<script>
    // CKEDITOR.replace('description');
     // var desc = CKEDITOR.instances.description.getData();
    $("#page_form").validate({
    rules:{
        meta_description:"required"
    },
submitHandler: function(form) {
        $.ajax({
    type: "POST",
    url: "/admin/add_sample_page",
    data: $("#page_form").serialize(),
    beforeSend:function() {
    },
     success: function(msg)
        {
        if(msg == 1){
Command: toastr["success"]("Page has been added Successfully. Redirecting you to Page lists!")

toastr.options = {
  "closeButton": false,
  "debug": false,
  "newestOnTop": false,
  "progressBar": false,
  "positionClass": "toast-top-right",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}
                 window.setTimeout(function() {
                    window.location.replace("/admin/view_sample_pages");
                 }, 3000);
}else if(msg=='updated'){
                Command: toastr["success"]("Page has been updated Successfully. Redirecting you to Page lists!")
                toastr.options = {
                  "closeButton": false,
                  "debug": false,
                  "newestOnTop": false,
                  "progressBar": false,
                  "positionClass": "toast-top-right",
                  "preventDuplicates": false,
                  "onclick": null,
                  "showDuration": "300",
                  "hideDuration": "1000",
                  "timeOut": "5000",
                  "extendedTimeOut": "1000",
                  "showEasing": "swing",
                  "hideEasing": "linear",
                  "showMethod": "fadeIn",
                  "hideMethod": "fadeOut"
                }
                 window.setTimeout(function() {
                    window.location.replace("/admin/view_sample_pages");
                 }, 3000);
            }else{
              alert('something went wrong with server! Please try again later!')
            }
        }
    });
        return false;
    },

});
   
</script>
@endsection