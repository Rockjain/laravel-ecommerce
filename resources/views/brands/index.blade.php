<meta name="csrf-token" content="{{ csrf_token() }}">
@extends('layouts.admin')
@section('content')
<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Add New Manufacturer</h3>
                </div>
                @include('partials.errors')
                @include('partials.success')
                <div class="panel-body">
                    <form method="post" id="brandForm" class="form-horizontal" action="{{route('brands.store')}}" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Brand Title</label>
                            <div class="col-md-6 col-xs-12">  
                                <input type="text" name="title" class="form-control" placeholder="Title" required/>                                       
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Link</label>
                            <div class="col-md-6 col-xs-12">  
                                <input type="text" name="link" class="form-control" placeholder="Link" required/>                                       
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Description</label>
                            <div class="col-md-6 col-xs-12">  
                                <textarea class="form-control" name="description" placeholder="Description" rows="4"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Is Featured</label>
                            <div class="col-md-6 col-xs-12">  
                                <select class="form-control" name="is_featured">
                                    <option value="1">Yes</option>
                                    <option value="0" selected>No</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12 col-xs-12"> 
                                <label class="col-md-3 col-xs-12 control-label">Upload Category Image</label>
                                <p><code>Recommended size 1660 X 550</code> </p>
                                <div action="files" class="">
                                    <label class="">
                                        <img src="<?= url('/'); ?>/adminassets/img/upload-icon.png" class="appendImage" width="75" height="75">
                                        <input type="file" name="image" onchange="readURL(this);" accept="image/*" style="display: none"/>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12 col-xs-12">  
                                <p class="text-center">
                                    <input type="submit" class="btn btn-info" value="Submit Category"/> 
                                </p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <!-- START Category DATATABLE -->
            <div class="panel panel-default">
                <div class="panel-heading">                                
                    <h3 class="panel-title">All Manufacturers</h3>                         
                </div>
                <div class="panel-body">
                    <table class="table datatable">
                        <thead>
                            <tr>
                                <th>Title</th>
                                <th>Link</th>
                                <th>Image</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($brands)
                            @foreach($brands as $key=>$brand)
                            <tr>
                                <td>{{$brand->title}}</td>
                                <td>{{$brand->link}}</td>
                                <td><img src="{{!empty($brand->fileDetail)?$brand->fileDetail->file_path.$brand->fileDetail->file_name:url('/').'/frontassets/images/no_img.png'}}" width="75" height="50"></td>
                                <td>
                                    <a href="/admin/brands/{{$brand->id}}/edit" data-toggle="tooltip" title="Edit" class="btn btn-info btn-rounded text-center"><i class="fa fa-edit"></i></a>  
                                    <label class="switch" style="top: 12px">
                                        <input type="checkbox" <?php
                                        if ($brand->status == '1') {
                                            echo "checked";
                                        }
                                        ?> id="{{$brand->id}}" onchange="checkStatus(this);" id="switch-{{$key}}}">
                                        <span class="slider round"></span>
                                    </label>
                                </td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END Category DATATABLE -->
        </div>
    </div>
</div>
<script>
    function checkStatus(obj) {
        var id = $(obj).attr("id");
        var checked = $(obj).is(':checked');
        if (checked == true) {
            var status = 1;
        } else {
            var status = 0;
        }
        $.ajax({
            url: '/admin/change_status',
            type: 'post',
            dataType: 'json',
            data: {method: 'changeBrandStatus', status: status, id: id},
            cache: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
        }).done(function (response) {
            if (response.status == '1') {
Command: toastr["success"]("Status updated successfully")

                        toastr.options = {
                            "closeButton": false,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": false,
                            "positionClass": "toast-top-right",
                            "preventDuplicates": false,
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "timeOut": "5000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        }
            } else {
                Command: toastr["error"]("Some error found, please try again")

                        toastr.options = {
                            "closeButton": false,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": false,
                            "positionClass": "toast-top-right",
                            "preventDuplicates": false,
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "timeOut": "5000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        }
            }
        });
    }
</script>
@endsection