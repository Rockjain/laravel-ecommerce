<?php

namespace App\Http\Controllers;

use App\Model\Brand;
use Illuminate\Http\Request;

class BrandsController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $brands = Brand::get();
//        echo "<pre>";print_r($brands);die;
        return view('brands.index', ['brands' => $brands]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $addArr = [
            'title' => $request->input('title'),
            'link' => $request->input('link'),
            'description' => $request->input('description'),
            'is_featured' => $request->input('is_featured'),
            'status' => '1',
        ];
        $path = "/images/brandimages/";
        $check = $this->uploadFile($request, 'image', $path);
        if ($check):
            $nameArray = explode('.', $check);
            $ext = end($nameArray);

            $insertParam['file_path'] = url('/') . $path;
            $insertParam['file_name'] = $check;
            $insertParam['file_type'] = $ext;

            $getImgData = $this->insertFile($insertParam);

            $addArr['image'] = $getImgData['id'];

        endif;

        $return = Brand::create($addArr);
        if ($return) {
            return redirect()->route('brands.index')->with('success', 'Brand created successfully');
        }
        return back()->withInput()->with('error', 'Error creating new brand');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function show(Brand $brand) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function edit(Brand $brand) {
        $brand = Brand::find($brand->id);
        return view('brands.edit', ['brand' => $brand]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Brand $brand) {
        $updateArr = [
            'title' => $request->input('title'),
            'link' => $request->input('link'),
            'description' => $request->input('description'),
            'is_featured' => $request->input('is_featured'),
        ];

        $path = "/images/brandimages/";
        $check = $this->uploadFile($request, 'image', $path);
        if ($check):
            $nameArray = explode('.', $check);
            $ext = end($nameArray);

            $insertParam['file_path'] = $path;
            $insertParam['file_name'] = $check;
            $insertParam['file_type'] = $ext;

            $getImgData = $this->insertFile($insertParam);

            $updateArr['image'] = $getImgData['id'];

        endif;

        $return = Brand::where('id', $brand->id)->update($updateArr);

        if ($return) {
            return redirect()->route('brands.index')->with('success', 'Brand updated successfully');
        }
        return back()->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function destroy(Brand $brand) {
        //
    }

}
