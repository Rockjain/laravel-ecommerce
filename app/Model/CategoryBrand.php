<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CategoryBrand extends Model {

	protected $table = 'category_brand_mapping';
	protected $fillable = array(
		'category_id',
		'brand_id',
		'is_filter',
		'status',
		'created_at',
		'updated_at',
	);

	public function categoryDetail() {
		return $this->belongsTo('App\Model\Category', 'category_id');
	}

	public function brandDetail() {
		return $this->belongsTo('App\Model\Brand', 'brand_id');
	}

}
