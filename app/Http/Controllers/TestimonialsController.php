<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class TestimonialsController extends Controller {

    public function index() {
        return view('testimonials.index');
    }

    public function view() {
        $testimonials = DB::table('testimonials')->select('*')->get();
        return view('testimonials.view', ['testimonials' => $testimonials]);
    }

    public function insert(Request $request) {
        extract($_POST);
        if ($form_type == 'add') {
            $path = "/images/testimonialsimages/";
            $check = $this->uploadFile($request, 'image', $path);
            if ($check):
                $nameArray = explode('.', $check);
                $ext = end($nameArray);

                $insertParam['file_path'] = $path;
                $insertParam['file_name'] = $check;
                $insertParam['file_type'] = $ext;

                $getImgData = $this->insertFile($insertParam);

                $adArr['image'] = $getImgData['id'];

            endif;
            $data = array('name' => $name, 'description' => $description, 'status' => $status, 'image' => $check);
            $data = DB::table('testimonials')->insert($data);
            echo"$data";
        }else if ($form_type == 'update') {
            $path = "/images/testimonialsimages/";
            $check = $this->uploadFile($request, 'image', $path);
            if ($check) {
                $nameArray = explode('.', $check);
                $ext = end($nameArray);

                $insertParam['file_path'] = $path;
                $insertParam['file_name'] = $check;
                $insertParam['file_type'] = $ext;

                $getImgData = $this->insertFile($insertParam);

                $adArr['image'] = $getImgData['id'];
            } else {
                $check = $image_update;
            }


            $data = DB::table('testimonials')->where('id', $id)->update([
                'name' => $name, 'description' => $description, 'status' => $status, 'image' => $check
            ]);
            if ($data) {
                echo 'updated';
            } else {
                echo 2;
            }
        }
    }

    public function edit(Request $request) {
        $id = $request->id;
        $testimonials = DB::table('testimonials')->select('*')->where('id', $id)->get();
        return view('testimonials.index', ['testimonials' => $testimonials]);
    }

    public function delete(Request $request) {
        $id = $request->id;
        $status = DB::table('testimonials')->delete($id);
        if ($status) {
            return redirect()->action('TestimonialsController@view');
        }
    }

}
