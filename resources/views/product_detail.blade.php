@extends('layouts.default')
@section('content')

<style type="text/css">
    .product-thumb .button-group span {

        visibility: visible!important;
    }
    .review_product{
        padding: 20px;
        border: 1px solid;
        margin-bottom: 20px;
    }
    .review_product .like{
        float: right;
    }
    .review_name{
        font-size: 25px;
        font-weight: bold;
    }
    .review_name a{

        color: #2c2f77;
    }
    .review_icon{
        float: right;
    }
    .gc-display-container {
    position: absolute;
    margin-left: -30%!important;
    width: 70%!important;
    max-height:100%!important;
    height: auto!important;

}
.img-responsive{
     max-width: 100%!important;
    max-height:100%!important;

   
}
.owl-carousel .owl-item img {
    width: auto!important;
}
.nav-tabs {
    margin-top: 10px!important;
}
.product-thumb .button-group span {
    color: transparent;
}
.product-thumb .button-group .addtocart {
    background: transparent url(../images/sprite1.png) no-repeat scroll -150px -469px;
    overflow: hidden;
    
    display: inline-block;
    text-align: center;
    cursor: pointer;
    
    transition: all 0.5s ease 0s;
}
.btn.login-button{
    padding:11px 10px;
}
.breadcrumb{
    margin-bottom: 0;
   

}
.owl-nav > div {
    height: 32px!important;
}
</style>

                <div class="row" style="margin:0;">
                    <div class="breadcrumb ptb_10">
                        <!-- <h1>Electronics</h1> -->
                        <ul>
                            <li><a>Home</a></li>
                            <li class="active">Electronics</li>
                        </ul>
                    </div>
                </div>
<div class="container fullwidth mb_10">
    <div class="row white-bg">

        <div class="col-sm-12 col-md-12 col-lg-12">

            <div class="row mt_10 ">
                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">

                </div>
            </div>
            <div class="row mt_10 ">

                <div class="col-md-5 col-lg-4">
                    <ul id="glasscase" class="gc-start">
                        <li><img src="{{!empty($productInfo->fileDetail)?$productInfo->fileDetail->file_path.$productInfo->fileDetail->file_name:url('/').'/frontassets/images/no_img.png'}}" alt="Img" /></li>
                        @if($productInfo->productImages)
                        @foreach($productInfo->productImages as $image)
                        <li><img src="{{!empty($image->fileDetail)?$image->fileDetail->file_path.$image->fileDetail->file_name:url('/').'/frontassets/images/no_img.png'}}" alt="Img" /></li>                        
                        @endforeach
                        @endif
                    </ul>

                </div>

                <div class="col-md-6 col-lg-5 prodetail caption product-thumb">
                    <h4 data-name="product_name" class="product-name"><a href="#" title="Casual Shirt With Ruffle Hem">{{$productInfo->name}}</a></h4>
                    <div class="rating">
                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                    </div>
                    <!-- <span><i class="fa fa-inr"></i> <del class="text-danger">14990</del></span>
                    <span class="price mb_10"><span class="amount"><span class="currencySymbol">&#8377; </span>{{$productInfo->price}}</span>
                    </span> -->
                    @if($productInfo->discount_percent!=0)
                    <span><i class="fa fa-inr"></i> <del class="text-danger">{{$productInfo->price}}</del></span>
                    @endif
    <span class="price mb_10"><span class="amount"><span class="currencySymbol">&#8377; </span><?= round(number_format($productInfo->price - (($productInfo->discount_percent * $productInfo->price) / 100), 2, '.', '')); ?></span>

                    </span>
                    <hr>
                    <p class="product-desc mtb_10"><?= $productInfo->short_desc; ?>
                    </p>
                    
                    <hr>
                    <p style="float: right;"><a href="#overview" style="color:#dd0016">See More Product Details</a></p>
                    <div class="clearfix"></div>
                    <div id="product">
                        <!-- <div id="socialHolder" class="form-group">
                            <div id="socialShare" class="btn-group share-group">
                                <a data-toggle="dropdown" class="btn btn-info">
                                    <i class="fa fa-share-alt fa-inverse"></i>
                                </a>
                                <button href="#" data-toggle="dropdown" class="btn btn-info dropdown-toggle share">
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a data-original-title="Facebook" rel="tooltip"  href="#" class="btn btn-facebook" data-placement="left">
                                            <i class="fa fa-facebook"></i>
                                        </a>
                                    </li>					
                                    <li>
                                        <a data-original-title="Google+" rel="tooltip"  href="#" class="btn btn-google" data-placement="left">
                                            <i class="fa fa-google-plus"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a data-original-title="Pinterest" rel="tooltip"  class="btn btn-pinterest" data-placement="left">
                                            <i class="fa fa-pinterest"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div> -->
                         @if($attribites)
                          @foreach($attribites as $key=>$value)
                          <?php
                           $attrib = DB::table('attribute_values')->where('attribute_id', '=', $value->id)->get();
                             ?>
            <div class="form-group">
            <span style="float: left;margin-top: 5px; color: #dd0016; font-weight: bold;">{{$value->name}}:</span>
            <div class="funkyradio" style="margin-left: 50px;">
               @if($attrib)
              @foreach($attrib as $key=>$data)  
            <div class="funkyradio-default">
              <input type="radio" name="radio" checked/>
              <label>{{$data->attribute_value}}</label>
            </div>
             @endforeach
             @endif
            <!-- <div class="funkyradio-primary">
              <input type="radio" name="radio" id="size2" checked />
              <label for="size2">Medium</label>
            </div>
            <div class="funkyradio-success">
              <input type="radio" name="radio" id="size3" />
              <label for="size3">Large</label>
            </div>
            <div class="funkyradio-danger">
              <input type="radio" name="radio" id="size4" />
              <label for="size4">Extra Large</label>
            </div> -->

            </div>
          </div>
          @endforeach
          @endif
                
                  <!-- <div class="form-group">
                <span style="float: left;margin-top: 5px; color: #dd0016; font-weight: bold;">Color:</span>
            <div class="funkyradio" style="margin-left: 50px;">
              <div class="funkyradio-default">
              <input type="radio" name="color" id="color1" />
              <label for="color1">Red</label>
              </div>
              <div class="funkyradio-primary">
              <input type="radio" name="color" id="color2" checked/>
              <label for="color2">Green</label>
              </div>
              <div class="funkyradio-success">
              <input type="radio" name="color" id="color3" />
              <label for="color3">Blue</label>
              </div>
              <div class="funkyradio-danger">
              <input type="radio" name="color" id="color4" />
              <label for="color4">Black</label>
              </div>

            </div>
            </div>  -->
                        <div class="form-group">
                            @if($productInfo->productAttributes)
                            @foreach($productInfo->productAttributes as $attribute)
                            <label>{{$attribute->attributeDetail->name}} :
                                <?php
                                if ($attribute->attributeDetail->type == 'SELECT'):
                                    echo $attribute->attributeValue->attribute_value;
                                else:
                                    echo $attribute->attribute_value;
                                endif;
                                ?>
                            </label><br>
                            @endforeach
                            @endif
                        </div>
                        <div class="form-group">
                            <div class="product-cartbutton-group">
                                <div class="qty form-group2 product-cartbutton">
                                    <label></label>
                                    <input id="quantity1" min="1" value="1" type="number" style="width:50px">
                                </div>
                                <div class="button-group product-cartbutton">
                                    <div class="addtocart">
                                        <a href="javascript:void(0)" product_id="{{$productInfo->id}}" onclick="addtocart(this)" class="btn login-button">
                                            <h6>Add to cart</h6>
                                        </a>
                                    </div>
                               <!--       <div class="buynow" style="overflow: hidden;display: inline-block;text-align: center;cursor: pointer;transition: all 0.5s ease 0s;">
                                    <div >
                                        <a href="" product_id="" onclick="" class="btn login-button">
                                            <h6>Buy Now</h6>
                                        </a>
                                    </div>
                                  
                                </div> -->
                                    <div class="wishlist">
                                        <a href="javascript:void(0)" product_id="{{$productInfo->id}}" onclick='addtowishlist(this)' data-toggle="tooltip" title="Add To Wishlist">
                                            <span>wishlist</span>
                                        </a>
                                    </div>
                                </div>
                               
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="product-cartbutton-group mt_10">
                                <hr>
                                <ul class="list-unstyled product_info mtb_10">
                                    <li>
                                        <label>Brand:</label>
                                        <span> <a href="#">{{!empty($productInfo->brandDetail)?$productInfo->brandDetail->title:''}}</a></span></li>
                                    <li>
                                        <label>Product Code:</label>
                                        <span>{{$productInfo->sku_code}}</span></li>
                                    <li>
                                        <label>Availability:</label>
                                        <span>{{$productInfo->quantity > 0 ?'In Stock':'Out of stock'}}</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <hr>
                    </div>
                </div>
                <div class="col-md-2 col-lg-2"></div>

            </div>
            <div class="row mtb_100">
                <div class="col-md-12">
                    <div id="exTab5" >
                        <ul class="nav nav-tabs">
                            <li class="active" id="overview"> <a href="#1c" data-toggle="tab">Overview</a> </li>
                            <li><a href="#2c" data-toggle="tab">Reviews (<?= ($reviewscount) ? $reviewscount : '0'; ?>)</a> </li>
                        </ul>
                        <div class="tab-content mt_20">
                            <div class="tab-pane active" id="1c">
                                <p><?= $productInfo->description; ?></p>
                            </div>

                            <div class="tab-pane" id="2c">

                                <div class="card">
                                    <div class="card-body">
                                        @if($reviews)
                                        @foreach($reviews as $review)
                                        <div class="row">
                                            <div class="col-md-10 review_product">
                                                <p class="review_name">
                                                    <a class="float-left" href="#"><strong>{{$review->name}}</strong></a>
                                                </p>
                                                <p class="review_icon">
                                                    <?php if ($review->rating == 1): ?>
                                                        <span class="float-right"><i class="text-warning fa fa-star"></i></span>
                                                        <span class="float-right"><i class="text-warning fa fa-star-o"></i></span>
                                                        <span class="float-right"><i class="text-warning fa fa-star-o"></i></span>
                                                        <span class="float-right"><i class="text-warning fa fa-star-o"></i></span>
                                                        <span class="float-right"><i class="text-warning fa fa-star-o"></i></span>
                                                    <?php elseif ($review->rating == 2): ?>
                                                        <span class="float-right"><i class="text-warning fa fa-star"></i></span>
                                                        <span class="float-right"><i class="text-warning fa fa-star"></i></span>
                                                        <span class="float-right"><i class="text-warning fa fa-star-o"></i></span>
                                                        <span class="float-right"><i class="text-warning fa fa-star-o"></i></span>
                                                        <span class="float-right"><i class="text-warning fa fa-star-o"></i></span>
                                                    <?php elseif ($review->rating == 3): ?>
                                                        <span class="float-right"><i class="text-warning fa fa-star"></i></span>
                                                        <span class="float-right"><i class="text-warning fa fa-star"></i></span>
                                                        <span class="float-right"><i class="text-warning fa fa-star"></i></span>
                                                        <span class="float-right"><i class="text-warning fa fa-star-o"></i></span>
                                                        <span class="float-right"><i class="text-warning fa fa-star-o"></i></span>
                                                    <?php elseif ($review->rating == 4): ?>
                                                        <span class="float-right"><i class="text-warning fa fa-star"></i></span>
                                                        <span class="float-right"><i class="text-warning fa fa-star"></i></span>
                                                        <span class="float-right"><i class="text-warning fa fa-star"></i></span>
                                                        <span class="float-right"><i class="text-warning fa fa-star"></i></span>
                                                        <span class="float-right"><i class="text-warning fa fa-star-o"></i></span>
                                                    <?php elseif ($review->rating == 5): ?>
                                                        <span class="float-right"><i class="text-warning fa fa-star"></i></span>
                                                        <span class="float-right"><i class="text-warning fa fa-star"></i></span>
                                                        <span class="float-right"><i class="text-warning fa fa-star"></i></span>
                                                        <span class="float-right"><i class="text-warning fa fa-star"></i></span>
                                                        <span class="float-right"><i class="text-warning fa fa-star"></i></span>
                                                    <?php endif; ?>
                                                </p>
                                                <div class="clearfix"></div>
                                                <p>{{$review->review}}</p>
                                                <!-- <p class="like">
                                                    <a class="float-right btn text-white btn-danger"> <i class="fa fa-heart"></i> Like</a>
                                                </p> -->
                                            </div>
                                        </div>
                                        @endforeach
                                        @endif
                                    </div>
                                </div>

                                <form class="form-horizontal" method="post" id="reviewForm">
                                    <div id="review"></div>
                                    <h4 class="mt_20 mb_30">Write a review</h4>
                                    <div class="form-group required">
                                        <div class="col-sm-12">
                                            <label class="control-label">Your Name</label>
                                            <input name="name" id="yourname" class="form-control" type="text" required>
                                        </div>
                                    </div>
                                    <div class="form-group required">
                                        <div class="col-sm-12">
                                            <label class="control-label">Your Review</label>
                                            <textarea name="review" id="yourreview" rows="5" class="form-control" required></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group required">
                                        <div class="col-md-6">
                                            <label class="control-label" style="margin-right: 30px;margin-top: -10px;">Rating</label>
                                            <div class="rates">
                                                <span>Bad</span>
                                                <input name="rating" value="1" type="radio">
                                                <input name="rating" value="2" type="radio">
                                                <input name="rating" value="3" type="radio">
                                                <input name="rating" value="4" type="radio">
                                                <input name="rating" value="5" type="radio">
                                                <span>Good</span>
                                            </div>
                                        </div>
                                        <div class="col-md-6">

                                        </div>
                                    </div>
                                    <div class="form-group required">
                                        <input type="hidden" name="product_id" value="{{$productInfo->id}}">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="heading-part text-center">
                        <h2 class="main_title mt_20">Related Products</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                @if($relatedproducts)
                <div class="product-layout  product-grid detailedrelated-produsts  owl-carousel mb_50">
                    @foreach($relatedproducts as $data)
                    <div class="item">
                        <div class="product-thumb">
                            <div class="imagFe product-imageblock"> 
                                <a href="/product_detail/{{$data->id}}"> 
                                    <img data-name="product_image" src="{{!empty($data->fileDetail)?$data->fileDetail->file_path.$data->fileDetail->file_name:url('/').'/frontassets/images/no_img.png'}}" alt="{{$data->name}}" title="{{$data->name}}" class="img-responsive" style="height: 234px;"> 
                                    <img src="{{!empty($data->fileDetail)?$data->fileDetail->file_path.$data->fileDetail->file_name:url('/').'/frontassets/images/no_img.png'}}" alt="{{$data->name}}" title="{{$data->name}}" class="img-responsive" style="height: 234px;"> 
                                </a> 
                            </div>
                            <div class="caption product-detail text-left">
                                <h6 data-name="product_name" class="product-name mt_20"><a href="/product_detail/{{$data->id}}" title="{{$data->name}}"><?= substr($data->name, 0, 30) . '...' ?></a></h6>
                                <div class="rating">
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i class="fa fa-star fa-stack-x"></i></span>
                                </div>

                                 @if($data->discount_percent!=0)
                                <span><i class="fa fa-inr"></i> <del class="text-danger">{{$data->price}}</del></span>
                                @endif
                                <span class="price"><span class="amount"><span class="currencySymbol">&#8377; </span><?= number_format($data->price - (($data->discount_percent * $data->price) / 100), 2, '.', ''); ?></span>
                                </span>
                                <!-- <span class="price"><span class="amount"><span class="currencySymbol">&#8377; </span>{{$data->price}}</span>
                                </span> -->
                                <div class="button-group text-center">
                                    <div class="wishlist" title="Add to wishlist"><a href="javascript:void(0)" product_id="{{$data->id}}" onclick="addtowishlist(this)"><span>wishlist</span></a></div>
                                    <div class="quickview" title="Quick View"><a href="/product_detail/{{$data->id}}"><span>Quick View</span></a></div>
                                    <div class="add-to-cart" title="Add to cart"><a href="javascript:void(0)" product_id="{{$data->id}}" onclick="addtocart(this)"><span>Add to cart</span></a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $("#reviewForm").validate();

    $("#reviewForm").on('submit', function (e) {
        e.preventDefault();

        var yourname = $("#yourname").val();
        var yourreview = $("#yourreview").val();

        if (yourname != '' && yourreview != '') {
            $.ajax({
                url: '/addreview',
                type: 'post',
                dataType: 'json',
                data: $(this).serialize(),
                cache: false
            }).done(function (response) {
                if (response.error_code == 200) {
                    $("#yourname").val('');
                    $("#yourreview").val('');

                    Command: toastr["success"](response.message)

                    toastr.options = {
                        "closeButton": false,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": false,
                        "positionClass": "toast-top-right",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "200",
                        "hideDuration": "200",
                        "timeOut": "200",
                        "extendedTimeOut": "200",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    }
                } else if (response.error_code == 201) {
                    window.location.href = '/loginuser';
                } else {
                    Command: toastr["error"](response.message)

                    toastr.options = {
                        "closeButton": false,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": false,
                        "positionClass": "toast-top-right",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "200",
                        "hideDuration": "200",
                        "timeOut": "200",
                        "extendedTimeOut": "200",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    }
                }
            });
        }
    });

    $(document).ready(function () {
        //If your <ul> has the id "glasscase"
        $('#glasscase').glassCase({'thumbsPosition': 'bottom', 'widthDisplay': 560});
    });
</script>
@endsection