@extends('layouts.account')
@section('content')
<div class="container mtb_30">
    <div class="row">
      <!--   <div class="col-sm-4 col-md-3" id="column-left">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="panel panel-default account-sidebar">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <?php
                                    $session = Session::get('user_data');
                                    if (!empty($session)):
                                        $customerInfo = getCustomer($session['customer_id']);
                                        if ($customerInfo):
                                            $fileInfo = getFile($customerInfo->photo);
                                            if ($fileInfo) {
                                                $photo = $fileInfo->file_path . $fileInfo->file_name;
                                            } else {
                                                $photo = url('/') . '/frontassets/images/no_img.png';
                                            }
                                        endif;
                                    endif;
                                    ?>
                                    <div class="pull-left"><p><img src="<?= $photo; ?>" alt="user" /></p></div>
                                    <div class="pull-left"><p>Welcome<br/> <strong><?= $customerInfo->firstname . ' ' . $customerInfo->lastname; ?></strong></p>
                                        <p>ID: <strong><?= 'AAIMA' . $customerInfo->id; ?></strong></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body account-sidebar-menu">
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <ul class="list-group">
                                        <li class="list-group-item"><a href="/myaccount"><i class="fa fa-user"></i> My Profile <span class="badge"><i class="fa fa-angle-right"></i></span></a></li>
                                        <li class="list-group-item"><a href="/myaddress"><i class="fa fa-map-marker"></i> Manage Addresses <span class="badge"><i class="fa fa-angle-right"></i></span></a></li>
                                        <li class="list-group-item"><a href="/myreward"><i class="fa fa-trophy"></i> My Rewards <span class="badge"><i class="fa fa-angle-right"></i></span></a></li>
                                        <li class="list-group-item"><a href="/mydownline"><i class="fa fa-sitemap"></i> My Downline <span class="badge"><i class="fa fa-angle-right"></i></span></a></li>
                                        <li class="list-group-item"><a href="/mywishlist"><i class="fa fa-heart"></i> My Wishlist <span class="badge"><i class="fa fa-angle-right"></i></span></a></li>
                                        <li class="list-group-item"><a href="/myorder" class="active"><i class="fa fa-list"></i> My Orders <span class="badge"><i class="fa fa-angle-right"></i></span></a></li>
                                        <li class="list-group-item"><a href="/myreturn"><i class="fa fa-retweet"></i> My Returns <span class="badge"><i class="fa fa-angle-right"></i></span></a></li>
                                        <li class="list-group-item"><a href="/logoutuser" class="logout"><i class="fa fa-power-off"></i> Logout</a></li>
                                    </ul> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
        <div class="col-sm-2 col-md-2">
        </div>

        <div class="col-sm-8 col-md-8" id="column-right">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="panel panel-default account-content">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <h5>Order Information : {{$orderInfo->order_no}}</h5>   
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <div id="order-details-table" class="col-sm-12">
                                        <table class="table table-bordered table-hover list">
                                            <thead class="order-detail-heading">
                                                <tr>
                                                    <td class="text-left" colspan="2">Order Details</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td class="text-left" style="width: 50%;"> 
                                                        <b>Invoice No.:</b> INV-{{$orderInfo->id}}<br>
                                                        <b>Order ID:</b> #{{$orderInfo->order_no}}<br>
                                                        <b>Order Date:</b> {{date('d-m-Y',strtotime($orderInfo->created_at))}}</td>
                                                    <td class="text-left">
                                                        <b>Payment Method:</b> PayPal Express Checkout<br>
<!--                                                         <b>Shipping Method:</b> Flat Shipping Rate           
 -->                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <table class="table table-bordered table-hover list">
                                            <thead class="order-detail-heading">
                                                <tr>
                                                    <td class="text-left" style="width: 50%;">Payment Address</td>
                                                    <td class="text-left">Shipping Address</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td class="text-left">{{$orderInfo->address->firstname.' '.$orderInfo->address->lastname}}<br>{{$orderInfo->address->address_1.' '.$orderInfo->address->address_2}}<br>{{$orderInfo->address->city.' '.$orderInfo->address->state}}<br>{{$orderInfo->address->country.' '.$orderInfo->address->postcode}}</td>
                                                    <td class="text-left">{{$orderInfo->address->firstname.' '.$orderInfo->address->lastname}}<br>{{$orderInfo->address->address_1.' '.$orderInfo->address->address_2}}<br>{{$orderInfo->address->city.' '.$orderInfo->address->state}}<br>{{$orderInfo->address->country.' '.$orderInfo->address->postcode}}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-hover list">
                                                <thead class="order-detail-heading">
                                                    <tr>
                                                        <td class="text-left">Sr. No.</td>
                                                        <td class="text-left">Product Name</td>
                                                        <td class="text-left">SKU Cod</td>
                                                        <td class="text-left">HSN Cod</td>
                                                        <td class="text-right">Quantity</td>
                                                        <td class="text-right">Unit Price</td>
                                                        <td class="text-right">Taxable Value</td>
                                                        <td class="text-right">CGST</td>
                                                        <td class="text-right">SGST</td>
                                                        <td class="text-right">Total Tax</td>
                                                        <td class="text-right">Total Amount</td>
<!--                                                         <td style="width: 20px;"></td>
 -->                                                    </tr>
                                                </thead>
                                                <tbody>
                                                     @if($orderInfo->orderItems)
           <?php 

           $i=1;?>
                                                    @foreach($orderInfo->orderItems as $item)
                                                 <?php
           // echo "<pre>";
           // print_r($item);
           // echo "</pre>";
           ?>
                <tr>
                    <td style="text-align:left;"><?php echo $i++;?></td>
          <td style="text-align:left;"><?= $item->name; ?></td>
          <td style="text-align:center;"><?= $item->sku_code; ?></td>
          <td style="text-align:center;"><?= $item->hsn_code; ?></td>

  <?php  $tax= $item->tax;echo "<br>";
          $tax2=$tax/2;
          $total_amount=$item->unit_price;
          $amount_without_tax=$total_amount*($tax2/100);
          $quantity=$item->quantity;
          $total_tax=$amount_without_tax*$quantity*2;
          $total_tax=$total_amount*$quantity -$total_tax ;

          ?>
          <td style="text-align:center;">{{$item->quantity}}</td>
          <td style="text-align:center;"><i class="fa fa-inr"></i> {{$item->unit_price}}</td>
          <td style="text-align:center;"><i class="fa fa-inr"></i> <?php echo $total_tax;?></td>
        
          <td style="text-align:center;"><i class="fa fa-inr"></i> <?php echo $amount_without_tax*$quantity;?></td>
          <td style="text-align:center;"><i class="fa fa-inr"></i> <?php echo $amount_without_tax*$quantity;?></td>
          <td style="text-align:center;"><i class="fa fa-inr"></i> <?php echo $amount_without_tax*$quantity*2;?></td>
          <td style="text-align:center;"><i class="fa fa-inr"></i> {{$item->total_amount}}</td>
      <!--     <td class="text-right">
                                                            <a href="" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Reorder"><i class="fa fa-shopping-cart"></i></a>
                                                            <a href="#" data-toggle="tooltip" title="" class="btn btn-danger" data-original-title="Return"><i class="fa fa-reply"></i></a>
                                                        </td> -->
        </tr>
          @endforeach
                                                    @endif

                                                 
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <td colspan="9"></td>
                                                        <td class="text-right"><b>Delivery Charges</b></td>
                                                        <td class="text-right"><i class="fa fa-inr"></i> 50</td>
                                                    
                                                    </tr>
                                                    <tr>
                                                        <td colspan="9"></td>
                                                        <td class="text-right"><b>Total</b></td>
                                                          <?php 
$grand_total=$orderInfo->grand_total
          ?>
                                                        <td class="text-right"><i class="fa fa-inr"></i> <?php echo $grand_total+50;?></td>
                                                       
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                        <h3 class="heading-title">Order History</h3>
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-hover list">
                                                <thead class="order-detail-heading">
                                                    <tr>
                                                        <td class="text-left">Date Added</td>
                                                        <td class="text-left">Status</td>
                                                        <td class="text-left">Comment</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td class="text-left">{{date('d-m-Y',strtotime($orderInfo->created_at))}}</td>
                                                        <td class="text-left"><?php 
        $status=$orderInfo->status;
if($status==1){
    echo "Request";
}
elseif($status==2){
    echo "Process";
}elseif($status==3){
    echo "Dispatched";
}
elseif($status==4){
    echo "Delivered";
}
elseif($status==5){
    echo "Cancel";
}
        ?></td>
                                                        <td class="text-left"></td>
                                                    </tr>
                                                    @if($orderInfo->history)
         @foreach($orderInfo->history as $history)

                                                  <tr>
        <td class="text-left">{{date('d-m-Y',strtotime($history->created_at))}}</td>
        <td class="text-left"><?php 
        $status=$history->status;
if($status==1){
    echo "Request";
}
elseif($status==2){
    echo "Process";
}elseif($status==3){
    echo "Dispatched";
}
elseif($status==4){
    echo "Delivered";
}
elseif($status==5){
    echo "Cancel";
}
        ?>
            
        </td>
                <td class="text-left">{{$history->comment}}</td>

      </tr>
                                                    @endforeach;
                                                    @endif;
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="buttons">
                                            <div class="pull-left"><a href="/myorder" class="btn btn-default button"><i class="fa fa-arrow-left"></i> Back To Orders</a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

         <div class="col-sm-2 col-md-2">
        </div>
    </div>
</div>
@endsection