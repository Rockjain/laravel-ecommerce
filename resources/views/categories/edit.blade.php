@extends('layouts.admin')
@section('content')
<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Edit Category</h3>
                </div>
                @include('partials.errors')
                @include('partials.success')
                <div class="panel-body">
                    <form method="post" id="categoryForm" class="form-horizontal" action="{{route('categories.update',[$category->id])}}" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <input type="hidden" name="_method" value="put">
                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Parent Category</label>
                            <div class="col-md-6 col-xs-12">                                                                                
                                <select class="form-control select" name="parent_id" data-live-search="true">
                                    <option value="0">Parent</option>
                                    @if($parents)
                                    @foreach($parents as $key=>$cat)
                                    <option value="{{$cat->id}}"<?= ($cat->id == $category->parent_id) ? 'selected' : ''; ?>>{{$cat->name}}</option>
                                    @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Category Name</label>
                            <div class="col-md-6 col-xs-12">  
                                <input type="text" name="name" class="form-control" placeholder="Category Title" value="{{$category->name}}" required/>                                       
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Description</label>
                            <div class="col-md-6 col-xs-12">  
                                <textarea class="form-control" name="description" placeholder="Description" rows="4">{{$category->description}}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Upload Category Image</label>
                            <label class="" style="margin-left: 300px;">
                                <?php if ($category->fileDetail): ?>
                                    <img src="<?= $category->fileDetail->file_path . $category->fileDetail->file_name; ?>" class="appendImage" width="75" height="75">
                                <?php else: ?>
                                    <img src="<?= url('/'); ?>/adminassets/img/upload-icon.png" class="appendImage" width="75" height="75">
                                <?php endif; ?>
                                <input type="file" name="image" onchange="readURL(this);" accept="image/*" style="display: none"/>
                            </label>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12 col-xs-12">  
                                <p class="text-center">
                                    <input type="submit" class="btn btn-info" value="Update"/> 
                                </p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection