<!DOCTYPE html>
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
    <!--<![endif]-->

    <head>
        <meta charset="utf-8">
        <title>Aaima Jewellers</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="viewport" content="width=device-width">

        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link rel="stylesheet" type="text/css" href="<?= url('/'); ?>/frontassets/css/font-awesome.min.css" />
        <link rel="stylesheet" type="text/css" href="<?= url('/'); ?>/frontassets/css/bootstrap.css" />
        <link rel="stylesheet" type="text/css" href="<?= url('/'); ?>/frontassets/css/bootstrap-datepicker3.css">
        <link rel="stylesheet" type="text/css" href="<?= url('/'); ?>/frontassets/css/style.css">
        <link rel="stylesheet" type="text/css" href="<?= url('/'); ?>/frontassets/css/user-account.css">
        <link rel="stylesheet" type="text/css" href="<?= url('/'); ?>/frontassets/css/custom.css">
        <link rel="stylesheet" type="text/css" href="<?= url('/'); ?>/frontassets/css/product-zoomifier.css">
        <link rel="stylesheet" type="text/css" href="<?= url('/'); ?>/frontassets/css/magnific-popup.css">
        <link rel="stylesheet" type="text/css" href="<?= url('/'); ?>/frontassets/css/owl.carousel.css">
        <link rel="stylesheet" type="text/css" href="<?= url('/'); ?>/frontassets/css/jquery-ui.css">
        <link rel="shortcut icon" href="<?= url('/'); ?>/frontassets/images/favicon.png">
        <link rel="apple-touch-icon" href="<?= url('/'); ?>/frontassets/images/apple-touch-icon.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?= url('/'); ?>/frontassets/images/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<?= url('/'); ?>/frontassets/images/apple-touch-icon-114x114.png">

        <style>
            .error{
                color:red;
            }
        </style>
        <script src="<?= url('/'); ?>/frontassets/js/jQuery_v3.1.1.min.js"></script>
        <script src="<?= url('/'); ?>/adminassets/js/jquery-validate.min.js" type="text/javascript"></script>

        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    </head>
    <body>
        <?php
        $categories = getParentCategory();
        ?>
        <!-- =====  LODER  ===== -->
        <!--<div class="loder"></div>-->
        <div class="wrapper">
            <div id="subscribe-me" class="modal animated fade in" role="dialog" data-keyboard="true" tabindex="-1">
                <div class="newsletter-popup">
                    <img class="offer" src="<?= url('/'); ?>/frontassets/images/newsbg.jpg" alt="offer">
                    <div class="newsletter-popup-static newsletter-popup-top">
                        <div class="popup-text">
                            <div class="popup-title">50% <span>off</span></div>
                            <div class="popup-desc">
                                <div>Sign up and get 50% off your next Order</div>
                            </div>
                        </div>
                        <form onsubmit="return  validatpopupemail();" method="post">
                            <div class="form-group required">
                                <input type="email" name="email-popup" id="email-popup" placeholder="Enter Your Email" class="form-control input-lg" required />
                                <button type="submit" class="btn btn-default btn-lg" id="email-popup-submit">Subscribe</button>
                                <label class="checkme">
                                    <input type="checkbox" value="" id="checkme" />Dont show again</label>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- =====  HEADER START  ===== -->
            <header id="header">
                <!--  <div class="header-top">
                     <div class="container">
                         <div class="row nomargin">
                             <div class="col-sm-6 hidden-xs">
                                 <ul class="header-top-left">
                                     <li>India's fastest online shopping destination</li>
                                 </ul>
                             </div>
                             <div class="col-sm-6">
                                 <ul class="header-top-right text-right">
                                     <li class="account"><a href="<?= url('/'); ?>/frontassets/login.php">About Us</a></li>
                                     <li class="account"><a href="<?= url('/'); ?>/frontassets/register.php">Help Center</a></li>
                                     <li class="cart"><a href="<?= url('/'); ?>/frontassets/cart.php"><i class="fa fa-android"></i> Download App</a></li>
                                 </ul>
                             </div>
                         </div>
                     </div>
                 </div> -->
                <?php
                $user_data = Session::get('user_data');
                $customer = getCustomer($user_data['customer_id']);
                if ($customer) {
                    $firstname = $customer->firstname;
                } else {
                    $firstname = 'Guest';
                }
                ?>
                <div class="header">
                    <div class="container">
                        <nav class="navbar">
                            <div class="navbar-header"> <a class="navbar-brand" href="<?= url('/'); ?>"> <img alt="Aaima" src="<?= url('/'); ?>/frontassets/images/logo.png"> </a> </div>
                            <div class="header-right pull-right mtb_30">
                                <div class="dropdown pull-right signin-menu">
                                    <a href="<?= url('/'); ?>/frontassets/" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user-o"></i>Welcome  <?= $firstname; ?> <span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="/myaccount"><i class="fa fa-user"></i> My Account</a></li>
                                        <li><a href="/myorder"><i class="fa fa-list"></i> Orders</a></li>
                                        <li><a href="/mywishlist"><i class="fa fa-heart"></i> My Wishlist</a></li>
                                        <li><a href="/mycart"><i class="fa fa-shopping-cart"></i> Go To Cart</a></li>
                                        <li><a href="/mybusinessassociate"><i class="fa fa-sitemap"></i> My Business Associate</a></li>
                                        <li><a href="/myreward"><i class="fa fa-trophy"></i> My Rewards</a></li>
                                        <li><a href=""><i class="fa fa-retweet"></i> Returns</a></li>
                                        <li><a href=""><i class="fa fa-lock"></i> Change Password</a></li>
                                        <li class="logout"><a href="/logoutuser" onclick="return confirm('Are you sure, you want to logout ?')"><i class="fa fa-power-off"></i> Logout</a></li>
                                    </ul>
                                </div>
                                <?php
                                $user_data = Session::get('user_data');
                                $usercart = getCart($user_data['customer_id']);
                                $cartCount = getCartCount($user_data['customer_id']);
                                if ($usercart):
                                    $cartArr = [];
                                    $s_total = '0';
                                    foreach ($usercart as $key => $row):
                                        $productInfo = getProduct($row->product_id);

                                        $cartArr[$key] = $row;

                                        $s_total = $s_total + ($row->quantity * $productInfo->price);
                                    endforeach;
                                endif;
                                ?>
                                <button class="navbar-toggle pull-left" type="button" data-toggle="collapse" data-target=".js-navbar-collapse"> <span class="i-bar"><i class="fa fa-bars"></i></span></button>
                                <div class="shopping-icon">
                                    <div class="cart-item " data-target="#cart-dropdown" data-toggle="collapse" aria-expanded="true" role="button">Cart <span class="cart-qty"><?= $cartCount != '' ? $cartCount : '0'; ?></span></div>
                                    <?php if (Session::get('user_data')): ?>
                                        <div id="cart-dropdown" class="cart-menu collapse">
                                            <ul>
                                                <li>
                                                    <div class="row">
                                                        <div class="scrollbar" id="style-2">
                                                            <table class="table table-striped">
                                                                <tbody>
                                                                    @if(!$cartArr)
                                                                <p class="noproduct" style="position: absolute;left: 90px;display: none">No Product Found</p>
                                                                @else
                                                                @foreach($cartArr as $cart)
                                                                <?php
                                                                $productInfo = getProduct($cart->product_id);
                                                                if ($productInfo) {
                                                                    $image = $productInfo->fileDetail->file_path . $productInfo->fileDetail->file_name;
                                                                } else {
                                                                    $image = url('/') . '/frontassets/images/no_img.png';
                                                                }
                                                                ?>
                                                                <tr class="cartlist<?= $cart->id; ?>">
                                                                    <td class="text-center"><a href="/prorduct_detail/{{$cart->product_id}}"><img src="{{$image}}" title="{{$productInfo->name}}" style="height: 75px;"></a></td>
                                                                    <td class="text-left product-name"><a href="/prorduct_detail/{{$cart->product_id}}">{{$productInfo->name}}</a>
                                                                        <span class="text-left price"><i class="fa fa-inr"></i> {{$productInfo->price}}</span>
                                                                    </td>
                                                                    <td class="text-center">
                                                                        <a class="close-cart" id="{{$cart->id}}" onclick="removefromcart(this,<?= $cart->id; ?>)"><i class="fa fa-times-circle"></i></a>
                                                                    </td>
                                                                </tr>
                                                                @endforeach
                                                                @endif
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <table class="table">
                                                        <tbody>
                                                            <tr>
                                                                <td class="text-right"><strong>Sub-Total</strong></td>
                                                                <td class="text-right subtotal"><i class="fa fa-inr"></i> <?= number_format($s_total, '2'); ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-right"><strong>Total</strong></td>
                                                                <td class="text-right subtotal"><i class="fa fa-inr"></i> <?= number_format($s_total, '2'); ?></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </li>
                                                <li>
                                                    <a href="/mycart">
                                                        <input class="btn pull-left mt_10" value="View cart" type="submit">
                                                    </a>
                                                    <a <?php if (!empty($user_data)): ?>href="/checkout"<?php else: ?>href="/loginuser/checkout"<?php endif; ?>>
                                                        <input class="btn pull-right mt_10" value="Checkout" type="submit">
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    <?php endif; ?>
                                </div>
                                <div class="main-search pull-right">
                                    <div class="search-overlay visible-xs visible-sm">
                                        <!-- Close Icon -->
                                        <a href="<?= url('/'); ?>/frontassets/javascript:void(0)" class="search-overlay-close"></a>
                                        <!-- End Close Icon -->
                                        <div class="container">
                                            <!-- Search Form -->
                                            <form role="search" id="searchform" action="/search" method="get">
                                                <label class="h5 normal search-input-label">Enter keywords To Search Entire Store</label>
                                                <input value="" name="q" placeholder="Search here..." type="search">
                                                <button type="submit"></button>
                                            </form>
                                            <!-- End Search Form -->
                                        </div>
                                    </div>
                                    <div class="header-search">
                                        <div class="main-seacrhbar hidden-xs hidden-sm">
                                            <div class="input-group">
                                                <input type="text" class="form-control" id="input-seacrh" placeholder="Search here..." value="" name="coupon">
                                                <span class="input-group-btn">
                                                    <input type="button" class="btn" data-loading-text="Loading..." id="button-search" value="Search">
                                                </span>
                                            </div>
                                        </div>
                                        <a id="search-overlay-btn" class="visible-xs visible-sm"></a> </div>
                                </div>

                            </div>

                            <!-- /.nav-collapse -->
                        </nav>
                    </div>
                </div>
                <div class="header-bottom">
                    <div class="container">
                        <div class="collapse navbar-collapse js-navbar-collapse">
                                   <ul id="menu" class="nav navbar-nav">
                                <li><a href="<?= url('/'); ?>">Home</a></li>


                               
                                @if($categories)
                                @foreach($categories as $category)

  <?php 
                                    $child_categories = getChildCategories($category->id);
                            $count=count($child_categories);
                                    ?>
                                   
                                <li <?php  if($count!="0"){?>class="dropdown"<?php }?>><a href="/products/{{$category->id}}" <?php  if($count!="0"){?>class="dropdown-toggle" <?php }?>>{{$category->name}}
<?php  if($count!="0"){?>
                                 <span class="caret"></span>
<?php }?>
                             </a>
<?php  if($count!="0"){?>
                                  <ul class="dropdown-menu">
                                    <?php foreach($child_categories as $val){?>
                                        <li> <a href="/products/{{$val->id}}">{{$val->name}}</a></li>
                                <?php }?>
                                    </ul>
<?php  }?>
                                </li>


                                @endforeach
                                @endif
                               
                                <li> <a href="/blog">Blog</a></li> 
                               <!-- <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown">Pages <span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li> <a href="#">Contact us</a></li>
                                        <li> <a href="#">Cart</a></li>
                                        <li> <a href="#">Checkout</a></li>
                                        <li> <a href="#">Product Detail Page</a></li>
                                        <li> <a href="#">Single Post</a></li>
                                    </ul>
                                </li>
                                 <li> <a href="#">About us</a></li> -->
                            </ul>
                        </div>
                    </div>
                </div>
            </header>
            <!-- =====  HEADER END  ===== -->
            @yield('content')
            <!-- =====  FOOTER START  ===== -->
            <div class="footer pt_60 pb_20 white-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3 footer-block">
                            <div class="content_footercms_right">
                                <div class="footer-contact">
                                    <div class="footer-logo mb_40"> 
                                        <a href="<?= url('/'); ?>"> 
                                            <img src="<?= url('/'); ?>/frontassets/images/logo.png" alt="Aaima"> 
                                        </a> 
                                    </div>
                                   <ul>
                                        <li>1st Floor, Anand Colony.Near Chintpurni Mata Mandir,Railway Road,Gurugram(Haryana)</li>
                                        <li><span style="font-weight: bold;">Custmer Care:-</span>01244241987</li>
                                        <li><span style="font-weight: bold;">Whatsapp No:-</span>8447951987</li>
                                    </ul>
                                    <!-- <div class="social_icon">
                                        <ul>
                                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                            <li><a href="#"><i class="fa fa-google"></i></a></li>
                                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                            <li><a href="#"><i class="fa fa-rss"></i></a></li>
                                        </ul>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 footer-block">
                            <h6 class="footer-title ptb_20">Categories</h6>
                            <ul>
                                <?php
                                if ($categories):
                                    foreach ($categories as $cat):
                                        ?>
                                        <li><a href="/products/<?= $cat->id; ?>"><?= $cat['name']; ?></a></li>
                                        <?php
                                    endforeach;
                                endif
                                ?>
                            </ul>
                        </div>
                        <div class="col-md-2 footer-block">
                            <h6 class="footer-title ptb_20">Information</h6>
                            <ul>
                                <li><a href="">Contact Us</a></li>
                                <li><a href="">About Us</a></li>
                                <li><a href="">Privacy Policy</a></li>
                                <li><a href="">Return Refund Policy</a></li>
                                <!-- <li><a href="">Best Sellers</a></li>
                                <li><a href="">Our Stores</a></li> -->
                            </ul>
                        </div>
                        <div class="col-md-2 footer-block">
                            <h6 class="footer-title ptb_20">My Account</h6>
                            <ul>
                                <?php
                                if (Session::get('user_data')):
                                    ?>
                                    <li><a href="/checkout">Checkout</a></li>
                                    <li><a href="/myaccount">My Account</a></li>
                                    <li><a href="/myorder">My Orders</a></li>
                                   <!--  <li><a href="">My Credit Slips</a></li> -->
                                    <li><a href="/myaddress">My Addresses</a></li>
                                    <!-- <li><a href="">My Personal Info</a></li> -->
                                <?php else: ?>
                                    <li><a href="/loginuser">Checkout</a></li>
                                    <li><a href="/loginuser">My Account</a></li>
                                    <li><a href="/loginuser">My Orders</a></li>
                                    <li><a href="/loginuser">My Credit Slips</a></li>
                                    <li><a href="/loginuser">My Addresses</a></li>
                                    <li><a href="/loginuser">My Personal Info</a></li>
                                <?php endif; ?>
                            </ul>
                        </div>
                        <div class="col-md-3">
                            <h6 class="ptb_20">SIGN UP OUR NEWSLETTER</h6>
                            <p class="mt_10 mb_20">For get offers from our favorite brands & get 20% off for next </p>
                            <div class="form-group">
                                <input class="mb_20" type="text" placeholder="Enter Your Email Address">
                                <button class="btn">Subscribe</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
             <div class="footer">
                <div class="footer-bottom ptb_10">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="copyright-part">@ 2017 All Rights AAIMA Pvt Ltd.</div>
                            </div>
                            <div class="col-sm-4">
                                <div class="copyright-part">Designed & Developed by <a href="http://shadowtechnology.in">Shadow technology</a></div>
                            </div>
                            <div class="col-sm-4">
                                <div class="payment-icon text-right">
                                    <ul>
                                        <li><i class="fa fa-cc-paypal "></i></li>
                                        <li><i class="fa fa-cc-stripe"></i></li>
                                        <li><i class="fa fa-cc-visa"></i></li>
                                        <li><i class="fa fa-cc-discover"></i></li>
                                        <li><i class="fa fa-cc-mastercard"></i></li>
                                        <li><i class="fa fa-cc-amex"></i></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- =====  FOOTER END  ===== -->
            <div id="EnquiryModal" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">×</button>
                            <h5 class="modal-title text-center">Make An Inquiry</h5>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-6">

                                    <div class="form-group">
                                        <label for="inputName">Your Name <span class="font10 text-danger">*</span></label>
                                        <input id="inputName" type="text" value="Balram" class="form-control" data-error="Your name is required" required="">
                                        <div class="help-block with-errors"></div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputEmail">Your Email *</label>
                                        <input id="inputEmail" type="email" value="balramagarwal07@gmail.com" class="form-control" disabled="">
                                        <div class="help-block with-errors"></div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputphone">Your Phone *</label>
                                        <input id="inputphone" type="text" value="9313484654" class="form-control" disabled="">
                                        <div class="help-block with-errors"></div>
                                    </div>

                                </div>

                                <div class="col-sm-6">

                                    <div class="form-group">
                                        <label>Quantity<span class="font10 text-danger">*</span></label>
                                        <input type="number" class="form-control" required="" min="1" value="1">
                                    </div>

                                    <div class="form-group">
                                        <label for="inputMessage">Message </label>
                                        <textarea id="inputMessage" class="form-control" rows="4"></textarea>
                                        <div class="help-block with-errors"></div>
                                    </div>

                                </div>

                                <div class="col-sm-12 text-right text-left-sm">
                                    <button type="submit" class="btn btn-primary mt-5">Send Inquiry</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <a id="scrollup">Scroll</a>
            <script>
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                function addtocart(obj) {
                    var quantity = $("#quantity").val();
                    if (quantity != '') {
                        quantity = 1;
                    }
                    var product_id = $(obj).attr('product_id');
                    $.ajax({
                        url: '/addtocart',
                        type: 'post',
                        dataType: 'json',
                        data: {quantity: quantity, product_id: product_id},
                        cache: false
                    }).done(function (response) {
                        if (response.error_code == 200) {
                            Command: toastr["success"](response.message)

                            toastr.options = {
                                "closeButton": false,
                                "debug": false,
                                "newestOnTop": false,
                                "progressBar": false,
                                "positionClass": "toast-top-right",
                                "preventDuplicates": false,
                                "onclick": null,
                                "showDuration": "300",
                                "hideDuration": "1000",
                                "timeOut": "5000",
                                "extendedTimeOut": "1000",
                                "showEasing": "swing",
                                "hideEasing": "linear",
                                "showMethod": "fadeIn",
                                "hideMethod": "fadeOut"
                            }
                        } else {
                            Command: toastr["error"](response.message)

                            toastr.options = {
                                "closeButton": false,
                                "debug": false,
                                "newestOnTop": false,
                                "progressBar": false,
                                "positionClass": "toast-top-right",
                                "preventDuplicates": false,
                                "onclick": null,
                                "showDuration": "300",
                                "hideDuration": "1000",
                                "timeOut": "5000",
                                "extendedTimeOut": "1000",
                                "showEasing": "swing",
                                "hideEasing": "linear",
                                "showMethod": "fadeIn",
                                "hideMethod": "fadeOut"
                            }
                        }
                    });

                }
                function movetocart(obj) {
                    var quantity = $("#quantity").val();
                    if (quantity != '') {
                        quantity = 1;
                    }
                    var product_id = $(obj).attr('product_id');
                    var id = $(obj).attr('id');
                    $.ajax({
                        url: '/addtocart',
                        type: 'post',
                        dataType: 'json',
                        data: {quantity: quantity, product_id: product_id},
                        cache: false
                    }).done(function (response) {
                        if (response.error_code == 200) {
                            $(".cart-qty").text(response.result.cartCount);

                            $.ajax({
                                url: '/removefromwishlist',
                                type: 'post',
                                dataType: 'json',
                                data: {id: id},
                                cache: false
                            }).done(function (response) {
                                $(obj).parent().parent().parent().parent('.product-layout-left').remove();
                            });

                            Command: toastr["success"](response.message)
                            toastr.options = {
                                "closeButton": false,
                                "debug": false,
                                "newestOnTop": false,
                                "progressBar": false,
                                "positionClass": "toast-top-right",
                                "preventDuplicates": false,
                                "onclick": null,
                                "showDuration": "300",
                                "hideDuration": "1000",
                                "timeOut": "5000",
                                "extendedTimeOut": "1000",
                                "showEasing": "swing",
                                "hideEasing": "linear",
                                "showMethod": "fadeIn",
                                "hideMethod": "fadeOut"
                            }
                        } else {
                            Command: toastr["error"](response.message)

                            toastr.options = {
                                "closeButton": false,
                                "debug": false,
                                "newestOnTop": false,
                                "progressBar": false,
                                "positionClass": "toast-top-right",
                                "preventDuplicates": false,
                                "onclick": null,
                                "showDuration": "300",
                                "hideDuration": "1000",
                                "timeOut": "5000",
                                "extendedTimeOut": "1000",
                                "showEasing": "swing",
                                "hideEasing": "linear",
                                "showMethod": "fadeIn",
                                "hideMethod": "fadeOut"
                            }
                        }
                    });
                }
                function addtowishlist(obj) {
                    var product_id = $(obj).attr('product_id');
                    $.ajax({
                        url: '/addtowishlist',
                        type: 'post',
                        dataType: 'json',
                        data: {product_id: product_id},
                        cache: false
                    }).done(function (response) {
                        if (response.error_code == 200) {
                            Command: toastr["success"](response.message)

                            toastr.options = {
                                "closeButton": false,
                                "debug": false,
                                "newestOnTop": false,
                                "progressBar": false,
                                "positionClass": "toast-top-right",
                                "preventDuplicates": false,
                                "onclick": null,
                                "showDuration": "300",
                                "hideDuration": "1000",
                                "timeOut": "5000",
                                "extendedTimeOut": "1000",
                                "showEasing": "swing",
                                "hideEasing": "linear",
                                "showMethod": "fadeIn",
                                "hideMethod": "fadeOut"
                            }
                        } else {
                            Command: toastr["error"](response.message)

                            toastr.options = {
                                "closeButton": false,
                                "debug": false,
                                "newestOnTop": false,
                                "progressBar": false,
                                "positionClass": "toast-top-right",
                                "preventDuplicates": false,
                                "onclick": null,
                                "showDuration": "300",
                                "hideDuration": "1000",
                                "timeOut": "5000",
                                "extendedTimeOut": "1000",
                                "showEasing": "swing",
                                "hideEasing": "linear",
                                "showMethod": "fadeIn",
                                "hideMethod": "fadeOut"
                            }
                        }
                    });
                }

                function removefromwishlist(obj) {
                    var id = $(obj).attr('id');
                    if (confirm("Are you sure, you want to remove this product ?")) {
                        $.ajax({
                            url: '/removefromwishlist',
                            type: 'post',
                            dataType: 'json',
                            data: {id: id},
                            cache: false
                        }).done(function (response) {
                            if (response.error_code == 200) {
                                $(obj).parent().parent().parent('.product-layout-left').remove();

                                Command: toastr["success"](response.message)

                                toastr.options = {
                                    "closeButton": false,
                                    "debug": false,
                                    "newestOnTop": false,
                                    "progressBar": false,
                                    "positionClass": "toast-top-right",
                                    "preventDuplicates": false,
                                    "onclick": null,
                                    "showDuration": "300",
                                    "hideDuration": "1000",
                                    "timeOut": "5000",
                                    "extendedTimeOut": "1000",
                                    "showEasing": "swing",
                                    "hideEasing": "linear",
                                    "showMethod": "fadeIn",
                                    "hideMethod": "fadeOut"
                                }
                            } else {
                                Command: toastr["error"](response.message)

                                toastr.options = {
                                    "closeButton": false,
                                    "debug": false,
                                    "newestOnTop": false,
                                    "progressBar": false,
                                    "positionClass": "toast-top-right",
                                    "preventDuplicates": false,
                                    "onclick": null,
                                    "showDuration": "300",
                                    "hideDuration": "1000",
                                    "timeOut": "5000",
                                    "extendedTimeOut": "1000",
                                    "showEasing": "swing",
                                    "hideEasing": "linear",
                                    "showMethod": "fadeIn",
                                    "hideMethod": "fadeOut"
                                }
                            }
                        });
                    }
                }
                function removefromcart(obj, number) {
                    var id = $(obj).attr('id');
                    if (confirm("Are you sure, you want to remove ?")) {
                        $.ajax({
                            url: '/removefromcart',
                            type: 'post',
                            dataType: 'json',
                            data: {id: id},
                            cache: false
                        }).done(function (response) {
                            if (response.error_code == 200) {
                                $(".cartlist" + number).remove();
                                $(".subtotal").html("<i class='fa fa-inr'></i>" + response.result.s_total.toFixed(2));
                                Command: toastr["success"](response.message)

                                toastr.options = {
                                    "closeButton": false,
                                    "debug": false,
                                    "newestOnTop": false,
                                    "progressBar": false,
                                    "positionClass": "toast-top-right",
                                    "preventDuplicates": false,
                                    "onclick": null,
                                    "showDuration": "300",
                                    "hideDuration": "1000",
                                    "timeOut": "5000",
                                    "extendedTimeOut": "1000",
                                    "showEasing": "swing",
                                    "hideEasing": "linear",
                                    "showMethod": "fadeIn",
                                    "hideMethod": "fadeOut"
                                }
                            } else {
                                Command: toastr["error"](response.message)

                                toastr.options = {
                                    "closeButton": false,
                                    "debug": false,
                                    "newestOnTop": false,
                                    "progressBar": false,
                                    "positionClass": "toast-top-right",
                                    "preventDuplicates": false,
                                    "onclick": null,
                                    "showDuration": "300",
                                    "hideDuration": "1000",
                                    "timeOut": "5000",
                                    "extendedTimeOut": "1000",
                                    "showEasing": "swing",
                                    "hideEasing": "linear",
                                    "showMethod": "fadeIn",
                                    "hideMethod": "fadeOut"
                                }
                            }
                        });
                    }
                }

                $("#profile-form").validate();
                $("#editAddressForm").validate();
                $("#addAddressForm").validate();
            </script>

            <script src="<?= url('/'); ?>/frontassets/js/owl.carousel.min.js"></script>
            <script src="<?= url('/'); ?>/frontassets/js/bootstrap.min.js"></script>
            <script src="<?= url('/'); ?>/frontassets/js/jquery.magnific-popup.js"></script>
            <script src="<?= url('/'); ?>/frontassets/js/jquery.firstVisitPopup.js"></script>
            <script src="<?= url('/'); ?>/frontassets/js/custom.js"></script>
            <script src="<?= url('/'); ?>/frontassets/js/product-zoomifier.js"></script>
    </body>

</html>