<?php
if (!empty($advertisements)) {
    $id = $advertisements[0]->id;
    $name = $advertisements[0]->name;
    $description = $advertisements[0]->description;
    $image = $advertisements[0]->image;
    $form_type = 'update';
} else {
    $name = '';
    $description = '';
    $image = '';
    $form_type = 'add';
    $id = '';
}
?>
<meta name="csrf-token" content="{{ csrf_token() }}">
@extends('layouts.admin')
@section('content')

<script src="https://cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>
<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Add Advertisement</h3>
                </div>
                @include('partials.errors')
                @include('partials.success')
                <div class="panel-body">
                    <form method="post" id="testimonials_form" class="form-horizontal" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Name</label>
                            <div class="col-md-6 col-xs-12">                                                                                
                                <input type="text" name="name" class="form-control" placeholder="Name"  value="{{$name}}" required/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Description</label>
                            <div class="col-md-6 col-xs-12">  
                                <textarea class="form-control" id="desc" name="description" placeholder="Description" rows="4" required="">{{$description}}</textarea>
                            </div>
                            <input type="hidden" name="form_type" value="{{$form_type}}">
                            <input type="hidden" name="id" value="{{$id}}">
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Status</label>
                            <div class="col-md-6 col-xs-12">                                                                                
                                <select class="form-control" name="status">
                                    <option value="1">Active</option>
                                    <option value="0">Inactive</option>
                                </select>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Image</label>
                            <div class="col-md-6 col-xs-12">                                                                                
                                <input type="file" name="image" class="form-control" <?php
                                if ($form_type == 'add') {
                                    echo 'required';
                                }
                                ?>/>
                            </div>
                            <input type="hidden" name="form_type" value="{{$form_type}}">
                            <input type="hidden" name="id" value="{{$id}}">
                            <input type="hidden" value="{{$image}}" name="image_update">
                        </div>

                        <div class="form-group">
                            <div class="col-md-12 col-xs-12">  
                                <p class="text-center">
                                    <input type="submit" class="btn btn-info" id="submit" value="Submit Advertisement"/> 
                                </p>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
<script>
// CKEDITOR.replace('description');
// var desc = CKEDITOR.instances.description.getData();
//     $("#page_form").validate({
//     rules:{
//         meta_description:"required"
//     },
// submitHandler: function(form) {
//         $.ajax({
//     type: "POST",
//     url: "/admin/add_sample_page",
//     data: $("#page_form").serialize(),
//     beforeSend:function() {
//     },
//      success: function(msg)
//         {
//         if(msg == 1){
// Command: toastr["success"]("Page has been added Successfully. Redirecting you to Page lists!")

// toastr.options = {
//   "closeButton": false,
//   "debug": false,
//   "newestOnTop": false,
//   "progressBar": false,
//   "positionClass": "toast-top-right",
//   "preventDuplicates": false,
//   "onclick": null,
//   "showDuration": "300",
//   "hideDuration": "1000",
//   "timeOut": "5000",
//   "extendedTimeOut": "1000",
//   "showEasing": "swing",
//   "hideEasing": "linear",
//   "showMethod": "fadeIn",
//   "hideMethod": "fadeOut"
// }
//                  window.setTimeout(function() {
//                     window.location.replace("/admin/view_sample_testimonials");
//                  }, 3000);
// }else if(msg=='updated'){
//                 Command: toastr["success"]("Page has been updated Successfully. Redirecting you to Page lists!")
//                 toastr.options = {
//                   "closeButton": false,
//                   "debug": false,
//                   "newestOnTop": false,
//                   "progressBar": false,
//                   "positionClass": "toast-top-right",
//                   "preventDuplicates": false,
//                   "onclick": null,
//                   "showDuration": "300",
//                   "hideDuration": "1000",
//                   "timeOut": "5000",
//                   "extendedTimeOut": "1000",
//                   "showEasing": "swing",
//                   "hideEasing": "linear",
//                   "showMethod": "fadeIn",
//                   "hideMethod": "fadeOut"
//                 }
//                  window.setTimeout(function() {
//                     window.location.replace("/admin/view_sample_testimonials");
//                  }, 3000);
//             }else{
//               alert('something went wrong with server! Please try again later!')
//             }
//         }
//     });
//         return false;
//     },

// });

$("#testimonials_form").validate({
    rules: {
        address: "required",
        name: "required"
    },
    submitHandler: function (form) {
        var formData = new FormData(form);
        $.ajax({
            type: "POST",
            url: "/admin/advert/insert",
            data: formData,
            type: "POST",
            //use contentType, processData for sure.
            contentType: false,
            processData: false,
            beforeSend: function () {
            },
            success: function (msg) {
                if (msg == 1) {
                    Command: toastr["success"]("Advertisement has been added Successfully. Redirecting you to Advertisement lists!")

                    toastr.options = {
                        "closeButton": false,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": false,
                        "positionClass": "toast-top-right",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    }
                    window.setTimeout(function () {
                        window.location.replace("/admin/view_advertisement");
                    }, 3000);
                } else if (msg == 'updated') {
                    Command: toastr["success"]("Advertisement has been updated Successfully. Redirecting you to Advertisement lists!")
                    toastr.options = {
                        "closeButton": false,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": false,
                        "positionClass": "toast-top-right",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    }
                    window.setTimeout(function () {
                        window.location.replace("/admin/view_advertisement");
                    }, 3000);
                } else {
                    alert('something went wrong with server! Please try again later!')
                }
            },
            error: function () {

            }
        });

        return false;
    },

});
</script>
@endsection